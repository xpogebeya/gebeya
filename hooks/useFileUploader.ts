import { storage } from "@/utils";
import { useState } from "react";
import { ref, uploadBytes, getDownloadURL } from "firebase/storage";

const useFileUploader = () => {
  const [file, setFile] = useState<FileList | null>(null);

  const upload = async (): Promise<string | null> => {
    if (file !== null) {
      const fileRef = ref(storage, `files/${file[0].name}`);
      const data = await uploadBytes(fileRef, file[0]);
      const url = await getDownloadURL(data.ref);
      return url;
    } else {
      return null;
    }
  };

  return {
    file,
    setFile,
    upload,
  };
};

export default useFileUploader;
