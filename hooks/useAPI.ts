/** @format */

"use client";

import { useExhibitorPortalContext } from "./../context/ExhibitorPortalContext";
import axios from "axios";
import { FormikValues } from "formik";
import { useState } from "react";
import { toast } from "react-hot-toast";
const useAPI = () => {
  const [progress, setProgress] = useState<boolean>(false);
  const [submit, setSubmit] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  const [message, setMessage] = useState<string>("");
  const [status, setStatus] = useState<number>();
  const [data, setData] = useState<any>();
  const { setCurrentUser } = useExhibitorPortalContext();

  const submitForm = async (values: FormikValues, URL: string) => {
    try {
      const { data, status } = await axios.post(URL, {
        data: values,
      });
      if (data) {
        setError(false);
        setMessage("");
        setProgress(true);
        setData(data);
        setStatus(200);
        toast.success("Successful!");
        
      }
    } catch (error: any) {
      if (error.response) {
        setSubmit(false);
        setStatus(error.response.status);
        setMessage(error.response.data.message);
        setError(true);
      }
    }
  };

  return {
    progress,
    submit,
    error,
    message,
    setSubmit,
    submitForm,
    status,
    data,
    setStatus,
    setMessage,
    setProgress,
    setError,
    setData,
  };
};

export default useAPI;
