import { useExhibitorPortalContext } from "@/context/ExhibitorPortalContext";
import { updateExhibitorProfile, useAppSelector } from "@/utils";
import axios from "axios";
import { FormikValues, useFormikContext } from "formik";
import { useDispatch } from "react-redux";

type Props = {
  initialValues: FormikValues | null;
};

const useTeamEditor = ({ initialValues }: Props) => {
  const { setEditing, setError } = useExhibitorPortalContext();
  const { currentUser } = useAppSelector((state) => state.ExhibtorAuth);
  const { dirty, isValid, values, handleReset, setValues } =
    useFormikContext<FormikValues>();
  const dispatch = useDispatch();
  const handleDiscard = () => {
    if (initialValues) {
      setValues(initialValues);
    }
    setEditing(false);
    handleReset();
  };

  const handleSave = async (currentTeam: FormikValues) => {
    if (currentUser?.teams) {
      const teamIndex = currentUser.teams.findIndex(
        (team) => team.id === currentTeam.id
      );

      if (teamIndex !== -1) {
        const updatedUser = { ...currentUser };

        const updatedTeams = [...updatedUser.teams];

        //@ts-ignore
        updatedTeams[teamIndex] = currentTeam;

        updatedUser.teams = updatedTeams;

        dispatch(updateExhibitorProfile(updatedUser));
        setEditing(false);
        try {
          await axios.put("/api/exhibitor/update-team", {
            data: {
              ...updatedTeams[teamIndex],
            },
          });
          setError("");
        } catch (error) {
          setError("Something went wrong");
        }

        handleReset();
      }
    }
  };

  return {
    handleDiscard,
    handleSave,
    isValid,
    dirty,
    values,
  };
};

export default useTeamEditor;
