import { useExhibitorPortalContext } from "@/context/ExhibitorPortalContext";
import { updateExhibitorProfile, useAppSelector } from "@/utils";
import axios from "axios";
import { FormikValues, useFormikContext } from "formik";
import { useDispatch } from "react-redux";

type Props = {
  initialValues: FormikValues | null;
};

const useProductEditor = ({ initialValues }: Props) => {
  const { setEditing, setError } = useExhibitorPortalContext();
  const { dirty, isValid, values, handleReset, setValues } =
    useFormikContext<FormikValues>();
  const { currentUser } = useAppSelector((state) => state.ExhibtorAuth);
  const dispatch = useDispatch();

  const handleDiscard = () => {
    if (initialValues) {
      setValues(initialValues);
    }
    setEditing(false);
    handleReset();
  };

  const handleSave = async (currentProduct: FormikValues) => {
    if (currentUser?.products) {
      const productIndex = currentUser?.products.findIndex(
        (team) => team.id === currentProduct.id
      );

      if (productIndex !== -1) {
        const updatedUser = { ...currentUser };

        const updatedProducts = [...updatedUser.products];
        // @ts-ignore
        updatedProducts[productIndex] = currentProduct;
        updatedUser.products = updatedProducts;
        dispatch(updateExhibitorProfile(updatedUser));

        setEditing(false);
        try {
          await axios.put("/api/exhibitor/update-product", {
            data: {
              ...updatedProducts[productIndex],
              oldCategories: currentUser.products[productIndex].categorie,
            },
          });
        } catch (error) {
          setError("Something went wrong");
        }

        handleReset();
      }
    }
  };

  return {
    handleDiscard,
    handleSave,
    isValid,
    dirty,
    values,
  };
};

export default useProductEditor;
