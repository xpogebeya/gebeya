/** @format */
"use client";

import { FormikValues, useFormikContext } from "formik";
import { useExhibitorPortalContext } from "@/context/ExhibitorPortalContext";

const useProductAdder = () => {
  const { dirty, isValid, values, handleReset, submitForm } =
    useFormikContext<FormikValues>();
  const { setAdding } = useExhibitorPortalContext();

  const handleDiscard = () => {
    handleReset();
    setAdding(false);
  };

  const handleAddMoreProducts = () => {
    submitForm().then(() => {
      handleReset();
    });
  };

  const handleSave = () => {
    handleAddMoreProducts();
    setAdding(false);
  };

  return {
    handleAddMoreProducts,
    handleDiscard,
    handleSave,
    isValid,
    dirty,
    values,
    handleReset,
  };
};

export default useProductAdder;
