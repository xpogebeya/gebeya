import { FormikValues, useFormikContext } from "formik";

const useImageUploader = () => {
  const { setFieldValue } = useFormikContext<FormikValues>();

  const handleFileUpload = (file: FileList, fieldName: string) => {
    const imageUrl = URL.createObjectURL(file[0]);
    setFieldValue(fieldName, imageUrl);
  };

  return { handleFileUpload };
};

export default useImageUploader;
