/** @format */

"use client";

import { Teams } from "@/types/exhibitor";
import { useState } from "react";
import { FormikValues, useFormikContext } from "formik";
import { useExhibitorPortalContext } from "@/context/ExhibitorPortalContext";
import { v4 as uuidv4 } from "uuid";
import { useAppSelector } from "@/utils";

const getId = (team: FormikValues): string => {
  const uniqueId = uuidv4();
  return uniqueId;
};

const useTeamAdder = () => {
  const { dirty, isValid, values, handleReset, submitForm } =
    useFormikContext<FormikValues>();
  const { setCurrentUser, setAdding } = useExhibitorPortalContext();
  const { currentUser } = useAppSelector((state) => state.ExhibtorAuth);
  // @ts-ignore
  const [CurrentTeams, setTeams] = useState<Teams[]>(currentUser?.teams);

  const handleDiscard = () => {
    handleReset();
    setAdding(false);
  };

  const handleAddMoreMembers = () => {
    if (isValid) {
      const id = getId(values);
      // @ts-ignore
      const newTeam = [{ ...values, id }, ...currentUser?.teams];
      // @ts-ignore
      setCurrentUser((prevUser) => ({
        ...prevUser,
        teams: newTeam,
      }));
      // @ts-ignore
      setTeams((prevTeams) => [...prevTeams, { ...values, id }]);

      submitForm().then(() => {
        handleReset();
      });
    }
  };

  const handleSave = () => {
    handleAddMoreMembers();
    setAdding(false);
  };

  const handleRemove = (team: Teams) => {
    const newTeam = currentUser?.teams.filter((item) => item.id !== team.id);
    // @ts-ignore
    setTeams(newTeam);
    // @ts-ignore
    setCurrentUser({ ...currentUser, teams: newTeam });
  };

  return {
    handleAddMoreMembers,
    handleDiscard,
    handleRemove,
    handleSave,
    CurrentTeams,
    isValid,
    dirty,
    values,
  };
};

export default useTeamAdder;
