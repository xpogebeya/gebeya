FROM node:18-alpine

WORKDIR /app

COPY package*.json ./
RUN npm install

COPY . .

# Perform Prisma migrations before building the image
RUN npx prisma generate

# Build the Next.js app
RUN npm run build

EXPOSE 3000

ENV DATABASE_URL="mysql://gebeyaxp_dawit:Dawit0945557307@109.70.148.174:3306/gebeyaxp_main-db"
ENV SHADOW_DATABASE_URL="mysql://gebeyaxp_dawit:Dawit0945557307@109.70.148.174:3306/gebeyaxp_main-db-shadow"

# Copy the generated Next.js production build to the appropriate directory
COPY .next /app/.next

CMD ["npm", "run", "dev"]
