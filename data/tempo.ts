interface packages {
  title: string;
  lists: {
    title: string;
    pack: string[];
  }[];
}
export interface Sponsorship {
  plan: string;
  price: string;
  packages: packages[];
  features?: string[];
}

export const Lists: Sponsorship[] = [
  {
    plan: "Diamond",
    price: "20.51 million Birr",
    packages: [
      {
        title: "Panel Discussion",
        lists: [
          {
            title: "Tea break",
            pack: [
              "Brochure placement (A5 size)",
              "A roll up banner on the side of the refreshment serving stand",
            ],
          },
          {
            title: "Verbal recognition",
            pack: ["General sponsor"],
          },
          {
            title: "Note Pads (A5 size)",
            pack: [
              "1,000 Branded notepads with the sponsors logo will be handed out for attendees of the panel discussion for  4 panel discussions",
            ],
          },
          {
            title: "Pens",
            pack: [
              "1,000 Branded pens with the sponsors logo will be handed out for attendees of the panel discussion for  4 panel discussions",
            ],
          },
          {
            title: "Screen or TV display on stage",
            pack: [
              "Company logo and information will be displayed on the screen on the stage on 4 panel discussions",
            ],
          },
          {
            title: "Banners around the hall",
            pack: [
              "8 Banners with sponsors logo will placed around the hall for 4 panel discussions",
            ],
          },
        ],
      },
      {
        title: "Printables",
        lists: [
          {
            title: "Notepads",
            pack: [
              "Branded notepads with the sponsors logo will be handed out to 500 exhibitors",
            ],
          },
          {
            title: "Visitors Badge",
            pack: [
              "Front side logo placement on the badge to be handed out to  up-to  27,000 visitors",
            ],
          },
          {
            title: "Visitors Badge-strings",
            pack: [
              "Sponsor’s logo will be placed on around 27,000 Visitor Badge strings along with GebeyaXpo",
            ],
          },
          {
            title:
              "Staff Badge or Id (Ushers, Out-sourced companies, security)",
            pack: [
              "Front side logo placement on the badge to be handed out to  up-to  200 staff members",
            ],
          },
          {
            title: "Staff Tshirt",
            pack: [
              "Front side Logos will be placed on the 200 in-house staff T-shirts",
            ],
          },
          {
            title: "Paper Hats",
            pack: [
              "Sponsors logo will be placed on the front of a paper hat of 54,000 visitors along with the GebeyaX logo8",
            ],
          },
          {
            title: "Selfie spots",
            pack: [
              "Sponsor’s Logo will be featured 750 times on official selfie spots which the attendees could capture the moment and share it with the outside world. 3 meters with 40 m length.",
            ],
          },
        ],
      },
      {
        title: "Advertising opportunities",
        lists: [
          {
            title: "Transportation sponsors",
            pack: [
              "Sponsors' Logos will be printed out on 30 buses that go around the city which will catch the attendees and the general public’s eye",
            ],
          },
          {
            title: "Banners around Addis Abeba",
            pack: [
              "Sponsor’s Logo will be featured on 10 Banners to be placed around Addis Abeba, along with GebeyaXpo",
            ],
          },
          {
            title: "Banners around the venue (Truss)",
            pack: [
              "Sponsor’s Logo will be featured on Banners to be placed around the whole venue on 10 spots, along with GebeyaXpo that is 18 m. square size.",
            ],
          },
          {
            title: "Banners at the top of the stadium (venue)",
            pack: [
              "Sponsors Logo will be printed on 2  banners ( size of 3*6 meters) and hang down from the top of the stadium to be visible from afar, along with GebeyaXpo.",
              "Sponsors Logo will be printed on 2 banners (size of 3*9 meters) and hang down from the top of the stadium to be visible from afar, along with GebeyaXpo.",
              "Sponsors Logo will be printed on 4 banners (size of 3*12 meters) and hang down from the top of the stadium to be visible from afar, along with GebeyaXpo.",
            ],
          },
          {
            title: "Flags at the top of the stadium (venue)",
            pack: [
              "8 Sponsor’s Logo will be printed as a Flag and placed at the top of the stadium for 2-4 weeks",
            ],
          },
          {
            title: "Banners On Maneuver rails",
            pack: [
              "Sponsors Logo will be placed on maneuver rails around the venue 235 places, along with GebeyaXpo",
            ],
          },
        ],
      },
      {
        title: "Digital opportunities",
        lists: [
          {
            title: "Online directory upgrade",
            pack: [
              "Sponsors can upload 40 minute videos of the products/document",
            ],
          },
        ],
      },
      {
        title: "Values at the Launch Dinner",
        lists: [
          {
            title: "For Presence",
            pack: [
              "Brochure placement on Table at the dinner",
              "Verbal Recognition",
              "Banners on stages and hall",
              "paper bag",
              "Picture background",
              "invitation card",
            ],
          },
          {
            title: "Social Media",
            pack: ["Partnership Mention"],
          },
          {
            title: "Online directory upgrade",
            pack: [
              "Sponsors can upload 40 minute videos of the products/documentary given by your company and how it works on GebeyaXpo’s website.",
            ],
          },
          {
            title: "Social Media",
            pack: ["Sponsorship Mentions", "tik tok"],
          },
          {
            title: "Mailing List",
            pack: [
              "250 exhibitors and sponsors Information",
              "54,000 visitors information",
            ],
          },
          {
            title: "Catalogs/ Digital document",
            pack: ["For visitors", "For Exhibitors"],
          },
        ],
      },
    ],
  },
  {
    plan: "Platinum",
    price: "8.5 million Birr",
    packages: [
      {
        title: "Panel Discussion",
        lists: [
          {
            title: "Tea break",
            pack: [
              "Brochure placement (A5 size)",
              "A roll up banner on the side of the refreshment serving stand",
            ],
          },
          {
            title: "Verbal recognition",
            pack: ["General sponsor"],
          },
          {
            title: "Note Pads (A5 size)",
            pack: [
              "250 Branded notepads with the sponsors logo will be handed out for attendees of the panel discussion for 1 panel discussions",
            ],
          },
          {
            title: "Pens",
            pack: [
              "250 Branded pens with the sponsors logo will be handed out for attendees of the panel discussion for 1 panel discussions",
            ],
          },
          {
            title: "Screen or TV display on stage",
            pack: [
              "Company logo and information will be displayed on the screen on the stage on 1 panel discussions",
            ],
          },
          {
            title: "Banners around the hall",
            pack: [
              "2 Banners with sponsors logo will placed around the hall for 1 panel discussions",
            ],
          },
        ],
      },
      {
        title: "Printables",
        lists: [
          {
            title: "Notepads",
            pack: [
              "Branded notepads with the sponsors logo will be handed out to 250 exhibitors",
            ],
          },
          {
            title: "Visitors Badge",
            pack: [
              "Front side small logo placement on the badge to be handed out to up-to 40,500 visitors",
              "Front side bigger logo placement on the badge to be handed out to up-to 6,750 visitors",
            ],
          },
          {
            title: "Visitors Badge-strings",
            pack: [
              "(Exclusive) logo will be placed on around 6,750 Visitor Badge strings along with GebeyaXpo",
            ],
          },
          {
            title:
              "Staff Badge or Id (Ushers, Out-sourced companies, security)",
            pack: [
              "Front side small logo placement on the badge to be handed out to up-to 200 staff members",
            ],
          },
          {
            title: "Staff T Shirt",
            pack: [
              "Front side small Logos on right side will be placed on the 200 in-house staff T-shirts",
            ],
          },
          {
            title: "Paper Hats",
            pack: [
              "Sponsors logo will be placed on the side of a paper hat of 54,000 visitors along with the GebeyaX logo",
            ],
          },
          {
            title: "Selfie spots",
            pack: [
              "Sponsor’s Logo will be featured 300 times on official selfie spots which the attendees could capture the moment and share it with the outside world. 3 meters with 40 m length.",
            ],
          },
        ],
      },
      {
        title: "Advertising opportunities",
        lists: [
          {
            title: "Transportation sponsors",
            pack: [
              "Sponsors' Logos will be printed out on 30 buses that go around the city which will catch the attendees and the general public’s eye",
            ],
          },
          {
            title: "Banners around Addis Ababa",
            pack: [
              "Sponsor’s Logo will be featured on 10 Banners to be placed around Addis Ababa, along with GebeyaXpo",
            ],
          },
          {
            title: "Banners around the venue (Truss)",
            pack: [
              "Sponsor’s Logo will be featured on Banners to be placed around the whole venue on 5 spots, along with GebeyaXpo that is 18 m. square size.",
            ],
          },
          {
            title: "Banners at the top of the stadium (venue)",
            pack: [
              "Sponsors Logo will be printed on 2 banners (size of 3*6 meters) and hang down from the top of the stadium to be visible from afar, along with GebeyaXpo.",
              "Sponsors Logo will be printed on 2 banners (size of 3*9 meters) and hang down from the top of the stadium to be visible from afar, along with GebeyaXpo.",
            ],
          },
          {
            title: "Flags at the top of the stadium (venue)",
            pack: [
              "4 Sponsor’s Logo will be printed as a Flag and placed at the top of the stadium for 2-4 weeks",
            ],
          },
          {
            title: "Banners On Maneuver rails",
            pack: [
              "Sponsors Logo will be placed on maneuver rails around the venue 500 places, along with GebeyaXpo",
            ],
          },
        ],
      },
      {
        title: "Digital opportunities",
        lists: [
          {
            title: "Online directory upgrade",
            pack: [
              "Sponsors can upload 40 minute videos of the products/documentary given by your company and how it works on GebeyaXpo’s website.",
            ],
          },
          {
            title: "Social Media",
            pack: ["Sponsorship Mentions", "tik tok"],
          },
          {
            title: "Mailing List",
            pack: ["250 exhibitors and sponsors Information"],
          },
          {
            title: "Catalogs/ Digital document",
            pack: ["For visitors", "For Exhibitors"],
          },
        ],
      },
      {
        title: "Values at the Launch Dinner",
        lists: [
          {
            title: "For Presence",
            pack: [
              "Brochure placement on Table at the dinner",
              "Verbal Recognition",
              "Banners on stages and hall",
            ],
          },
          {
            title: "Social Media",
            pack: ["Partnership Mention"],
          },
        ],
      },
    ],
  },
  {
    plan: "Gold",
    price: "3.2 Million Birr",
    packages: [
      {
        title: "Printables",
        lists: [
          {
            title: "Visitors Badge",
            pack: [
              "Back side logo placement on the badge to be handed out to up-to 54,000 visitors",
            ],
          },
          {
            title: "Staff T Shirt",
            pack: [
              "Right arm side Logos will be placed on the 200 in-house staff T-shirts",
            ],
          },
          {
            title: "Selfie spots",
            pack: [
              "Sponsor’s Logo will be featured 750 times on official selfie spots which the attendees could capture the moment and share it with the outside world. 3 meters with 40 m length.",
            ],
          },
        ],
      },
      {
        title: "Advertising opportunities",
        lists: [
          {
            title: "Transportation sponsors",
            pack: [
              "Sponsors' Logos will be printed out on 30 buses that go around the city which will catch the attendees and the general public’s eye",
            ],
          },
          {
            title: "Banners around Addis Ababa",
            pack: [
              "Sponsor’s Logo will be featured on 10 Banners to be placed around Addis Ababa, along with GebeyaXpo",
            ],
          },
          {
            title: "Banners around the venue (Truss)",
            pack: [
              "Sponsor’s Logo will be featured on Banners to be placed around the whole venue on 2 spots, along with GebeyaXpo that is 18 m. square size.",
            ],
          },
        ],
      },
      {
        title: "Digital opportunities",
        lists: [
          {
            title: "Online directory upgrade",
            pack: [
              "Sponsors can upload 40 minute videos of the products/documentary given by your company and how it works on GebeyaXpo’s website.",
            ],
          },
          {
            title: "Social Media",
            pack: ["Sponsorship Mentions", "tik tok"],
          },
          {
            title: "Catalogs/ Digital document",
            pack: ["For visitors"],
          },
          {
            title: "For Presence",
            pack: ["Brochure placement on Table at the dinner"],
          },
        ],
      },
    ],
  },
  {
    plan: "Silver",
    price: "953,659 Birr",
    packages: [
      {
        title: "Printables",
        lists: [
          {
            title: "Selfie spots",
            pack: [
              "Sponsor’s Logo will be featured 750 times on official selfie spots which the attendees could capture the moment and share it with the outside world. 3 meters with 40 m length.",
            ],
          },
        ],
      },
      {
        title: "Advertising opportunities",
        lists: [
          {
            title: "Transportation sponsors",
            pack: [
              "Sponsors' Logos will be printed out on 30 buses that go around the city which will catch the attendees and the general public’s eye",
            ],
          },
        ],
      },
      {
        title: "Digital opportunities",
        lists: [
          {
            title: "Social Media",
            pack: ["Sponsorship Mentions", "tik tok"],
          },
          {
            title: "Catalogs/ Digital document",
            pack: ["For visitors"],
          },
          {
            title: "For Presence",
            pack: ["Brochure placement on Table at the dinner"],
          },
        ],
      },
    ],
  },
  {
    plan: "Bronze",
    price: "245,664 Birr",
    packages: [
      {
        title: "Printables",
        lists: [
          {
            title: "Selfie spots",
            pack: [
              "Sponsor’s Logo will be featured 750 times on official selfie spots which the attendees could capture the moment and share it with the outside world. 3 meters with 40 m length.",
            ],
          },
        ],
      },
      {
        title: "Digital opportunities",
        lists: [
          {
            title: "Catalogs/ Digital document",
            pack: [
              "For visitors",
              "¼ Full page logo on a catalog in which 450 people will receive",
            ],
          },
        ],
      },
    ],
  },
];
