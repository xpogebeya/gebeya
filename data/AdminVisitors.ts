import { VisitorsType } from "./../types/admin/visitors";
export const Visitors: VisitorsType[] = [
  {
    firstName: "John",
    lastName: "Doe",
    email: "john.doe@example.com",
    phone: "1234567890",
    sex: "Male",

    companyName: "ABC Inc",
    jobTitle: "Manager",
  },
  {
    firstName: "Jane",
    lastName: "Smith",
    middleName: "Lee",
    email: "jane.smith@example.com",
    phone: "9876543210",
    sex: "Female",

    companyName: "XYZ Corp",
    jobTitle: "Engineer",
    telephone: "555-555-5555",
  },
  {
    firstName: "Michael",
    lastName: "Johnson",
    email: "michael.johnson@example.com",
    phone: "5555555555",
    sex: "Male",

    companyName: "PQR Ltd",
    jobTitle: "Designer",
  },
  {
    firstName: "Emily",
    lastName: "Brown",
    middleName: "Marie",
    email: "emily.brown@example.com",
    phone: "1231231234",
    sex: "Female",

    companyName: "LMN Industries",
    jobTitle: "Sales Representative",
    telephone: "777-777-7777",
  },
  {
    firstName: "David",
    lastName: "Wilson",
    email: "david.wilson@example.com",
    phone: "6666666666",
    sex: "Male",

    companyName: "EFG Corporation",
    jobTitle: "Software Developer",
  },
  {
    firstName: "Sarah",
    lastName: "Johnson",
    middleName: "Ann",
    email: "sarah.johnson@example.com",
    phone: "7777777777",
    sex: "Female",

    companyName: "RST Solutions",
    jobTitle: "Accountant",
  },
  {
    firstName: "Robert",
    lastName: "Davis",
    email: "robert.davis@example.com",
    phone: "5551237890",
    sex: "Male",

    companyName: "UVW Enterprises",
    jobTitle: "Marketing Specialist",
  },
  {
    firstName: "Olivia",
    lastName: "Smith",
    middleName: "Grace",
    email: "olivia.smith@example.com",
    phone: "5555551234",
    sex: "Female",

    companyName: "GHI Solutions",
    jobTitle: "HR Manager",
    telephone: "888-888-8888",
  },
  {
    firstName: "William",
    lastName: "Brown",
    email: "william.brown@example.com",
    phone: "1235557890",
    sex: "Male",

    companyName: "JKL Corporation",
    jobTitle: "Product Manager",
  },
  {
    firstName: "Ava",
    lastName: "Miller",
    middleName: "Rose",
    email: "ava.miller@example.com",
    phone: "5555557890",
    sex: "Female",

    companyName: "MNO Tech",
    jobTitle: "Web Developer",
  },

  {
    firstName: "John",
    lastName: "Doe",
    email: "john.doe@example.com",
    phone: "1234567890",
    sex: "Male",

    companyName: "ABC Inc",
    jobTitle: "Manager",
  },
  {
    firstName: "Jane",
    lastName: "Smith",
    middleName: "Lee",
    email: "jane.smith@example.com",
    phone: "9876543210",
    sex: "Female",

    companyName: "XYZ Corp",
    jobTitle: "Engineer",
    telephone: "555-555-5555",
  },
  {
    firstName: "Michael",
    lastName: "Johnson",
    email: "michael.johnson@example.com",
    phone: "5555555555",
    sex: "Male",

    companyName: "PQR Ltd",
    jobTitle: "Designer",
  },
  {
    firstName: "Emily",
    lastName: "Brown",
    middleName: "Marie",
    email: "emily.brown@example.com",
    phone: "1231231234",
    sex: "Female",

    companyName: "LMN Industries",
    jobTitle: "Sales Representative",
    telephone: "777-777-7777",
  },
  {
    firstName: "David",
    lastName: "Wilson",
    email: "david.wilson@example.com",
    phone: "6666666666",
    sex: "Male",

    companyName: "EFG Corporation",
    jobTitle: "Software Developer",
  },
  {
    firstName: "Sarah",
    lastName: "Johnson",
    middleName: "Ann",
    email: "sarah.johnson@example.com",
    phone: "7777777777",
    sex: "Female",

    companyName: "RST Solutions",
    jobTitle: "Accountant",
  },
  {
    firstName: "Robert",
    lastName: "Davis",
    email: "robert.davis@example.com",
    phone: "5551237890",
    sex: "Male",

    companyName: "UVW Enterprises",
    jobTitle: "Marketing Specialist",
  },
  {
    firstName: "Olivia",
    lastName: "Smith",
    middleName: "Grace",
    email: "olivia.smith@example.com",
    phone: "5555551234",
    sex: "Female",

    companyName: "GHI Solutions",
    jobTitle: "HR Manager",
    telephone: "888-888-8888",
  },
  {
    firstName: "William",
    lastName: "Brown",
    email: "william.brown@example.com",
    phone: "1235557890",
    sex: "Male",

    companyName: "JKL Corporation",
    jobTitle: "Product Manager",
  },
  {
    firstName: "Ava",
    lastName: "Miller",
    middleName: "Rose",
    email: "ava.miller@example.com",
    phone: "5555557890",
    sex: "Female",

    companyName: "MNO Tech",
    jobTitle: "Web Developer",
  },
  {
    firstName: "James",
    lastName: "Wilson",
    middleName: "Richard",
    email: "james.wilson@example.com",
    phone: "7771234567",
    sex: "Male",

    companyName: "XYZ Tech",
    jobTitle: "UI Designer",
    telephone: "999-999-9999",
  },
  {
    firstName: "Sophia",
    lastName: "Anderson",
    email: "sophia.anderson@example.com",
    phone: "1239876543",
    sex: "Female",

    companyName: "ABC Enterprises",
    jobTitle: "Sales Manager",
  },
  {
    firstName: "Daniel",
    lastName: "Thomas",
    middleName: "John",
    email: "daniel.thomas@example.com",
    phone: "5555557890",
    sex: "Male",

    companyName: "XYZ Corporation",
    jobTitle: "Product Owner",
  },
  {
    firstName: "Ella",
    lastName: "Garcia",
    email: "ella.garcia@example.com",
    phone: "5551239876",
    sex: "Female",

    companyName: "MNO Inc",
    jobTitle: "Data Analyst",
    telephone: "777-777-1234",
  },
];
