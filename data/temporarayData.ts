import {
  ExhibitorsDataType,
  Notification,
  SocialLinks,
  Teams,
} from "@/types/exhibitor";
export let notification: Notification[] = [];
let ExhibitorsData: ExhibitorsDataType[] = [];

export const AdminSocial: SocialLinks = {
  youTubeLink: "https://www.youtube.com",
  tikTokLink: "https://www.tiktok.com",
  telegramChannel: "https://www.telegram.com",
  linkedinLink: "https://www.linkdin.com",
  faceBookLink: "https://www.fb.com",
  instagram: "https://www.instagram.com",
};

export const AdminTeams: Teams[] = [
  {
    id: 1,
    firstName: "Rayn Tompson",
    lastName: "",
    middleName: "",
    jobTitle: "React Developer",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Lorem ipsum dolor sit amet consectetur adipisicing elit.",
    linkedinLink: "https://www.linkdin.com",
    telegramLink: "https://www.linkdin.com",
    facebookLink: "https://www.linkdin.com",
    image:
      "https://demos.creative-tim.com/material-tailwind-kit-react/img/team-1.jpg",
  },
  {
    id: 2,
    firstName: "Romina Hadid",
    lastName: "",
    middleName: "",
    jobTitle: "Marketing Specialist",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Lorem ipsum dolor sit amet consectetur adipisicing elit.",
    linkedinLink: "https://www.linkdin.com",
    telegramLink: "https://www.linkdin.com",
    facebookLink: "https://www.linkdin.com",
    image:
      "https://demos.creative-tim.com/material-tailwind-kit-react/img/team-2.jpg",
  },
  {
    id: 3,
    firstName: "Alexa Smith",
    lastName: "",
    middleName: "",
    jobTitle: "UI/UX Designer",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Lorem ipsum dolor sit amet consectetur adipisicing elit.",
    linkedinLink: "https://www.linkdin.com",
    telegramLink: "https://www.linkdin.com",
    facebookLink: "https://www.linkdin.com",
    image:
      "https://demos.creative-tim.com/material-tailwind-kit-react/img/team-3.jpg",
  },
  {
    id: 4,
    firstName: "Jenna Kardi",
    lastName: "",
    middleName: "",
    jobTitle: "Founder and CEO",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Lorem ipsum dolor sit amet consectetur adipisicing elit.",
    linkedinLink: "https://www.linkdin.com",
    telegramLink: "https://www.linkdin.com",
    facebookLink: "https://www.linkdin.com",
    image:
      "https://demos.creative-tim.com/material-tailwind-kit-react/img/team-4.png",
  },
  {
    id: 5,
    firstName: "Rayn Tompson",
    lastName: "",
    middleName: "",
    jobTitle: "React Developer",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Lorem ipsum dolor sit amet consectetur adipisicing elit.",
    linkedinLink: "https://www.linkdin.com",
    telegramLink: "https://www.linkdin.com",
    facebookLink: "https://www.linkdin.com",
    image:
      "https://demos.creative-tim.com/material-tailwind-kit-react/img/team-1.jpg",
  },
  {
    id: 6,
    firstName: "Romina Hadid",
    lastName: "",
    middleName: "",
    jobTitle: "Marketing Specialist",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Lorem ipsum dolor sit amet consectetur adipisicing elit.",
    linkedinLink: "https://www.linkdin.com",
    telegramLink: "https://www.linkdin.com",
    facebookLink: "https://www.linkdin.com",
    image:
      "https://demos.creative-tim.com/material-tailwind-kit-react/img/team-2.jpg",
  },
  {
    id: 7,
    firstName: "Alexa Smith",
    lastName: "",
    middleName: "",
    jobTitle: "UI/UX Designer",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Lorem ipsum dolor sit amet consectetur adipisicing elit.",
    linkedinLink: "https://www.linkdin.com",
    telegramLink: "https://www.linkdin.com",
    facebookLink: "https://www.linkdin.com",
    image:
      "https://demos.creative-tim.com/material-tailwind-kit-react/img/team-3.jpg",
  },
  {
    id: 8,
    firstName: "Jenna Kardi",
    lastName: "",
    middleName: "",
    jobTitle: "Founder and CEO",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Lorem ipsum dolor sit amet consectetur adipisicing elit.",
    linkedinLink: "https://www.linkdin.com",
    telegramLink: "https://www.linkdin.com",
    facebookLink: "https://www.linkdin.com",
    image:
      "https://demos.creative-tim.com/material-tailwind-kit-react/img/team-4.png",
  },
  {
    id: 9,
    firstName: "Alexa Smith",
    lastName: "",
    middleName: "",
    jobTitle: "UI/UX Designer",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Lorem ipsum dolor sit amet consectetur adipisicing elit.",
    linkedinLink: "https://www.linkdin.com",
    telegramLink: "https://www.linkdin.com",
    facebookLink: "https://www.linkdin.com",
    image:
      "https://demos.creative-tim.com/material-tailwind-kit-react/img/team-3.jpg",
  },
  {
    id: 10,
    firstName: "Jenna Kardi",
    lastName: "",
    middleName: "",
    jobTitle: "Founder and CEO",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Lorem ipsum dolor sit amet consectetur adipisicing elit.",
    linkedinLink: "https://www.linkdin.com",
    telegramLink: "https://www.linkdin.com",
    facebookLink: "https://www.linkdin.com",
    image:
      "https://demos.creative-tim.com/material-tailwind-kit-react/img/team-4.png",
  },
];
export default ExhibitorsData;
