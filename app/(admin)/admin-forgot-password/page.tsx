'use client'
import { Field } from '@/types/register';
import { FormikValues } from 'formik';
import React, { useState } from 'react'
import * as Yup from "yup";
import { AppButton, AppForm, AppFormField, AppSubmitButton, Progress } from '@/components';
import { useAPI } from '@/hooks';
import { useRouter } from 'next/navigation';
import axios from 'axios';

type Props = {

}

const validationSchema = Yup.object().shape({
  password: Yup.string()
    .min(8, 'Password must be at least 8 characters')
    .required('Password is required')
    .matches(
      /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/,
      'Password must have characters, including letters and numbers'
    ),
  confirmPassword: Yup.string()

    .oneOf([Yup.ref('password')], 'Passwords must match')
    .required('Confirm Password is required'),
});

const initialValues: FormikValues = {
  confirmPassword: "",
  password: "",
};
const FieldValue: Field[] = [
  {
    name: "password",
    label: "Password",
    required: true,
    type: "password",
  },
  {
    name: "confirmPassword",
    label: "Confirm Password",
    required: true,
    type: "password",

  }
];

const validationSchemaEmail = Yup.object().shape({
  email: Yup.string().required().email().label("Email"),

});

const initialValuesEmail: FormikValues = {
  email: "",

};
const FieldValueEmail: Field[] = [
  {
    name: "email",
    label: "Email",
    required: true,
  },
];

const validationSchemaVerficationCode = Yup.object().shape({
  verficationCode: Yup.string().required().label("Verfication Code"),

});

const initialValuesVerficationCode: FormikValues = {
  verficationCode: "",

};
const FieldValueVerficationCode: Field[] = [
  {
    name: "verficationCode",
    label: "Verfication Code",
    required: true,
  },
];

const page = ({ }: Props) => {
  const { message, progress, setSubmit, submit, error, status, setMessage } = useAPI();
  const [step, setStape] = useState<number>(1)
  const [email, setEmail] = useState<string>("")


  const router = useRouter()

  const handleSubmit = async (values: FormikValues) => {
    const { status, data } = await axios.put('/api/admin/change-password', {
      data: {
        email,
        password: values.password,
        type: "admin"
      }
    })


    if (status == 200) {
      setSubmit(false)
      setStape(prev => prev + 1)
    } else {
      setSubmit(false)
      setMessage(data.message)
    }
  }

  const handleEmailAccept = async (values: FormikValues) => {

    const { status, data } = await axios.post("/api/admin/send-verfication", {
      email: values.email,
      type: "admin"

    })
    setEmail(values.email)
    if (status == 200) {
      setStape(prev => prev + 1)
      setSubmit(false)
    } else {
      setSubmit(false)
      setMessage(data.message)
    }

  }

  const handleVerficationCode = async (values: FormikValues) => {
    const { status, data } = await axios.get(`/api/admin/send-verfication?passcode=${values.verficationCode}&email=${email}&type=admin`)
    if (data.allow) {
      setStape(prev => prev + 1)
      setSubmit(false)
    } else {
      setSubmit(false)
      setMessage(data.message)
    }
  }

  return (
    <div className='  items-center justify-center flex flex-col'>
      {
        submit ? (<Progress
          process={progress}
          message={message}
          error={error}
          status={status}
        />) : (
          <div className=" antialiased  w-[90%] xs:w-[80%]  sm:w-[75%] xss:w-[70%] lg:w-[45%] mx-auto h-[100vh] mt-10 flex flex-col items-center justify-center py-5">
            {
              step == 1 &&
              <div>
                <div className=" pb-0 mb-0 p-0 md:py-6">
                  <h4 className="font-bold text-xl ">Reset password</h4>
                  <p className="mb-0 opacity-60">
                    Enter your email address to rest your password
                  </p>
                  <p className="mb-0 opacity-60 text-red-500">
                    {message}
                  </p>
                </div>
                <div className=" xs:w-[85%] lg:w-[60%] mx-auto flex flex-col gap-5 items-center ">
                  <AppForm
                    initialValues={initialValuesEmail}
                    onSubmit={(values: FormikValues) => handleEmailAccept(values)}
                    validationSchema={validationSchemaEmail} >
                    {FieldValueEmail.map((item, index) => (
                      <AppFormField
                        key={item.name}
                        required={item.required}
                        name={item.name}
                        type={item.type}
                        label={item.label}
                        className=" min-w-[300px] xs:min-w-[400px] my-3"
                      />
                    ))}
                    <AppSubmitButton
                      title={"Send Link"}
                      setProcess={setSubmit}
                      className="min-w-[300px] xs:min-w-[400px] mt-5 "
                    />
                  </AppForm>
                  <p className=" text-center text-gray-500">
                    Copyright © GebeyaExpo {new Date().getFullYear()}.
                  </p>
                </div>
              </div>
            }

            {
              step == 2 &&
              <div>
                <div className=" pb-0 mb-0 p-0 md:py-6">
                  <h4 className="font-bold text-xl ">Verfication code</h4>
                  <p className="mb-0 opacity-60">
                    Enter verfication code sent to your email address.
                  </p>
                  <p className="mb-0 opacity-60 text-red-500">
                    {message}
                  </p>
                </div>
                <div className=" xs:w-[85%] lg:w-[60%] mx-auto flex flex-col gap-5 items-center ">
                  <AppForm
                    initialValues={initialValuesVerficationCode}
                    onSubmit={(values: FormikValues) => handleVerficationCode(values)}
                    validationSchema={validationSchemaVerficationCode} >
                    {FieldValueVerficationCode.map((item, index) => (
                      <AppFormField
                        key={item.name}
                        required={item.required}
                        name={item.name}
                        type={item.type}
                        label={item.label}
                        className=" min-w-[300px] xs:min-w-[400px] my-3"
                      />
                    ))}
                    <AppSubmitButton
                      title={"Verfiy Link"}
                      setProcess={setSubmit}
                      className="min-w-[300px] xs:min-w-[400px] mt-5 "
                    />
                  </AppForm>
                  <p className=" text-center text-gray-500">
                    Copyright © GebeyaExpo {new Date().getFullYear()}.
                  </p>
                </div>
              </div>
            }

            {
              step == 3 &&
              <div>
                <div className=" pb-0 mb-0 p-0 md:py-6">
                  <h4 className="font-bold text-xl ">Set password</h4>
                  <p className="mb-0 opacity-60">
                    Enter your new password to set up your account
                  </p>
                  <p className="mb-0 opacity-60 text-red-500">
                    {message}
                  </p>
                </div>
                <div className=" xs:w-[85%] lg:w-[60%] mx-auto flex flex-col gap-5 items-center ">
                  <AppForm
                    initialValues={initialValues}
                    onSubmit={(values: FormikValues) => handleSubmit(values)}
                    validationSchema={validationSchema} >
                    {FieldValue.map((item, index) => (
                      <AppFormField
                        key={item.name}
                        required={item.required}
                        name={item.name}
                        type={item.type}
                        label={item.label}
                        className=" min-w-[300px] xs:min-w-[400px] my-3"
                      />
                    ))}
                    <AppSubmitButton
                      title={"Update"}
                      setProcess={setSubmit}
                      className="min-w-[300px] xs:min-w-[400px] mt-5 "
                    />
                  </AppForm>
                  <p className=" text-center text-gray-500">
                    Copyright © GebeyaExpo {new Date().getFullYear()}.
                  </p>
                </div>
              </div>
            }

            {
              step == 4 && <>
                <div className=" pb-0 p-0 md:py-6  text-center mb-5">
                  <h4 className="font-bold text-xl ">Sucessful!</h4>
                  <p className="mb-0 opacity-60">
                    You have sucessfully changed your password
                  </p>
                </div><AppButton label={'Sign in'} handleAction={() => router.push("/admin-auth")} className=' w-[85%] mx-auto' /></>

            }

          </div>
        )
      }
    </div >
  )
}

export default page