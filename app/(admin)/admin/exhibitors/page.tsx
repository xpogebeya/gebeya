"use client";

import React, { useEffect, useState } from "react";
import {
  AppAdminApplicationInfoList,
  AppAdminSettingsIcon,
  AppExhibitorListItemSkeleton,
  AppAdminExhibitorCard,
} from "@/components";
import { useDate } from "@/hooks";
import { Pagination, PaginationItem, Stack } from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import {
  AiOutlineSortAscending,
  AiOutlineSortDescending,
} from "react-icons/ai";
import IconButton from "@mui/joy/IconButton";
import { AppDispatch, useAppSelector } from "@/utils";
import { ExhibitorsDataType, InitialExibitors } from "@/types/exhibitor";
import { useSearchParams } from "next/navigation";
import { ApplicationInfo } from "@/types";
import { useDispatch } from "react-redux";
import axios from "axios";
import { setExhibitors, updateLoading, updateStatus } from '@/utils'
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';
const menuList = ["all", "accepted", "pending", "rejected"];

const page = () => {
  const searchParams = useSearchParams();
  const currentStatus = searchParams.get("status");
  const dispatch = useDispatch<AppDispatch>();

  const [uploading, setUploading] = useState<boolean>(false);
  const [asc, setAsc] = useState<boolean>(true);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [error, setError] = useState<string>("");

  const [currentExhibitorSize, setCurrentExhibitorSize] = useState<number>(0);

  const [currentluUpdatind, setCurrentluUpdatind] = useState<number | string | null>(
    null
  );
  const [currentExhibitors, setCurrentExhibitors] = useState<ExhibitorsDataType[]>(
    []
  );

  const { day, month, year } = useDate();
  const { exhibitors, isLoading } = useAppSelector((state) => state.exhibtorReducers);
  const { searchQuery } = useAppSelector((state) => state.userClients);

  const exhibitorsPerPage: number = 5;

  const indexOfLastExhibitor: number = currentPage * exhibitorsPerPage;
  const indexOfFirstExhibitor: number =
    indexOfLastExhibitor - exhibitorsPerPage;

  const handleChangePage = (
    event: React.ChangeEvent<unknown>,
    page: number
  ) => {
    setCurrentPage(page);
  };
  
  const filteredExhibitors = searchQuery
    ? exhibitors.filter((exhibitors) => {
      const searchLower = searchQuery.toLowerCase();
      return (
        exhibitors.businessName.toLowerCase().includes(searchLower) ||
        exhibitors.bussinessType && exhibitors.bussinessType.toLowerCase().includes(searchLower) ||
        exhibitors.rentedBooth && exhibitors.rentedBooth.toLowerCase().includes(searchLower) ||
        exhibitors.email.toLowerCase().includes(searchLower) ||
        exhibitors.status.toLowerCase().includes(searchLower)
      );
    })
    : currentExhibitors;

  function filterExhibitorsByStatus(status: string) {
    if (status === "all") {
      return exhibitors;
    }
    return exhibitors.filter((exhibitor) => exhibitor.status === status);
  }

  const setExhibitor = async () => {
    try {
      const { data, status } = await axios.get("/api/admin/get-exhibitors")

      if (status == 200) {
        const Accepeted = data.exhibitors.filter((ex: any) => ex.status == "accepted").length;
        const Rejected = data.exhibitors.filter((ex: any) => ex.status == "rejected").length;
        const Pending = data.exhibitors.filter((ex: any) => ex.status == "pending").length;
        const Total = data.exhibitors.length;
        const status = {
          total: Total,
          accepted: Accepeted,
          rejected: Rejected,
          pending: Pending,
        }
        dispatch(setExhibitors(data.exhibitors))
        dispatch(updateLoading(false))
        dispatch(updateStatus(status))
        setError("")

      }
    } catch (error) {
      dispatch(updateLoading(false))
      setError("something went wrong")
    }

  }

  useEffect(() => {
    setExhibitor()
  }, [])

  useEffect(() => {
    if (!currentStatus) {

      setCurrentExhibitors(
        exhibitors.slice(indexOfFirstExhibitor, indexOfLastExhibitor)
      );
      setCurrentExhibitorSize(exhibitors.length);
    } else {
      const filteredExhibitors = filterExhibitorsByStatus(currentStatus);
      setCurrentExhibitors(
        filteredExhibitors.slice(indexOfFirstExhibitor, indexOfLastExhibitor)
      );
      setCurrentExhibitorSize(filteredExhibitors.length);
    }
  }, [exhibitors, currentPage, currentStatus]);

  function arrangeExhibitorsAscending() {
    setCurrentExhibitors(
      [...currentExhibitors].sort((a, b) =>
        a.businessName.localeCompare(b.businessName)
      )
    );
    setAsc(true);
  }

  function arrangeExhibitorsDescending() {
    setCurrentExhibitors(
      [...currentExhibitors].sort((a, b) =>
        b.businessName.localeCompare(a.businessName)
      )
    );
    setAsc(false);
  }

  const handleSort = () => {
    if (asc) {
      arrangeExhibitorsDescending();
    } else {
      arrangeExhibitorsAscending();
    }
  };

  const { status } = useAppSelector((state) => state.exhibtorReducers);

  let pages = Math.ceil(currentExhibitorSize / exhibitorsPerPage);

  const applicationData: ApplicationInfo[] = [
    {
      name: "Total Applicant",
      amount: status.total,
    },
    {
      name: "Accepted",
      amount: status.accepted,
    },
    {
      name: "Pending",
      amount: status.pending,
    },
    {
      name: "Rejected",
      amount: status.rejected,
    },
  ];
  return (
    <div className="-mt-32">
      <div className=" flex justify-between items-center">
        <div>
          {filteredExhibitors.length > 0 && (
            <Stack spacing={2} className="my-5 ">
              <Pagination
                shape="rounded"
                count={pages}
                page={currentPage}
                onChange={handleChangePage}
                renderItem={(item) => (
                  <PaginationItem
                    className=" text-white"
                    slots={{ previous: ArrowBackIcon, next: ArrowForwardIcon }}
                    {...item}
                  />
                )}
              />
            </Stack>
          )}
        </div>
        <Stack direction="row">
          <IconButton
            onClick={handleSort}
            className=" text-white text-2xl hover:bg-BlueLighter hover:scale-110 transform ease-in-out duration-200"
          >
            {asc ? <AiOutlineSortAscending /> : <AiOutlineSortDescending />}
          </IconButton>

          <AppAdminSettingsIcon
            menuList={menuList}
            currentStatus={currentStatus ? currentStatus : "all"}
            link="/admin/exhibitors?status="
          />
        </Stack>
      </div>
      <div className="flex flex-wrap-reverse relative -mx-3">
        <div className="w-full max-w-full px-3 mt-6 md:w-4/6 md:flex-none pb-20">
          <div className="relative flex flex-col min-w-0 break-words bg-white border-0 shadow-xl dark:bg-slate-850 dark:shadow-dark-xl rounded-2xl bg-clip-border">
            <div className="p-6 px-4 pb-0 mb-0 border-b-0 rounded-t-2xl">
              <h6 className="mb-0 ">Exhibitors Information</h6>
            </div>
            <p className=" text-center text-red-500">{error}</p>
            <div className="flex-auto p-4 pt-6">
              {
                uploading && <Box sx={{ width: '70%' }} className=" mb-5 mx-auto">
                  <LinearProgress color="info" />
                </Box>
              }
              {!isLoading ? (
                filteredExhibitors.length > 0 ?
                  <ul className="flex flex-col pl-0 mb-0 rounded-lg">
                    {filteredExhibitors.map((exhibitor, index) => (
                      <AppAdminExhibitorCard
                        setError={setError}
                        setUploading={setUploading}
                        currentluUpdatind={currentluUpdatind}
                        setCurrentluUpdatind={setCurrentluUpdatind}
                        data={{ ...exhibitor }}
                        key={exhibitor.id}
                      />
                    ))}
                  </ul>
                  :
                  <div>
                    <p>No exhibitors found</p>
                  </div>
              ) : (
                <div className=" flex flex-col gap-2">
                  {
                    [1, 2, 3, 4, 5]
                      .slice(0, exhibitorsPerPage)
                      .map((exhitor) => (
                        <AppExhibitorListItemSkeleton key={exhitor} />
                      ))}
                </div>
              )}
            </div>
          </div>
        </div>
        <div className="w-full max-w-full px-3 mt-6 md:w-2/6 md:flex-none  md:absolute top-1 right-0">
          <div className="relative flex flex-col h-full min-w-0 mb-6 break-words bg-white border-0 shadow-xl dark:bg-slate-850 dark:shadow-dark-xl rounded-2xl bg-clip-border">
            <div className="p-6 px-4 pb-0 mb-0 border-b-0 rounded-t-2xl ">
              <div className="flex flex-wrap -mx-3">
                <div className="max-w-full px-3 md:w-1/2 md:flex-none">
                  <h6 className="mb-0 ">Current Status</h6>
                </div>
                <div className="flex items-center justify-end max-w-full px-3 /80 md:w-1/2 md:flex-none">
                  <i className="mr-2 far fa-calendar-alt"></i>
                  <small>
                    {day}-{month}-{year}
                  </small>
                </div>
              </div>
            </div>
            <div className="flex-auto p-4 pt-6">
              <h6 className="mb-4 text-xs font-bold leading-tight uppercase  text-slate-500">
                Newest
              </h6>
              <AppAdminApplicationInfoList applicationData={applicationData} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default page;
