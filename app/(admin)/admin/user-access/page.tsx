"use client";

import React, { useState } from "react";
import { AppAdminUserAdder, AppAdminUserPermissionCard, AppForm } from "@/components";
import { AppDispatch, addClientUser, clientUsersInitialValues, clientUsersSchema, useAppSelector } from "@/utils";
import { useDispatch } from "react-redux";
import { FormikValues } from "formik";
import axios from "axios";
import { Skeleton } from "@mui/material";
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';
const page = () => {
  const [errMsg, setErrMsg] = useState<string>("")
  const [uploading, setUploading] = useState<boolean>(false);

  const { user } = useAppSelector((state) => state.adminAuth)
  const { users } = useAppSelector((state) => state.userClients);
  const dispatch = useDispatch<AppDispatch>();

  const handleSubmit = async (values: FormikValues) => {
    setUploading(true)
    const currentUser = {
      email: values.email,
      fullName: values.fullName,
      assinedBy: user?.id.toString(),
      type: values.permissions
    }
    try {
      const { data } = await axios.post("/api/admin/create-admin", {
        data: currentUser
      })

      if (data.data !== null) {
        // @ts-ignore
        dispatch(addClientUser(currentUser))
      } else {
        setErrMsg(data.message)
      }
    } catch (error) {
      setErrMsg("something went wrong")
    }
    setUploading(false)
  }

  const { isLoading } = useAppSelector((state) => state.userClients)
  const { searchQuery } = useAppSelector((state) => state.userClients);
  const filtereUsers = users
    ? users.filter((user) => {
      const searchLower = searchQuery.toLowerCase();
      return (
        user.fullName.toLowerCase().includes(searchLower) ||
        user.assinedBy.toLowerCase().includes(searchLower) ||
        user.type && user.type[0].toLowerCase().includes(searchLower) ||
        user.email && user.email.toLowerCase().includes(searchLower)
      );
    })
    : [];

  return (
    <div className=" -mt-16 md:-mt-28 px-2 lg:pb-5">

      {
        uploading && <Box sx={{ width: '70%' }} className=" mb-5 mx-auto">
          <LinearProgress color="info" />
        </Box>
      }

      <div className="flex flex-wrap-reverse relative -mx-3">
        {
          !isLoading ? (
            filtereUsers.length > 0 ? (
              <div className="flex-auto">
                {filtereUsers.map((client) => (
                  <AppAdminUserPermissionCard
                    data={client}
                    key={client.email}
                    setUploading={setUploading}
                    setErrMsg={setErrMsg}
                  />
                ))}
              </div>
            ) : (
              <div className=" w-1/2">
                <p className=" text-white w-full text-center">No user found!</p>
              </div>)

          ) : (
            <div className="flex-auto flex flex-col justify-center items-center">
              {
                [1, 2, 3, 4, 5].map((item) => (
                  <div className=" p-6 mb-2 border-0 rounded-lg bg-gray-100 dark:bg-slate-850  w-full" key={item}>
                    <Skeleton variant="text" width={400} height={20} />
                    <Skeleton variant="text" width={220} height={15} />
                    <Skeleton variant="text" width={250} height={15} />
                    <Skeleton variant="text" width={300} height={15} />
                  </div>
                ))
              }
            </div>
          )
        }

        <div className="w-full max-w-full px-3 lg:w-1/2 lg:flex-none  mb-10">
          <div className="lg:max-h-[350px] relative flex flex-col h-full break-words bg-white border-0 border-transparent border-solid shadow-xl dark:bg-slate-850 dark:shadow-dark-xl rounded-2xl bg-clip-border">
            <h1 className={`text-base font-bold text-center text-BlueDark mt-4 ${errMsg && "text-red-400"}`}>{errMsg ? errMsg : "Add new users"}</h1>
            <AppForm
              initialValues={clientUsersInitialValues}
              onSubmit={(values: FormikValues) => handleSubmit(values)}
              validationSchema={clientUsersSchema}
            >
              <AppAdminUserAdder />
            </AppForm>
          </div>
        </div>
      </div>
    </div>
  );
};

export default page;
