import { AppAdminLayout, AppAdminProtectedRoute } from "@/containers";
import React, { ReactNode } from "react";

type Props = {
  children: ReactNode;
};

const layout = ({ children }: Props) => {
  return <AppAdminLayout>
    <AppAdminProtectedRoute>
      <div>
        {children}
      </div>
    </AppAdminProtectedRoute>
  </AppAdminLayout>;
};

export default layout;
