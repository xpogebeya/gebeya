"use client";
import React, { useEffect, useState } from "react";
import { CircularProgress, Pagination, PaginationItem, Stack } from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import { SponsorsType } from "@/types";
import axios from "axios";
import { useAppSelector } from "@/utils";
const page = () => {
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [currentsponsorss, setCurrentsponsorss] = useState<SponsorsType[]>([]);
  const [allSponsors, setAllSponsors] = useState<SponsorsType[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(true)

  const sponsorsPerPage: number = 10;
  const { searchQuery } = useAppSelector((state) => state.userClients);

  const indexOfLastExhibitor: number = currentPage * sponsorsPerPage;
  const indexOfFirstExhibitor: number =
    indexOfLastExhibitor - sponsorsPerPage;

  const handleChangePage = (
    event: React.ChangeEvent<unknown>,
    page: number
  ) => {
    setCurrentPage(page);
  };


  const setsponsorss = async () => {
    const { data, status } = await axios.get("/api/admin/sponsors-list")
    if (status == 200) {

      setAllSponsors(
        data.sponsors.slice(indexOfFirstExhibitor, indexOfLastExhibitor)
      );
      setCurrentsponsorss(
        data.sponsors.slice(indexOfFirstExhibitor, indexOfLastExhibitor)
      );
      setIsLoading(false)
    }
  }

  useEffect(() => {
    setsponsorss()
  }, [])

  useEffect(() => {
    setCurrentsponsorss(
      allSponsors.slice(indexOfFirstExhibitor, indexOfLastExhibitor)
    );

  }, [currentPage]);


  const filteredSponsors = currentsponsorss
    ? allSponsors.filter((sponsors) => {
      const searchLower = searchQuery.toLowerCase();
      return (
        sponsors.fullName.toLowerCase().includes(searchLower) ||
        sponsors.email.toLowerCase().includes(searchLower) ||
        sponsors.plan.toLowerCase().includes(searchLower)

      );
    })
    : [];


  let pages = Math.ceil(allSponsors.length / sponsorsPerPage);

  return (
    <div>
      {
        isLoading ? <div className=" h-[50vh] flex flex-col items-center justify-center">
          <CircularProgress
            sx={{
              color: "#0878ab",
            }}
          />
        </div> :
          <div className=" -mx-3 -mt-32">
            <div className=" flex justify-between items-center pr-10">
              <Stack spacing={2} className="my-5 ">
                <Pagination
                  shape="rounded"
                  count={pages}
                  page={currentPage}
                  onChange={handleChangePage}
                  renderItem={(item) => (
                    <PaginationItem
                      className=" text-white"
                      slots={{ previous: ArrowBackIcon, next: ArrowForwardIcon }}
                      {...item}
                    />
                  )}
                />
              </Stack>

            </div>
            <div className=" antialiased flex-none w-full max-w-full px-3 pb-20">
              <div className="relative flex flex-col min-w-0 mb-6 break-words bg-white border-0 border-transparent border-solid shadow-xl dark:bg-slate-850 dark:shadow-dark-xl rounded-2xl bg-clip-border">
                <div className="p-6 pb-0 mb-0 border-b-0 border-b-solid rounded-t-2xl border-b-transparent">
                  <h6 className="">Sponsors request list</h6>
                </div>
                {
                  allSponsors.length > 0 ? (
                    <div className="flex-auto px-0 pt-0 pb-2">
                      <div className="p-0 overflow-x-auto">
                        <table className="items-center w-full mb-0 align-top border-collapse dark:border-white/40 text-slate-500">
                          <thead className="align-bottom">
                            <tr>
                              <th className="px-6 py-3 font-bold text-left uppercase align-middle bg-transparent border-b border-collapse shadow-none dark:border-white/40  text-xxs border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                Full Name
                              </th>
                              <th className="px-6 py-3 pl-2 font-bold text-left uppercase align-middle bg-transparent border-b border-collapse shadow-none dark:border-white/40  text-xxs border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                Email
                              </th>
                              <th className="px-6 py-3 pl-2 font-bold text-left uppercase align-middle bg-transparent border-b border-collapse shadow-none dark:border-white/40  text-xxs border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                Phone Number
                              </th>
                              <th className="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-collapse shadow-none dark:border-white/40  text-xxs border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                Plan
                              </th>
                              <th className="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-collapse shadow-none dark:border-white/40  text-xxs border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                Message
                              </th>
                              <th className="px-6 py-3 font-semibold capitalize align-middle bg-transparent border-b border-collapse border-solid shadow-none dark:border-white/40  tracking-none whitespace-nowrap text-slate-400 opacity-70"></th>
                            </tr>
                          </thead>
                          <tbody className="">
                            {filteredSponsors.map((sponsors) => (
                              <tr key={sponsors.email} className=" hover:bg-gray-100">
                                <td className="p-2 align-middle bg-transparent border-b dark:border-white/40 whitespace-nowrap shadow-transparent">
                                  <div className="flex px-2 py-1">
                                    <div className="flex flex-col justify-center">
                                      <h6 className="mb-0 text-sm leading-normal ">
                                        {sponsors.fullName}
                                      </h6>
                                      <p className="mb-0 text-xs leading-tight  dark:opacity-80 text-slate-400">
                                        {sponsors.email}
                                      </p>
                                    </div>
                                  </div>
                                </td>
                                <td className="p-2 align-middle bg-transparent border-b dark:border-white/40 whitespace-nowrap shadow-transparent">
                                  <p className="mb-0 text-xs font-semibold leading-tight  dark:opacity-80">
                                    {sponsors.email}
                                  </p>
                                </td>
                                <td className="p-2 text-sm leading-normal text-center align-middle bg-transparent border-b dark:border-white/40 whitespace-nowrap shadow-transparent">
                                  {sponsors.phoneNumber}
                                </td>
                                <td className="p-2 text-sm leading-normal text-center align-middle bg-transparent border-b dark:border-white/40 whitespace-nowrap shadow-transparent">
                                  {sponsors.plan}
                                </td>
                                <td className="p-2 text-center align-middle bg-transparent border-b dark:border-white/40 whitespace-nowrap shadow-transparent">
                                  <span className="text-xs font-semibold leading-tight  dark:opacity-80 text-slate-400">
                                    {sponsors.message}
                                  </span>
                                </td>
                              </tr>
                            ))}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  )
                    : (
                      <div>
                        <p className=" px-7">No sponsors found</p>

                      </div>
                    )
                }
              </div>
            </div>
          </div>
      }
    </div>
  );
};

export default page;
