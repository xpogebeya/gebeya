"use client";

import React from "react";
import { AiOutlineRise } from "react-icons/ai";
import { AppAdminStateByCategory, AppAdminStaticCard } from "@/components";
import { Card } from "@/components/Cards/admin/AppAdminStaticCard";
import { BsClipboardDataFill } from "react-icons/bs";
import { FaClipboardUser } from "react-icons/fa6";
import { useAppSelector } from "@/utils";

const CardValue: Card[] = [
  {
    title: "New Exhibitors",
    newAmount: "45+",
    newStat: "+55",
    time: "yesterday",
    Icon: <AiOutlineRise />,
    inc: true,
  },
  {
    title: "Today's Visitors",
    newAmount: "99+",
    newStat: "+15",
    time: "Last week",
    Icon: <FaClipboardUser />,
    inc: true,
  },
  {
    title: "New Media Request",
    newAmount: "+35",
    newStat: "-2",
    time: "yesterday",
    Icon: <BsClipboardDataFill />,
    inc: false,
  },
];

const countries = [
  {
    countryName: "United States",
    flagImageSrc: "/US.png",
    value: 500,
  },
  {
    countryName: "Germany",
    flagImageSrc: "/DE.png",
    value: 300,
  },
  {
    countryName: "Ethiopia",
    flagImageSrc: "/Ethiopia.webp",
    value: 4500,
  },
  {
    countryName: "Brasil",
    flagImageSrc: "/BR.png",
    value: 90,
  },
];

const page = () => {
  const { user } = useAppSelector((state) => state.adminAuth)

  return (
    <div className=" -mt-24 md:-mt-40  pb-20">
      <div className=" flex flex-wrap">
        {CardValue.map((card) => (
          <AppAdminStaticCard key={card.title} currentValue={card} />
        ))}
      </div>

      <div className="flex flex-wrap gap-y-5 mt-6">
        <AppAdminStateByCategory
          title="Top Exhibitors by Country"
          countryData={countries}
          url={"/admin/exhibitors"}
        />

        <AppAdminStateByCategory
          title="Top Visitors by Country"
          countryData={countries}
          url={"/admin/visitors"}
        />

        <AppAdminStateByCategory
          title="Top Medias by Country"
          countryData={countries}
          url={"/admin/media"}
        />
      </div>
    </div>
  );
};

export default page;
