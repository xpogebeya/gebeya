"use client";
import React, { useEffect, useState } from "react";
import { CircularProgress, Pagination, PaginationItem, Stack } from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import { VisitorsType } from "@/types";
import IconButton from "@mui/joy/IconButton";
import {
  AiOutlineSortAscending,
  AiOutlineSortDescending,
} from "react-icons/ai";
import axios from "axios";
import { useAppSelector } from "@/utils";
const page = () => {
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [currentVisitors, setCurrentVisitors] = useState<VisitorsType[]>([]);
  const [allVisitors, setAllVisitors] = useState<VisitorsType[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(true)
  const [asc, setAsc] = useState<boolean>(true);

  const exhibitorsPerPage: number = 10;
  const { searchQuery } = useAppSelector((state) => state.userClients);

  const indexOfLastExhibitor: number = currentPage * exhibitorsPerPage;
  const indexOfFirstExhibitor: number =
    indexOfLastExhibitor - exhibitorsPerPage;

  const handleChangePage = (
    event: React.ChangeEvent<unknown>,
    page: number
  ) => {
    setCurrentPage(page);
  };

  function arrangeExhibitorsAscending() {
    setCurrentVisitors(
      [...currentVisitors].sort((a, b) =>
        a.firstName.localeCompare(b.firstName)
      )
    );
    setAsc(true);
  }

  function arrangeExhibitorsDescending() {
    setCurrentVisitors(
      [...currentVisitors].sort((a, b) =>
        b.firstName.localeCompare(a.firstName)
      )
    );
    setAsc(false);
  }

  const handleSort = () => {
    if (asc) {
      arrangeExhibitorsDescending();
    } else {
      arrangeExhibitorsAscending();
    }
  };

  const setVisitors = async () => {
    const { data, status } = await axios.get("/api/visitor-list")
    if (status == 200) {

      setAllVisitors(
        data.visitors.slice(indexOfFirstExhibitor, indexOfLastExhibitor)
      );
      setCurrentVisitors(
        data.visitors.slice(indexOfFirstExhibitor, indexOfLastExhibitor)
      );
      setIsLoading(false)
    }
  }

  useEffect(() => {
    setVisitors()
  }, [])

  useEffect(() => {

    setCurrentVisitors(
      allVisitors.slice(indexOfFirstExhibitor, indexOfLastExhibitor)
    );

  }, [currentPage]);


  const filteredVisitors = currentVisitors
    ? allVisitors.filter((visitor) => {
      const searchLower = searchQuery.toLowerCase();
      return (
        visitor.firstName.toLowerCase().includes(searchLower) ||
        visitor.lastName.toLowerCase().includes(searchLower) ||
        visitor.companyName.toLowerCase().includes(searchLower) ||
        visitor.email.toLowerCase().includes(searchLower) ||
        visitor.jobTitle.toLowerCase().includes(searchLower)
      );
    })
    : [];


  let pages = Math.ceil(allVisitors.length / exhibitorsPerPage);

  return (
    <div>
      {
        isLoading ? <div className=" h-[50vh] flex flex-col items-center justify-center">
          <CircularProgress
            sx={{
              color: "#0878ab",
            }}
          />
        </div> :
          <div className=" -mx-3 -mt-32">
            <div className=" flex justify-between items-center pr-10">
              <Stack spacing={2} className="my-5 ">
                <Pagination
                  shape="rounded"
                  count={pages}
                  page={currentPage}
                  onChange={handleChangePage}
                  renderItem={(item) => (
                    <PaginationItem
                      className=" text-white"
                      slots={{ previous: ArrowBackIcon, next: ArrowForwardIcon }}
                      {...item}
                    />
                  )}
                />
              </Stack>
              <IconButton
                onClick={handleSort}
                className=" text-white text-2xl hover:bg-BlueLighter hover:scale-110 transform ease-in-out duration-200"
              >
                {asc ? <AiOutlineSortAscending /> : <AiOutlineSortDescending />}
              </IconButton>
            </div>
            <div className=" antialiased flex-none w-full max-w-full px-3 pb-20">
              <div className="relative flex flex-col min-w-0 mb-6 break-words bg-white border-0 border-transparent border-solid shadow-xl dark:bg-slate-850 dark:shadow-dark-xl rounded-2xl bg-clip-border">
                <div className="p-6 pb-0 mb-0 border-b-0 border-b-solid rounded-t-2xl border-b-transparent">
                  <h6 className="">Visitors list</h6>
                </div>
                {
                  allVisitors.length > 0 ? (
                    <div className="flex-auto px-0 pt-0 pb-2">
                      <div className="p-0 overflow-x-auto">
                        <table className="items-center w-full mb-0 align-top border-collapse dark:border-white/40 text-slate-500">
                          <thead className="align-bottom">
                            <tr>
                              <th className="px-6 py-3 font-bold text-left uppercase align-middle bg-transparent border-b border-collapse shadow-none dark:border-white/40  text-xxs border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                Full Name
                              </th>
                              <th className="px-6 py-3 pl-2 font-bold text-left uppercase align-middle bg-transparent border-b border-collapse shadow-none dark:border-white/40  text-xxs border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                Job Title
                              </th>
                              <th className="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-collapse shadow-none dark:border-white/40  text-xxs border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                sex
                              </th>
                              <th className="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-collapse shadow-none dark:border-white/40  text-xxs border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                Phone Number
                              </th>
                              <th className="px-6 py-3 font-semibold capitalize align-middle bg-transparent border-b border-collapse border-solid shadow-none dark:border-white/40  tracking-none whitespace-nowrap text-slate-400 opacity-70"></th>
                            </tr>
                          </thead>
                          <tbody className="">
                            {filteredVisitors.map((visitor) => (
                              <tr key={visitor.email} className=" hover:bg-gray-100">
                                <td className="p-2 align-middle bg-transparent border-b dark:border-white/40 whitespace-nowrap shadow-transparent">
                                  <div className="flex px-2 py-1">
                                    <div className="flex flex-col justify-center">
                                      <h6 className="mb-0 text-sm leading-normal ">
                                        {visitor.firstName} {visitor.middleName}{" "}
                                        {visitor.lastName}
                                      </h6>
                                      <p className="mb-0 text-xs leading-tight  dark:opacity-80 text-slate-400">
                                        {visitor.email}
                                      </p>
                                    </div>
                                  </div>
                                </td>
                                <td className="p-2 align-middle bg-transparent border-b dark:border-white/40 whitespace-nowrap shadow-transparent">
                                  <p className="mb-0 text-xs font-semibold leading-tight  dark:opacity-80">
                                    {visitor.jobTitle}
                                  </p>
                                  <p className="mb-0 text-xs leading-tight  dark:opacity-80 text-slate-400">
                                    {visitor.companyName}
                                  </p>
                                </td>
                                <td className="p-2 text-sm leading-normal text-center align-middle bg-transparent border-b dark:border-white/40 whitespace-nowrap shadow-transparent">
                                  {visitor.sex}
                                </td>
                                <td className="p-2 text-center align-middle bg-transparent border-b dark:border-white/40 whitespace-nowrap shadow-transparent">
                                  <span className="text-xs font-semibold leading-tight  dark:opacity-80 text-slate-400">
                                    {visitor.phone}
                                  </span>
                                </td>
                              </tr>
                            ))}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  )
                    : (
                      <div>
                        <p className=" px-7">No Visitor found</p>

                      </div>
                    )
                }
              </div>
            </div>
          </div>
      }
    </div>
  );
};

export default page;
