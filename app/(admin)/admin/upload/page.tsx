"use client";
import { AppAdminUpload, AppForm } from "@/components";
import { useFileUploader } from "@/hooks";
import { AppDispatch, deleteFile, setFiles, updateFiles, updateIsLoading, useAppSelector } from "@/utils";
import { CircularProgress } from "@mui/material";
import axios from "axios";
import { FormikValues } from "formik";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import * as Yup from 'yup'
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';
import { Field } from "@/types/register";

const validationSchema = Yup.object().shape({
  title: Yup.string().required().label("Title"),
  section: Yup.string().required().label("Section"),
});

const initialValues: FormikValues = {
  title: "",
  section: ""
};

const sectionOptions = ["Gallery", 'Download']

const FieldValue: Field[] = [
  {
    name: 'title',
    label: 'Title',
    required: true,
  },
  {
    name: 'section',
    label: 'Section',
    required: true,
    options: sectionOptions
  },
];

const page = () => {
  const [uploading, setUploading] = useState<boolean>(false);
  const [error, setError] = useState<string>("")
  const { file, setFile, upload } = useFileUploader();
  const { user } = useAppSelector((state) => state.adminAuth)
  const { files, isLoading } = useAppSelector((state) => state.fileSlice)
  const { searchQuery } = useAppSelector((state) => state.userClients);
  const dispatch = useDispatch<AppDispatch>();

  const filteredfiles = (searchQuery && files)
    ? files.filter((files) => {
      const searchLower = searchQuery.toLowerCase();
      return (files.title.toLowerCase().includes(searchLower))
    })
    : files;

  const handleSubmit = async (values: FormikValues) => {
    setUploading(true)
    const url = await upload();
    if (url !== null) {
      if (file) {
        const tempData = {
          url,
          title: values.title,
          type: file[0].type,
          uploadBy: user?.id,
          section: values.section
        }
        try {

          const { status, data } = await axios.post("/api/admin/upload", {
            data: tempData
          })
          // @ts-ignore
          dispatch(updateFiles(data.file))
        } catch (error) {
          setError("Something went wrong")
        }
        setUploading(false)
      }

    }
  };

  const getFiles = async () => {

    const res = await fetch('/api/admin/upload/getFiles', { cache: 'no-store' });
    const { files } = await res.json()
    if (res.ok) {
      dispatch(setFiles(files))
      setError("")
    } else {
      setError("Something went wrong")
    }
    dispatch(updateIsLoading(false))


  }

  const handleDelete = async (id: number | undefined) => {
    setUploading(true)

    if (id) {
      try {

        await axios.delete("/api/admin/upload", {
          data: { id }
        })
        dispatch(deleteFile(id))
      } catch (error) {
        setError("Something went wrong")
      }
    }
    setUploading(false)

  }

  useEffect(() => {
    getFiles()
  }, [])
  return (

    <>
      {
        !isLoading ? (<div className=" flex flex-wrap-reverse relative gap-10 md:gap-0">
          <div className="w-full px-3 md:w-[45%] lg:flex-none md:-mt-32  md:flex-none pb-20">
            <div className="relative flex flex-col h-full min-w-0 break-words bg-white border-0 border-transparent border-solid shadow-xl dark:bg-slate-850 dark:shadow-dark-xl rounded-2xl bg-clip-border">
              <div className="p-4 pb-0 mb-0 border-b-0 border-b-solid rounded-t-2xl border-b-transparent">
                <div className="flex flex-wrap -mx-3">
                  <div className="flex items-center flex-none w-1/2 max-w-full px-3">
                    <h6 className="mb-0 text-gray-500 ">Files</h6>
                    <h6 className="mb-0 text-red-500 ">{error && error}</h6>

                  </div>
                </div>
              </div>
              <div className="flex-auto p-4 pb-0">
                {
                  uploading && <Box sx={{ width: '70%' }} className=" mb-5 mx-auto">
                    <LinearProgress color="info" />
                  </Box>
                }
                {
                  filteredfiles && filteredfiles?.length > 0 ? (<ul className="flex flex-col pl-0 mb-0 rounded-lg">
                    <li

                      className="relative flex justify-between px-4 py-2 pl-0 mb-2 border-0 rounded-t-inherit text-inherit rounded-xl">
                      <div className="flex flex-col">
                        <h6 className="mb-1 text-sm font-semibold leading-normal  text-slate-700">
                          Title
                        </h6>
                      </div>
                      <div className=" inline-block px-0 py-2.5 mb-0 ml-6 font-bold leading-normal text-center uppercase align-middle transition-all bg-transparent border-0 rounded-lg shadow-none cursor-pointer ease-in bg-150 text-sm active:opacity-85 tracking-tight-rem bg-x-25 text-slate-700">
                        <i className="mr-1 text-lg leading-none fas fa-file-pdf"></i>{" "}
                        Type
                      </div>
                      <div className=" inline-block px-0 py-2.5 mb-0 ml-6 font-bold leading-normal text-center uppercase align-middle transition-all bg-transparent border-0 rounded-lg shadow-none cursor-pointer ease-in bg-150 text-sm active:opacity-85 tracking-tight-rem bg-x-25 text-slate-700">
                        <i className="mr-1 text-lg leading-none fas fa-file-pdf"></i>{" "}
                        Section
                      </div>
                      <div className=" inline-block px-0 py-2.5 mb-0 ml-6 font-bold leading-normal text-center uppercase align-middle transition-all bg-transparent border-0 rounded-lg shadow-none cursor-pointer ease-in bg-150 text-sm active:opacity-85 tracking-tight-rem bg-x-25 text-slate-700">
                        <i className="mr-1 text-lg leading-none fas fa-file-pdf"></i>{" "}
                        {""}
                      </div>
                      <div className=" inline-block px-0 py-2.5 mb-0 ml-6 font-bold leading-normal text-center uppercase align-middle transition-all bg-transparent border-0 rounded-lg shadow-none cursor-pointer ease-in bg-150 text-sm active:opacity-85 tracking-tight-rem bg-x-25 text-slate-700">
                        <i className="mr-1 text-lg leading-none fas fa-file-pdf"></i>{" "}
                        {""}

                      </div>

                    </li>
                    {
                      filteredfiles?.map((file) => (
                        <li
                          key={file.id}
                          className="relative flex justify-between px-4 py-2 pl-0 mb-2 border-0 rounded-t-inherit text-inherit rounded-xl">
                          <div className="flex flex-col">
                            <h6 className="mb-1 text-sm font-semibold leading-normal  text-slate-700">
                              {file.title}
                            </h6>
                          </div>
                          <div className=" inline-block px-0 py-2.5 mb-0 ml-6 font-bold leading-normal text-center uppercase align-middle transition-all bg-transparent border-0 rounded-lg shadow-none cursor-pointer ease-in bg-150 text-sm active:opacity-85 tracking-tight-rem bg-x-25 text-slate-700">
                            <i className="mr-1 text-lg leading-none fas fa-file-pdf"></i>{" "}
                            {file.type}
                          </div>
                          <div className=" inline-block px-0 py-2.5 mb-0 ml-6 font-bold leading-normal text-center uppercase align-middle transition-all bg-transparent border-0 rounded-lg shadow-none cursor-pointer ease-in bg-150 text-sm active:opacity-85 tracking-tight-rem bg-x-25 text-slate-700">
                            <i className="mr-1 text-lg leading-none fas fa-file-pdf"></i>{" "}
                            {file.section}
                          </div>
                          <div className=" flex gap-2 items-center">
                            <Link href={file.url} target="_blank"
                              rel="noopener noreferrer" className="flex items-center text-sm leading-normal /80 cursor-pointer  hover:bg-slate-200 px-2 rounded-sm">
                              View
                            </Link >
                            <div className="flex items-center text-sm leading-normal /80 cursor-pointer  hover:bg-red-400 hover:text-White px-2 rounded-sm" onClick={() => handleDelete(file.id)}>
                              Delete
                            </div>
                          </div>
                        </li>
                      ))
                    }
                  </ul>) : (
                    <div>
                      <p className=" text-center text-gray-500">No files found</p>
                    </div>
                  )
                }
              </div>
            </div>
          </div>
          <AppForm
            initialValues={initialValues}
            onSubmit={(values: FormikValues) => handleSubmit(values)}
            validationSchema={validationSchema} >
            <div className=" ">
              <AppAdminUpload
                file={file}
                setFile={setFile}
                FieldValue={FieldValue}
                sectionOptions={sectionOptions}
                
              />
            </div>
          </AppForm>
        </div>) : (<div className=" h-[50vh] flex flex-col justify-center items-center">
          <CircularProgress />
        </div>)
      }
    </>

  );
};

export default page;
