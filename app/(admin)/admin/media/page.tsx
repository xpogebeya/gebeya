"use client";

import React, { useEffect, useState } from "react";
import {
  AppAdminApplicationInfoList,
  AppAdminSettingsIcon,
  AppExhibitorListItemSkeleton,
  AppAdminVistorsCard,
} from "@/components";
import { useDate } from "@/hooks";
import { Pagination, PaginationItem, Stack } from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import {
  AiOutlineSortAscending,
  AiOutlineSortDescending,
} from "react-icons/ai";
import IconButton from "@mui/joy/IconButton";
import { AppDispatch, setMedia, updateMediaLoading, updateMediaStatus, useAppSelector } from "@/utils";
import { useRouter, useSearchParams } from "next/navigation";
import { ApplicationInfo, Media } from "@/types";
import { useDispatch } from "react-redux";
import axios from "axios";
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';
const menuList = ["all", "accepted", "pending", "rejected"];

const page = () => {
  const searchParams = useSearchParams();
  const currentStatus = searchParams.get("status");
  const { status } = useAppSelector((state) => state.mediasReducer);

  const [asc, setAsc] = useState<boolean>(true);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [error, setError] = useState<string>("");

  const [currentMediaSize, setCurrentMediaSize] = useState<number>(0);
  const [uploading, setUploading] = useState<boolean>(false);

  const [currentluUpdatind, setCurrentluUpdatind] = useState<string | null>(
    null
  );
  const [currentMedia, setCurrentMedia] = useState<Media[]>([]);

  const { day, month, year } = useDate();
  const { media, isMediaLoading } = useAppSelector((state) => state.mediasReducer);
  const { searchQuery } = useAppSelector((state) => state.userClients);
  const router = useRouter()

  const mediaPerPage: number = 5;
  const dispatch = useDispatch<AppDispatch>();

  const indexOfLastMedia: number = currentPage * mediaPerPage;
  const indexOfFirstmedia: number = indexOfLastMedia - mediaPerPage;

  const handleChangePage = (
    event: React.ChangeEvent<unknown>,
    page: number
  ) => {
    setCurrentPage(page);
  };

  function filtermediasByStatus(status: string) {
    if (status === "all") {
      return media;
    }
    return media.filter((exhibitor) => exhibitor.status === status);
  }

  const getMedia = async () => {
    try {
      const { data, status } = await axios.get("/api/admin/get-medias")
      if (status == 200) {
        const Accepeted = data.medias.filter((ex: any) => ex.status == "accepted").length;
        const Rejected = data.medias.filter((ex: any) => ex.status == "rejected").length;
        const Pending = data.medias.filter((ex: any) => ex.status == "pending").length;
        const Total = data.medias.length;
        const status = {
          total: Total,
          accepted: Accepeted,
          rejected: Rejected,
          pending: Pending,
        }
        dispatch(setMedia(data.medias))
        dispatch(updateMediaLoading(false))
        dispatch(updateMediaStatus(status))

      }
    } catch (error) {
      router.refresh()
    }

  }

  useEffect(() => {
    getMedia()
  }, [])

  useEffect(() => {
    if (!currentStatus) {
      setCurrentMedia(media.slice(indexOfFirstmedia, indexOfLastMedia));
      setCurrentMediaSize(media.length);
    } else {
      const filteredMedia = filtermediasByStatus(currentStatus);
      setCurrentMedia(filteredMedia.slice(indexOfFirstmedia, indexOfLastMedia));
      setCurrentMediaSize(filteredMedia.length);
    }
  }, [media, currentPage, currentStatus]);

  function arrangeMediaAscending() {
    setCurrentMedia(
      [...currentMedia].sort((a, b) => a.firstName.localeCompare(b.firstName))
    );
    setAsc(true);
  }

  function arrangeMediaDescending() {
    setCurrentMedia(
      [...currentMedia].sort((a, b) => b.firstName.localeCompare(a.firstName))
    );
    setAsc(false);
  }

  const handleSort = () => {
    if (asc) {
      arrangeMediaDescending();
    } else {
      arrangeMediaAscending();
    }
  };


  const filteredmedias = searchQuery
    ? media.filter((media) => {
      const searchLower = searchQuery.toLowerCase();
      return (
        media.firstName.toLowerCase().includes(searchLower) ||
        media.lastName.toLowerCase().includes(searchLower) ||
        media.mediaName.toLowerCase().includes(searchLower) ||
        media.email.toLowerCase().includes(searchLower) ||
        media.status.toLowerCase().includes(searchLower)
      );
    })
    : currentMedia;

  let pages = Math.ceil(currentMediaSize / mediaPerPage);
  const applicationData: ApplicationInfo[] = [
    {
      name: "Total Applicant",
      amount: status.total,
    },
    {
      name: "Accepted",
      amount: status.accepted,
    },
    {
      name: "Pending",
      amount: status.pending,
    },
    {
      name: "Rejected",
      amount: status.rejected,
    },
  ];
  return (
    <div className="-mt-32">
      <div className=" flex justify-between items-center">
        <div>
          {currentMedia.length > 0 && (
            <Stack spacing={2} className="my-5 ">
              <Pagination
                shape="rounded"
                count={pages}
                page={currentPage}
                onChange={handleChangePage}
                renderItem={(item) => (
                  <PaginationItem
                    className=" text-white"
                    slots={{ previous: ArrowBackIcon, next: ArrowForwardIcon }}
                    {...item}
                  />
                )}
              />
            </Stack>
          )}
        </div>
        <Stack direction="row">
          <IconButton
            onClick={handleSort}
            className=" text-white text-2xl hover:bg-BlueLighter hover:scale-110 transform ease-in-out duration-200"
          >
            {asc ? <AiOutlineSortAscending /> : <AiOutlineSortDescending />}
          </IconButton>

          <AppAdminSettingsIcon
            menuList={menuList}
            currentStatus={currentStatus ? currentStatus : "all"}
            link="/admin/media?status="
          />
        </Stack>
      </div>
      {
        uploading && <Box sx={{ width: '70%' }} className=" mb-5 mx-auto">
          <LinearProgress color="info" />
        </Box>
      }
      <div className="flex flex-wrap-reverse relative -mx-3">
        <div className="w-full max-w-full px-3 mt-6 md:w-4/6 md:flex-none pb-20">
          <div className="relative flex flex-col min-w-0 break-words bg-white border-0 shadow-xl dark:bg-slate-850 dark:shadow-dark-xl rounded-2xl bg-clip-border">
            <div className="p-6 px-4 pb-0 mb-0 border-b-0 rounded-t-2xl">
              <h6 className="mb-0 ">Medias Information</h6>
            </div>

            <p className=" text-center text-red-500">{error}</p>
            <div className="flex-auto p-4 pt-6">

              {

                !isMediaLoading ?

                  (filteredmedias.length > 0 ? (
                    <ul className="flex flex-col pl-0 mb-0 rounded-lg">
                      {filteredmedias.map((exhibitor, index) => (
                        <AppAdminVistorsCard
                          currentluUpdatind={currentluUpdatind}
                          setCurrentluUpdatind={setCurrentluUpdatind}
                          data={{ ...exhibitor }}
                          key={exhibitor.email}
                          setUploading={setUploading}
                          setError={setError} />
                      ))}
                    </ul>
                  )
                    : <div>
                      <p>No media found</p>
                    </div>
                  ) : (
                    <div className=" flex flex-col gap-2">
                      {
                        [1, 2, 3, 4, 5].map((media) => (
                          <AppExhibitorListItemSkeleton key={media} />
                        ))}
                    </div>
                  )}
            </div>
          </div>
        </div>
        <div className="w-full max-w-full px-3 mt-6 md:w-2/6 md:flex-none  md:w-5/12px] md:absolute top-1 right-0">
          <div className="relative flex flex-col h-full min-w-0 mb-6 break-words bg-white border-0 shadow-xl dark:bg-slate-850 dark:shadow-dark-xl rounded-2xl bg-clip-border">
            <div className="p-6 px-4 pb-0 mb-0 border-b-0 rounded-t-2xl ">
              <div className="flex flex-wrap -mx-3">
                <div className="max-w-full px-3 md:w-1/2 md:flex-none">
                  <h6 className="mb-0 ">Current Status</h6>
                </div>
                <div className="flex items-center justify-end max-w-full px-3 /80 md:w-1/2 md:flex-none">
                  <i className="mr-2 far fa-calendar-alt"></i>
                  <small>
                    {day}-{month}-{year}
                  </small>
                </div>
              </div>
            </div>
            <div className="flex-auto p-4 pt-6">
              <h6 className="mb-4 text-xs font-bold leading-tight uppercase  text-slate-500">
                Newest
              </h6>
              <AppAdminApplicationInfoList applicationData={applicationData} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default page;
