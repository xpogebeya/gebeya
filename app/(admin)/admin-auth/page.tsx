/** @format */

"use client";
import React, { useEffect, useState } from "react";
import {
  AppForm,
  AppFormField,
  AppSubmitButton,
  Pargraph,
  Progress,
} from "@/components";
import { FormikValues } from "formik";
import { Field } from "@/types/register";
import { AiOutlineLogin } from "react-icons/ai";
import { useAPI } from "@/hooks";
import * as Yup from "yup";
import { useRouter } from "next/navigation";

import axios from "axios";
import Cookies from "js-cookie";
import Link from "next/link";

const validationSchema = Yup.object().shape({
  password: Yup.string().required().min(8).label("Password"),
  email: Yup.string().required().email().label("Email"),
});

const initialValues: FormikValues = {
  email: "",
  password: "",
};
const FieldValue: Field[] = [
  {
    name: "email",
    label: "Email",
    required: true,
  },
  {
    name: "password",
    label: "Password",
    required: true,
    type: "password",
  },
];

const AppLoginForm: React.FC = () => {
  const [fieldError, setFieldError] = useState<boolean>(false);
  const { message, progress, setSubmit, submit, error, status, setMessage } =
    useAPI();

  const router = useRouter();
  const user = Cookies.get("user");
  const type = Cookies.get("type");

  const handleSubmit = async (val: FormikValues) => {
    const { data, status } = await axios.post("/api/admin/sign-in", {
      data: val,
    });

    if (data.allow) {
      router.push(data.path);
      Cookies.set("user", data.user.id, { expires: 7 });
      Cookies.set("type", data.user.type, { expires: 7 });
    } else {
      if (data.path) {
        router.push(`${data.path}?email=${val.email}`);
      } else {
        setSubmit(false);
        setMessage(data.message);
      }
    }
  };

  useEffect(() => {
    if (user) {
      if (type == "Admin") {
        router.push("/admin/exhibitors");
      } else if (type == "Blogger") {
        router.push("/blog/dashboard");
      } else if (type == "Ticketer") {
        router.push("/ticketer/visitors-list");
      }
    }
  }, [user]);

  return (
    <div className=" h-screen flex flex-col  gap-5 items-center justify-center max-w-[1200px] mx-auto">
      {submit ? (
        <Progress
          process={progress}
          message={message}
          error={error}
          status={status}
        />
      ) : (
        <>
          <div className=" antialiased  w-[90%] xs:w-[80%]  sm:w-[75%] xss:w-[60%] lg:w-[55%] mx-auto border-dashed border-2 h-[60vh] mt-10 flex flex-col py-5">
            <div className=" xs:w-[85%] mx-auto flex flex-col ">
              <div className=" pb-0 mb-0 p-0 md:py-6 ">
                <h4 className="font-bold text-xl ">Sign In</h4>
                <p className="mb-0 opacity-60">
                  Enter your email and password to sign in
                </p>
              </div>

              <AppForm
                initialValues={initialValues}
                onSubmit={(val: FormikValues) => handleSubmit(val)}
                validationSchema={validationSchema}
              >
                {fieldError && (
                  <Pargraph
                    lable={"Please fill all required field"}
                    className=" text-red-600 text-center mt-5"
                  />
                )}
                {message && (
                  <Pargraph
                    lable={message}
                    className=" text-red-600 text-center mt-5"
                  />
                )}
                <div className=" flex flex-col mt-10 gap-6">
                  {FieldValue.map((item, index) => (
                    <AppFormField
                      key={item.name}
                      required={item.required}
                      name={item.name}
                      type={item.type}
                      label={item.label}
                      className=" min-w-[300px] xs:min-w-[400px] "
                    />
                  ))}

                  <AppSubmitButton
                    title={"Sign in"}
                    setProcess={setSubmit}
                    setError={setFieldError}
                    Icon={<AiOutlineLogin className=" text-White text-xl" />}
                    className="min-w-[300px] xs:min-w-[400px]  w-full bg-BlueLighter text-white"
                  />
                </div>
              </AppForm>
            </div>
            <div className=" flex  flex-col items-center mt-5 gap-2">
              <Link
                href="/admin-forgot-password"
                className=" text-gray-500 hover:text-BlueLighter cursor-pointer "
              >
                Forgot password?
              </Link>
            </div>
          </div>
          <p className=" text-center text-gray-500">
            Copyright © GebeyaExpo {new Date().getFullYear()}.
          </p>
        </>
      )}
    </div>
  );
};

export default AppLoginForm;
