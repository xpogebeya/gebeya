
'use client'

import React, { useEffect, useState } from "react";
import Link from "next/link";
import styles from "./page.module.css";
import { AppButtonDefult, Card } from "@/components";
import { MediaCard } from "@/types";
import axios from "axios";

const MediaListPage = () => {
  const [loading, setLoading] = useState<boolean>(true)
  const [medias, setMedias] = useState<MediaCard[]>([])


  const getMedia = async () => {
    setLoading(true)
    try {
      const { data } = await axios.get("/api/home/get-media")
      setMedias(data.medias)
    } catch (error) {
      console.log(error)
    }

    setLoading(false)
  }


  useEffect(() => {
    getMedia()
  }, [])

  return (
    <div className={styles.list}>
      <div className="mt-20 md:mt-32 flex items-center justify-around xl:justify-between w-[85%] mx-auto">
        <h1 className="text-lg sm:text-xl md:text-4xl  text-BlueDark">
          Medias working with us
        </h1>
        <Link
          href="/media-badge-request"
          className=""
        >
          <AppButtonDefult
            label={"Join us"}
            handleAction={() => console.log()}
            conditionalClass=" md:block md:text-2xl"
          />
        </Link>
      </div>
      {
        loading ? (<div className=" h-[40dvh] flex flex-col justify-center items-center">

          <div className="w-16 h-16 border-4 border-dashed rounded-full animate-spin  border-BlueLighter"></div>
        </div>) : (

          medias && medias.length > 0 ? (
            <div className=" w-[85%] mx-auto grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4  gap-2 mt-5">
              {medias.map((item, index) => (
                <Card key={index} media={item} />
              ))}
            </div>
          )
            : (
              <div className=" h-[50dvh] items-center justify-center text-center">
                <p>There are no media at the moment</p>
              </div>
            )
        )
      }
    </div>
  );
};

export default MediaListPage;
