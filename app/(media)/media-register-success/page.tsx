'use client'
import { LogoContainer } from '@/containers'
import { Typography } from '@mui/material'
import React from 'react'
import { AppAdminSocial, AppSponsorCardList } from '@/components'
import { useAppSelector } from '@/utils'

type Props = {}

const page = (props: Props) => {
  const { homePageData, heroData } = useAppSelector(state => state.homePage)

  return (
    <div>
      <LogoContainer
        backGroundImage={heroData?.backgroundImageUrl || ""}
        className="  h-[500px] lg:h-[700px] text-center items-center flex-col gap-20 flex justify-center align-middle"
      >
        <div>
          <Typography
            variant="h2"
            className=" tracking-wide  font-bold text-BlueDark capitalize text-3xl md:text-4xl xl:text-6xl"
          >
            Thank You for Registering


            <br className="  block " />{" "}
            <span className="  text-BlueLight">as a Media Partner</span>
          </Typography>
          <Typography
            variant="h6"
            className="font-light max-w-xs lg:max-w-3xl text-center mx-auto text-sm lg:text-base text-gray-500"
          >
            Your Journey Begins at GebeyaXpo!
          </Typography>
        </div>
        <div className=" flex flex-col gap-3">
          <Typography className=" text-BlueLight font-bold text-lg">
            Find us on
          </Typography>

          <AppAdminSocial />
        </div>
      </LogoContainer>
      <div className=' -mt-12 lg:-mt-32 bg-white w-[85%] mx-auto border-t rounded-t-xl '>
        <div className=" mb-10 pt-5 ">
          {
            homePageData &&

            <AppSponsorCardList data={homePageData} />
          }
        </div>
      </div>
    </div>
  )
}

export default page