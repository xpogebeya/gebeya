"use client";

import * as React from "react";
import ImageList from "@mui/material/ImageList";
import ImageListItem from "@mui/material/ImageListItem";
import { LogoContainer } from "@/containers";
import { CircularProgress, Divider, IconButton, Typography } from "@mui/material";
import { AppAdminSocial } from "@/components";
import Button from "@mui/joy/Button";
import Modal from "react-modal";
import { useEffect, useState } from "react";
import { AiFillCloseCircle } from "react-icons/ai";
import axios from "axios";
import { useAppSelector } from "@/utils";

interface Item {
  id: number
  url: string
  title: string

}

export default function page() {
  const [imageNumber, setImageNumber] = useState<number>(10);
  const [itemData, setItemData] = useState<Item[]>([])
  const [selectedItem, setSelectedItem] = useState<Item | null>(null);
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const { heroData } = useAppSelector(state => state.homePage)
  const openModal = (
    item: React.SetStateAction<Item | null>
  ) => {
    setSelectedItem(item);
    setModalIsOpen(true);
  };

  const closeModal = () => {
    setSelectedItem(null);
    setModalIsOpen(false);
  };

  const getData = async () => {
    try {

      const { data } = await axios.get("/api/home/get-gallery")
      setItemData(data.photos)
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    getData()
  }, [])

  return (
    <div className="  min-h-screen">
      <LogoContainer
        backGroundImage={heroData?.backgroundImageUrl || ""}
        className="  h-[63vh] md:h-[75vh] text-center items-center flex-col gap-20 flex justify-center align-middle"
      >
        <div>
          <Typography
            variant="h2"
            className=" tracking-wide font-bold text-BlueDark capitalize text-4xl md:text-5xl xl:text-7xl"
          >
            GebeyaXpo <br className="  block xl:hidden" />{" "}
            <span className="  text-BlueLight">2023</span> Gallery
          </Typography>
          <Typography
            variant="h6"
            className=" mt-5 font-light max-w-xs lg:max-w-3xl text-center mx-auto text-sm lg:text-base text-gray-500"
          >
            We had a remarkable exhibition last year with thousands of
            exhibitors and visitors, including special guests. It was a truly
            memorable event.
          </Typography>
        </div>
        <div className=" flex flex-col gap-3">
          <Typography className=" text-BlueLight font-bold text-lg">
            Find us on
          </Typography>

          <AppAdminSocial />
        </div>
      </LogoContainer>
      <Divider />
      {
        itemData.length > 0 ? <div className=" w-[85%] lg:w-[65%] mx-auto -mt-20 rounded-lg overflow-hidden">
          <ImageList
            variant="woven"
            className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-2"
          >
            {itemData.slice(0, imageNumber).map((item) => (
              <ImageListItem
                key={item.id}
                className="relative group"
                onClick={() => openModal(item)}
              >
                <img
                  srcSet={`${item.url}?w=248&fit=crop&auto=format&dpr=2 2x`}
                  src={`${item.url}?w=248&fit=crop&auto=format`}
                  alt={item.title}
                  loading="lazy"
                  className="rounded-md cursor-pointer"
                />
                <div className=" cursor-pointer absolute inset-0  bg-BlueLighter bg-opacity-50 transition-opacity opacity-0 group-hover:opacity-100" />
              </ImageListItem>
            ))}
          </ImageList>

          {imageNumber < itemData.length && (
            <Button
              onClick={() => setImageNumber((prev) => prev + 10)}
              variant="soft"
              fullWidth
              size="lg"
            >
              Load more images...
            </Button>
          )}

          <Modal
            isOpen={modalIsOpen}
            onRequestClose={closeModal}
            contentLabel="Image Modal"
            ariaHideApp={false}
            className="fixed top-0 left-0 right-0 bottom-0 flex items-center justify-center  bg-BlueDark bg-opacity-50"
          >
            {selectedItem && (
              <div className=" bg-transparent rounded-md p-4 text-center">
                <img
                  src={selectedItem.url}
                  alt={selectedItem.title}
                  className="rounded-md"
                />
                <IconButton
                  onClick={closeModal}
                  className=" text-5xl  text-BlueLight"
                >
                  <AiFillCloseCircle />
                </IconButton>
              </div>
            )}
          </Modal>
        </div>
          :
          <div className=" h-[50vh] flex flex-col items-center justify-center">
            <CircularProgress />
          </div>
      }
    </div>
  );
}
