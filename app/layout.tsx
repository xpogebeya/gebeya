

import { AppSanityWrapper, AppSessionProvider, AppSliderWrapper } from "@/containers";
import "./globals.css";
import type { Metadata } from "next";
import { Roboto_Condensed, Poppins, Roboto_Serif } from "next/font/google";
import { ExhibitorPortalContext } from "@/context";
import { AppReduxProvider } from "@/utils";


const roboto = Poppins({
  subsets: ["latin"],
  weight: "300",
});

export const metadata: Metadata = {
  title: "GebeyaExpo",
  description: "An Expo app under construction",
  icons: {
    icon: "/logo.png",
  },

};

export const dynamic = 'force-dynamic'

export default async function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {

  return (
    <html lang="en">
      <AppReduxProvider>
        <ExhibitorPortalContext>
          <body className={` ${roboto.className}  bg-BlueDark relative`}>
            <AppSliderWrapper>
              <AppSanityWrapper>
                {children}
              </AppSanityWrapper>
            </AppSliderWrapper>
          </body>
        </ExhibitorPortalContext>
      </AppReduxProvider>

    </html>
  );
}
