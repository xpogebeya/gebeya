/** @format */

"use client";

import { AppSponsorCardList } from "@/components";
import {
  Hero,
  MoreFacts,
  PhotoGallery,
  VideoPlayer,
  WhyExibiti,
} from "@/containers";
import { useAppSelector } from "@/utils";
import { CircularProgress } from "@mui/material";

export default function Home() {
  const { heroData, homePageData } = useAppSelector(state => state.homePage)
  return (
    <main>
      {
        heroData && homePageData ? <>
          <Hero data={heroData} />
          <VideoPlayer videoId={heroData.videoId} />
          <WhyExibiti data={homePageData} />
          <MoreFacts facts={homePageData} />
          <PhotoGallery data={homePageData} />
          <div className=" mb-10">
            <AppSponsorCardList data={homePageData} />
          </div>

        </>
          :
          <div className=" h-screen flex flex-col justify-center  items-center">
            <CircularProgress />
          </div>

      }
    </main>
  );
}
