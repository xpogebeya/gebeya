import { prisma } from "@/utils";
import { NextResponse } from "next/server";

export const GET = async (req: Request) => {
  try {
    const res = await prisma.exhibitors.findMany();
    return NextResponse.json({
      res,
    });
  } catch (error) {
    return NextResponse.json({
      error,
    });
  }
};
