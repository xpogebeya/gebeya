import { prisma } from "@/utils";
import { NextRequest, NextResponse } from "next/server";
import { revalidatePath } from "next/cache";

interface User {
  id: string;
  firstName: string;
  lastName: string;
  middleName?: string | null;
  email?: string | null;
  phone: string;
  sex: any;
  companyName?: string | null;
  jobTitle: string;
  telephone?: string | null;
}

const updateVister = async (vister: User) => {
  try {
    await prisma.visitor.update({
      where: {
        id: vister.id,
      },
      data: {
        companyName: vister.companyName,
        email: vister.email,
        firstName: vister.firstName,
        telephone: vister.telephone,
        jobTitle: vister.jobTitle,
        lastName: vister.lastName,
        middleName: vister.middleName,
        phone: vister.phone,
        sex: vister.sex.toString(),
      },
    });
    return {
      status: 204,
    };
  } catch (error) {
    return {
      status: 400,
      error,
    };
  } finally {
    await prisma.$disconnect();
  }
};

const deleteVister = async (id: string) => {
  try {
    const visitor = await prisma.visitor.findFirst({
      where: {
        id: id,
      },
    });

    if (!visitor) {
      return {
        status: 400,
      };
    }
    await prisma.visitor.delete({
      where: {
        id: id,
      },
    });
    return {
      status: 204,
    };
  } catch (error) {
    return {
      status: 400,
      error,
    };
  } finally {
    await prisma.$disconnect();
  }
};

export const GET = async (req: NextRequest) => {
  const path = req.nextUrl.searchParams.get("path");
  if (path) revalidatePath(path);

  try {
    const visitors = await prisma.visitor.findMany({
      select: {
        companyName: true,
        email: true,
        firstName: true,
        telephone: true,
        jobTitle: true,
        lastName: true,
        middleName: true,
        phone: true,
        sex: true,
        id: true,
      },
    });

    return NextResponse.json({
      visitors,
    });
  } catch (error) {
    return NextResponse.json(
      {
        error,
      },
      {
        status: 404,
      }
    );
  } finally {
    await prisma.$disconnect();
  }
};

export const PUT = async (request: NextRequest) => {
  const { data } = await request.json();
  const result = await updateVister(data);

  if (result.status == 400) {
    return NextResponse.json(
      { error: result.error },
      {
        status: 400,
      }
    );
  }

  return NextResponse.json({ message: "ok" }, { status: 204 });
};

export const DELETE = async (request: NextRequest) => {
  const { searchParams } = new URL(request.url);
  const id = searchParams.get("id");
  if (id) {
    const result = await deleteVister(id);

    if (result.status == 404) {
      return NextResponse.json({ message: "Bad request" }, { status: 404 });
    }

    return NextResponse.json({ message: "ok" }, { status: 204 });
  }

  return NextResponse.json({ message: "Bad request" }, { status: 404 });
};
