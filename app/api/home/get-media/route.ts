import { prisma } from "@/utils";
import { revalidatePath } from "next/cache";
import { NextRequest, NextResponse } from "next/server";

export const GET = async (req: NextRequest) => {
  const path = req.nextUrl.searchParams.get("path");
  if (path) revalidatePath(path);
  try {
    const medias = await prisma.media.findMany({
      select: {
        email: true,
        firstName: true,
        id: true,
        lastName: true,
        logo: true,
        mediaName: true,
        middleName: true,
        phone: true,
        sex: true,
        telephone: true,
        social: {
          select: {
            faceBookLink: true,
            instagram: true,
            linkedinLink: true,
            telegramChannel: true,
            tikTokLink: true,
            tvChannelName: true,
            websiteLink: true,
            youTubeLink: true,
          },
        },
      },
      where: {
        status: "accepted",
      },
    });
    return NextResponse.json({
      medias,
    });
  } catch (error) {
    return NextResponse.json({
      error,
    });
  } finally {
    await prisma.$disconnect();
  }
};
