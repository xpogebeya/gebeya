import { prisma } from "@/utils";
import { NextResponse } from "next/server";

const setSponsors = async (data: any) => {
  const { fullName, email, message, plan, phoneNumber, selectedItem } = data;
  try {
    const newSponsor = await prisma.sponsors.create({
      data: {
        fullName,
        email,
        message,
        plan,
        phoneNumber,
      },
    });

    if (selectedItem) {
      for (const item of selectedItem) {
        const { title, items } = item;

        const newAdditionalPlan = await prisma.additionalPlans.create({
          data: {
            title,
            sponsorId: newSponsor.id,
          },
        });

        for (const listItem of items) {
          await prisma.listItem.create({
            data: {
              title: listItem.title,
              quantity: listItem.quantity,
              unit_price: listItem.unit_price,
              total_price: listItem.total_price,
              additionalPlanId: newAdditionalPlan.id,
            },
          });
        }
      }
    }

    return {
      data: newSponsor,
      status: 200,
    };
  } catch (error) {
    return {
      status: 404,
      error,
    };
  } finally {
    await prisma.$disconnect();
  }
};

export const POST = async (req: Request) => {
  const { data } = await req.json();
  const res = await setSponsors(data);

  if (res.status == 200) {
    return NextResponse.json({ data: res });
  }

  return NextResponse.json(
    { error: res.error },
    {
      status: 404,
    }
  );
};
