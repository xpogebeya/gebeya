import { prisma } from "@/utils";
import { revalidatePath } from "next/cache";
import { NextRequest, NextResponse } from "next/server";

export const GET = async (req: NextRequest) => {
  try {
    const path = req.nextUrl.searchParams.get("path");
    if (path) revalidatePath(path);
    const res = await prisma.file.findMany({
      where: {
        section: "Gallery",
      },
      select: {
        url: true,
        id: true,
        title: true,
      },
    });

    return NextResponse.json({
      photos: res,
    });
  } catch (error) {
    return NextResponse.json(
      {
        error,
      },
      {
        status: 400,
      }
    );
  }
};
