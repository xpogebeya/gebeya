import { prisma } from "@/utils";
import { NextResponse } from "next/server";

const bookMeeting = async (data: any) => {
  try {
    const vister = await prisma.bookMeeting.findFirst({
      where: {
        email: data.email,
      },
    });
    if (!vister) {
      const res = await prisma.bookMeeting.create({
        data: {
          email: data.email,
          fullName: data.fullName,
          phone: data.phone,
          exhibitor: {
            connect: {
              id: data.exhibitorId,
            },
          },
          message: data.message,
        },
      });

      return {
        data: res,
        status: 200,
      };
    }
    return {
      error: "Vister already exist",
      status: 404,
    };
  } catch (error) {
    return {
      error,
      status: 404,
    };
  } finally {
    await prisma.$disconnect();
  }
};

export const POST = async (req: Request) => {
  const { data } = await req.json();
  const result = await bookMeeting(data);

  if (result?.status == 200) {
    return NextResponse.json({
      data: result.data,
    });
  }
  return NextResponse.json(
    {
      error: result?.error,
    },
    {
      status: 404,
    }
  );
};
