import { exhibtors } from "./../../../../utils/redux/features/adminExhibitorSlice";
import { Teams } from "@/types/exhibitor";
import { deleteFirebaseFile, prisma } from "@/utils";
import { NextResponse } from "next/server";
const findTeamById = async (id: string) => {
  try {
    const res = await prisma.team.findFirst({
      where: {
        id,
      },
    });
    return {
      data: res,
      status: 200,
    };
  } catch (error) {
    return {
      status: 404,
      error,
    };
  }
};

const addTeam = async (team: any) => {
  try {
    const res = await prisma.team.create({
      data: {
        description: team.description,
        firstName: team.firstName,
        jobTitle: team.jobTitle,
        lastName: team.lastName,
        facebookLink: team.facebookLink,
        image: team.url,
        linkedinLink: team.linkedinLink,
        middleName: team.middleName,
        telegramLink: team.telegramLink,
        imageFileName: team.imageFileName,
        exhibitor: {
          connect: {
            id: team.exhibitorId,
          },
        },
      },
    });

    return {
      data: res,
      status: 200,
    };
  } catch (error) {
    return {
      error,
      status: 404,
    };
  } finally {
    await prisma.$disconnect();
  }
};

const deleteTeam = async (team: any) => {
  try {
    const res = await prisma.team.delete({
      where: {
        id: team.id,
      },
    });

    return {
      data: res,
      status: 200,
    };
  } catch (error) {
    return {
      error,
      status: 404,
    };
  } finally {
    await prisma.$disconnect();
  }
};

export const PUT = async (req: Request) => {
  const { data } = await req.json();

  const res = await addTeam(data);
  if (res.status == 200) {
    return NextResponse.json(
      {
        data: res.data,
      },
      {
        status: 200,
      }
    );
  }

  return NextResponse.json(
    {
      error: res.error,
    },
    {
      status: 404,
    }
  );
};

export const POST = async (req: Request) => {
  const { data } = await req.json();
  const user = await findTeamById(data.id);
  if (user.data?.imageFileName)
    await deleteFirebaseFile(user.data?.imageFileName);
  const res = await deleteTeam(data);

  if (res.status == 200) {
    return NextResponse.json({
      data: res.data,
    });
  }

  return NextResponse.json(
    {
      error: res.error,
    },
    {
      status: 404,
    }
  );
};
