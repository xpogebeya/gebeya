import { ExhibitorsDataType, SocialLinks } from "@/types/exhibitor";
import { deleteFirebaseFile, prisma } from "@/utils";
import { NextResponse } from "next/server";
const findUserById = async (id: string) => {
  try {
    const res = await prisma.exhibitors.findFirst({
      where: {
        id,
      },
    });
    return {
      data: res,
      status: 200,
    };
  } catch (error) {
    return {
      status: 404,
      error,
    };
  }
};

async function updateExhibitor(exhibitorData: ExhibitorsDataType) {
  try {
    const updatedExhibitor = await prisma.exhibitors.update({
      where: { id: exhibitorData.id },
      data: {
        businessName: exhibitorData.businessName,
        DescriptionOfBusiness: exhibitorData.DescriptionOfBusiness,
        email: exhibitorData.email,
        rentedBooth: exhibitorData.rentedBooth,
        image: exhibitorData.image,
        bussinessType: exhibitorData.bussinessType,
        status: exhibitorData.status,
        messageForDeclien: exhibitorData.messageForDeclien,
        imageFileName: exhibitorData.imageFileName,
        address: {
          update: {
            where: {
              exhibitorId: exhibitorData.id,
            },
            data: {
              city: exhibitorData.address.city,
              country: exhibitorData.address.country,
              phone: exhibitorData.address.phone,
              streetNumber: exhibitorData.address.streetNumber,
              telephone: exhibitorData.address.telephone,
              zipCode: exhibitorData.address.zipCode,
            },
          },
        },
        social: {
          update: {
            where: {
              exhibitorId: exhibitorData.id,
            },
            data: {
              faceBookLink: exhibitorData.social.faceBookLink,
              instagram: exhibitorData.social.instagram,
              linkedinLink: exhibitorData.social.linkedinLink,
              telegramChannel: exhibitorData.social.telegramChannel,
              tikTokLink: exhibitorData.social.tikTokLink,
              tvChannelName: exhibitorData.social.tvChannelName,
              websiteLink: exhibitorData.social.websiteLink,
              youTubeLink: exhibitorData.social.youTubeLink,
            },
          },
        },
      },
    });

    return {
      data: updatedExhibitor,
      status: 200,
    };
  } catch (error) {
    return {
      status: 404,
      error,
    };
  } finally {
    await prisma.$disconnect();
  }
}
export const POST = async (req: Request) => {
  const { data } = await req.json();
  const user = await findUserById(data.exhibitorData);
  if (user.data?.imageFileName)
    await deleteFirebaseFile(user.data?.imageFileName);

  const res = await updateExhibitor(data);
  if (res.status == 200) {
  
    return NextResponse.json(
      {
        data: res.data,
      },
      {
        status: 200,
      }
    );
  }
  return NextResponse.json(
    {
      error: res.error,
    },
    {
      status: res.status,
    }
  );
};
