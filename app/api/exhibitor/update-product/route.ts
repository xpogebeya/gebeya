import { deleteFirebaseFile, prisma } from "@/utils";
import { NextResponse } from "next/server";

const findProductById = async (id: string) => {
  try {
    const res = await prisma.product.findFirst({
      where: {
        id,
      },
    });
    return {
      data: res,
      status: 200,
    };
  } catch (error) {
    return {
      status: 404,
      error,
    };
  }
};

const updateproduct = async (product: any,oldCategories: any) => {
  try {
    const oldCategorie = await prisma.categorie.findMany({
      where: {
        name: {
          in: oldCategories.map((categorie :any) => categorie.name),
        },
      },
    });
    const oldCategoryIds = oldCategories.map((categorie :any) => ({ id: categorie.id }));
    const newCategories = await prisma.categorie.findMany({
      where: {
        name: {
          in: product.categorie,
        },
      },
    });
    const newCategoryIds = newCategories.map((category: any) => category.id);
    const categoriesToDisconnect = oldCategoryIds.filter((item :any) => !newCategoryIds.includes(item.id)).map((item: any) => ({ id: item.id }));
    const categoriesToConnect = newCategoryIds.filter(id => !oldCategoryIds.includes(id)).map(id => ({ id }));
    const res = await prisma.product.update({
      where: {
        id: product.id,
      },
      data: {
        description: product.description,
        title: product.title,
        categorie: {
          disconnect: categoriesToDisconnect,
          connect : categoriesToConnect
        },
        image: product.image,
        imageFileName: product.imageFileName,
      },
    });
    return {
      data: res,
      status: 200,
    };
  } catch (error) {
    return {
      status: 404,
      error,
    };
  } finally {
    await prisma.$disconnect();
  }
};
export const PUT = async (req: Request) => {
  const {data } = await req.json();
  const { oldCategories, ...otherData } = data;
  const product = await findProductById(data.id);
  if (product.data?.image != data.image && product.data?.imageFileName) {
    await deleteFirebaseFile(product.data?.imageFileName);
  }
  const updatedproduct = await updateproduct(otherData,oldCategories);
  if (updatedproduct.status == 200) {
    return NextResponse.json({
      data: updatedproduct.data,
    });
  }
  return NextResponse.json(
    {
      error: updatedproduct.error,
    },
    {
      status: 404,
    }
  );
};
