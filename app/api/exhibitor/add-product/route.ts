import { Teams } from "@/types/exhibitor";
import { deleteFirebaseFile, prisma } from "@/utils";
import { NextResponse } from "next/server";
const findProductById = async (id: string) => {
  try {
    const res = await prisma.product.findFirst({
      where: {
        id,
      },
    });
    return {
      data: res,
      status: 200,
    };
  } catch (error) {
    return {
      status: 404,
      error,
    };
  }
};

const addProduct = async (product: any) => {
  try {
    const categories = await prisma.categorie.findMany({
      where: {
        name: {
          in: product.categorie,
        },
      },
    });
    const categoriesId = categories.map((categorie) => ({id: categorie.id}));
    const res = await prisma.product.create({
      data: {
        description: product.description,
        title: product.title,
        // categorie: product.categorie,
        image: product.image,
        imageFileName: product.imageFileName,
        exhibitor: {
          connect: {
            id: product.exhibitorId,
          },
        },
        categorie: {
          connect: categoriesId,
        }
      },
    });
    return {
      data: res,
      status: 200,
    };
  } catch (error) {
    console.log(error);

    return {
      error,
      status: 404,
    };
  } finally {
    await prisma.$disconnect();
  }
};

const deleteProduct = async (product: any) => {
  try {
    const res = await prisma.product.delete({
      where: {
        id: product.id,
      },
    });

    return {
      data: res,
      status: 200,
    };
  } catch (error) {
    return {
      error,
      status: 404,
    };
  } finally {
    await prisma.$disconnect();
  }
};

export const PUT = async (req: Request) => {
  const { data } = await req.json();
  const res = await addProduct(data);
  if (res.status == 200) {
    return NextResponse.json(
      {
        data: res.data,
      },
      {
        status: 200,
      }
    );
  }

  return NextResponse.json(
    {
      error: res.error,
    },
    {
      status: 404,
    }
  );
};

export const POST = async (req: Request) => {
  const { data } = await req.json();
  const user = await findProductById(data.id);
  if (user.data?.imageFileName)
    await deleteFirebaseFile(user.data?.imageFileName);
  const res = await deleteProduct(data);

  if (res.status == 200) {
    return NextResponse.json({
      data: res.data,
    });
  }

  return NextResponse.json(
    {
      error: res.error,
    },
    {
      status: 404,
    }
  );
};
