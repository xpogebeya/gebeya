// app/api/exhibitor/get-categorie.js
import { PrismaClient } from '@prisma/client';
import { NextResponse , NextRequest} from "next/server";

export const GET = async (req:NextRequest, res:NextResponse) => {
  const prisma = new PrismaClient();
  try {
    const categories = await prisma.categorie.findMany();
     return NextResponse.json(
      {
        data: categories,
      }
     )
  } catch (error) {
    return NextResponse.json(
        {
            error,
        },
        {
          status: 404,
        }
      );
  } finally {
    await prisma.$disconnect();
  }
}