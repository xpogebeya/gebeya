import { prisma } from "@/utils";
import { NextRequest, NextResponse } from "next/server";
import bcrypt from "bcrypt";

const findUserByEmail = async (email: string): Promise<any | Error> => {
  try {
    return await prisma.exhibitors.findFirst({
      where: { email },
    });
  } catch (error) {
    return error;
  } finally {
    await prisma.$disconnect();
  }
};

const checkPasswordSet = async (email: string): Promise<boolean> => {
  const user = await findUserByEmail(email);
  return user?.isSet || false;
};

const validatePassword = async (
  email: string,
  password: string
): Promise<boolean> => {
  const user = await findUserByEmail(email);

  if (user) {
    const match = await bcrypt.compare(password, user.password);
    return match;
  }
  return false;
};

const checkTempPassword = async (
  email: string,
  password: string
): Promise<boolean> => {
  const user = await findUserByEmail(email);
  return user?.password === password;
};

export async function POST(req: NextRequest) {
  const { data } = await req.json();
  const passwordSet = await checkPasswordSet(data.email);
  const validPassword = await validatePassword(data.email, data.password);
  const user = await findUserByEmail(data.email);

  if (!user) {
    return NextResponse.json({
      message: "No user found",
    });
  } else {
    if (passwordSet) {
      if (validPassword) {
        const user = await findUserByEmail(data.email);
        return NextResponse.json({
          path: "/exhibitor-portal/profile",
          allow: true,
          user: user,
        });
      } else {
        return NextResponse.json({
          allow: false,
          message: "Please provide a correct credential",
        });
      }
    } else {
      const correctPassword = await checkTempPassword(
        data.email,
        data.password
      );

      if (correctPassword) {
        return NextResponse.json({
          path: "/exhibitors-password-setup",
        });
      } else {
        return NextResponse.json({
          message: "Please use the correct credential",
        });
      }
    }
  }
}
