import { prisma } from "@/utils";
import { revalidatePath } from "next/cache";
import { NextRequest, NextResponse } from "next/server";

const getMeetings = async (id: string) => {
  try {
    const data = await prisma.bookMeeting.findMany();
    return {
      data,
      status: 200,
    };
  } catch (error) {
    return {
      error,
      status: 404,
    };
  } finally {
    await prisma.$disconnect();
  }
};

export const GET = async (req: NextRequest) => {
  const { searchParams } = new URL(req.url);
  const id = searchParams.get("id");

  const path = req.nextUrl.searchParams.get("path");
  if (path) revalidatePath(path);

  if (id) {
    const result = await getMeetings(id);
    if (result.status == 200) {
      return NextResponse.json({
        meeting: result.data,
      });
    }
    return NextResponse.json(
      {
        error: result.error,
      },
      {
        status: 404,
      }
    );
  }
};
