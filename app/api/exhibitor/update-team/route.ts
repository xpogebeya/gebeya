import { deleteFirebaseFile, prisma } from "@/utils";
import { NextResponse } from "next/server";

const findTeamById = async (id: string) => {
  try {
    const res = await prisma.team.findFirst({
      where: {
        id,
      },
    });
    return {
      data: res,
      status: 200,
    };
  } catch (error) {
    return {
      status: 404,
      error,
    };
  }
};

const updateTeam = async (team: any) => {
  try {
    const res = await prisma.team.update({
      where: {
        id: team.id,
      },
      data: {
        description: team.description,
        facebookLink: team.facebookLink,
        firstName: team.firstName,
        image: team.image,
        imageFileName: team.imageFileName,
        jobTitle: team.jobTitle,
        lastName: team.lastName,
        linkedinLink: team.linkedinLink,
        middleName: team.middleName,
      },
    });
    return {
      data: res,
      status: 200,
    };
  } catch (error) {
    console.log(error);
    return {
      status: 404,
      error,
    };
  } finally {
    await prisma.$disconnect();
  }
};
export const PUT = async (req: Request) => {
  const { data } = await req.json();
  const team = await findTeamById(data.id);
  if (team.data?.image != data.image && team.data?.imageFileName) {
    await deleteFirebaseFile(team.data?.imageFileName);
  }

  const updatedTeam = await updateTeam(data);
  if (updatedTeam.status == 200) {
    return NextResponse.json({
      data: updatedTeam.data,
    });
  }

  return NextResponse.json(
    {
      error: updatedTeam.error,
    },
    {
      status: 404,
    }
  );
};
