import { prisma } from "@/utils";
import { revalidatePath } from "next/cache";
import { NextRequest, NextResponse } from "next/server";

type Props = {
  data?: any;
  status: number;
  error?: any;
};

const getExhibitor = async (id: string): Promise<Props> => {
  try {
    const user = await prisma.exhibitors.findFirst({
      where: {
        id,
      },
      select: {
        address: true,
        businessName: true,
        bussinessType: true,
        DescriptionOfBusiness: true,
        email: true,
        image: true,
        notifications: true,
        products: {
          select: {
            categorie: true,
            description: true,
            id: true,
            image: true,
            title: true,
            imageFileName: true,
            exhibitorId: true,
          },
        },
        rentedBooth: true,
        social: {
          select: {
            faceBookLink: true,
            instagram: true,
            linkedinLink: true,
            telegramChannel: true,
            tikTokLink: true,
            tvChannelName: true,
            websiteLink: true,
            youTubeLink: true,
          },
        },
        teams: true,
        id: true,
      },
    });

    if (user) {
      return {
        status: 200,
        data: user,
      };
    } else {
      return {
        status: 400,
        error: "NO user found",
      };
    }
  } catch (error) {
    return {
      status: 404,
      error,
    };
  } finally {
    await prisma.$disconnect();
  }
};

export const GET = async (req: NextRequest) => {
  const { searchParams } = new URL(req.url);
  const id = searchParams.get("id");

  const path = req.nextUrl.searchParams.get("path");
  if (path) revalidatePath(path);

  if (id) {
    const exhibitor = await getExhibitor(id);
    if (exhibitor.status == 200) {
      return NextResponse.json({
        exhibitor,
      });
    } else if (exhibitor.status == 400) {
      return NextResponse.json({
        message: "exhibitor not found",
      });
    } else if (exhibitor.status == 404) {
      return NextResponse.json({
        message: "Something went wrong",
      });
    }
  }
  return NextResponse.json({
    message: "exhibitor not found",
  });
};
