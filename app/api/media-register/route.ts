import { SendWelcomeEmail } from "@/emails/WelcomeTemplate";
import { SocialLinks } from "@/types/exhibitor";
import { prisma } from "@/utils";
import { NextResponse } from "next/server";
async function createMedia(mediaData: any, socialLinks: SocialLinks) {
  try {
    const existingMedia = await prisma.media.findFirst({
      where: { email: mediaData.email },
    });

    if (existingMedia) {
      return {
        status: 400,
        message: "Media with the same email already exists",
      };
    }

    const newMedia = await prisma.media.create({
      data: {
        ...mediaData,
        social: {
          create: {
            ...socialLinks,
          },
        },
      },
    });

    return {
      status: 200,
      data: newMedia,
    };
  } catch (error) {
    return {
      status: 500,
      message: "Internal Server Error",
      error,
    };
  } finally {
    await prisma.$disconnect();
  }
}

export const POST = async (req: Request) => {
  const { data } = await req.json();
  const userData = {
    firstName: data.firstName,
    lastName: data.lastName,
    middleName: data.middleName,
    email: data.email,
    phone: data.phone,
    telephone: data.telephone,
    sex: data.sex,
    mediaName: data.mediaName,
    logo: data.logo,
  };
  const socialLinks: SocialLinks = {
    faceBookLink: data.faceBookLink,
    instagram: data.instagram,
    linkedinLink: data.linkedinLink,
    telegramChannel: data.telegramChannel,
    tikTokLink: data.tikTokLink,
    tvChannelName: data.tvChannelName,
    websiteLink: data.websiteLink,
    youTubeLink: data.youTubeLink,
  };

  const createMediaResult = await createMedia(userData, socialLinks);
  if (createMediaResult.status === 200) {
    const optionalMessage =
      "We want to inform you that you can expect to receive an update on the status of your application in the near future.";
    SendWelcomeEmail(data.email, "", "", optionalMessage);
    return NextResponse.json(createMediaResult.data, {
      status: createMediaResult.status,
    });
  } else {
    return NextResponse.json(
      {
        data: null,
        message: createMediaResult.message,
        error: createMediaResult.error,
      },
      {
        status: createMediaResult.status,
        statusText: createMediaResult.message,
      }
    );
  }
};
