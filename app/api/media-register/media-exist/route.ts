import { prisma } from "@/utils";
import { NextResponse } from "next/server";
import { type NextRequest } from "next/server";
export async function GET(req: NextRequest) {
  const searchParams = req.nextUrl.searchParams;
  const email = searchParams.get("email");

  if (email) {
    const existingMedia = await prisma.media.findFirst({
      where: { email: email },
    });
    return NextResponse.json({
      user: existingMedia,
    });
  }
  return NextResponse.json({
    message: "No email provided",
  });
}
