import { SendWelcomeEmail } from "@/emails/WelcomeTemplate";
import {
  AppGenerateQRCode,
  AppPasscodeGen,
  AppUploadQRCodeToCloudinary,
} from "@/services";
import { prisma } from "@/utils";
import { NextResponse } from "next/server";

async function createVisitor(visitorData: any, passCode: string) {
  try {
    const existingVisitor = await prisma.visitor.findFirst({
      where: { email: visitorData.email },
    });

    if (existingVisitor) {
      return {
        status: 400,
        message: "Visitor with the same email already exists",
      };
    }

    const newVisitor = await prisma.visitor.create({
      data: { passcode: passCode, ...visitorData },
    });

    return {
      status: 200,
      data: newVisitor,
    };
  } catch (error) {
    console.log(error);

    return {
      status: 500,
      message: "Internal Server Error",
      error,
    };
  } finally {
    await prisma.$disconnect();
  }
}

export const POST = async (req: Request) => {
  const { data } = await req.json();
  const passCode = AppPasscodeGen();

  const createVisitorResult = await createVisitor(data, passCode);
  if (createVisitorResult.status === 200) {
    const qrCodeBuffer = await AppGenerateQRCode({ data: data.email });
    const imageUrl = await AppUploadQRCodeToCloudinary(qrCodeBuffer);
    SendWelcomeEmail(data.email, imageUrl, passCode);
    return NextResponse.json(createVisitorResult.data, {
      status: createVisitorResult.status,
    });
  } else {
    return NextResponse.json(
      {
        data: null,
        message: createVisitorResult.message,
        error: createVisitorResult.error,
      },
      {
        status: createVisitorResult.status,
        statusText: createVisitorResult.message,
      }
    );
  }
};
