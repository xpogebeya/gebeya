import { SendWelcomeEmail } from "@/emails/WelcomeTemplate";
import { Address, SocialLinks } from "@/types/exhibitor";
import { prisma } from "@/utils";
import { NextResponse } from "next/server";
async function createExhibitor(
  exhibitorData: any,
  socialLinks: SocialLinks,
  address: Address
) {
  try {
    const existingExhibitor = await prisma.exhibitors.findFirst({
      where: { email: exhibitorData.email },
    });

    if (existingExhibitor) {
      return {
        status: 400,
        message: "Exhibitor with the same email already exists",
      };
    }

    const newExhibitor = await prisma.exhibitors.create({
      data: {
        ...exhibitorData,
        address: {
          create: {
            ...address,
          },
        },
        social: {
          create: {
            ...socialLinks,
          },
        },
      },
    });

    return {
      status: 200,
      data: newExhibitor,
    };
  } catch (error) {
    console.log(error);
    return {
      status: 500,
      message: "Internal Server Error exhibtor creator",
      error,
    };
  } finally {
    await prisma.$disconnect();
  }
}

export const POST = async (req: Request) => {
  const { data } = await req.json();
  const userData = {
    businessName: data.businessName,
    DescriptionOfBusiness: data.DescriptionOfBusiness,
    email: data.email,
    image: data.image,
    bussinessType: data.bussinessType,
    rentedBooth: "",
  };
  const socialLinks: SocialLinks = {
    faceBookLink: data.faceBookLink,
    instagram: data.instagram,
    linkedinLink: data.linkedinLink,
    telegramChannel: data.telegramChannel,
    tikTokLink: data.tikTokLink,
    tvChannelName: data.tvChannelName,
    websiteLink: data.websiteLink,
    youTubeLink: data.youTubeLink,
  };

  const address: Address = {
    phone: data.phone,
    city: data.city,
    country: data.country,
    streetNumber: data.streetNumber,
    telephone: data.telephone,
    zipCode: data.zipCode,
  };

  const createExhibitorResult = await createExhibitor(
    userData,
    socialLinks,
    address
  );
  if (createExhibitorResult.status === 200) {
    const optionalMessage =
      "We want to inform you that one of our co-workers will check your application and  you can expect to receive an update on the status of your application in the near future. Thank you agine for joining us as an exhibitor!";
    SendWelcomeEmail(data.email, "", "", optionalMessage);
    return NextResponse.json(createExhibitorResult.data, {
      status: createExhibitorResult.status,
    });
  } else {
    return NextResponse.json(
      {
        data: null,
        message: createExhibitorResult.message,
        error: createExhibitorResult.error,
      },
      {
        status: createExhibitorResult.status,
        statusText: createExhibitorResult.message,
      }
    );
  }
};
