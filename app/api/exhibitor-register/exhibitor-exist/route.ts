import { prisma } from "@/utils";
import { NextResponse } from "next/server";
import { type NextRequest } from "next/server";
export async function GET(req: NextRequest) {
  const searchParams = req.nextUrl.searchParams;
  const email = searchParams.get("email");

  if (email) {
    try {
      const existingExhibitor = await prisma.exhibitors.findFirst({
        where: { email: email },
      });
      if (existingExhibitor) {
        return NextResponse.json({
          user: existingExhibitor,
        });
      } else {
        return NextResponse.json({
          message: "No your found",
          user: null,
        });
      }
    } catch (error) {
      return NextResponse.json({
        message: "Server error occured",
        error,
      });
    }
  }
  return NextResponse.json({
    message: "No email provided",
  });
}
