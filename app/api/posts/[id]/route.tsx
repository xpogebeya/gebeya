/** @format */

import { NextRequest, NextResponse } from "next/server";

import { client } from "@/sanity/lib/client";

import prisma from "@/prisma/client";

export async function GET(
  request: NextRequest,
  { params: { id } }: { params: { id: string } }
) {
  const query = `*[_type=="blog"&&_id=="${id}"]{date,title,_id, description, "imageUrls": images[].asset->url, subpost[]{subtitle, description, "imageUrls": images[].asset->url}}`;
  const post = await client.fetch(query);
  if (!post)
    return NextResponse.json(
      { Error: "Error in fetching data" },
      { status: 404 }
    );

  return NextResponse.json(post);
}

export async function DELETE(
  request: NextRequest,
  { params: { id } }: { params: { id: string } }
) {
  const post = await prisma.post.findUnique({
    where: {
      id: parseInt(id),
    },
  });
  if (!post)
    return NextResponse.json({ Error: "post not found" }, { status: 404 });
  const { count } = await prisma.subPost.deleteMany({
    where: {
      parentId: post.id,
    },
  });
  const deletedPosts = await prisma.post.delete({
    where: {
      id: post.id,
    },
  });
  return NextResponse.json(
    { ...deletedPosts, count: `${count} additional subtitles are deleted` },
    { status: 200 }
  );
}
