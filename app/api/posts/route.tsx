/** @format */

import { NextRequest, NextResponse } from "next/server";
import { client } from "@/sanity/lib/client";
export async function GET(request: NextRequest) {
  const query =
    '*[_type=="blog"]{date,category,title,_id, description, "imageUrls": images[].asset->url, subpost[]{subtitle, description, "imageUrls": images[].asset->url}}';

  const query_press =
    '*[_type=="blog" && pressRelease]{date,category, title,_id, description, "imageUrls": images[].asset->url, _updatedAt, subpost[]{subtitle, description, "imageUrls": images[].asset->url}}';

  const query_technology =
    '*[_type=="blog" && (category=="Technology" || category=="technology")]{date,category, title,_id, description, "imageUrls": images[].asset->url, _updatedAt, subpost[]{subtitle, description, "imageUrls": images[].asset->url}}';

  const query_construction =
    '*[_type=="blog" && (category=="Construction" || category=="construction")]{date,category, title,_id, description, "imageUrls": images[].asset->url, _updatedAt, subpost[]{subtitle, description, "imageUrls": images[].asset->url}}';

  const query_policy =
    '*[_type=="blog" && (category=="Policy" || category=="policy")]{date,category,title,_id, description, "imageUrls": images[].asset->url, _updatedAt, subpost[]{subtitle, description, "imageUrls": images[].asset->url}}';

  const query_finance =
    '*[_type=="blog" && (category=="Finance" || category=="finance")]{date,category, title,_id, description, "imageUrls": images[].asset->url, _updatedAt, subpost[]{subtitle, description, "imageUrls": images[].asset->url}}';

  const query_machinery =
    '*[_type=="blog" && (category=="Machinery" || category=="machinery")]{date,category, title,_id, description, "imageUrls": images[].asset->url, _updatedAt, subpost[]{subtitle, description, "imageUrls": images[].asset->url}}';
  const blogCategoryQuery =
    '*[_type=="category"]{_id,"technology": technology.asset->url,"finance":finance.asset->url,"policy": policy.asset->url,"construction": construction.asset->url,"machinery":machinery.asset->url,}';

  const allPosts = await client.fetch(query);
  const pressRelease = await client.fetch(query_press);
  const policy = await client.fetch(query_policy);
  const technology = await client.fetch(query_technology);
  const construction = await client.fetch(query_construction);
  const finance = await client.fetch(query_finance);
  const machinery = await client.fetch(query_machinery);

  return NextResponse.json({
    pressRelease,
    Policy: policy,
    Technology: technology,
    Construction: construction,
    Finance: finance,
    machinery,
    allPosts,
  });
}
