/** @format */

import { NextRequest, NextResponse } from "next/server";
import { client } from "@/sanity/lib/client";
export async function GET(request: NextRequest) {
  const blogCategoryQuery =
    '*[_type=="category"]{"technology": technology.asset->url,"finance":finance.asset->url,"policy": policy.asset->url,"construction": construction.asset->url,"machinery":machinery.asset->url,"centralImage":centralImage.asset->url}';
  const blogCategory = await client.fetch(blogCategoryQuery);
  return NextResponse.json(blogCategory);
}
