import { SendForgotPasswordEmail } from "@/emails";
import { AppPasscodeGen } from "@/services";
import { prisma } from "@/utils";
import { NextResponse } from "next/server";

const findUserByEmail = async (email: string): Promise<any | Error> => {
  try {
    return await prisma.admin.findFirst({
      where: { email },
    });
  } catch (error) {
    return error;
  } finally {
    await prisma.$disconnect();
  }
};

const createVerficationExhibitor = async (
  email: string,
  passcode: string
): Promise<any | Error> => {
  try {
    const tempUser = await findUserByEmailExhibitor(email);
    const user = await prisma.exhibitors.update({
      where: { id: tempUser.id },
      data: {
        tempoPasscode: passcode,
      },
    });

    return {
      email: user.email,
      id: user.id,
    };
  } catch (error) {
    return error;
  } finally {
    await prisma.$disconnect();
  }
};

const findUserByEmailExhibitor = async (
  email: string
): Promise<any | Error> => {
  try {
    return await prisma.exhibitors.findFirst({
      where: { email },
    });
  } catch (error) {
    return error;
  } finally {
    await prisma.$disconnect();
  }
};

const createVerfication = async (
  email: string,
  passcode: string
): Promise<any | Error> => {
  try {
    const user = await prisma.admin.update({
      where: { email },
      data: {
        tempoPasscode: passcode,
      },
    });

    return {
      assinedBy: user.assinedBy,
      email: user.email,
      id: user.id,
      fullName: user.fullName,
      tempoPasscode: user.tempoPasscode,
      type: user.type,
    };
  } catch (error) {
    return error;
  } finally {
    await prisma.$disconnect();
  }
};

export const POST = async (req: Request) => {
  const { email, type } = await req.json();
  const passCode = AppPasscodeGen();
  if (type == "admin") {
    const user = await findUserByEmail(email);
    if (!user) {
      return NextResponse.json(
        {
          message: "No user found",
        },
        {
          status: 404,
        }
      );
    }
    const updatedUser = await createVerfication(email, passCode);
    SendForgotPasswordEmail(email, passCode);
    return NextResponse.json({
      updatedUser,
    });
  } else {
    const user = await findUserByEmailExhibitor(email);
    if (!user) {
      return NextResponse.json(
        {
          message: "No user found",
        },
        {
          status: 404,
        }
      );
    }
    const updatedUser = await createVerficationExhibitor(email, passCode);
    SendForgotPasswordEmail(email, passCode);
    return NextResponse.json({
      updatedUser,
    });
  }
};

export const GET = async (req: Request) => {
  const { searchParams } = new URL(req.url);
  const passcode = searchParams.get("passcode");
  const email = searchParams.get("email");
  const type = searchParams.get("type");

  try {
    if (email) {
      if (type == "admin") {
        const user = await findUserByEmail(email);
        if (user.tempoPasscode == passcode) {
          return NextResponse.json({
            allow: true,
          });
        } else {
          return NextResponse.json({
            allow: false,
            message: "Not a correct link",
          });
        }
      } else {
        const user = await findUserByEmailExhibitor(email);
        if (user.tempoPasscode == passcode) {
          return NextResponse.json({
            allow: true,
          });
        } else {
          return NextResponse.json({
            allow: false,
            message: "Not a correct link",
          });
        }
      }
    } else {
      return NextResponse.json({
        message: "Email not found",
      });
    }
  } catch (error) {
    return NextResponse.json({
      error,
    });
  } finally {
    await prisma.$disconnect();
  }
};
