import { SendMediaAcceptedEmail, SendMediaRejectionEmail } from "@/emails";
import { prisma } from "@/utils";
import { revalidatePath } from "next/cache";
import { NextRequest, NextResponse } from "next/server";

export const GET = async (req: NextRequest) => {
  try {
    const path = req.nextUrl.searchParams.get("path");
    if (path) revalidatePath(path);

    const medias = await prisma.media.findMany({
      select: {
        email: true,
        firstName: true,
        mediaName: true,
        lastName: true,
        id: true,
        logo: true,
        middleName: true,
        telephone: true,
        phone: true,
        social: true,
        status: true,
      },
    });
    return NextResponse.json({
      medias,
    });
  } catch (error) {
    return NextResponse.json(
      {
        error,
      },
      {
        status: 404,
      }
    );
  } finally {
    await prisma.$disconnect();
  }
};

export const PUT = async (req: Request) => {
  const { type, email, id, messageForDeclien } = await req.json();

  if (type == "approve") {
    try {
      const media = await prisma.media.update({
        where: {
          id,
        },
        data: {
          status: "accepted",
          messageForDeclien: "",
        },
      });

      SendMediaAcceptedEmail(email);
      return NextResponse.json(
        {
          media,
        },
        {
          status: 200,
        }
      );
    } catch (error) {
      return NextResponse.json(
        {
          error,
        },
        {
          status: 404,
        }
      );
    } finally {
      await prisma.$disconnect();
    }
  } else {
    try {
      const media = await prisma.media.update({
        where: {
          id,
        },
        data: {
          status: "rejected",
          messageForDeclien,
        },
      });
      SendMediaRejectionEmail(email);
      return NextResponse.json(
        {
          media,
        },
        {
          status: 200,
        }
      );
    } catch (error) {
      return NextResponse.json(
        {
          error,
        },
        {
          status: 404,
        }
      );
    } finally {
      await prisma.$disconnect();
    }
  }
};
