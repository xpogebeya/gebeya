import { prisma } from "@/utils";
import { revalidatePath } from "next/cache";
import { NextRequest, NextResponse } from "next/server";

const getSponsors = async () => {
  try {
    const sposnors = await prisma.sponsors.findMany();
    return {
      data: sposnors,
      status: 200,
    };
  } catch (error) {
    return {
      error,
      status: 404,
    };
  } finally {
    await prisma.$disconnect();
  }
};

export const GET = async (req: NextRequest) => {
  const path = req.nextUrl.searchParams.get("path");
  if (path) revalidatePath(path);
  
  const sponsors = await getSponsors();
  if (sponsors.status == 200) {
    return NextResponse.json({
      sponsors: sponsors.data,
    });
  }

  return NextResponse.json(
    {
      error: sponsors.error,
    },
    {
      status: 404,
    }
  );
};
