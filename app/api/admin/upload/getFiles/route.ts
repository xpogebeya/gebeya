/** @format */

import { prisma } from "@/utils";
import { revalidatePath } from "next/cache";
import { NextRequest, NextResponse } from "next/server";

export const GET = async (req: NextRequest) => {
  const path = req.nextUrl.searchParams.get("path");
  if (path) revalidatePath(path);
  const { searchParams } = new URL(req.url);
  const id = searchParams.get("id");

  try {
    if (id) {
      const files = await prisma.file.findMany({
        where: {
          uploadBy: id,
        },
      });
      return NextResponse.json({
        files,
      });
    } else {
      const files = await prisma.file.findMany();
      return NextResponse.json({
        files,
      });
    }
  } catch (error) {
    return NextResponse.json({
      error,
    });
  } finally {
    await prisma.$disconnect();
  }
};
