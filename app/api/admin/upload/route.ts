import { prisma } from "@/utils";
import { NextResponse } from "next/server";

const uploadFile = async (files: any) => {
  try {
    const file = await prisma.file.create({
      data: {
        ...files,
      },
    });
    return {
      data: file,
      status: 200,
    };
  } catch (error) {
    return {
      error,
      status: 404,
    };
  } finally {
    await prisma.$disconnect();
  }
};

const deleteFile = async (id: number) => {
  try {
    const file = await prisma.file.delete({
      where: {
        id: id,
      },
    });
    return {
      data: file,
      status: 200,
    };
  } catch (error) {
    return {
      error,
      status: 404,
    };
  } finally {
    await prisma.$disconnect();
  }
};

export const POST = async (req: Request) => {
  const { data } = await req.json();

  const file = await uploadFile(data);
  if (file.status == 200) {
    return NextResponse.json({
      file: file.data,
    });
  }
  return NextResponse.json(
    {
      error: file.error,
    },
    {
      status: file.status,
    }
  );
};

export const DELETE = async (req: Request) => {
  const { id } = await req.json();

  const file = await deleteFile(id);

  if (file.status == 200) {
    return NextResponse.json({
      file: file.data,
    });
  }
  return NextResponse.json(
    {
      error: file.error,
    },
    {
      status: file.status,
    }
  );
};
