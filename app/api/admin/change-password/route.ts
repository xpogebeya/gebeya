/** @format */

import { prisma } from "@/utils";
import bcrypt from "bcrypt";

import { NextRequest, NextResponse } from "next/server";
interface Props {
  data?: any;
  status: number;
  error?: any;
  message?: string;
  password?: string;
}

const changePassword = async (
  email: string,
  password: string
): Promise<Props> => {
  try {
    const user = await prisma.admin.update({
      where: {
        email: email,
      },
      data: {
        password,
        isSet: true,
      },
    });
    return {
      data: user,
      status: 200,
    };
  } catch (error) {
    return {
      status: 404,
      error,
      message: "Something went wrong",
    };
  } finally {
    await prisma.$disconnect();
  }
};

const changeExhibitorPassword = async (
  email: string,
  password: string
): Promise<Props> => {
  try {
    const tempUser = await prisma.exhibitors.findFirst({
      where: {
        email,
      },
    });
    if (tempUser) {
      const user = await prisma.exhibitors.update({
        where: {
          id: tempUser?.id,
        },
        data: {
          password,
          isSet: true,
        },
      });
      return {
        data: user,
        status: 200,
      };
    } else {
      return {
        status: 404,
        message: "No user Found",
      };
    }
  } catch (error) {
    return {
      status: 404,
      error,
      message: "Something went wrong",
    };
  } finally {
    await prisma.$disconnect();
  }
};

const saltRounds = 10;
export const PUT = async (req: NextRequest) => {
  const { data } = await req.json();
  const hashedPassword = await bcrypt.hash(data.password, saltRounds);
  if (data.type == "admin") {
    const res = await changePassword(data.email, hashedPassword);
    if (res.status == 200) {
      return NextResponse.json({
        data: res,
      });
    } else {
      return NextResponse.json({
        error: "something went wrong",
      });
    }
  } else {
    const res = await changeExhibitorPassword(data.email, hashedPassword);
    if (res.status == 200) {
      return NextResponse.json({
        data: res,
      });
    } else {
      return NextResponse.json({
        error: "something went wrong",
      });
    }
  }
};
