/** @format */

import { prisma } from "@/utils";
import { revalidatePath } from "next/cache";
import { NextRequest, NextResponse } from "next/server";

const getAdmin = async (id: string) => {
  try {
    const user = await prisma.admin.findFirst({
      where: {
        id: id,
        type: "Admin",
      },
    });

    return {
      id: user?.id,
      email: user?.email,
      type: user?.type,
    };
  } catch (error) {
    return error;
  } finally {
    await prisma.$disconnect();
  }
};

export const GET = async (req: NextRequest) => {
  const { searchParams } = new URL(req.url);
  const id = searchParams.get("id");
  const path = req.nextUrl.searchParams.get("path");
  if (path) revalidatePath(path);

  if (id) {
    const admin = await getAdmin(id);
    if (admin)
      return NextResponse.json({
        admin,
      });
  }
  return NextResponse.json({
    message: "admin not found",
  });
};
