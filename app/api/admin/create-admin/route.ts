import { SendAdminEmail } from "@/emails";
import { AppPasscodeGen } from "@/services";
import { prisma } from "@/utils";
import { NextRequest, NextResponse } from "next/server";

interface ReqData {
  email: string;
  assinedBy: string;
  type: string;
  fullName: string;
}

const createAdmin = async (reqData: ReqData, password: string) => {
  try {
    const existingAdmin = await prisma.admin.findFirst({
      where: { email: reqData.email },
    });

    if (existingAdmin) {
      return {
        status: 400,
        message: "Admin with the same email already exists",
      };
    }

    const newAdmin = await prisma.admin.create({
      data: {
        ...reqData,
        password,
      },
    });

    return {
      status: 200,
      data: newAdmin,
    };
  } catch (error) {
    console.log(error);
    return {
      status: 500,
      message: "Internal Server Error",
      error,
    };
  } finally {
    await prisma.$disconnect();
  }
};

export async function POST(req: NextRequest) {
  try {
    const { data } = await req.json();
    const password = AppPasscodeGen();
    const createdAdmin = await createAdmin(data, password);

    if (createdAdmin.status == 200) {
      SendAdminEmail(data.email, password);
      return NextResponse.json(
        {
          data: createdAdmin.data,
        },
        {
          status: createdAdmin.status,
        }
      );
    } else {
      return NextResponse.json({
        data: null,
        message: createdAdmin.message,
        error: createdAdmin.error,
      });
    }
  } catch (error) {
    return NextResponse.json({
      error,
    });
  }
}
