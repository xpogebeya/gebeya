/** @format */

import { prisma } from "@/utils";
import { revalidatePath } from "next/cache";
import { NextRequest, NextResponse } from "next/server";

export const GET = async (req: NextRequest) => {
  try {
    const path = req.nextUrl.searchParams.get("path");
    if (path) revalidatePath(path);

    const admins = await prisma.admin.findMany({
      select: {
        id: true,
        fullName: true,
        email: true,
        assinedBy: true,
        type: true,
      },
    });
    return NextResponse.json({
      admins,
    });
  } catch (error) {
    return NextResponse.json({
      error,
    });
  } finally {
    await prisma.$disconnect();
  }
};

export const DELETE = async (req: Request) => {
  const { searchParams } = new URL(req.url);
  const id = searchParams.get("id");

  try {
    if (id) {
      const deletedUser = await prisma.admin.delete({
        where: {
          id: id,
        },
      });
      return NextResponse.json({
        admin: deletedUser,
      });
    }
    return NextResponse.json({
      message: "admin not found",
    });
  } catch (error) {
    return NextResponse.json({
      error,
    });
  } finally {
    prisma.$disconnect();
  }
};
