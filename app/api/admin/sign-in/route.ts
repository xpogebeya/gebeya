import { prisma } from "@/utils";
import { NextRequest, NextResponse } from "next/server";
import bcrypt from "bcrypt";

const findUserByEmail = async (email: string): Promise<any | Error> => {
  try {
    return await prisma.admin.findFirst({
      where: { email },
    });
  } catch (error) {
    return error;
  } finally {
    await prisma.$disconnect();
  }
};

const checkPasswordSet = async (email: string): Promise<boolean> => {
  const user = await findUserByEmail(email);
  return user?.isSet || false;
};

const validatePassword = async (
  email: string,
  password: string
): Promise<boolean> => {
  const user = await findUserByEmail(email);

  if (user) {
    const match = await bcrypt.compare(password, user.password);
    return match;
  }
  return false;
};
const validatePermission = async (email: string): Promise<string> => {
  const user = await findUserByEmail(email);

  if (user) {
    if (user.type == "Admin") {
      return "/admin/exhibitors";
    } else if (user.type == "Blogger") {
      return "/blog/dashboard";
    } else if (user.type == "Ticketer") {
      return "/ticketer/visitors-list";
    }
  }
  return "/admin-auth";
};

const checkTempPassword = async (
  email: string,
  password: string
): Promise<boolean> => {
  const user = await findUserByEmail(email);
  return user?.password === password;
};

export async function POST(req: NextRequest) {
  const { data } = await req.json();
  const passwordSet = await checkPasswordSet(data.email);
  const validPassword = await validatePassword(data.email, data.password);
  const validPermission = await validatePermission(data.email);
  if (passwordSet) {
    if (validPassword) {
      const user = await findUserByEmail(data.email);
      return NextResponse.json({
        path: validPermission,
        allow: true,
        user: {
          id: user.id,
          email: user.email,
          type: user.type,
        },
      });
    } else {
      return NextResponse.json({
        allow: false,
        message: "Please provide a correct credential",
      });
    }
  } else {
    const correctPassword = await checkTempPassword(data.email, data.password);

    if (correctPassword) {
      return NextResponse.json({
        path: "/admin-password-setup",
      });
    } else {
      return NextResponse.json({
        message: "Please use the correct credential",
      });
    }
  }
}
