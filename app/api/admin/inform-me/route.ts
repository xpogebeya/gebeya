import { prisma } from "@/utils";
import { NextResponse } from "next/server";

const inform = async (data: any) => {
  try {
    const res = await prisma.inform.create({
      data: {
        email: data.email,
        fullName: data.fullName,
        phone: data.mobile,
        telePhone: data.telePhone,
      },
    });

    return {
      data: res,
      status: 200,
    };
  } catch (error) {
    return {
      error,
      status: 404,
    };
  } finally {
    await prisma.$disconnect();
  }
};

export const POST = async (req: Request) => {
  const { data } = await req.json();

  const res = await inform(data);

  if (res.status == 200) {
    return NextResponse.json({
      data: res.data,
    });
  }

  return NextResponse.json(
    {
      error: res.error,
    },
    {
      status: 404,
    }
  );
};
