import { SendAcceptedEmail, SendRejectionEmail } from "@/emails";
import { AppPasscodeGen } from "@/services";
import { prisma } from "@/utils";
import { revalidatePath } from "next/cache";
import { NextRequest, NextResponse } from "next/server";

export const GET = async (req: NextRequest) => {
  const path = req.nextUrl.searchParams.get("path");
  if (path) revalidatePath(path);

  try {
    const exhibitors = await prisma.exhibitors.findMany({
      select: {
        address: true,
        businessName: true,
        bussinessType: true,
        DescriptionOfBusiness: true,
        email: true,
        id: true,
        image: true,
        messageForDeclien: true,
        notifications: true,
        products: true,
        rentedBooth: true,
        social: {
          select: {
            faceBookLink: true,
            instagram: true,
            linkedinLink: true,
            telegramChannel: true,
            tikTokLink: true,
            tvChannelName: true,
            websiteLink: true,
            youTubeLink: true,
          },
        },
        teams: true,
        status: true,
      },
    });
    return NextResponse.json({
      exhibitors,
    });
  } catch (error) {
    return NextResponse.json({
      error,
    });
  } finally {
    await prisma.$disconnect();
  }
};

export const PUT = async (req: Request) => {
  const { type, email, id, messageForDeclien } = await req.json();
  const password = AppPasscodeGen();

  if (type == "approve") {
    try {
      const exhibitor = await prisma.exhibitors.update({
        where: {
          id,
        },
        data: {
          status: "accepted",
          messageForDeclien: "",
          password,
        },
      });
      SendAcceptedEmail(email, password);
      return NextResponse.json(
        {
          exhibitor,
        },
        {
          status: 200,
        }
      );
    } catch (error) {
      return NextResponse.json(
        {
          error,
        },
        {
          status: 404,
        }
      );
    } finally {
      await prisma.$disconnect();
    }
  } else {
    try {
      const exhibitor = await prisma.exhibitors.update({
        where: {
          id,
        },
        data: {
          status: "rejected",
          messageForDeclien,
        },
      });
      SendRejectionEmail(email, messageForDeclien);
      return NextResponse.json(
        {
          exhibitor,
        },
        {
          status: 200,
        }
      );
    } catch (error) {
      return NextResponse.json(
        {
          error,
        },
        {
          status: 404,
        }
      );
    } finally {
      await prisma.$disconnect();
    }
  }
};
