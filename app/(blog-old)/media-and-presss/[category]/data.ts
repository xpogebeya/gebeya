/** @format */
import prisma from "@/prisma/client";
import { NextApiRequest, NextApiResponse } from "next";
export type Post = {
  imageUrl: string;
  heading: string;
  desc: string;
  date: string;
  period: string;
};

export type Posts = {
  [key: string]: Post[];
};

export const posts: Posts = {
  Technology: [
    {
      imageUrl: "/Today-trend-7.webp",
      heading: "All of these special-features come at an affordable price!",
      desc: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical literature from 459, making it over 2000 years old. Richard McClintock, a Latin professor at Virginia looked up one of the more obscure. Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical literature from 459, making it over 2000 years old. Richard McClintock, a Latin professor at Virginia looked up one of the more obscure",
      date: "15 June 2021",
      period: "14 min read",
    },
    {
      imageUrl: "/Today-trend-8.webp",
      heading: "Customize i WooCommerce theme-with-countless Video",
      desc: " Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical literature from 459, making it over 2000 years old. Richard McClintock, a Latin professor at Virginia looked up one of the more obscure. Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical literature from 459, making it over 2000 years old. Richard McClintock, a Latin professor at Virginia looked up one of the more obscure",
      date: "15 June 2021",
      period: "14 min read",
    },
    {
      imageUrl: "/Today-trend-9.webp",
      heading: "WooCommerce with intuitive drag-and-drop",
      desc: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical literature from 459, making it over 2000 years old. Richard McClintock, a Latin professor at Virginia looked up one of the more obscure. Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical literature from 459, making it over 2000 years old. Richard McClintock, a Latin professor at Virginia looked up one of the more obscure",
      date: "15 June 2021",
      period: "14 min read",
    },

    {
      imageUrl: "/Today-trend-10.webp",
      heading: "Create beautiful designs that will help convert more",
      desc: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical literature from 459, making it over 2000 years old. Richard McClintock, a Latin professor at Virginia looked up one of the more obscure. Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical literature from 459, making it over 2000 years old. Richard McClintock, a Latin professor at Virginia looked up one of the more obscure",
      date: "15 June 2021",
      period: "14 min read",
    },
  ],
  Construction: [
    {
      imageUrl: "/Today-trend-7.webp",
      heading: "All of these special-features come at an affordable price!",
      desc: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical literature from 459, making it over 2000 years old. Richard McClintock, a Latin professor at Virginia looked up one of the more obscure…",
      date: "15 June 2021",
      period: "14 min read",
    },
    {
      imageUrl: "/Today-trend-8.webp",
      heading: "Customize i WooCommerce theme-with-countless Video",
      desc: " Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical literature from 459, making it over 2000 years old. Richard McClintock, a Latin professor at Virginia looked up one of the more obscure…",
      date: "15 June 2021",
      period: "14 min read",
    },
    {
      imageUrl: "/Today-trend-9.webp",
      heading: "WooCommerce with intuitive drag-and-drop",
      desc: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical literature from 459, making it over 2000 years old. Richard McClintock, a Latin professor at Virginia looked up one of the more obscure…",
      date: "15 June 2021",
      period: "14 min read",
    },
    {
      imageUrl: "/Today-trend-10.webp",
      heading: "Create beautiful designs that will help convert more...",
      desc: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical literature from 459, making it over 2000 years old. Richard McClintock, a Latin professor at Virginia looked up one of the more obscure…",
      date: "15 June 2021",
      period: "14 min read",
    },
  ],
  Policy: [
    {
      imageUrl: "/Today-trend-7.webp",
      heading: "All of these special-features come at an affordable price!",
      desc: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical literature from 459, making it over 2000 years old. Richard McClintock, a Latin professor at Virginia looked up one of the more obscure…",
      date: "15 June 2021",
      period: "14 min read",
    },
    {
      imageUrl: "/Today-trend-8.webp",
      heading: "Customize i WooCommerce theme-with-countless Video",
      desc: " Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical literature from 459, making it over 2000 years old. Richard McClintock, a Latin professor at Virginia looked up one of the more obscure…",
      date: "15 June 2021",
      period: "14 min read",
    },
    {
      imageUrl: "/Today-trend-9.webp",
      heading: "WooCommerce with intuitive drag-and-drop",
      desc: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical literature from 459, making it over 2000 years old. Richard McClintock, a Latin professor at Virginia looked up one of the more obscure…",
      date: "15 June 2021",
      period: "14 min read",
    },
    {
      imageUrl: "/Today-trend-10.webp",
      heading: "Create beautiful designs that will help convert more...",
      desc: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical literature from 459, making it over 2000 years old. Richard McClintock, a Latin professor at Virginia looked up one of the more obscure…",
      date: "15 June 2021",
      period: "14 min read",
    },
  ],
  Finance: [
    {
      imageUrl: "/Today-trend-7.webp",
      heading: "All of these special-features come at an affordable price!",
      desc: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical literature from 459, making it over 2000 years old. Richard McClintock, a Latin professor at Virginia looked up one of the more obscure…",
      date: "15 June 2021",
      period: "14 min read",
    },
    {
      imageUrl: "/Today-trend-8.webp",
      heading: "Customize i WooCommerce theme-with-countless Video",
      desc: " Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical literature from 459, making it over 2000 years old. Richard McClintock, a Latin professor at Virginia looked up one of the more obscure…",
      date: "15 June 2021",
      period: "14 min read",
    },
    {
      imageUrl: "/Today-trend-9.webp",
      heading: "WooCommerce with intuitive drag-and-drop",
      desc: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical literature from 459, making it over 2000 years old. Richard McClintock, a Latin professor at Virginia looked up one of the more obscure…",
      date: "15 June 2021",
      period: "14 min read",
    },
    {
      imageUrl: "/Today-trend-10.webp",
      heading: "Create beautiful designs that will help convert more...",
      desc: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical literature from 459, making it over 2000 years old. Richard McClintock, a Latin professor at Virginia looked up one of the more obscure…",
      date: "15 June 2021",
      period: "14 min read",
    },
  ],
  Machinery: [
    {
      imageUrl: "/Today-trend-7.webp",
      heading: "All of these special-features come at an affordable price!",
      desc: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical literature from 459, making it over 2000 years old. Richard McClintock, a Latin professor at Virginia looked up one of the more obscure…",
      date: "15 June 2021",
      period: "14 min read",
    },
    {
      imageUrl: "/Today-trend-8.webp",
      heading: "Customize i WooCommerce theme-with-countless Video",
      desc: " Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical literature from 459, making it over 2000 years old. Richard McClintock, a Latin professor at Virginia looked up one of the more obscure…",
      date: "15 June 2021",
      period: "14 min read",
    },
    {
      imageUrl: "/Today-trend-9.webp",
      heading: "WooCommerce with intuitive drag-and-drop",
      desc: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical literature from 459, making it over 2000 years old. Richard McClintock, a Latin professor at Virginia looked up one of the more obscure…",
      date: "15 June 2021",
      period: "14 min read",
    },
    {
      imageUrl: "/Today-trend-10.webp",
      heading: "Create beautiful designs that will help convert more...",
      desc: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical literature from 459, making it over 2000 years old. Richard McClintock, a Latin professor at Virginia looked up one of the more obscure…",
      date: "15 June 2021",
      period: "14 min read",
    },
  ],
  Press_release: [
    {
      imageUrl: "/Today-trend-7.webp",
      heading: "All of these special-features come at an affordable price!",
      desc: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical literature from 459, making it over 2000 years old. Richard McClintock, a Latin professor at Virginia looked up one of the more obscure…",
      date: "15 June 2021",
      period: "14 min read",
    },
    {
      imageUrl: "/Today-trend-8.webp",
      heading: "Customize i WooCommerce theme-with-countless Video",
      desc: " Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical literature from 459, making it over 2000 years old. Richard McClintock, a Latin professor at Virginia looked up one of the more obscure…",
      date: "15 June 2021",
      period: "14 min read",
    },
    {
      imageUrl: "/Today-trend-9.webp",
      heading: "WooCommerce with intuitive drag-and-drop",
      desc: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical literature from 459, making it over 2000 years old. Richard McClintock, a Latin professor at Virginia looked up one of the more obscure…",
      date: "15 June 2021",
      period: "14 min read",
    },
    {
      imageUrl: "/Today-trend-10.webp",
      heading: "Create beautiful designs that will help convert more...",
      desc: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical literature from 459, making it over 2000 years old. Richard McClintock, a Latin professor at Virginia looked up one of the more obscure…",
      date: "15 June 2021",
      period: "14 min read",
    },
  ],
};
