/** @format */

import React from "react";

type Props = any;
const Layout = ({ children }: Props) => {
  return (
    <div className=" pt-32 px-[5vw] lg:px-[10vw] 2xl:pr-8  xl:pl-[15vw] text-3xl">
      {children}
    </div>
  );
};

export default Layout;
