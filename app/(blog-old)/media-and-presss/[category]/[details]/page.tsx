/** @format */

/** @format */
"use client";
import React, { useEffect, useState } from "react";
import Image from "next/image";
import Link from "next/link";

import { BlogCard } from "@/components";

type PostType = {
  _id: string;
  category: string;
  title: string;
  description: string;
  imageUrls: string[];
  subpost: SubPost[];
  date: string;
};
type SubPost = {
  subtitle: string;
  description: string;
  imageUrls: string[];
};
const page = ({ params: { details } }: { params: { details: string } }) => {
  const [post, setPost] = useState<PostType[]>();
  const fetchPost = async () => {
    try {
      const response = await fetch(`/api/posts/${details}`);
      const data = await response.json();
      setPost(data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  useEffect(() => {
    fetchPost();
  }, []);
  return (
    post && (
      <div className="p-20 mx-20 ">
        <Link
          className="hover:text-BluePurple mb-10 py-10"
          href="/media-and-press"
        >
          Home
        </Link>{" "}
        <div className="flex flex-wrap">
          {post &&
            post[0].imageUrls.map((url) => (
              <Image
                className=" rounded-2xl"
                src={url}
                width="800"
                height="50"
                alt="trend's image"
              />
            ))}
        </div>
        {
          <BlogCard
            heading={post[0].title}
            desc={post[0].description}
            large_title={false}
            description={true}
            category={post[0].category}
            nolink={true}
          />
        }
        <div className="">
          <div className="flex flex-wrap gap-5 2xl:mr-10">
            {post[0].subpost?.map((subpost) =>
              subpost.imageUrls?.map((url) => (
                <Image
                  className=" rounded-2xl"
                  src={url}
                  width="300"
                  height="50"
                  alt="trend's image"
                />
              ))
            )}
          </div>
          {post[0].subpost?.map((subpost) => (
            <BlogCard
              heading={subpost.subtitle}
              desc={subpost.description}
              large_title={false}
              description={true}
              category={post[0].category}
              nolink={true}
            />
          ))}
        </div>
      </div>
    )
  );
};

export default page;
