/** @format */
"use client";
import React, { useState, useEffect } from "react";
import Image from "next/image";
import { notFound } from "next/navigation";
import Link from "next/link";

import { BlogCard } from "@/components";
import SocialLinks from "@/components/SocialLinks/SocialLinks";

type Props = {
  params: { category: string };
};

const getData = (cat: string, postData: PostData) => {
  cat =
    cat === "Machinery%20and%20spare%20parts"
      ? "machinery"
      : cat === "Press%20releases"
      ? "pressRelease"
      : cat;
  const data = postData[cat];
  if (data) {
    return data;
  }

  return notFound();
};
type PostData = {
  [name: string]: PostType[];
};
type PostType = {
  _id: string;
  title: string;
  category: string;
  description: string;
  imageUrls: string[];
  subpost: SubPost[];
  date: string;
};
type SubPost = {
  subtitle: string;
  description: string;
  imageUrls: string[];
};
const Page = async ({ params }: Props) => {
  const [postData, setPostData] = useState<PostData>();

  const data = postData && getData(params.category, postData);

  const fetchPost = async () => {
    try {
      const response = await fetch("/api/posts");
      const data = await response.json();
      setPostData(data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };
  useEffect(() => {
    fetchPost();
  }, []);

  return (
    data && (
      <div className="lg:grid lg:grid-cols-12 xl:relative">
        <div className=" flex flex-col gap-5 col-span-12 2xl:col-span-8 mx-6 md:mx-16  2xl:h-[90vh] 2xl:overflow-scroll">
          <p className=" ml-[5vw] my-[10vh] text-lg md:text-xl text-BlueDark ">
            {" "}
            <Link className="hover:text-BluePurple" href="/media-and-press">
              Home
            </Link>{" "}
            /
            <Link className="hover:text-BluePurple" href="/media-and-press/">
              {params.category === "Machinery%20and%20spare%20parts"
                ? "Machinery and spare parts"
                : params.category === "Press%20releases"
                ? "Press Release"
                : params.category}
            </Link>
          </p>

          {data.map((item) => (
            <div className="">
              {item.imageUrls.map((url) => (
                <Image
                  className=" rounded-2xl"
                  src={url}
                  width="800"
                  height="50"
                  alt="trend's image"
                />
              ))}
              <div className="">
                <BlogCard
                  heading={item.title}
                  desc={item.description}
                  large_title={false}
                  description={true}
                  category={item.category}
                  id={item._id}
                />
              </div>
            </div>
          ))}
          <div className="2xl:hidden">
            <SocialLinks />
          </div>
        </div>
        <div className="hidden 2xl:block ml-3 col-span-3 top-[5vh] left-[70%] xl:absolute">
          <div className="">
            <p className=" text-BlueDark mb-2">Latest Post</p>
            {data.slice(0, 4).map((item) => (
              <div className="flex flex-row">
                <Image
                  className="object-contain rounded-2xl"
                  src={item.imageUrls[0]}
                  width="150"
                  height="50"
                  alt="trend's image"
                />
                <div className="">
                  <BlogCard
                    heading={item.title}
                    desc={item.description}
                    large_title={false}
                    description={false}
                    category={item.category}
                    latest={true}
                    id={item._id}
                  />
                </div>
              </div>
            ))}
          </div>
          <div className="mt-20">
            <SocialLinks />
          </div>
        </div>
      </div>
    )
  );
};

export default Page;
