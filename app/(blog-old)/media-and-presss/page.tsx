/** @format */
"use client";
import React, { useEffect, useState } from "react";
import Image from "next/image";
import Link from "next/link";

import { FcPrevious, FcNext } from "react-icons/fc";
import { AiOutlineArrowUp } from "react-icons/ai";
import { AiOutlineArrowDown } from "react-icons/ai";

import { BlogCard } from "@/components";

const page = () => {
  type Post = {
    technology: category[];
  };

  type category = {
    heading: string;
    desc: string;
    period: string;
    date: string;
    imageUrl: string;
  };
  const [currentData, setCurrentData] = useState(0);
  const [initial, setInitial] = useState(0);
  const [final, setFinal] = useState(3);

  type PostData = {
    [name: string]: PostType[];
  };

  type PostType = {
    _id: string;
    category: string;
    title: string;
    description: string;
    imageUrls: string[];
    subpost: SubPost[];
    date: string;
  };

  type SubPost = {
    subtitle: string;
    description: string;
    imageUrls: string[];
  };

  type Category = {
    technology: string;
    construction: string;
    finance: string;
    policy: string;
    machinery: string;
    centralImage: string;
  };

  // const item = posts.technology[currentData];
  const [postData, setPostData] = useState<PostData>();
  const [blog_Category, setCategory] = useState<Category[]>();

  const allPosts = postData && postData["allPosts"];

  const BlogCategories: { key: string; value: string }[] = blog_Category
    ? Object.entries(blog_Category[0]).map(([key, value]) => ({ key, value }))
    : [];
  const centralImage =
    BlogCategories &&
    BlogCategories.find(({ key, value }) => key === "centralImage");

  const pressReleasePost = postData && postData["pressRelease"][currentData];
  const fetchCategory = async () => {
    try {
      const response = await fetch("/api/posts/category");
      const category = await response.json();
      setCategory(category);
    } catch (error) { }
  };
  const fetchPost = async () => {
    try {
      const response = await fetch("/api/posts");
      const data = await response.json();
      setPostData(data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };
  useEffect(() => {
    fetchPost();
    fetchCategory();
  }, []);

  return (
    <>
      <div className="flex w-[85%] ml-[12%] mt-36 gap-6">
        <div className="w-1/3 bg-gray-300 h-[1px] mt-5"></div>
        <p className=" text-4xl  text-BlueDark">Trending Today’s</p>
        <div className="w-1/3 bg-gray-300 h-[1px] mt-5"></div>
      </div>
      <div className=" flex flex-col  md:mr-[8%] xl:mr-0 md:ml-[2%] 2xl:ml-[11%] 2xl:mx-[10%] max-w-full  2xl:w-[85%] xl:px-0 mt-5 md:mt-10 lg:mt-20 gap-20">
        <div className="flex w-full xl:ml-0 ">
          {currentData > 0 ? (
            <button
              className=" text-white hover:text-BlueLighter "
              onClick={() => {
                setCurrentData(currentData - 1);
              }}
            >
              <FcPrevious className="w-6 h-6 md:w-12 md:h-12" />
            </button>
          ) : null}
          <div className="flex flex-col ml-0 md:ml-7 2xl:ml-20 xl:flex-row ">
            {pressReleasePost && (
              <>
                <Image
                  className=" rounded-full p-10 pb-0 w-full "
                  src={pressReleasePost.imageUrls[0]}
                  width="450"
                  height="450"
                  alt="trend's image"
                />

                <div className="">
                  <BlogCard
                    heading={pressReleasePost.title}
                    desc={pressReleasePost.description}
                    date={pressReleasePost.date}
                    large_title={true}
                    description={true}
                    id={pressReleasePost._id}
                    category={pressReleasePost.category}
                  />
                </div>
              </>
            )}
          </div>
          {postData && currentData < postData["pressRelease"].length - 1 ? (
            <button
              className=" text-gray-200 hover:text-gray-500"
              onClick={() => {
                setCurrentData(currentData + 1);
              }}
            >
              <FcNext className=" w-6 h-6 md:w-12 md:h-12  text-white " />
            </button>
          ) : null}
        </div>

        <div className="">
          <p className="ml-5 py-10 md:ml-10 2xl:ml-28 text-4xl  text-BlueDark">
            Trending Topics
          </p>
          <div className=" flex flex-col lg:flex-row mx-7 xl:mx-5  md:ml-10 2xl:ml-28 gap-5 2xl:grid 2xl:grid-cols-6">
            <div className="flex flex-col-reverse md:flex-row gap-10 2xl:col-span-3 2xl:grid 2xl:grid-cols-3 ">
              <div className="flex flex-col gap-4 xl:gap-5 2xl:col-span-1 ">
                {initial > 0 ? (
                  <div className="flex flex-col gap-1 items-center">
                    <button
                      className=" bg-BlueLighter p-3   text-center text-gray-400 hover:text-white text-xl rounded-full"
                      onClick={() => {
                        setInitial(initial - 1);
                        setFinal(final - 1);
                      }}
                    >
                      <AiOutlineArrowUp />
                    </button>
                  </div>
                ) : null}

                {BlogCategories?.filter(({ key }) => key !== "centralImage")
                  .slice(initial, final)
                  .map(({ key, value }) => (
                    <Link href={`/media-and-press/${key}`}>
                      <div className="relative group">
                        <Image
                          className=" object-contain rounded-lg hover:opacity-8 "
                          src={value}
                          width="450"
                          height="100"
                          alt="image"
                        />
                        <div className="absolute inset-0 hidden group-hover:flex justify-center items-center">
                          <div className=" bg-BlueDark bg-opacity-70 w-full h-full rounded-lg">
                            <p className=" text-white font-bold text-xl pt-6 text-center">
                              {key}
                            </p>
                          </div>
                        </div>
                      </div>{" "}
                    </Link>
                  ))}

                {final < BlogCategories.length ? (
                  <div className="flex flex-col gap-1 items-center">
                    <button
                      className=" bg-BlueLighter p-3 text-gray-400  hover:text-white text-xl rounded-full"
                      onClick={() => {
                        setFinal(final + 1);
                        setInitial(initial + 1);
                      }}
                    >
                      <AiOutlineArrowDown />
                    </button>
                  </div>
                ) : null}
              </div>
              {centralImage && (
                <div className="w-full 2xl:col-span-2">
                  <Image
                    className="lg:h-[100%] xl:h-[98%] 2xl:h-[95%] rounded-2xl"
                    src={centralImage.value}
                    width="700"
                    height="200"
                    alt="image"
                  />
                </div>
              )}
            </div>

            <div className="flex flex-col lg:pl-8 gap-7 pt-3 lg:pt-4  xl:gap-14 xl:pt-0 2xl:pt-3 2xl:gap-5 2xl:col-span-3 ">
              {allPosts?.slice(0, 2).map((post) => (
                <BlogCard
                  heading={post.title}
                  desc={post.description}
                  date={post.date}
                  description={true}
                  topic={true}
                  id={post._id}
                  category={post.category}
                />
              ))}
            </div>
          </div>
        </div>
        <div className=" mx-5 md:ml-10 2xl:ml-28 mr-1 2xl:mr-40">
          <p className="text-4xl  text-BlueDark">Trending Articles</p>
          <div className="grid grid-cols-1 lg:grid-cols-2 gap-6 mt-10 ">
            {allPosts?.map((item) => (
              <div className="flex">
                <Image
                  className=" w-1/3  rounded-xl"
                  src={item.imageUrls[0]}
                  width="200"
                  height="150"
                  alt="trend's image"
                />
                <BlogCard
                  heading={item.title}
                  desc={item.description}
                  // period={item.period}
                  date={item.date}
                  description={false}
                  id={item._id}
                  category={item.category}
                />
              </div>
            ))}
          </div>
        </div>

      </div>
    </>
  );
};

export default page;
