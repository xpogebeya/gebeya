import React from 'react'

type Props = {}

const page = (props: Props) => {
  return (
    <div className="container mx-auto p-4 w-[85%] lg:w-[75%] mt-20 md:mt-40">
      <h1 className="text-2xl font-bold mb-4 text-BlueDark">Terms and Conditions</h1>
      <section className="mb-8">
        <h2 className=" text-base lg:text-xl font-semibold mb-2 text-BlueDark">Terms and Conditions of Use for GebeyaXpo Website</h2>
        <p className=' text-gray-500 text-xs leading-5 md:text-sm lg:text-base'>
          These Terms and Conditions outline the rules and regulations for accessing and using the GebeyaXpo website.
          By accessing or using the Website, you agree to be bound by these Terms. If you do not agree with any part of
          these Terms, please refrain from using the Website.
        </p>
      </section>
      <section className="mb-8">
        <h2 className=" text-base lg:text-xl font-semibold mb-2 text-BlueDark">Intellectual Property</h2>
        <p className=' text-gray-500 text-xs leading-5 md:text-sm lg:text-base'>
          a. The Website and its content, including but not limited to text, graphics, logos, images, videos, and
          software, are the property of GebeyaXpo and protected by intellectual property laws. You may not modify,
          reproduce, distribute, or exploit any part of the Website without prior written permission from GebeyaXpo.
        </p>
        <p className=' text-gray-500 text-xs leading-5 md:text-sm lg:text-base'>
          b. Any trademarks, service marks, logos, or trade names displayed on the Website are the property of their
          respective owners. You are prohibited from using any trademarks or logos without the explicit consent of the
          respective owners.
        </p>
      </section>

      <section className="mb-8">
        <h2 className=" text-base lg:text-xl font-semibold mb-2 text-BlueDark">User Conduct</h2>
        <p className=' text-gray-500 text-xs leading-5 md:text-sm lg:text-base'>
          a. When using the Website, you agree to abide by all applicable laws and regulations. You shall not engage in
          any activity that may disrupt, interfere with, or harm the functioning of the Website or infringe upon the
          rights of others.
        </p>
        <p className=' text-gray-500 text-xs leading-5 md:text-sm lg:text-base'>
          b. You are solely responsible for any content you post or transmit through the Website. You agree not to post
          any unlawful, offensive, defamatory, or misleading content. GebeyaXpo reserves the right to remove or modify
          any user-generated content that violates these Terms.
        </p>
      </section>

      <section className="mb-8">
        <h2 className=" text-base lg:text-xl font-semibold mb-2 text-BlueDark">Third-Party Links</h2>
        <p className=' text-gray-500 text-xs leading-5 md:text-sm lg:text-base'>
          a. The Website contains links to third-party websites or resources. These links are provided for your
          convenience and do not imply GebeyaXpo's endorsement or responsibility for the linked content. You access
          third-party websites at your own risk, and GebeyaXpo is not liable for any damages or losses arising from
          your use of such websites.
        </p>
      </section>

      <section className="mb-8">
        <h2 className=" text-base lg:text-xl font-semibold mb-2 text-BlueDark">Limitation of Liability</h2>
        <p className=' text-gray-500 text-xs leading-5 md:text-sm lg:text-base'>
          a. GebeyaXpo strives to ensure the accuracy and reliability of the information on the Website but does not
          guarantee its completeness or timeliness. The use of the Website is at your own risk, and GebeyaXpo shall not
          be held liable for any direct, indirect, incidental, or consequential damages arising from your use of the
          Website or reliance on its content.
        </p>
      </section>

      <section className="mb-8">
        <h2 className=" text-base lg:text-xl font-semibold mb-2 text-BlueDark">Modifications</h2>
        <p className=' text-gray-500 text-xs leading-5 md:text-sm lg:text-base'>
          a. GebeyaXpo reserves the right to modify or update these Terms at any time without prior notice. It is your
          responsibility to review these Terms periodically for any changes. Continued use of the Website after any
          modifications constitutes your acceptance of the revised Terms.
        </p>
      </section>

      <section className="mb-8">
        <h2 className=" text-base lg:text-xl font-semibold mb-2 text-BlueDark">Governing Law</h2>
        <p className=' text-gray-500 text-xs leading-5 md:text-sm lg:text-base'>
          a. These Terms shall be governed by and construed in accordance with the laws of the jurisdiction where
          GebeyaXpo operates.
        </p>
      </section>

      <p className="mt-8 text-gray-500 text-xs leading-5 md:text-sm lg:text-base">
        By using the GebeyaXpo Website, you acknowledge and agree to these Terms and Conditions.
      </p>
    </div>
  )
}

export default page
