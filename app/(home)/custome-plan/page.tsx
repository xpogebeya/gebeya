'use client'
import { PlanProps } from '@/types/sanity';
import { useAppSelector } from '@/utils';
import React, { useState } from 'react'
import { AppCurrentPlan, AppPlanLister, AppWorkWithUs } from '@/components';
import { useRouter } from 'next/navigation';
import { CircularProgress } from '@mui/material';
import { IoIosRemove } from "react-icons/io";
import IconButton from '@mui/joy/IconButton';
interface ListItem {
    title: string;
    quantity: number;
    unit_price: number;
    total_price: number;
}
export interface PlanSection {
    id: string
    title: string;
    items: ListItem[];
}

const planSections: PlanSection[][] = [
    [
        {
            id: "a",
            "title": "Brochure placement (A5 size)",
            "items": [
                {
                    "title": "Mini brochure on snack plates",
                    "quantity": 8,
                    "unit_price": 69.63,
                    "total_price": 139259.25
                }
            ]
        },
        {
            id: "b",

            "title": "Snacks + tea + coffee + Milk",
            "items": [
                {
                    "title": "Teabreak sponsorship",
                    "quantity": 8,
                    "unit_price": 1059.58,
                    "total_price": 2119162.50
                }
            ]
        },
        {
            id: "c",

            "title": "Tier 1",
            "items": [
                {
                    "title": "Refreshment for 2040 Tier 1 Exhibitors",
                    "quantity": 2040,
                    "unit_price": 1412.78,
                    "total_price": 2882061.00
                },
                {
                    "title": "Water for 2040 Tier 1 Exhibitors",
                    "quantity": 2040,
                    "unit_price": 34.31,
                    "total_price": 69992.91
                },
                {
                    "title": "Brochure for 54000 passersby",
                    "quantity": 2040,
                    "unit_price": 46.42,
                    "total_price": 64696.29
                }
            ]
        },
        {
            id: "d",

            "title": "Tier 2",
            "items": [
                {
                    "title": "Refreshment for 2448 Tier 2 Exhibitors",
                    "quantity": 2448,
                    "unit_price": 1412.78,
                    "total_price": 3458473.20
                },
                {
                    "title": "Water for 2448 Tier 2 Exhibitors",
                    "quantity": 2448,
                    "unit_price": 34.31,
                    "total_price": 83991.49
                },
                {
                    "title": "Brochure for 54000 passersby",
                    "quantity": 2448,
                    "unit_price": 46.42,
                    "total_price": 113635.55
                }
            ]
        },
        {
            id: "e",

            "title": "Tier 3",
            "items": [
                {
                    "title": "Refreshment for 1632 Tier 3 Exhibitors",
                    "quantity": 1632,
                    "unit_price": 1412.78,
                    "total_price": 2305648.00
                },
                {
                    "title": "Water for 68 Tier 3 Exhibitors",
                    "quantity": 1632,
                    "unit_price": 34.31,
                    "total_price": 55994.33
                },
                {
                    "title": "Brochure for 54000 passersby",
                    "quantity": 1632,
                    "unit_price": 46.42,
                    "total_price": 75757.03
                }
            ]
        },
        {
            id: "f",

            "title": "Standard booth",
            "items": [
                {
                    "title": "Refreshment for 68 Tier 4 Exhibitors",
                    "quantity": 540,
                    "unit_price": 1412.78,
                    "total_price": 762898.50
                },
                {
                    "title": "Water for 68 Tier 4 Exhibitors",
                    "quantity": 540,
                    "unit_price": 34.31,
                    "total_price": 18527.54
                },
                {
                    "title": "Brochure for 54000 passersby",
                    "quantity": 540,
                    "unit_price": 46.42,
                    "total_price": 25066.67
                }
            ]
        },
        {
            id: "g",

            "title": "On demand education",
            "items": [
                {
                    "title": "Online accessible training",
                    "quantity": 5,
                    "unit_price": 483500.00,
                    "total_price": 2417500.00
                }
            ]
        }
    ],
    [
        {
            id: "h",

            "title": "3 day documentation video",
            "items": [
                {
                    "title": "Upload videos, photos, interviews, and user-friendly content to YouTube and website showcasing diverse work categories.",
                    "quantity": 5,
                    "unit_price": 483500,
                    "total_price": 2417500
                }
            ]
        },
        {
            id: "i",

            "title": "Social media",
            "items": [
                {
                    "title": "Sponsorship mentions - 4 times for tier 1, 3 times for tier 2, 2 times for tier 3, 1 time for tier 4.",
                    "quantity": 100,
                    "unit_price": 18164.25,
                    "total_price": 1816425
                }
            ]
        },
        {
            id: "j",

            "title": "TikTok",
            "items": [
                {
                    "title": "A challenge to share, post, hashtag, repost will go viral. Winner gets a house or car.",
                    "quantity": 1,
                    "unit_price": 12000000,
                    "total_price": 12000000
                }
            ]
        },
        {
            id: "k",

            "title": "YouTube",
            "items": []
        },
        {
            id: "al",

            "title": "Website monetization",
            "items": [
                {
                    "title": "Logo placement, online directory, and blogs",
                    "quantity": 200,
                    "unit_price": 10000,
                    "total_price": 2000000
                },
                {
                    "title": "Blogs",
                    "quantity": 200,
                    "unit_price": 5000,
                    "total_price": 1000000
                }
            ]
        }
    ],
    [
        {
            id: "ajj",

            "title": "Presence",
            "items": [
                {
                    "title": "Brochure placement on dinner table",
                    "quantity": 2250,
                    "unit_price": 1000.00,
                    "total_price": 2250000.00
                },
                {
                    "title": "Brochure distribution to companies",
                    "quantity": 450,
                    "unit_price": 10000.00,
                    "total_price": 4500000.00
                }
            ]
        },
        {
            id: "asa",

            "title": "Verbal recognition",
            "items": [
                {
                    "title": "Mentions",
                    "quantity": 3,
                    "unit_price": 500000.00,
                    "total_price": 2500000.00
                }
            ]
        },
        {
            id: "asad",

            "title": "Banners on stages and hall",
            "items": [
                {
                    "title": "Large banners",
                    "quantity": 2,
                    "unit_price": 500.00,
                    "total_price": 450000.00
                },
                {
                    "title": "Small banners",
                    "quantity": 10,
                    "unit_price": 300.00,
                    "total_price": 1350000.00
                }
            ]
        },
        {
            id: "tyuia",

            "title": "Paper bag",
            "items": [
                {
                    "title": "Paper bags for attendants",
                    "quantity": 450,
                    "unit_price": 300.00,
                    "total_price": 135000.00
                }
            ]
        },
        {
            id: "al;kkp",

            "title": "Social media",
            "items": [
                {
                    "title": "Partnership mention",
                    "quantity": 0,
                    "unit_price": 0,
                    "total_price": 0
                }
            ]
        },
        {
            id: "aoph",

            "title": "Picture background",
            "items": [
                {
                    "title": "Logo placement on 15 m2 picture banner",
                    "quantity": 13,
                    "unit_price": 500.00,
                    "total_price": 325000.00
                }
            ]
        },
        {
            id: "asdhsjh",

            "title": "Invitation card",
            "items": [
                {
                    "title": "Quantity of invitation cards",
                    "quantity": 400,
                    "unit_price": 500.00,
                    "total_price": 200000.00
                }
            ]
        }
    ]

]

const menus: string[] = ["Varieties", "e-chances", "Premiere principles"]

const page = () => {
    const [current, setCurrent] = useState<number>(0)
    const [selectedItem, setSelectedItem] = useState<PlanSection[]>([])
    const [total, setTotal] = useState<number>(0)
    const { sponsors } = useAppSelector(state => state.homePage)
    const [selectedPackaging, setSelectedPackaging] = useState<string>("")
    const [mainPackPrice, setMainPackPrice] = useState<number>(0)
    const [currentPackage, setCurrentPackage] = useState<PlanProps[]>([])
    const router = useRouter();

    const handleSubmit = (plan: string) => {
        router.push("#detail")
        if (sponsors) {
            const res = sponsors?.packages.filter((pack) => (
                pack.plan == plan
            ))
            setCurrentPackage(res)
        }

    }
    const checkSelected = (item: PlanSection): boolean => {
        const found = selectedItem.find((list) => list.id == item.id)
        if (found) {
            return true
        }

        return false
    }
    const handleSelect = (selectedItem: PlanSection, isSelected: boolean) => {
        let totalPriceChange = 0
        selectedItem.items.map((item) => {
            totalPriceChange += item.total_price
        });


        if (isSelected) {
            setTotal(total - totalPriceChange);
            setSelectedItem(prevSelected => prevSelected.filter(item => item.id !== selectedItem.id));
        } else {
            setTotal(total + totalPriceChange);
            setSelectedItem(prevSelected => [selectedItem, ...prevSelected]);
        }
    };

    const handleTotalPrice = (selectedItem: PlanSection): string => {

        let res = 0
        selectedItem.items.forEach((item) => {
            res += item.total_price
        })
        return res.toLocaleString()
    }

    const handlePackSelect = (price: string, plan: string) => {

        const currentRemianing = total - mainPackPrice
        if (mainPackPrice > 0) {
            setTotal(currentRemianing + parseInt(price))
        } else {
            setTotal(prevTotal => prevTotal + parseInt(price))
        }

        setSelectedPackaging(plan)
        router.push("#workWithUS")
        setMainPackPrice(parseInt(price))

    }

    const handleMainPackRemove = () => {
        setSelectedPackaging("")
        setTotal(total - mainPackPrice)
        setMainPackPrice(0)

    }

    const handleAfterSubmit = () => {
        setSelectedPackaging("")
        setSelectedItem([])
        setTotal(0)
    }



    return (
        <div className=' mt-10  min-h-[100dvh]'>
            {
                sponsors ? (
                    <>
                        <section className="py-8 text-gray-800">
                            {
                                sponsors &&
                                <AppPlanLister sponsors={sponsors} setSelectedPackaging={handleSubmit} />
                            }
                            {
                                currentPackage.length > 0 &&
                                <AppCurrentPlan name={currentPackage[0].plan} price={currentPackage[0].price} packages={currentPackage[0].packages} handleSelect={handlePackSelect} />
                            }
                            <div className=" container mx-auto">
                                <div className="p-4 mx-auto text-center md:px-10 lg:px-32 xl:max-w-3xl mt-5">
                                    <h2 className="text-2xl font-bold leadi sm:text-4xl text-BlueDark">Personalize your plan.</h2>
                                    <p className="px-8 my-4 text-gray-400 text-base">Tailor a unique and effective plan that resonates with your needs and aspirations.</p>
                                </div>
                                <div className="grid grid-cols-4 p-4 md:p-8">
                                    <div className="flex justify-center px-4 col-span-full py-3">
                                        {
                                            menus.map((menu, index) => (
                                                <button
                                                    key={menu}
                                                    onClick={() => setCurrent(index)}
                                                    className={` text-sm md:text-base px-5 py-1 border-b-2 md:border-l-2 md:border-b-0 md:py-3  ${current == index ? "border-BlueLighter text-gray-600 bg-BlueLighter/20" : "border-gray-300 text-gray-400 hover:bg-gray-100"}`}>
                                                    {menu}
                                                </button>
                                            ))
                                        }

                                    </div>
                                    <div className="grid gap-12 py-4 text-center sm:grid-cols-2 lg:grid-cols-3 col-span-full md:col-span-4 md:text-left mx-auto ">
                                        {
                                            planSections[current].map((item, index) => (
                                                <div
                                                    onClick={() => handleSelect(item, checkSelected(item))}
                                                    key={item.id}
                                                    className={`group max-w-[360px] xl:w-[360px] flex flex-col items-center justify-center space-y-3 md:justify-start md:items-start cursor-pointer  hover:bg-BlueLighter/10 rounded-lg p-3 ${checkSelected(item) && "bg-BlueLighter/20"}`}>
                                                    <svg fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" className="w-6 h-6  text-BlueLighter">
                                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9 3v2m6-2v2M9 19v2m6-2v2M5 9H3m2 6H3m18-6h-2m2 6h-2M7 19h10a2 2 0 002-2V7a2 2 0 00-2-2H7a2 2 0 00-2 2v10a2 2 0 002 2zM9 9h6v6H9V9z"></path>
                                                    </svg>
                                                    <h5 className=" text-sm md:text-xl font-semibold  ">{item.title}</h5>
                                                    {
                                                        item.items.map((list) => (
                                                            <p className=' text-sm md:text-base text-gray-400' key={list.title}>{list.title}</p>
                                                        ))
                                                    }
                                                </div>
                                            ))
                                        }

                                    </div>
                                </div>
                            </div>
                        </section>




                        <section className="py-6 text-gray-500">
                            {
                                (selectedPackaging || selectedItem) &&
                                <div className="grid max-w-6xl grid-cols-1 px-6 mx-auto lg:px-8 md:grid-cols-2 md:divide-x">
                                    <div className="flex flex-col max-w-lg p-6 space-y-4 divide-y sm:w-96 sm:p-10  text-gray-600">
                                        <h2 className="text-2xl font-semibold text-gray-600">Selected plans</h2>
                                        <ul className="flex flex-col pt-4 space-y-2">
                                            {
                                                selectedPackaging &&
                                                <li className="flex items-start justify-between">
                                                    <h3>Main Plan
                                                    </h3>
                                                    <div className="text-right flex gap-1 items-center">
                                                        <span className="block">{selectedPackaging}</span>
                                                        <IconButton
                                                            onClick={handleMainPackRemove}
                                                            className="block text-xl text-red-500 cursor-pointer"><IoIosRemove /></IconButton>

                                                    </div>
                                                </li>
                                            }

                                            {
                                                selectedItem.map((item) => (
                                                    <li key={item.title} className="flex items-start justify-between">
                                                        <h3>{item.title}
                                                        </h3>
                                                        <div className="text-right">
                                                            <span className="block">{handleTotalPrice(item)}</span>
                                                        </div>
                                                    </li>
                                                ))
                                            }

                                        </ul>
                                        <></>
                                        <div className="pt-4 space-y-2">
                                            <div className="space-y-6">
                                                <div className="flex justify-between">
                                                    <span>Total</span>
                                                    <span className="font-semibold">{total.toLocaleString()}{" "} Birr</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <AppWorkWithUs selectedValue={selectedPackaging} setSelectedPackaging={handleAfterSubmit} selectedItem={selectedItem} />
                                </div>
                            }
                        </section>
                    </>
                ) :
                    (
                        <div className=' h-[100dvh] flex-col flex justify-center items-center '>
                            <CircularProgress />
                        </div>
                    )
            }
        </div>
    )
}

export default page