/** @format */

"use client";
import { LogoContainer } from "@/containers";
import { CircularProgress, Typography } from "@mui/material";
import React from "react";
import { AppAdminSocial, AppSponsorCardList, AppWorkWithUs } from "@/components";
import { AiFillStar, AiOutlineTeam } from "react-icons/ai";
import {
  BsFingerprint,
  BsPersonWorkspace,
  BsStack,
} from "react-icons/bs";
import { HiRocketLaunch } from "react-icons/hi2";
import { FaArrowsRotate } from "react-icons/fa6";
import { useAppSelector } from "@/utils";
import { SocialLinks } from "@/constants/homepage";

const page = () => {


  const { socialLinks, aboutus, homePageData, heroData } = useAppSelector(state => state.homePage)
  return (
    <>
      {
        aboutus ? <div className="min-h-screen">
          <LogoContainer
            backGroundImage={heroData?.backgroundImageUrl || ""}
            className="  h-[75vh] text-center items-center flex-col gap-20 flex justify-center align-middle"
          >
            <div>
              <Typography
                variant="h2"
                className=" tracking-wide  font-bold text-BlueDark capitalize text-4xl md:text-5xl xl:text-7xl"
              >
                Get to know <br className="  block xl:hidden" />{" "}
                <span className="  text-BlueLight">us</span>
              </Typography>
              <Typography
                variant="h6"
                className="font-light max-w-xs lg:max-w-3xl text-center mx-auto text-sm lg:text-base text-gray-500"
              >
                Kehsud technologies provides modernised solutions to the problems
                faced by various industries in Ethiopia.
              </Typography>
            </div>
            <div className=" flex flex-col gap-3">
              <Typography className=" text-BlueLight font-bold text-lg">
                Find us on
              </Typography>

              <AppAdminSocial />
            </div>
          </LogoContainer>
          <div className=" w-[90%] flex flex-col gap-20   min-h-screen bg-white border-t border-t-gray-300  mx-auto ">
            <div className=" grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-5 -mt-20 w-[90%] mx-auto">
              <div className=" mx-auto px-8 items-center p-6 shadow-lg shadow-gray-200 text-gray-700 bg-white rounded-2xl flex-col flex relative">
                <div className=" h-14 w-14 text-2xl rounded-full bg-blue-500 text-White flex flex-col gap-2 items-center justify-center">
                  <AiFillStar />
                </div>
                <h5 className="block mt-2 antialiased tracking-normal  text-xl font-semibold leading-snug text-blue-gray-900 mb-2">
                  {aboutus.factAboutXpo[0].title}
                </h5>
                <p className="block antialiased  text-base leading-relaxed font-normal text-blue-gray-600 max-w-sm">
                  {aboutus.factAboutXpo[0].description}
                </p>
              </div>
              <div className=" mx-auto px-8 items-center p-6 shadow-lg shadow-gray-200 text-gray-700 bg-white rounded-2xl flex-col flex relative">
                <div className=" h-14 w-14 text-2xl rounded-full bg-red-500 text-White flex flex-col items-center justify-center">
                  <FaArrowsRotate />
                </div>
                <h5 className="block antialiased tracking-normal  text-xl font-semibold leading-snug text-blue-gray-900 mb-2">
                  {aboutus.factAboutXpo[1].title}
                </h5>
                <p className="block antialiased  text-base leading-relaxed font-normal text-blue-gray-600 max-w-sm">
                  {aboutus.factAboutXpo[1].description}
                </p>
              </div>
              <div className=" mx-auto px-8 items-center p-6 shadow-lg shadow-gray-200 text-gray-700 bg-white rounded-2xl flex-col flex relative">
                <div className=" h-14 w-14 text-2xl rounded-full bg-green-600 text-White flex flex-col items-center justify-center">
                  <BsFingerprint />
                </div>
                <h5 className="block antialiased tracking-normal  text-xl font-semibold leading-snug text-blue-gray-900 mb-2">
                  {aboutus.factAboutXpo[2].title}
                </h5>
                <p className="block antialiased  text-base leading-relaxed font-normal text-blue-gray-600 max-w-sm">
                  {aboutus.factAboutXpo[2].description}
                </p>
              </div>
            </div>
            <div className=" mt-[3rem] lg:mt-20 w-[80%]  lg:w-[85%] justify-between mx-auto flex lg:flex-row f flex-col-reverse  lg:items-center gap-5 mb-10">
              <div className=" flex flex-col gap-5">
                <div className=" flex xs:flex-row flex-col gap-5">
                  <div>
                    <AiFillStar className="text-BlueLight text-3xl" />
                    <h4 className=" mt-[16px] mb-[12px] font-[700] text-[1.25rem]  text-BlueLighter">
                      {aboutus.leftSideList[0].title}

                    </h4>
                    <p className="antialiased  text-[1rem] font-[300] leading-[1.6] pr-[48px] text-gray-400 max-w-xs">
                      {aboutus.leftSideList[0].description}

                    </p>
                  </div>
                  <div>
                    <BsStack className="text-BlueLight text-3xl" />
                    <h4 className=" mt-[16px] mb-[12px] font-[700] text-[1.25rem]  text-BlueLighter">
                      {aboutus.leftSideList[1].title}

                    </h4>
                    <p className=" text-[1rem] font-[300] leading-[1.6] pr-[48px] text-gray-400 max-w-xs antialiased ">
                      {aboutus.leftSideList[1].title}

                    </p>
                  </div>
                </div>
                <div className=" flex xs:flex-row flex-col gap-5">
                  <div>
                    <HiRocketLaunch className="text-BlueLight text-3xl" />
                    <h4 className=" mt-[16px] mb-[12px] font-[700] text-[1.25rem]  text-BlueLighter">
                      {aboutus.leftSideList[2].title}
                    </h4>
                    <p className="antialiased  text-[1rem] font-[300] leading-[1.6] pr-[48px] text-gray-400 max-w-xs">
                      {aboutus.leftSideList[2].description}
                    </p>
                  </div>
                  <div>
                    <BsPersonWorkspace className="text-BlueLight text-3xl" />
                    <h4 className=" mt-[16px] mb-[12px] font-[700] text-[1.25rem]  text-BlueLighter">
                      {aboutus.leftSideList[3].title}
                    </h4>
                    <p className="antialiased  text-[1rem] font-[300] leading-[1.6] pr-[48px] text-gray-400 max-w-xs">
                      {aboutus.leftSideList[3].description}
                    </p>
                  </div>
                </div>
              </div>
              <div className=" flex flex-col gap-5 text-left">
                <div className=" text-2xl text-BlueLight h-16 w-16 rounded-full shadow-sm shadow-BlueLighter items-center flex flex-col  justify-center">
                  <AiOutlineTeam />
                </div>
                <Typography className=" text-BlueDark leading-[1.375] font-[700] mb-[0.75rem] text-[1.875rem]">
                  {aboutus.subHeader}
                </Typography>
                <p className=" max-w-xl block antialiased  text-base leading-relaxed mb-8 font-normal text-gray-500">
                  {aboutus.backgroundOfCompany}
                </p>
              </div>
            </div>
            <div>
              <div className="mx-auto w-full px-4 text-center lg:w-6/12">
                <h2 className=" text-BlueDark antialiased tracking-normal  text-4xl font-semibold leading-[1.3] text-blue-gray-900 mb-3">
                  Here are our heroes
                </h2>
                <p className="block antialiased  text-xl font-normal leading-relaxed text-gray-500">

                </p>
              </div>
              <div className=" w-[80%] mx-auto mt-24 grid grid-cols-2 gap-12 gap-x-24 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-5">
                {aboutus.teams.map((team, index) => (
                  <div
                    className="relative flex flex-col bg-clip-border rounded-xl bg-transparent text-gray-700 shadow-none text-center"
                    key={index}
                  >
                    <img
                      src={team.image}
                      alt="Ryan Tompson"
                      className="inline-block relative object-cover object-center rounded-full h-full w-full shadow-lg shadow-gray-500/25  border-BlueLighter border-2"
                    />
                    <h5 className="block antialiased tracking-normal text-sm  md:text-xl font-semibold leading-snug text-blue-gray-900 mt-6 mb-1">
                      {team.name}
                    </h5>
                    <p className="block antialiased text-xs  md:text-base leading-relaxed font-normal text-blue-gray-500">
                      {team.position}
                    </p>
                    {/* <div className="mx-auto mt-5">
                      <div className="flex items-center gap-2 ">
                        <IconButton>
                          <Link
                            href={team.facebookLink ?? ""}
                            target="_blank"
                            rel="noopener noreferrer"
                          >
                            <BiLogoFacebookCircle className="  hover:text-blue-400 text-gray-400 hover:scale-105 text-xs" />
                          </Link>
                        </IconButton>
                        <IconButton>
                          <Link
                            href={team.linkedinLink ?? ""}
                            target="_blank"
                            rel="noopener noreferrer"
                          >
                            <BsLinkedin className="  hover:text-blue-400 text-gray-400 hover:scale-105 text-xs" />
                          </Link>
                        </IconButton>
                        <IconButton>
                          <Link
                            href={team.telegramLink ?? ""}
                            target="_blank"
                            rel="noopener noreferrer"
                          >
                            <BsTelegram className="  hover:text-blue-400 text-gray-400 hover:scale-105 text-xs" />
                          </Link>
                        </IconButton>
                      </div>
                    </div> */}
                  </div>
                ))}
              </div>
            </div>
            <div className=" grid grid-cols-1 md:grid-cols-4  gap-10 w-[80%] lg:w-[65%] mx-auto my-10">
              {homePageData?.facts.map((data) => (
                <div className=" text-center flex flex-col gap-2" key={data.title}>
                  <h1 className="antialiased text-[3rem] leading-[1.25] font-[700] text-BlueLight">
                    <span>{`${data.number}`}</span>+
                  </h1>
                  <h5 className=" uppercase text-[1.25rem] leading-[1.375] font-[700] text-gray-400">
                    {data.title}
                  </h5>
                </div>
              ))}
            </div>
            {homePageData && <AppSponsorCardList data={homePageData} />}

          </div>
        </div>
          :
          <div className=" h-screen flex flex-col justify-center items-center">
            <CircularProgress />
          </div>
      }
    </>
  );
};

export default page;
