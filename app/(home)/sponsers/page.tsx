/** @format */

"use client";

import { LogoContainer } from "@/containers";
import { CircularProgress, Typography } from "@mui/material";
import React, { useState } from "react";
import { AppAdminSocial, AppCurrentPlan, AppPlanLister, AppWorkWithUs } from "@/components";
import { BiSolidChevronDown, BiSolidChevronUp } from "react-icons/bi";
import {
  AiFillStar,
  AiOutlineMinusCircle,
  AiOutlinePlusCircle,
  AiOutlineTeam,
} from "react-icons/ai";
import IconButton from "@mui/joy/IconButton";
import { BsPersonWorkspace, BsStack } from "react-icons/bs";
import { HiRocketLaunch } from "react-icons/hi2";
import { MdOutlineHandshake } from "react-icons/md";
import Button from "@mui/joy/Button";
import { LiaQuestionSolid } from "react-icons/lia";
import { useRouter } from "next/navigation";
import { useAppSelector } from "@/utils";

import { FaAngleDoubleDown } from "react-icons/fa";
import { PlanProps } from "@/types/sanity";


const page = () => {
  const [open, setOpen] = useState<boolean>(true);
  const [selectedPackaging, setSelectedPackaging] = useState<string>("")
  const [currentOpen, setCurrentOpen] = useState<number | null>();
  const router = useRouter();
  const { homePageData, sponsors, heroData } = useAppSelector(state => state.homePage)
  const [currentPackage, setCurrentPackage] = useState<PlanProps[]>([])




  const handleSubmit = (plan: string) => {
    setSelectedPackaging(plan)
    if (sponsors) {
      const res = sponsors?.packages.filter((pack: any) => (
        pack.plan == plan
      ))
      setCurrentPackage(res)
    }
    router.push("#workWithUS")

  }

  const handleAfterSubmit = () => {
    setSelectedPackaging("")

  }
  return (
    <>
      {
        sponsors ?
          <div className="  min-h-screen overflow-y-auto scroll-smooth">
            <LogoContainer
              backGroundImage={heroData?.backgroundImageUrl || ""}
              className="  h-[500px] lg:h-[700px] text-center items-center flex-col gap-20 flex justify-center align-middle"
            >
              <div>
                <Typography
                  variant="h2"
                  className=" tracking-wide  font-bold text-BlueDark capitalize text-3xl md:text-4xl xl:text-6xl"
                >
                  Unlock Extraordinary Potential!!
                  <br className="  block " />{" "}

                </Typography>
                <Typography
                  variant="h6"
                  className="font-light max-w-xs lg:max-w-3xl text-center mx-auto text-sm lg:text-base text-gray-500"
                >
                  For further questions, including partnership opportunities, please
                  email{" "}
                  <span className="  text-BlueLight hover:underline  cursor-pointer">
                    {homePageData?.email}{" "}
                  </span>{" "}
                  or contact using our contact form.
                </Typography>

              </div>
              <div className=" flex flex-col gap-2">
                <Typography className=" text-BlueLight font-bold text-lg">
                  Find us on
                </Typography>

                <AppAdminSocial />
              </div>
            </LogoContainer>
            <div className=" md:w-[85%] pb-20 mx-auto bg-white min-h-screen -mt-20 border-t rounded-[3rem]">
              <div className="mx-auto w-full px-4 text-center lg:w-[85%] flex  items-center gap-5 mt-5">
                <div className=" h-[1px] w-1/2 bg-gray-400"></div>
                <div>
                  <p className="  text-BlueLight text-2xl  flex flex-col  items-center justify-center">
                    <AiFillStar />{" "}
                  </p>

                  <h2 className=" text-BlueDark antialiased tracking-normal text-xl  lg:text-4xl font-semibold leading-[1.3] text-blue-gray-900 mb-3">
                    <span className=" text-BlueLighter">
                      {sponsors.header}
                    </span>
                  </h2>
                  <p className="block antialiased  text-base font-normal leading-relaxed text-gray-500">
                    {sponsors.subHeader}
                    {" "}
                  </p>
                </div>
                <div className=" h-[1px] w-1/2 bg-gray-400"></div>
              </div>
              <div className=" mt-[3rem] lg:mt-20 w-[80%]  lg:w-[85%] justify-between mx-auto flex lg:flex-row f flex-col-reverse  lg:items-center gap-5 mb-10">
                <div className=" flex flex-col gap-5">
                  <div className=" flex xs:flex-row flex-col gap-5">
                    <div>
                      <AiFillStar className="text-BlueLight text-3xl" />
                      <h4 className=" mt-[16px] mb-[12px] font-[700] text-[1.25rem]  text-BlueLighter">
                        {sponsors.leftSideList[2].title}
                      </h4>
                      <p className="antialiased  text-[1rem] font-[300] leading-[1.6] pr-[48px] text-gray-400 max-w-xs">
                        {sponsors.leftSideList[2].description}
                      </p>
                    </div>
                    <div>
                      <BsStack className="text-BlueLight text-3xl" />
                      <h4 className=" mt-[16px] mb-[12px] font-[700] text-[1.25rem]  text-BlueLighter">
                        {sponsors.leftSideList[3].title}
                      </h4>
                      <p className=" text-[1rem] font-[300] leading-[1.6] pr-[48px] text-gray-400 max-w-xs antialiased ">
                        {sponsors.leftSideList[3].description}
                      </p>
                    </div>
                  </div>
                  <div className=" flex xs:flex-row flex-col gap-5">
                    <div>
                      <HiRocketLaunch className="text-BlueLight text-3xl" />
                      <h4 className=" mt-[16px] mb-[12px] font-[700] text-[1.25rem]  text-BlueLighter">
                        {sponsors.leftSideList[0].title}

                      </h4>
                      <p className="antialiased  text-[1rem] font-[300] leading-[1.6] pr-[48px] text-gray-400 max-w-xs">
                        {sponsors.leftSideList[0].description}
                      </p>
                    </div>
                    <div>
                      <BsPersonWorkspace className="text-BlueLight text-3xl" />
                      <h4 className=" mt-[16px] mb-[12px] font-[700] text-[1.25rem]  text-BlueLighter">
                        {sponsors.leftSideList[1].title}

                      </h4>
                      <p className="antialiased  text-[1rem] font-[300] leading-[1.6] pr-[48px] text-gray-400 max-w-xs">
                        {sponsors.leftSideList[1].description}

                      </p>
                    </div>
                  </div>
                </div>

                <div className=" flex flex-col gap-5 text-left">
                  <div className=" text-2xl text-BlueLight h-16 w-16 rounded-full shadow-sm shadow-BlueLighter items-center flex flex-col  justify-center">
                    <AiOutlineTeam />
                  </div>
                  <Typography className=" text-BlueDark leading-[1.375] font-[700] mb-[0.75rem] text-[1.875rem]">
                    {sponsors.rightSideHeader}
                  </Typography>
                  {/*
             */}
                  <p className=" max-w-md block antialiased  text-base leading-relaxed mb-8 font-normal text-gray-500">
                    {sponsors.rightSideDescription}
                  </p>
                </div>
              </div>

              <div className="mx-auto w-full px-4 text-center lg:w-[85%] flex  items-center gap-5 mt-5">
                <div className=" h-[1px] w-1/2 bg-gray-400"></div>
                <div>
                  <p className="   text-BlueLight text-2xl  flex flex-col  items-center justify-center">
                    <MdOutlineHandshake />{" "}
                  </p>

                  <h2 className=" text-BlueDark antialiased tracking-normal text-xl  lg:text-4xl font-semibold leading-[1.3] text-blue-gray-900 mb-3">
                    <span className=" text-BlueLighter">Unlock the Process</span>
                  </h2>
                  <p className="block antialiased  text-base font-normal leading-relaxed text-gray-500">
                    Follow These Simple Steps to Become Our Valued Partner!{" "}
                  </p>
                </div>
                <div className=" h-[1px] w-1/2 bg-gray-400"></div>
              </div>
              <div className=" relative w-[75%] mx-auto pt-10">
                <div className=" xs:absolute left-0 lg:left-3 flex gap-5 mt-5">
                  <div className=" flex flex-col items-center text-2xl lg:text-4xl xl:text-6xl text-BlueLighter antialiased font-bold">
                    01
                    <div className=" h-12 w-[2px]  bg-BlueLighter" />
                  </div>
                  <div className=" flex flex-col gap-2 antialiased">
                    <h1 className=" font-bold text-BlueDark text-xl lg:text-2xl xl:text-3xl  ">
                      {sponsors.steps[0].title}

                    </h1>
                    <p className=" line-clamp-3 max-w-xs text-gray-400 text-sm">
                      {sponsors.steps[0].description}

                    </p>
                  </div>
                </div>
                <div className="xs:absolute right-0 top-48  lg:right-[2rem] xl:top-32 flex  gap-5 mt-5 ">
                  <div className=" flex flex-col items-center text-2xl lg:text-4xl xl:text-6xl text-BlueLighter antialiased font-bold">
                    02
                    <div className=" h-12 w-[2px]  bg-BlueLighter" />
                  </div>
                  <div className=" flex flex-col gap-2 antialiased">
                    <h1 className=" font-bold text-BlueDark text-xl lg:text-2xl xl:text-3xl  ">
                      {sponsors.steps[1].title}

                    </h1>
                    <p className=" line-clamp-3 max-w-xs text-gray-400 text-sm">
                      {sponsors.steps[1].title}

                    </p>
                  </div>
                </div>
                <div className="xs:absolute left-5 lg:left-24 top-80 xl:top-64 flex  gap-5 mt-5 ">
                  <div className=" flex flex-col items-center text-2xl lg:text-4xl xl:text-6xl text-BlueLighter antialiased font-bold">
                    03
                    <div className=" h-12 w-[2px]  bg-BlueLighter" />
                  </div>
                  <div className=" flex flex-col gap-2 antialiased">
                    <h1 className=" font-bold text-BlueDark text-xl lg:text-2xl xl:text-3xl  ">
                      {sponsors.steps[2].title}
                    </h1>
                    <p className=" line-clamp-3 max-w-xs text-gray-400 text-sm">
                      {sponsors.steps[2].title}
                    </p>
                  </div>
                </div>
                <div className="xs:absolute -right-5 top-[30rem] lg:right-0 xl:top-96 flex  gap-5 mt-5 ">
                  <div className=" flex flex-col items-center text-2xl lg:text-4xl xl:text-6xl text-BlueLighter antialiased font-bold">
                    04
                    <div className=" h-12 w-[2px]  bg-BlueLighter" />
                  </div>
                  <div className=" flex flex-col gap-2 antialiased">
                    <h1 className=" font-bold text-BlueDark text-xl lg:text-2xl xl:text-3xl  ">
                      {sponsors.steps[3].title}
                    </h1>
                    <p className=" line-clamp-3 max-w-xs text-gray-400 text-sm">
                      {sponsors.steps[3].title}
                    </p>
                  </div>
                </div>
                <div className="xs:absolute -bottom-[650px] mt-10 xl:-bottom-[610px] left-[45%]">
                  <Button
                    size="lg"
                    className=" bg-BlueLighter hover:bg-opacity-80 text-White"
                    endDecorator={
                      <MdOutlineHandshake className=" text-2xl text-White " />
                    }
                    onClick={() => router.push("#workWithUS")}
                  >
                    Join us
                  </Button>
                </div>
              </div>
              <div className=" xs:mt-[44rem] items-center">
                {
                  sponsors &&
                  <AppPlanLister sponsors={sponsors} setSelectedPackaging={handleSubmit} />
                }
                <div className=" flex justify-center">

                  <Button
                    onClick={() => router.push("/custome-plan")}
                    startDecorator={<FaAngleDoubleDown />}

                    className=" bg-BlueLighter text-White hover:bg-BlueLighter/90">See all plans</Button>
                </div>
                {/* {
                  currentPackage.length > 0 &&
                  <AppCurrentPlan name={currentPackage[0].plan} price={currentPackage[0].price} packages={currentPackage[0].packages} />
                } */}

                {
                  selectedPackaging &&
                  <AppWorkWithUs selectedValue={selectedPackaging} setSelectedPackaging={handleAfterSubmit} />
                }

                <div id="faq">
                  <div className=" mb-10  mx-auto  w-full px-4 text-center lg:w-[85%] flex  items-center gap-5 mt-16">
                    <div className=" h-[1px] w-1/2 bg-gray-400"></div>
                    <div>
                      <p className="   text-BlueLight text-2xl flex flex-col items-center justify-center">
                        <LiaQuestionSolid />
                      </p>

                      <h2 className=" text-BlueDark antialiased tracking-normal text-xl  lg:text-4xl font-semibold leading-[1.3] text-blue-gray-900 mb-3">
                        <span className=" text-BlueLighter">
                          Frequently Asked Questions
                        </span>
                      </h2>
                      <p className="block antialiased  text-[12px]  md:text-base font-normal leading-relaxed text-gray-500">
                        Use this section to find more answers to questions related to
                        our partnership.{" "}
                      </p>
                    </div>
                    <div className=" h-[1px] w-1/2 bg-gray-400"></div>
                  </div>
                  <div className=" flex flex-col gap-3">
                    {sponsors.faq.map((faq, index) => (
                      <div
                        className={`w-[90%] cursor-pointer lg:w-[60%] mx-auto  border rounded-lg overflow-hidden ${open && index == currentOpen
                          ? " border-BlueLight border-2"
                          : "border-gray-200"
                          }`}
                        key={faq.title}
                        onClick={() => {
                          setOpen(!open);
                          setCurrentOpen(index);
                        }}
                      >
                        <div className=" flex items-center justify-between gap-2 bg-BlueLighter bg-opacity-20 py-3 px-5 rounded-t-lg">
                          <IconButton
                            onClick={() => {
                              setOpen(!open);
                              setCurrentOpen(index);
                            }}
                            className="  hover:bg-transparent "
                          >
                            {open && index == currentOpen ? (
                              <AiOutlineMinusCircle className=" text-2xl    text-BlueLighter cursor-pointer hover:text-BlueLight" />
                            ) : (
                              <AiOutlinePlusCircle className=" text-2xl   text-BlueLighter cursor-pointer hover:text-BlueLight" />
                            )}
                          </IconButton>
                          <p className=" text-sm lg:text-base text-BlueDark font-bold ">
                            {faq.title}
                          </p>
                          <IconButton
                            onClick={() => {
                              setOpen(!open);
                              setCurrentOpen(index);
                            }}
                            className="  hover:bg-transparent "
                          >
                            {open && index == currentOpen ? (
                              <BiSolidChevronUp className=" text-2xl   text-BlueLighter cursor-pointer  hover:text-BlueLight" />
                            ) : (
                              <BiSolidChevronDown className=" text-2xl   text-BlueLighter cursor-pointer  hover:text-BlueLight" />
                            )}
                          </IconButton>
                        </div>
                        <div
                          className={`bg-gray-100 transition-all ease-in-out duration-500 ${open && currentOpen == index ? "h-[200px]" : "h-0"
                            }`}
                        >
                          <p className=" max-w-2xl pt-5 text-gray-500 mx-auto items-center justify-center">
                            {faq.description}
                          </p>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>

              </div>
            </div>
          </div>
          :
          <div className=" h-screen flex flex-col justify-center items-center">
            <CircularProgress />
          </div>
      }
    </>
  );
};

export default page;
