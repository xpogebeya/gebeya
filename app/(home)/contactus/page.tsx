/** @format */

"use client";
import { LogoContainer } from "@/containers";
import { Typography } from "@mui/material";
import React, { useState } from "react";
import { AppAdminSocial, AppCustomForm, AppForm } from "@/components";
import { AiFillPhone } from "react-icons/ai";
import { FaLocationPin } from "react-icons/fa6";
import { BiLogoGmail } from "react-icons/bi";
import * as Yup from "yup";
import { FormikValues } from "formik";
import { useAPI } from "@/hooks";
import { useAppSelector } from "@/utils";
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';

const validationSchema = Yup.object().shape({
  fullName: Yup.string().required().label("Full name"),
  email: Yup.string().required().email().label("Email"),
  message: Yup.string().required().label("Message"),
  phoneNumber: Yup.string().required().label("phone number")
});

const initialValues: FormikValues = {
  fullName: "",
  email: "",
  message: "",
  phoneNumber: ""
};

const Fields = [
  {
    fieldName: "fullName",
    lable: "Full Name",
  },

  {
    fieldName: "email",
    lable: " Email Address",
    type: "email",
  },
  {
    fieldName: "phoneNumber",
    lable: "Phone Number",
  },
];

const TextArea = {
  fieldName: "message",
  lable: "Message",
};

const page = () => {
  const { submitForm } = useAPI();
  const { officeAddress, homePageData, heroData } = useAppSelector(state => state.homePage)
  const [uploading, setUploading] = useState<boolean>(false);

  const URL: string = "/api/home/set-sponsor";
  const handleSubmit = async (values: FormikValues) => {
    setUploading(true)
    submitForm({ ...values, plan: "contact" }, URL).then(() => {
      setUploading(false)
    })

  }
  return (
    <div className="min-h-screen ">
      <LogoContainer
        backGroundImage={heroData?.backgroundImageUrl || ""}
        className="  h-[75vh] text-center items-center flex-col gap-20 flex justify-center align-middle"
      >
        <div>
          <Typography
            variant="h2"
            className=" tracking-wide  font-bold text-BlueDark capitalize text-4xl md:text-5xl xl:text-7xl"
          >
            Contact <br className="  block xl:hidden" />{" "}
            <span className="  text-BlueLight">us</span>
          </Typography>
          <Typography
            variant="h6"
            className="font-light max-w-xs lg:max-w-3xl text-center mx-auto text-sm lg:text-base text-gray-500"
          >
            For further questions, including partnership opportunities, please
            email{" "}
            <span className="  text-BlueLight hover:underline  cursor-pointer">
              {homePageData?.email}{" "}
            </span>{" "}
            or contact using our contact form.
          </Typography>
        </div>
        <div className=" flex flex-col gap-3">
          <Typography className=" text-BlueLight font-bold text-lg">
            Find us on
          </Typography>

          <AppAdminSocial />
        </div>
      </LogoContainer>
      <div className=" w-[90%] flex flex-col gap-20   min-h-screen bg-white border-t border-t-gray-300  mx-auto ">
        <div className=" my-20">
          <div className="mx-auto w-full px-4 text-center lg:w-6/12">
            <p className="  text-BlueDark">Contact Us</p>
            <h2 className=" text-BlueDark antialiased tracking-normal text-4xl font-semibold leading-[1.3] text-blue-gray-900 mb-3">
              Got a Question?
            </h2>
            <p className="block antialiased text-xl font-normal leading-relaxed text-gray-500">
              We'd like to talk more about what you need
            </p>
          </div>
          <div className=" w-[80%] mx-auto my-20 mb-40 grid grid-cols-1 xs:grid-cols-2 lg:grid-cols-3  gap-10">
            <div className=" flex flex-col items-center gap-3">
              <div className=" h-12 w-12 rounded-full bg-BlueLight flex flex-col justify-center items-center text-White text-2xl">
                <FaLocationPin />
              </div>
              <h5 className=" text-2xl  font-bold text-BlueDark">Address</h5>
              <p className=" text-gray-400 text-base">Find us at the office</p>
              <div className=" flex flex-col gap-1">

                {
                  officeAddress.map((data) => (
                    <p className=" text-BlueDark font-bold" key={data.location}>
                      {data.location}
                    </p>
                  ))
                }
              </div>
            </div>
            <div className=" flex flex-col items-center gap-3">
              <div className=" h-12 w-12 rounded-full bg-BlueLight flex flex-col justify-center items-center text-White text-2xl">
                <BiLogoGmail />
              </div>
              <h5 className=" text-2xl  font-bold text-BlueDark">Email</h5>
              <p className=" text-gray-400 text-base">Send us your feedback</p>
              <p className=" text-BlueDark font-bold">{homePageData?.email}</p>
            </div>
            <div className=" flex flex-col items-center gap-3">
              <div className=" h-12 w-12 rounded-full bg-BlueLight flex flex-col justify-center items-center text-White text-2xl">
                <AiFillPhone />
              </div>
              <h5 className=" text-2xl  font-bold text-BlueDark">Phone</h5>
              <p className=" text-gray-400 text-base">Give us a ring</p>
              <div className=" flex flex-col gap-1">

                {
                  officeAddress.map((data) => (
                    data.phone.map(phone => (
                      <p className=" text-BlueDark font-bold" key={phone}>
                        {phone}
                      </p>
                    ))
                  ))
                }
              </div>

            </div>
          </div>
          {
            uploading && <Box sx={{ width: '70%' }} className=" mb-5 mx-auto">
              <LinearProgress color="info" />
            </Box>
          }
          <AppForm
            initialValues={initialValues}
            onSubmit={(values: FormikValues) => handleSubmit(values)}
            validationSchema={validationSchema}
          >
            <AppCustomForm
              btnLable={"Send Message"}
              Fields={Fields}
              textArea={TextArea}
            />
          </AppForm>
        </div>
      </div>
    </div>
  );
};

export default page;
