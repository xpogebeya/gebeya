'use client'
import { Field } from '@/types/register';
import { FormikValues } from 'formik';
import React, { useState } from 'react'
import * as Yup from "yup";
import { Box, CircularProgress, LinearProgress } from "@mui/material";
import { AppExhibitorProfileEditor, AppTicketerVisitorRegister } from '@/components';
import { useExhibitorPortalContext } from '@/context/ExhibitorPortalContext';

type Props = {}
const SexMenu: string[] = ["Male", "Female"];

const validationSchema = Yup.object().shape({
  firstName: Yup.string().required().label("First name"),
  lastName: Yup.string().required().label("Last name"),
  middleName: Yup.string().label("Middle name"),
  email: Yup.string().required().email().label("Email"),
  phone: Yup.string().min(10).max(13).required().label("Phone"),
  sex: Yup.string().required().label("Sex"),
  companyName: Yup.string().required().label("Company Name"),
  jobTitle: Yup.string().required().label("Job Title"),
  telephone: Yup.string(),
});

const initialValues: FormikValues = {
  firstName: "",
  lastName: "",
  middleName: "",
  email: "",
  phone: "",
  sex: "",
  companyName: "",
  jobTitle: "",
  telephone: "",
};

const fields: Field[][] = [
  [
    {
      name: "firstName",
      label: "First Name",
      required: true,
    },
    {
      name: "middleName",
      label: "Middle Name",
      required: false,
    },
    {
      name: "lastName",
      label: "Last Name",
      required: true,
    },
    {
      name: "sex",
      label: "Sex",
      required: true,
      options: SexMenu,
    },

  ],
  [
    {
      name: "email",
      label: "Email",
      required: true,
    },
    {
      name: "phone",
      label: "Phone",
      required: true,
    },
    {
      name: "telephone",
      label: "TelePhone Number",
      required: false,
    },
  ],
  [
    {
      name: "companyName",
      label: "Company Name",
      required: true,
    },
    {
      name: "jobTitle",
      label: "Job Title",
      required: true,
    },
  ],
];
const page = (props: Props) => {
  const [uploading, setUploading] = useState<boolean>(false);
  const { error } = useExhibitorPortalContext();

  const URL = process.env.NEXT_PUBLIC_VISITER_REGISTER_URL  as string;
  const steps = [
    "Personal Information",
    "Contact Information",
    "Career Information",
  ];

  return (
    <div className=" flex flex-col gap-10  -mt-8 px-2 xs:px-10">
      {
        uploading && <Box sx={{ width: '100%' }} className=" mb-5">
          <LinearProgress color="info" />
        </Box>
      }
      <p className=" text-center text-red-500">{error}</p>

      <div className=" min-h-[90vh] overflow-y-auto">

        <AppTicketerVisitorRegister
          initialValues={initialValues}
          validationSchema={validationSchema}
          fields={fields}
          steps={steps}
          setUploading={setUploading}
          URL={URL}
        />
      </div>
    </div>
  )
}

export default page