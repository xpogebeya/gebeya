import { AppTicketerLayout, AppAdminProtectedRoute } from "@/containers";
import React, { ReactNode } from "react";

type Props = {
    children: ReactNode;
};

const layout = ({ children }: Props) => {
    return <AppTicketerLayout>
        <AppAdminProtectedRoute>
            <div>
                {children}
            </div>
        </AppAdminProtectedRoute>
    </AppTicketerLayout>;
};

export default layout;
