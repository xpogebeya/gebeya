"use client";
import { LogoContainer } from "@/containers";
import { Typography } from "@mui/material";
import React, { useState } from "react";
import { AppAdminSocial, AppCustomForm, AppForm } from "@/components";
import * as Yup from "yup";
import { FormikValues } from "formik";
import { useAPI } from "@/hooks";
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';
import { useAppSelector } from "@/utils";

const phoneRegExp =
  /^((\+[1-9]{1,4}[ -]?)|(\([0-9]{2,3}\)[ -]?)|([0-9]{2,4})[ -]?)*?(\+[0-9]{1,4}[ -]?)?[0-9]{3,4}[ -]?[0-9]{3,4}$/;

const validationSchema = Yup.object().shape({
  fullName: Yup.string().required().label("Full name"),
  email: Yup.string().required().email().label("Email"),
  mobile: Yup.string()
    .required()
    .matches(phoneRegExp, "Phone number is not valid"),
  telePhone: Yup.string().matches(phoneRegExp, "Telephone number is not valid"),
});

const initialValues: FormikValues = {
  fullName: "",
  email: "",
  mobile: "",
  telePhone: "",
};

const Fields = [
  {
    fieldName: "fullName",
    lable: "Full Name*",
  },
  {
    fieldName: "email",
    lable: " Email Address*",
    type: "email",
  },
  {
    fieldName: "mobile",
    lable: "Phone Number with county code*",
  },
  {
    fieldName: "telePhone",
    lable: "Telephone Number with county code",
  },
];

const page = () => {
  const { submitForm } = useAPI();
  const URL: string = "/api/admin/inform-me";
  const [uploading, setUploading] = useState<boolean>(false);

  const { heroData } = useAppSelector(state => state.homePage)
  const handleSubmit = (values: FormikValues) => {
    setUploading(true)
    submitForm(values, URL).then(() => {
      setUploading(false)
    })

  }
  return (
    <div className="min-h-screen ">
      <LogoContainer
        backGroundImage={heroData?.backgroundImageUrl || ""}
        className="  h-[75vh] text-center items-center flex-col gap-20 flex justify-center align-middle"
      >
        <div>
          <Typography
            variant="h2"
            className=" tracking-wide font-bold text-BlueDark  text-4xl md:text-5xl xl:text-7xl"
          >
            Don't let me miss out!
            <br className="  block xl:hidden" />{" "}
          </Typography>
          <Typography
            variant="h6"
            className="font-light max-w-xs lg:max-w-3xl text-center mx-auto text-sm lg:text-base text-gray-500 mt-2"
          >
            For further questions, including partnership opportunities, please
            email{" "}
            <span className="  text-BlueLight hover:underline  cursor-pointer">
              hello@gebeyaExpo.com{" "}
            </span>{" "}
            or contact using our contact form.
          </Typography>
        </div>
        <div className=" flex flex-col gap-3">
          <Typography className=" text-BlueLight font-bold text-lg">
            Find us on
          </Typography>

          <AppAdminSocial />
        </div>
      </LogoContainer>

      <div className=" w-[90%] flex flex-col gap-20   min-h-screen bg-white border-t border-t-gray-300  mx-auto  rounded-lg ">
        <div className=" my-20">
          <div className="mx-auto w-full px-4 text-center lg:w-6/12">
            <p className="  text-BlueDark">Hi</p>
            <h2 className=" text-BlueDark antialiased tracking-normal font-sans text-2xl font-semibold leading-[1.3] mb-3">
              Please complete the form below to inform you when registration is
              live
            </h2>
            <p className="block antialiased font-sans text-xl font-normal leading-relaxed text-gray-500">
              We'd like to see you at our expo
            </p>
          </div>

          {
            uploading && <Box sx={{ width: '70%' }} className=" my-5 mx-auto">
              <LinearProgress color="info" />
            </Box>
          }

          <AppForm
            initialValues={initialValues}
            onSubmit={(values: FormikValues) => handleSubmit(values)}
            validationSchema={validationSchema}
          >
            <AppCustomForm btnLable={"Submit"} Fields={Fields} />
          </AppForm>
        </div>
      </div>
    </div>
  );
};

export default page;
