"use client";

import { LogoContainer } from "@/containers";
import { Divider, Typography } from "@mui/material";
import React from "react";
import { AppAdminSocial, AppMap } from "@/components";
import { useAppSelector } from "@/utils";

const page = () => {
  const { heroData } = useAppSelector(state => state.homePage)
  return (
    <div className=" min-h-screen ">
      <LogoContainer
        backGroundImage={heroData?.backgroundImageUrl || ""}
        className="  h-[63vh] md:h-[75vh] text-center items-center flex-col gap-20 flex justify-center align-middle"
      >
        <div>
          <Typography
            variant="h2"
            className=" tracking-wide  font-bold text-BlueDark capitalize text-4xl md:text-5xl xl:text-7xl"
          >
            Find us <br className="  block xl:hidden" />{" "}
            <span className="  text-BlueLight">Here</span>
          </Typography>
          <Typography
            variant="h6"
            className="font-light max-w-xs lg:max-w-3xl text-center mx-auto text-sm lg:text-base text-gray-500"
          >
            We're constantly trying to express ourselves and actualize our
            dreams. If you have the opportunity to play this game
          </Typography>
        </div>
        <div className=" flex flex-col gap-3 mb-4">
          <Typography className=" text-BlueLight font-bold text-lg">
            Find us on
          </Typography>

          <AppAdminSocial />
        </div>
      </LogoContainer>
      <div>
        <Divider />
        <AppMap />
      </div>
    </div>
  );
};

export default page;
