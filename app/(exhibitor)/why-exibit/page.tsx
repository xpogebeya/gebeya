/** @format */
"use client";

import React, { useEffect, useState } from "react";
import {
  WhyCard,
  ShowCaseCards,
  ExhibitListCard,
  NetworkStatistics,
  AppSponsorCardList,
} from "@/components";
import { useAppSelector } from "@/utils";
import { CircularProgress } from "sanity";

const page = () => {
  const isSmallScreen =
    typeof window !== "undefined" && window.innerWidth < 540;

  const { homePageData, whyExhibit, whyVisit } = useAppSelector(
    (state) => state.homePage
  );
  return whyExhibit ? (
    <div className={"flex flex-col"}>
      <div className={`bg-xpo-image bg-cover bg-center bg-no-repeat`}>
        <p className=" mb-4 m-28 ml-8 md:mt-40 lg:m-48  x:ml-12 md:ml-32 lg:ml-28 lg:mb-36 xl:ml-40 text-3xl lg:text-7xl font-bold text-BlueDark ">
          Why <span className="text-BlueLight">exhibit?</span>
        </p>
      </div>

      <div
        className={
          "flex flex-col gap-8 lg:gap-0 xl:flex-row pt-12 pb-12 xl:flex-wrap lg:ml-20 xl:ml-40  "
        }
      >
        <p
          className={
            "text-BlueDark m-6 mt-0 pt-4 mb-0 md:mb-3 lg:mb-12 x:ml-20 md:ml-32 lg:ml-2 text-2xl md:text-3xl lg:text-4xl"
          }
        >
          {whyExhibit.header}
        </p>
        {whyExhibit.reasonToVist.map((reason) => (
          <div className=" lg:basis-1/2 flex flex-col mx-6 x:ml-0 md:mr-0 md:ml-16 lg:ml-0 x:flex-row ">
            <img
              src={reason.imageUrl}
              className={
                isSmallScreen
                  ? "w-full x:w-1/3 lg:w-1/4 lg:h-260 lg:m-2 p-3 pb-0 lg:p-0"
                  : " ml-16 lg:m-2   p-3 pb-0"
              }
              width="200"
              height="100"
              alt="image"
            />
            <WhyCard
              key={reason.title}
              title={reason.title}
              desc={reason.desc}
            />
          </div>
        ))}
      </div>

      <div className=" mt-20 ">
        <p className=" text-3xl font-bold x:ml-7 md:ml-20 xl:ml-36 px-10 text-BlueDark">
          {whyExhibit.secondHeader}
        </p>
        <div className="flex flex-row flex-wrap gap-6 pt-4 mx-8 md:ml-28 md:pt-8 x:ml-16 lg:ml-20 xl:ml-44 pb-12">
          {whyExhibit.showCases.map(({ title, imageUrl }) => (
            <ShowCaseCards key={title} title={title} imageUrl={imageUrl} />
          ))}
        </div>
      </div>
      <div className="flex  flex-col-reverse mx-6 lg:pt-24 xl:flex-row md:ml-28 md:pt-6 x:ml-12 lg:ml-20 xl:ml-32">
        {whyExhibit.buyers.lists.length
          ? whyExhibit.buyers.lists.length > 0 && (
              <ExhibitListCard
                title={whyExhibit.professionals.title}
                desc={whyExhibit.professionals.desc}
                list={whyExhibit.professionals.lists}
              />
            )
          : null}
        <img
          className="xl:mx-28 lg:w-full xl:w-auto "
          src={whyExhibit.professionals.image}
          width="600"
          height="300"
          alt="professionals.jpg"
        />
      </div>
      {whyVisit?.piechart && (
        <NetworkStatistics UserData={whyVisit?.piechart} />
      )}
      <div className="flex flex-col-reverse mx-6 lg:pt-24 xl:flex-row md:ml-28 md:pt-6 x:ml-12 lg:ml-20 xl:ml-32 mb-32">
        {whyExhibit.buyers.lists.length
          ? whyExhibit.buyers.lists.length > 0 && (
              <ExhibitListCard
                title={whyExhibit.buyers.title}
                desc={whyExhibit.buyers.desc}
                list={whyExhibit.buyers.lists}
              />
            )
          : null}
        <img
          className="xl:mx-28 lg:w-full xl:w-auto "
          src={whyExhibit.buyers.image}
          width="600"
          height="300"
          alt="professionals.jpg"
        />
      </div>

      <div className=" mb-10 pt-5 ">
        {homePageData && <AppSponsorCardList data={homePageData} />}
      </div>
    </div>
  ) : (
    <div className=" h-screen flex flex-col justify-center items-center">
      <CircularProgress value={0} />
    </div>
  );
};

export default page;
