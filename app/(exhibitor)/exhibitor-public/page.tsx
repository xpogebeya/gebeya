"use client";
import React, { useEffect, useState } from "react";
import { Avatar, Pagination, Skeleton, Stack } from "@mui/material";
import Input from "@mui/joy/Input";
import SearchRoundedIcon from "@mui/icons-material/SearchRounded";
import { BiSolidCategoryAlt } from "react-icons/bi";
import { useRouter } from "next/navigation";
import { ExhibitorsDataType } from "@/types/exhibitor";
import IconButton from "@mui/joy/IconButton";
import { AiOutlineUnorderedList } from "react-icons/ai";
import { AppSocial } from "@/components";
import {
  setExhibitors,
  textCliper,
  updateLoading,
  useAppSelector,
} from "@/utils";
import Button from "@mui/joy/Button";
import { useDispatch } from "react-redux";
import axios from "axios";

type Props = {};

const page: React.FC<Props> = ({}) => {
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [searchQuery, setSearchQuery] = useState<string>("");
  const [category, setCategory] = useState<boolean>(true);
  const { exhibitors, isLoading } = useAppSelector(
    (state) => state.exhibtorReducers
  );
  const router = useRouter();
  const dispatch = useDispatch();

  const productsPerPage: number = 10;
  const filteredExhibitors: ExhibitorsDataType[] =
    exhibitors && exhibitors.length > 0
      ? exhibitors.filter((exhibitor) =>
          exhibitor.businessName
            .toLowerCase()
            .includes(searchQuery.toLowerCase())
        )
      : [];
  const indexOfLastProduct: number = currentPage * productsPerPage;
  const indexOfFirstProduct: number = indexOfLastProduct - productsPerPage;
  const currentExhibitors: ExhibitorsDataType[] = filteredExhibitors
    .slice(indexOfFirstProduct, indexOfLastProduct)
    .filter((exhibitor) => exhibitor.status == "accepted");

  const handleChangePage = (
    event: React.ChangeEvent<unknown>,
    page: number
  ) => {
    setCurrentPage(page);
  };

  const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchQuery(event.target.value);
    setCurrentPage(1);
  };

  const setExhibitor = async () => {
    try {
      const { data, status } = await axios.get("/api/admin/get-exhibitors");

      if (status == 200) {
        dispatch(setExhibitors(data.exhibitors));
        dispatch(updateLoading(false));
      }
    } catch (error) {
      dispatch(updateLoading(false));
    }
  };

  useEffect(() => {
    setExhibitor();
  }, []);

  return (
    <div className=" w-[85%]  min-h-screen mt-32 lg:mt-40 mx-auto">
      <div>
        <>
          <div className=" flex xs:flex-row flex-col justify-between my-10">
            <div className=" flex gap-2 items-center">
              <Input
                placeholder="Type here.."
                variant="outlined"
                value={searchQuery}
                onChange={handleSearchChange}
                className=" w-[80%]  md:w-[50%] lg:w-[20%]"
                size="md"
                sx={{
                  flexBasis: "250px",
                }}
                startDecorator={<SearchRoundedIcon color="primary" />}
              />
              <div className=" flex">
                <IconButton onClick={() => setCategory(true)}>
                  <BiSolidCategoryAlt
                    className={`${
                      category ? " text-BlueLighter" : "text-gray-400"
                    } text-4xl`}
                  />
                </IconButton>
                <IconButton onClick={() => setCategory(false)}>
                  <AiOutlineUnorderedList
                    className={`${
                      !category ? "text-BlueLighter" : " text-gray-400"
                    } text-4xl`}
                  />
                </IconButton>
              </div>
            </div>
            {currentExhibitors.length > 0 && (
              <Stack spacing={2} className="my-5 ">
                <Pagination
                  count={Math.ceil(filteredExhibitors.length / productsPerPage)}
                  page={currentPage}
                  shape="rounded"
                  onChange={handleChangePage}
                />
              </Stack>
            )}
          </div>
          {!isLoading && (
            <div>
              {currentExhibitors.length > 0 && (
                <>
                  <div className=" 2xl:w-[90%]">
                    {category ? (
                      <div className="  grid grid-cols-1 md:grid-cols-2 xl:grid-cols-4 gap-5">
                        {currentExhibitors.map((exhibitor) => (
                          <div className=" border-BlueLighter border h-[250px] max-w-sm flex flex-col justify-between p-5 rounded-lg  hover:cursor-pointer">
                            <div className=" flex flex-col gap-5 items-center">
                              <div className="flex justify-between items-center w-full h-1/3">
                                <div className=" flex gap-2 items-center">
                                  <Avatar
                                    src={exhibitor.image}
                                    className=" border border-BlueLighter h-16 w-16"
                                  />
                                  <div className=" flex flex-col">
                                    <h5 className="block antialiased tracking-normal font-sans text-base font-semibold leading-snug text-blue-gray-900">
                                      {exhibitor.businessName}
                                    </h5>
                                    <p className="block antialiased font-sans text-[12px] font-normal leading-relaxed text-gray-500">
                                      {exhibitor.bussinessType}
                                    </p>
                                  </div>
                                </div>
                                <Button
                                  onClick={() =>
                                    router.push(
                                      `/exhibitor-public/${exhibitor.id}`
                                    )
                                  }
                                  variant="soft"
                                  size="sm"
                                >
                                  Profile
                                </Button>
                              </div>
                              <div>
                                <p className=" text-gray-500 w-full text-xs ">
                                  {textCliper(
                                    exhibitor.DescriptionOfBusiness,
                                    100
                                  )}
                                  ...{" "}
                                </p>
                                <Button
                                  variant="plain"
                                  size="sm"
                                  className=" text-BlueLighter px-0"
                                  onClick={() =>
                                    router.push(
                                      `/exhibitor-public/${exhibitor.id}`
                                    )
                                  }
                                >
                                  Read more...
                                </Button>
                              </div>
                              <div className=" self-start  pr-20">
                                <AppSocial socila={exhibitor.social} />
                              </div>
                            </div>
                          </div>
                        ))}
                      </div>
                    ) : (
                      currentExhibitors.map((exhibitor) => (
                        <div key={exhibitor.id}>
                          <div className="flex md:flex-row flex-col md:justify-between h-[140px] md:h-[100px] w-full md:w-[80%] p-5 mb-3 border border-BlueLighter rounded-md items-center gap-5 cursor-pointer  transition-transform ease-in-out duration-700 ">
                            <div className=" flex gap-5  lg:justify-between items-center w-full">
                              <div className=" flex gap-5">
                                <Avatar
                                  src={exhibitor.image}
                                  className=" border-BlueDark border"
                                />
                                <div className=" flex flex-col gap-1">
                                  <h5 className="block antialiased tracking-normal font-sans text-base md:text-lg font-semibold leading-snug text-BlueDark">
                                    {exhibitor.businessName}
                                  </h5>
                                  <p className="block antialiased font-sans text-[12px] font-normal leading-relaxed text-gray-500">
                                    {exhibitor.bussinessType}
                                  </p>
                                </div>
                              </div>
                              <Button
                                onClick={() =>
                                  router.push(
                                    `/exhibitor-public/${exhibitor.id}`
                                  )
                                }
                                variant="soft"
                                size="sm"
                              >
                                Profile
                              </Button>
                            </div>

                            <div>
                              {/* <AppSocial socila={exhibitor.social} /> */}
                            </div>
                          </div>
                        </div>
                      ))
                    )}
                  </div>
                </>
              )}
            </div>
          )}
          {isLoading && (
            <div className="  grid grid-cols-1 md:grid-cols-2 xl:grid-cols-4 gap-5">
              {[1, 2, 3, 6, 5, 4].map((index) => (
                <div
                  key={index}
                  className="border-BlueLighter border h-[250px] max-w-sm flex flex-col justify-between p-5 rounded-lg hover:cursor-pointer"
                >
                  <div className="flex flex-col gap-5">
                    <div className="flex justify-between items-center w-full h-1/3">
                      <div className="flex gap-2 items-center">
                        <Skeleton variant="circular" width={40} height={40} />
                        <div className="flex flex-col">
                          <Skeleton width={120} height={20} />
                          <Skeleton width={80} height={15} />
                        </div>
                      </div>
                      <Skeleton variant="rectangular" width={70} height={30} />
                    </div>
                    <div>
                      <Skeleton variant="text" width={180} height={50} />
                      <Skeleton variant="text" width={140} height={30} />
                    </div>
                    <div className="self-start pr-20">
                      {/* <AppSocial socila={exhibitor.social} /> */}
                      <Skeleton variant="rectangular" width={80} height={20} />
                    </div>
                  </div>
                </div>
              ))}
            </div>
          )}
        </>

        {searchQuery && currentExhibitors.length == 0 && (
          <div className="lg:w-[45%] w-[90%] md:w-[75%] mx-auto border-dashed border-2 h-[50vh] p-10 flex flex-col items-center justify-center">
            <h3 className=" text-base lg:text-2xl text-gray-400 font-bold">
              No exhibitor found
            </h3>
          </div>
        )}
      </div>
    </div>
  );
};

export default page;
