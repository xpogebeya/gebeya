"use client";

import { ExhibitorsDataType } from "@/types/exhibitor";
import { GoVerified } from "react-icons/go";
import React, { useCallback, useEffect, useState } from "react";
import { Avatar, CircularProgress, Typography } from "@mui/material";
import {
  AppButton,
  AppButtonDefult,
  AppForm,
  AppFormWithDatePIcker,
  AppProductCard,
  AppSocial,
  Header,
} from "@/components";
import { useRouter } from "next/navigation";
import { AppDispatch, setFiles, textCliper, updateIsLoading, useAppSelector } from "@/utils";
import { Field } from "@/types/register";
import { FormikValues } from "formik";
import * as Yup from "yup";
import { AiFillPhone } from "react-icons/ai";
import { FaMapLocation } from "react-icons/fa6";
import { useAPI } from "@/hooks";
import IconButton from "@mui/joy/IconButton";
import { BiLogoFacebookCircle } from "react-icons/bi";
import Link from "next/link";
import { BsLinkedin, BsTelegram } from "react-icons/bs";
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';
import axios from "axios";
import { useDispatch } from 'react-redux'


const validationSchema = Yup.object().shape({
  fullName: Yup.string().required().label("Full name"),
  email: Yup.string().required().email().label("Email"),
  phone: Yup.string().min(10).max(13).required().label("Phone"),
  message: Yup.string(),
});

const initialValues: FormikValues = {
  fullName: "",
  email: "",
  phone: "",
  message: "",
};

const fields: Field[] = [
  {
    name: "fullName",
    label: "Full Name",
    required: true,
  },

  {
    name: "email",
    label: "Email",
    required: true,
  },
  {
    name: "phone",
    label: "Phone",
    required: true,
  },
];

type Props = {
  params: {
    id: string;
  };
};

const page = ({ params: { id } }: Props) => {
  const [exhibitor, setExhibitor] = useState<ExhibitorsDataType | null>(null);
  const [uploading, setUploading] = useState<boolean>(false);

  const [show, setShow] = useState<boolean>(false);
  const [showAllTeam, setShowAllTeam] = useState<boolean>(true);
  const router = useRouter();
  const URL = "/api/exhibitor/book-meeting";
  const { submitForm } = useAPI();

  const { files } = useAppSelector((state) => state.fileSlice)
  const dispatch = useDispatch<AppDispatch>();

  const getFiles = async () => {
    const res = await fetch(`/api/admin/upload/getFiles?id=${id}`, { cache: 'no-store' });
    const { files } = await res.json()
    if (res.ok) {
      dispatch(setFiles(files))
    } else {
    }
    dispatch(updateIsLoading(false))
  }



  const getCurrentExhibitors = async () => {
    try {
      const { data } = await axios.get(`/api/exhibitor/get-exhibitor?id=${id}`)
      setExhibitor(data.exhibitor.data)
    } catch (error) {
      console.log("error")
    }
  }

  useEffect(() => {
    // getCurrentExhibitor();
    getCurrentExhibitors()
    getFiles()

  }, []);
  return (
    <div className=" min-h-screen ">
      {exhibitor ? (
        <div >

          <div className=" flex flex-col gap-20 lg:w-[80%] mx-auto ">
            <div className=" relative  flex flex-col mt-[8rem]">
              <div className=" h-[300px] lg:h-[500px] w-full overflow-hidden border-b-gray-300 border-b ">
                {exhibitor?.image ? (
                  <img
                    src={exhibitor?.image}
                    className=" w-full h-[300px] lg:h-[500px] object-contain "
                  />
                ) : (
                  <div className="w-full h-[300px] lg:h-[500px] flex flex-col items-center justify-center">
                    <img
                      src={"/logoMain.png"}
                      className=" w-full h-[300px] lg:h-[500px] object-contain "
                    />
                  </div>
                )}
              </div>
              <div className=" absolute top-[17rem] lg:top-[28rem] left-[1rem]  sm:left-[5rem] md:left-[10rem] xl:left-[12rem] w-[120px] h-[120px] sm:w-[150px] sm:h-[150px] lg:w-[200px] lg:h-[200px] a">
                {exhibitor.image ? (
                  <img
                    src={exhibitor?.image}
                    className=" w-[90px] h-[90px] sm:w-[150px] sm:h-[150px] lg:w-[200px] lg:h-[200px] rounded-full object-contain border border-BlueLighter"
                  />
                ) : (
                  <Avatar className=" w-[90px] h-[90px] sm:w-[150px] sm:h-[150px] lg:w-[200px] lg:h-[200px]" />
                )}
              </div>
              <div className=" absolute top-[20rem] lg:top-[33rem] left-[7rem] sm:left-[15rem] md:left-[20rem]  lg:left-[26rem] xl:left-[26rem] flex flex-col gap-1 px-5 ">
                <div className=" flex  gap-[5.25rem] xs:gap-[13.25rem] sm:gap-[9rem] md:gap-[11rem] lg:gap-[15rem] xl:gap-[17rem] 2xl:gap-[30rem]  items-start">
                  <div className=" flex gap-1">
                    <h3 className=" text-[16px] sm:text-[20px] md:text-2xl xl:text-3xl">
                      {exhibitor?.businessName}
                    </h3>
                    <GoVerified className=" text-blue-500 text-[12px]" />
                  </div>

                  <AppButtonDefult
                    label="Book A Meeting"
                    handleAction={() => router.push("#bookMeeting")}
                  />
                </div>
                <p className=" max-w-xs lg:max-w-lg text-[11px] sm:text-[13px] md:text-[14px] xl:text-[16px] text-gray-500 mb-1">
                  {exhibitor.address.city} , {exhibitor.address.country}
                </p>

                <AppSocial socila={exhibitor.social} />
              </div>

              <div className=" mt-[12rem] lg:mt-[16rem] w-[85%] mx-auto flex flex-col gap-3  ">
                <Typography
                  variant="h4"
                  className=" font-bold  text-BlueDark max-w-xl"
                >
                  About {exhibitor.businessName}
                </Typography>
                <Typography className=" w-[98%] lg:w-[85%] text-sm md:text-base lg:text-lg text-gray-600 max-w-4 font-extralight">
                  {show
                    ? exhibitor.DescriptionOfBusiness
                    : textCliper(exhibitor.DescriptionOfBusiness, 400)}
                </Typography>
                <div>
                  <AppButton
                    label={show ? " Read Less" : "Read More..."}
                    handleAction={() => setShow(!show)}
                  />
                </div>
              </div>

              <div className=" flex-col flex  justify-center w-[85%] py-5 mx-auto">
                {
                  files && files.length > 0 &&
                  <section className="p-4 my-6 md:p-8  bg-BlueLighter/10 dark:text-gray-100">
                    <div className="container grid grid-cols-1 gap-6 m-4 mx-auto md:m-0 md:grid-cols-2 xl:grid-cols-4">
                      {
                        files.map((file) => (
                          <Link
                            href={file.url} target="_blank"
                            rel="noopener noreferrer"
                            key={file.id}
                            className="flex cursor-pointer overflow-hidden rounded-lg   bg-BlueDark dark:text-gray-100">

                            <div className="flex items-center justify-center px-4  bg-BlueLight text-White">
                              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" fill="currentColor" className="w-6 h-6">
                                <path d="M487.938,162.108l-224-128a16,16,0,0,0-15.876,0l-224,128a16,16,0,0,0,.382,28l224,120a16,16,0,0,0,15.112,0l224-120a16,16,0,0,0,.382-28ZM256,277.849,65.039,175.548,256,66.428l190.961,109.12Z"></path>
                                <path d="M263.711,394.02,480,275.061V238.539L256,361.74,32,238.539v36.522L248.289,394.02a16.005,16.005,0,0,0,15.422,0Z"></path>
                                <path d="M32,362.667,248.471,478.118a16,16,0,0,0,15.058,0L480,362.667V326.4L256,445.867,32,326.4Z"></path>
                              </svg>
                            </div>
                            <div className="flex items-center justify-between flex-1 p-3">
                              {/* <p className="text-xl font-semibold">{file.title}</p> */}
                              <p>{file.title}</p>
                            </div>
                          </Link>

                        ))
                      }
                    </div>
                  </section>
                }
              </div>
            </div>

            {exhibitor.products.length > 0 && (
              <div className="  mx-auto w-[85%] md:w-[70%] lg:w-[85%] ">
                <div className=" flex lg:w-[75%] 2xl:w-[85%]  justify-between">
                  <Header
                    lable="Check Our Products"
                    className=" text-BlueDark "
                  />
                  <p
                    className=" hover:text-BlueLighter cursor-pointer hover:underline hover:scale-110"
                    onClick={() =>
                      router.push(`/exhibitor-public/${id}/exhibitor-product`)
                    }
                  >
                    Show all
                  </p>
                </div>
                <div className="w-[90%] grid grid-cols-1 gap-12 gap-x-10 md:grid-cols-2 xl:grid-cols-4 my-12  ">
                  {exhibitor.products.map(
                    (product, index) =>
                      index < 4 && (
                        <AppProductCard
                          id={id}
                          key={product.id}
                          product={product}
                        />
                      )
                  )}
                </div>
              </div>
            )}

            {exhibitor.teams.length > 0 && (
              <div className="  mx-auto w-[85%] md:w-[70%] lg:w-[84%] xl:w-[80%]  xll:w-[85%] ">
                <div className=" flex lg:w-[75%] 2xl:w-[85%]  justify-between mb-10">
                  <Header lable="Meet the team" className=" text-BlueDark" />
                  {exhibitor.teams.length > 4 && (
                    <p
                      className=" hover:text-BlueLighter cursor-pointer hover:underline hover:scale-110"
                      onClick={() => setShowAllTeam(!showAllTeam)}
                    >
                      {showAllTeam ? "Show all" : "Show less"}
                    </p>
                  )}
                </div>
                <div className="  grid grid-cols-1 gap-12 gap-x-24 sm:grid-cols-2 xl:grid-cols-4">
                  {exhibitor.teams &&
                    exhibitor.teams.map(
                      (team, index) =>
                        index < 4 && (
                          <div
                            className="relative flex flex-col bg-clip-border rounded-xl bg-transparent text-gray-700 shadow-none text-center "
                            key={team.id}
                          >
                            <img
                              src={team.image}
                              alt="Ryan Tompson"
                              className="inline-block relative object-cover object-center rounded-2xl h-full w-full shadow-lg shadow-gray-500/25 "
                            />
                            <h5 className="block antialiased tracking-normal font-sans text-xl font-semibold leading-snug text-blue-gray-900 mt-6 mb-1">
                              {team.firstName} {team.lastName}
                            </h5>
                            <p className="block antialiased font-sans text-base leading-relaxed font-normal text-blue-gray-500">
                              {team.jobTitle}
                            </p>
                            <div className="mx-auto mt-5">
                              <div className="flex items-center gap-2 ">
                                <IconButton>
                                  <Link
                                    href={team.facebookLink ?? ""}
                                    target="_blank"
                                    rel="noopener noreferrer"
                                  >
                                    <BiLogoFacebookCircle className="  hover:text-blue-400 text-gray-400 hover:scale-105" />
                                  </Link>
                                </IconButton>
                                <IconButton>
                                  <Link
                                    href={team.linkedinLink ?? ""}
                                    target="_blank"
                                    rel="noopener noreferrer"
                                  >
                                    <BsLinkedin className="  hover:text-blue-400 text-gray-400 hover:scale-105" />
                                  </Link>
                                </IconButton>
                                <IconButton>
                                  <Link
                                    href={team.telegramLink ?? ""}
                                    target="_blank"
                                    rel="noopener noreferrer"
                                  >
                                    <BsTelegram className="  hover:text-blue-400 text-gray-400 hover:scale-105" />
                                  </Link>
                                </IconButton>
                              </div>
                            </div>
                          </div>
                        )
                    )}
                </div>
              </div>
            )}

            <div className=" w-[85%] mx-auto" id="bookMeeting">


              {
                uploading && <Box sx={{ width: '80%' }} className=" my-5 mx-auto">
                  <LinearProgress color="info" />
                </Box>
              }

              <div className="grid max-w-6xl grid-cols-1 px-6 mx-auto lg:px-8 md:grid-cols-2 md:divide-x  ">
                <div className="py-6 md:py-0 md:px-6 text-gray-600">
                  <h1 className=" text-2xl lg:text-4xl font-bold">Book A Meeting</h1>
                  <p className="pt-2 pb-4">Fill in the form to start a conversation</p>
                  <div className="space-y-4">
                    <p className="flex items-center">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" className="w-5 h-5 mr-2 sm:mr-6">
                        <path fillRule="evenodd" d="M5.05 4.05a7 7 0 119.9 9.9L10 18.9l-4.95-4.95a7 7 0 010-9.9zM10 11a2 2 0 100-4 2 2 0 000 4z" clipRule="evenodd"></path>
                      </svg>
                      <span>{exhibitor.address.city},{" "}{exhibitor.address.country}</span>
                    </p>
                    <p className="flex items-center">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" className="w-5 h-5 mr-2 sm:mr-6">
                        <path d="M2 3a1 1 0 011-1h2.153a1 1 0 01.986.836l.74 4.435a1 1 0 01-.54 1.06l-1.548.773a11.037 11.037 0 006.105 6.105l.774-1.548a1 1 0 011.059-.54l4.435.74a1 1 0 01.836.986V17a1 1 0 01-1 1h-2C7.82 18 2 12.18 2 5V3z"></path>
                      </svg>
                      <span>{exhibitor.address.phone}</span>
                    </p>
                    <p className="flex items-center">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" className="w-5 h-5 mr-2 sm:mr-6">
                        <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path>
                        <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path>
                      </svg>
                      <span>{exhibitor.email}</span>
                    </p>
                  </div>
                </div>

                <div className=" flex flex-col gap-2 justify-center px-3">

                  <AppForm
                    initialValues={initialValues}
                    onSubmit={(values: FormikValues) => {
                      setUploading(true)
                      submitForm({ exhibitorId: id, ...values }, URL).then(() => {
                        setUploading(false)
                      })


                    }}
                    validationSchema={validationSchema}
                  >
                    <AppFormWithDatePIcker
                      fields={fields}
                      buttonLable={"Book"}
                    />
                  </AppForm>
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div className=" h-screen flex flex-col justify-center items-center">
          <CircularProgress />
        </div>
      )}
    </div>
  );
};

export default page;
