"use client";
import ExhibitorsData from "@/data/temporarayData";
import { Product } from "@/types/exhibitor";
import { useAppSelector } from "@/utils";
import { CircularProgress } from "@mui/material";
import React, { useEffect, useState } from "react";


type Props = {
  params: {
    productID: number;
  };
};

const page = ({ params: { productID } }: Props) => {
  const [currentProduct, setCurrentProduct] = useState<Product | null>(null);
  const [productList, setProductList] = useState<Product[]>([]);
  const { exhibitors } = useAppSelector(state => state.exhibtorReducers)

  useEffect(() => {
    let products: Product[] = [];
    exhibitors.forEach((exhibitor) => {
      products = [...products, ...exhibitor.products];
    });
    let product: Product[] = products.filter((pro) => pro.id == productID);

    setCurrentProduct(product[0]);
    if (product.length > 0) {
      const similarCategories = product[0].categorie;
      const similarProducts = products.filter((pro) =>
        pro.categorie.some((category) => similarCategories.includes(category))
      );
      setProductList(similarProducts);
    }
  }, []);

  return (
    <div>
      {currentProduct ? (
        <div className=" min-h-[80vh] justify-center flex flex-col items-center mt-[5rem]  overflow-x-clip">
          <section className="text-gray-700 body-font overflow-hidden bg-white">
            <div className="container px-5  mx-auto">
              <div className="lg:w-4/5 mx-auto flex flex-wrap">
                <img
                  alt="product image "
                  className="lg:w-1/2 w-full rounded-lg object-contain object-center  h-[300px] lg:h-[400px] xl:h-[500px]  "
                  src={currentProduct.image}
                />
                <div className="lg:w-1/2 w-full lg:pl-10 lg:py-6 mt-6 lg:mt-0">
                  <h2 className="text-sm title-font text-gray-500 tracking-widest">
                    BRAND NAME
                  </h2>
                  <h1 className="text-gray-900 text-3xl title-font font-medium mb-1">
                    {currentProduct.title}
                  </h1>

                  <p className="leading-relaxed">
                    {currentProduct.description}
                  </p>

                  <div className=" flex gap-2 h-40 mt-10  overflow-x-auto w-full">
                    {productList.map(
                      (product, index) =>
                        index < 4 && (
                          <div
                            key={product.id}
                            className=" rounded-lg h-32 w-1/4 hover:scale-105 cursor-pointer"
                            onClick={() => setCurrentProduct(product)}
                          >
                            <img
                              src={product.image}
                              className=" rounded-lg w-full object-contain h-32 "
                            />
                          </div>
                        )
                    )}
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      ) : (
        <div className="h-screen flex flex-col justify-center items-center">
          <CircularProgress />
        </div>
      )}
    </div>
  );
};

export default page;
