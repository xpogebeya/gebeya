"use client";

import ExhibitorsData from "@/data/temporarayData";
import React, { useEffect, useState } from "react";
import { Product } from "@/types/exhibitor";
import { Box, CircularProgress, Skeleton, TextField } from "@mui/material";
import Pagination from "@mui/material/Pagination";
import Stack from "@mui/material/Stack";
import Input from "@mui/joy/Input";
import SearchRoundedIcon from "@mui/icons-material/SearchRounded";
import { AppProductCard } from "@/components";
import { useAppSelector } from "@/utils";

type Props = {
  params: {
    id: string | number;
  };
};

const page: React.FC<Props> = ({ params: { id } }) => {
  const [productList, setProductList] = useState<Product[]>([]);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [searchQuery, setSearchQuery] = useState<string>("");
  const { exhibitors } = useAppSelector(state => state.exhibtorReducers)
  const productsPerPage: number = 10;

  useEffect(() => {
    let currentExibitor = exhibitors.filter((exhitor) => exhitor.id == id);
    if(currentExibitor.length > 0 && currentExibitor[0].products){

      let products: Product[] = [...currentExibitor[0].products]
      setProductList(products);
    }
  }, []);

  const filteredProducts: Product[] = productList.filter((product) =>
    product.title.toLowerCase().includes(searchQuery.toLowerCase())
  );

  const indexOfLastProduct: number = currentPage * productsPerPage;
  const indexOfFirstProduct: number = indexOfLastProduct - productsPerPage;
  const currentProducts: Product[] = filteredProducts.slice(
    indexOfFirstProduct,
    indexOfLastProduct
  );

  const handleChangePage = (
    event: React.ChangeEvent<unknown>,
    page: number
  ) => {
    setCurrentPage(page);
  };

  const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchQuery(event.target.value);
    setCurrentPage(1);
  };
  const fakeArray = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  return (
    <div className="min-h-screen mt-[7rem] md:mt-[10rem] w-[80%] mx-auto">
      <div>
        {productList.length > 0 ? (
          <>
            <Input
              placeholder="Search"
              variant="outlined"
              value={searchQuery}
              onChange={handleSearchChange}
              className="mb-3  md:w-[50%] lg:w-[20%]"
              size="lg"
              sx={{
                flexBasis: "500px",
                boxShadow: "sm",
              }}
              startDecorator={<SearchRoundedIcon color="primary" />}
            />
            {currentProducts.length > 0 && (
              <>
                <div className="grid grid-cols-1 gap-12 gap-x-10 md:grid-cols-2 xl:grid-cols-4 mb-2">
                  {currentProducts.map((product, index) => (
                    <AppProductCard id={id} key={index} product={product} />
                  ))}
                </div>
                <Stack spacing={2} className="mt-5 ">
                  <Pagination
                    count={Math.ceil(filteredProducts.length / productsPerPage)}
                    page={currentPage}
                    shape="rounded"
                    onChange={handleChangePage}
                  />
                </Stack>
              </>
            )}
            {searchQuery && currentProducts.length == 0 && (
              <div className="lg:w-[45%] w-[90%] md:w-[75%] mx-auto border-dashed border-2 h-[50vh] p-10 flex flex-col items-center justify-center">
                <h3 className=" text-base lg:text-2xl text-gray-400 font-bold">
                  No Products found
                </h3>
              </div>
            )}
          </>
        ) : (
          <div className="h-screen flex flex-col justify-center items-center">
            <div className=" w-full grid grid-cols-1 gap-12 gap-x-10 md:grid-cols-2 xl:grid-cols-4 mb-2">
              {fakeArray.map((id) => (
                <Stack spacing={-0.5} key={id}>
                  <Skeleton
                    variant="rectangular"
                    sx={{
                      height: "250px",
                    }}
                    width={240}
                    height={120}
                  />
                  <Skeleton
                    variant="text"
                    width={160}
                    sx={{
                      fontSize: "1.5rem",
                    }}
                  />
                  <Skeleton
                    variant="text"
                    width={240}
                    sx={{
                      fontSize: "3rem",
                    }}
                  />
                  <Skeleton variant="rectangular" width={120} height={30} />
                </Stack>
              ))}
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default page;
