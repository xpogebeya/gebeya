'use client'
import { Field } from '@/types/register';
import { FormikValues } from 'formik';
import React from 'react'
import * as Yup from "yup";
import { AppButton, AppForm, AppFormField, AppSubmitButton, Progress } from '@/components';
import { useAPI } from '@/hooks';
import { useRouter } from 'next/navigation';
import axios from 'axios';

type Props = {
  searchParams: {
    email: string
  }
}

const validationSchema = Yup.object().shape({
  password: Yup.string()
    .min(8, 'Password must be at least 8 characters')
    .required('Password is required')
    .matches(
      /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/,
      'Password must have characters, including letters and numbers'
    ),
  confirmPassword: Yup.string()

    .oneOf([Yup.ref('password')], 'Passwords must match')
    .required('Confirm Password is required'),
});

const initialValues: FormikValues = {
  confirmPassword: "",
  password: "",
};
const FieldValue: Field[] = [
  {
    name: "password",
    label: "Password",
    required: true,
    type: "password",
  },
  {
    name: "confirmPassword",
    label: "Confirm Password",
    required: true,
    type: "password",

  }
];
const page = ({ searchParams: { email } }: Props) => {
  const { message, progress, setSubmit, submit, error, status, setStatus, setMessage } = useAPI();
  const router = useRouter()
  const handleSubmit = async (values: FormikValues) => {
    const { status, data } = await axios.put('/api/admin/change-password', {
      data: {
        email,
        password: values.password,
        type: "exhibitor"
      }
    })
    setSubmit(false)
    if (status == 200) {
      setStatus(200)
    } else {
      setMessage(data.error)
    }
  }

  return (
    <div className='  items-center justify-center flex flex-col'>
      {
        submit ? (<Progress
          process={progress}
          message={message}
          error={error}
          status={status}
        />) : (
          <div className=" antialiased  w-[90%] xs:w-[80%]  sm:w-[75%] xss:w-[70%] lg:w-[45%] mx-auto h-[100vh] mt-10 flex flex-col items-center justify-center py-5">
            {
              status == 200 ? <div>

                <div className=" pb-0 p-0 md:py-6  text-center mb-5">
                  <h4 className="font-bold text-xl ">Sucessful!</h4>
                  <p className="mb-0 opacity-60">
                    You have sucessfully changed your password
                  </p>
                </div>
                <AppButton label={'Sign in'} handleAction={() => router.push("/exhibitor-login")} className=' w-[85%] mx-auto' />
              </div> : (
                <>
                  <div className=" pb-0 mb-0 p-0 md:py-6  px-5">
                    <h4 className="font-bold text-xl ">Set password</h4>
                    <p className="mb-0 opacity-60">
                      Enter your new password to set up your account
                    </p>
                    <p className="mb-0 opacity-60">
                      {message}
                    </p>
                  </div>
                  <div className=" xs:w-[85%] lg:w-[60%] mx-auto flex flex-col gap-5 items-center ">
                    <AppForm
                      initialValues={initialValues}
                      onSubmit={(values: FormikValues) => handleSubmit(values)}
                      validationSchema={validationSchema} >
                      {FieldValue.map((item, index) => (
                        <AppFormField
                          key={item.name}
                          required={item.required}
                          name={item.name}
                          type={item.type}
                          label={item.label}
                          className=" min-w-[300px] xs:min-w-[400px] my-3"
                        />
                      ))}
                      <AppSubmitButton
                        title={"Update"}
                        setProcess={setSubmit}
                        className="min-w-[300px] xs:min-w-[400px] mt-5 "
                      />
                    </AppForm>
                    <p className=" text-center text-gray-500">
                      Copyright © GebeyaExpo {new Date().getFullYear()}.
                    </p>
                  </div>
                </>
              )
            }
          </div>
        )
      }
    </div>
  )
}

export default page