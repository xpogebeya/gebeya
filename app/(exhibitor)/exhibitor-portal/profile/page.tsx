"use client";

import React, { useState } from "react";
import * as Yup from "yup";
import { Field } from "@/types/register";
import { FormikValues } from "formik";
import { AppExhibitorProfileEditor } from "@/components";
import { useAppSelector } from "@/utils";
import { Box, CircularProgress, LinearProgress } from "@mui/material";
import { useExhibitorPortalContext } from "@/context/ExhibitorPortalContext";


const bussinessTypeList: string[] = [
  "Manufacturer", "Agent-distributor-supplier", "Service Provider", "Government/Municipality", "Trade Association", "other"
]
const page = () => {

  const [uploading, setUploading] = useState<boolean>(false);
  const { currentUser, exhibitorLoading } = useAppSelector((state) => state.ExhibtorAuth)
  const { error } = useExhibitorPortalContext();

  const validationSchema = Yup.object().shape({
    businessName: Yup.string().required().label("Business Name"),
    DescriptionOfBusiness: Yup.string()
      .required()
      .label("Description of the business"),
    city: Yup.string().required().label("City name"),
    email: Yup.string().required().email().label("Email"),
    phone: Yup.string().min(10).max(13).required().label("Phone"),
    country: Yup.string().required().label("country"),
    zipCode: Yup.string().label("Zip Code"),
    streetNumber: Yup.string().label("Zip Code"),
    telephone: Yup.string(),
    youTubeLink: Yup.string(),
    tikTokLink: Yup.string(),
    telegramChannel: Yup.string(),
    websiteLink: Yup.string(),
    linkedinLink: Yup.string(),
    tvChannelName: Yup.string(),
    faceBookLink: Yup.string(),
    instagram: Yup.string(),
    rentedBooth: Yup.string(),
    image: Yup.string(),
    bussinessType: Yup.string()
  });

  const initialValues: FormikValues = {
    businessName: currentUser?.businessName,
    DescriptionOfBusiness: currentUser?.DescriptionOfBusiness,
    city: currentUser?.address.city,
    country: currentUser?.address.country,
    streetNumber: currentUser?.address.streetNumber,
    zipCode: currentUser?.address.zipCode,
    email: currentUser?.email,
    phone: currentUser?.address.phone,
    telephone: currentUser?.address.telephone,
    youTubeLink: currentUser?.social.websiteLink,
    tikTokLink: currentUser?.social.tikTokLink,
    telegramChannel: currentUser?.social.telegramChannel,
    websiteLink: currentUser?.social.websiteLink,
    linkedinLink: currentUser?.social.linkedinLink,
    tvChannelName: currentUser?.social.tvChannelName,
    faceBookLink: currentUser?.social.faceBookLink,
    instagram: currentUser?.social.instagram,
    rentedBooth: currentUser?.rentedBooth,
    image: currentUser?.image,
    bussinessType: currentUser?.bussinessType
  };

  const fields: Field[][] = [
    [
      {
        name: "businessName",
        label: "Business Name",
        required: true,
      },
      {
        name: "bussinessType",
        label: "Business Type",
        required: false,
        options: bussinessTypeList
      },
      {
        name: "DescriptionOfBusiness",
        label: "Description of the business",
        required: true,
      },
      {
        name: "rentedBooth",
        label: "Reneted Booth",
        required: false,
      },
    ],
    [
      {
        name: "city",
        label: "City",
        required: true,
      },
      {
        name: "country",
        label: "Country",
        required: true,
      },
      {
        name: "zipCode",
        label: "Zip Code",
        required: false,
      },
      {
        name: "streetNumber",
        label: "Street Number",
        required: false,
      },
    ],
    [
      {
        name: "email",
        label: "Email",
        required: true,
      },
      {
        name: "phone",
        label: "phone",
        required: true,
      },
      {
        name: "telephone",
        label: "TelePhone Number",
        required: false,
      },
    ],
    [
      {
        name: "youTubeLink",
        label: "Youtube Channel",
        required: false,
      },
      {
        name: "tikTokLink",
        label: "TikTok Channel",
        required: false,
      },
      {
        name: "telegramChannel",
        label: "Telegram Channel",
        required: false,
      },
      {
        name: "websiteLink",
        label: "Website ",
        required: false,
      },
      {
        name: "linkedinLink",
        label: "Linkedin Channel",
        required: false,
      },
      {
        name: "tvChannelName",
        label: "TV Channel",
        required: false,
      },
      {
        name: "faceBookLink",
        label: "Facebook Channel",
        required: false,
      },
      {
        name: "instagram",
        label: "Instagram Channel",
        required: false,
      },
    ],
  ];

  const steps = [
    "Business Information",
    "Address",
    "Contact Information",
    "Media Information",
  ];

  return (
    <div className=" flex flex-col gap-10  -mt-8 px-2 xs:px-10">
      {
        uploading && <Box sx={{ width: '100%' }} className=" mb-5">
          <LinearProgress color="info" />
        </Box>
      }
      <p className=" text-center text-red-500">{error}</p>

      <div className=" min-h-[90vh] overflow-y-auto">

        {
          currentUser ? <AppExhibitorProfileEditor
            initialValues={initialValues}
            validationSchema={validationSchema}
            fields={fields}
            steps={steps}
            setUploading={setUploading}
          />
            :
            <div className="flex-col flex items-center justify-center">
              <CircularProgress />
            </div>
        }
      </div>
    </div>
  );
};

export default page;
