"use client";
import React, { useState } from "react";
import {
  AppButton,
  AppForm,
  AppTeamAdderDesktop,
  AppTeamAdderMobile,
  AppTeamEditor,
  AppTeamLister,
} from "@/components";
import * as Yup from "yup";
import { Field } from "@/types/register";
import { FormikValues } from "formik";
import { useAPI, useFileUploader } from "@/hooks";
import { useExhibitorPortalContext } from "@/context/ExhibitorPortalContext";
import { LinkItem, Teams } from "@/types/exhibitor";
import Typography from "@mui/joy/Typography";
import { AppMenuItemContent } from "@/components";
import { RiDeleteBin6Line } from "react-icons/ri";
import { AiOutlineEdit } from "react-icons/ai";
import { Menu, MenuItem } from "@mui/material";
import { updateExhibitorProfile, useAppSelector } from "@/utils";
import axios from "axios";
import { useDispatch } from "react-redux";
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';

const validationSchema = Yup.object().shape({
  firstName: Yup.string().required().label("First name"),
  lastName: Yup.string().required().label("Last name"),
  middleName: Yup.string().label("Middle name"),
  jobTitle: Yup.string().required().label("Job title"),
  description: Yup.string().required().label("Description"),
  linkedinLink: Yup.string(),
  telegramLink: Yup.string(),
  facebookLink: Yup.string(),
  image: Yup.string(),
});

const initialValues: FormikValues = {
  firstName: "",
  lastName: "",
  middleName: "",
  jobTitle: "",
  description: "",
  linkedinLink: "",
  telegramLink: "",
  facebookLink: "",
  image: "",
};

const fields: Field[] = [
  {
    name: "firstName",
    label: "First Name",
    required: true,
  },
  {
    name: "middleName",
    label: "Middle Name",
    required: false,
  },
  {
    name: "lastName",
    label: "Last Name",
    required: true,
  },
  {
    name: "jobTitle",
    label: "job Title",
    required: true,
  },
  {
    name: "description",
    label: "Description",
    required: true,
  },
  {
    name: "linkedinLink",
    label: "Linkedin Channel",
    required: false,
  },
  {
    name: "telegramLink",
    label: "Telegram username",
    required: false,
  },
  {
    name: "facebookLink",
    label: "Facebook username",
    required: false,
  },
];



const page = () => {
  const {
    editing,
    setEditing,
    editingTeam,
    setEditingTeam,
  } = useExhibitorPortalContext();
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const [currentTeam, setCurrentTeam] = useState<Teams | null>(null);
  const [uploading, setUploading] = useState<boolean>(false);

  const { searchQuery } = useAppSelector((state) => state.userClients);

  const { currentUser } = useAppSelector(state => state.ExhibtorAuth)
  const { file, setFile, upload } = useFileUploader();
  const { adding, setAdding, error, setError } = useExhibitorPortalContext();

  const dispatch = useDispatch()
  const open = Boolean(anchorEl);


  const filteredTeams = currentUser?.teams
    ? currentUser?.teams.filter((team) => {
      const searchLower = searchQuery.toLowerCase();
      return (
        team.firstName.toLowerCase().includes(searchLower) ||
        team.lastName.toLowerCase().includes(searchLower) ||
        team.jobTitle.toLowerCase().includes(searchLower) ||
        team.description.toLowerCase().includes(searchLower)
      );
    })
    : [];

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleSubmit = async (values: FormikValues) => {
    setUploading(true)
    const url = await upload()

    if (url) {
      try {

        const { data, status } = await axios.put("/api/exhibitor/add-team", {
          data: {
            description: values.description,
            firstName: values.firstName,
            jobTitle: values.jobTitle,
            lastName: values.lastName,
            facebookLink: values.facebookLink,
            url: url,
            linkedinLink: values.linkedinLink,
            middleName: values.middleName,
            telegramLink: values.telegramLink,
            exhibitorId: currentUser?.id,
            imageFileName: file ? file[0].name : ""
          }
        })

      

        if (currentUser) {
          dispatch(updateExhibitorProfile({
            ...currentUser, teams: [...currentUser.teams, {
              description: values.description,
              firstName: values.firstName,
              jobTitle: values.jobTitle,
              lastName: values.lastName,
              facebookLink: values.facebookLink,
              image: url,
              linkedinLink: values.linkedinLink,
              middleName: values.middleName,
              telegramLink: values.telegramLink,
              id: data.data.id
            }]
          }))

        }

        setError("")

      } catch (error) {
        setError("Something went wrong")

      }

      setUploading(false)
    }
  };

  const handleDelete = async (team: Teams | null) => {
    if (team) {
      setUploading(true)
      handleClose();

      const newTeam = currentUser?.teams.filter((item) => item.id !== team.id);

      if (editing) {
        setEditing(false);
      }
      try {
        await axios.post("/api/exhibitor/add-team", {
          data: {
            id: team.id
          }
        })
        setError("")
        if (currentUser && newTeam) {
          dispatch(updateExhibitorProfile({
            ...currentUser, teams: newTeam
          }))
        }
      } catch (error) {
        setError("Something went wrong")
      }

      setUploading(false)


    }
  };

  const handleClick = (
    event: React.MouseEvent<HTMLButtonElement>,
    team: Teams
  ) => {
    setAnchorEl(event.currentTarget);
    setCurrentTeam(team);
  };

  const handleEdit = (team: Teams | null) => {
    if (team) {
      setEditing(true);
      setAdding(false);
      setEditingTeam(team);
      handleClose();
    }
  };


  return (
    <div className=" -mt-16 md:-mt-32  ml-5  md:px-2 mx-auto mr-5  ">
      <div className=" flex justify-between items-center mb-5 flex-row w-full gap-2 ">
        <Typography level="h2" className=" text-2xl  block  text-White">
          Teams
        </Typography>

        <AppButton
          label="Add New Member"
          handleAction={() => {
            setEditing(false);
            setAdding(true);
          }}
          className=" bg-white  text-BlueLighter"
        />
      </div>
      <div className=" px-5">

        {
          uploading && <Box sx={{ width: '70%' }} className=" mb-5 mx-auto">
            <LinearProgress color="info" />
          </Box>
        }
        <AppForm
          initialValues={initialValues}
          onSubmit={handleSubmit}
          validationSchema={validationSchema}
        >
          <AppTeamAdderMobile fields={fields} openModal={adding} setFile={setFile} />
          <p className=" text-center text-red-500">{error}</p>
          <div className=" flex flex-col md:flex-row justify-between gap-10 ">
            {currentUser?.teams && (
              <AppTeamLister
                teams={filteredTeams}
                openModal={handleClick}
                fields={fields}
                searchQuery={searchQuery}
              />
            )}
            {editing && (
              <AppTeamEditor
                // @ts-ignore
                team={editingTeam}
                setEditing={setEditing}
                fields={fields}
                // @ts-ignore
                initialValues={editingTeam}
                setUploading={setUploading}
              />
            )}
            {adding && (
              <AppTeamAdderDesktop
                fields={fields}
                setEditing={setAdding}
                initialValues={initialValues}
                setFile={setFile} />
            )}
          </div>
          <Menu
            id="basic-menu"
            anchorEl={anchorEl}
            open={open}
            onClose={handleClose}
            MenuListProps={{
              "aria-labelledby": "basic-button",
            }}
          >
            <MenuItem onClick={() => handleDelete(currentTeam)}>
              <AppMenuItemContent
                icon={<RiDeleteBin6Line className="text-2xl text-red-500" />}
                title="Delete"
                description="Delete this member"
              />
            </MenuItem>
            <MenuItem onClick={() => handleEdit(currentTeam)}>
              <AppMenuItemContent
                icon={<AiOutlineEdit className="text-2xl text-yellow-500" />}
                title="Edit"
                description="Edit this member profile"
              />
            </MenuItem>
          </Menu>
        </AppForm>
      </div>
    </div>
  );
};

export default page;
