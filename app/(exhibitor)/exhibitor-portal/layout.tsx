import { AppAuthWrapper, AppExhibitorProtectedRoute } from "@/containers";
import { MenuListType } from "@/types/dashboard";

import React, { FC, ReactNode } from "react";
import { AiOutlineCloudUpload, AiOutlineTeam } from "react-icons/ai";
import { BsPersonFillGear } from "react-icons/bs";
import { CiShoppingCart } from "react-icons/ci";
import { SiGotomeeting } from "react-icons/si";

type Props = {
  children: ReactNode;
};

const layout: FC<Props> = ({ children }) => {
  const menuItems: MenuListType[] = [
    {
      label: "Profile",
      Icon: <BsPersonFillGear className=" text-3xl text-BlueLighter" />,
      path: "/exhibitor-portal/profile",
    },
    {
      label: "Teams",
      Icon: <AiOutlineTeam className=" text-3xl text-BlueLighter" />,
      path: "/exhibitor-portal/team",
    },
    {
      label: "Products",
      Icon: <CiShoppingCart className=" text-3xl text-BlueLighter" />,
      path: "/exhibitor-portal/products",
    },
    {
      label: "Meetings",
      Icon: <SiGotomeeting className=" text-3xl text-BlueLighter" />,
      path: "/exhibitor-portal/meetings",
    },
    {
      label: "Upload",
      Icon: <AiOutlineCloudUpload className=" text-3xl text-BlueLighter" />,
      path: "/exhibitor-portal/upload",
    },
  ];

  return (
    <AppExhibitorProtectedRoute>
      <AppAuthWrapper menuItems={menuItems} notifications={true}>
        {children}
      </AppAuthWrapper>
    </AppExhibitorProtectedRoute>
  );
};

export default layout;
