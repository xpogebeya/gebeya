"use client";
import React, { useState, useEffect } from "react";
import {
  AppButton,
  AppForm,
  AppProductAdderDesktop,
  AppProductAdderMobile,
  AppProductEditorDesktop,
  AppProductLister,

} from "@/components";
import * as Yup from "yup";
import { Field } from "@/types/register";
import { FormikValues } from "formik";
import { useFileUploader } from "@/hooks";
import { useExhibitorPortalContext } from "@/context/ExhibitorPortalContext";
import { Product } from "@/types/exhibitor";
import Typography from "@mui/joy/Typography";
import { updateExhibitorProfile, useAppSelector } from "@/utils";
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';
import axios from "axios";
import { useDispatch } from "react-redux";



const validationSchema = Yup.object().shape({
  title: Yup.string().required().label("Title"),
  description: Yup.string().required().label("Description"),
  image: Yup.string(),
  categorie: Yup.array()
    .of(Yup.string().required("Category is required"))
    .min(1, "At least one category is required"),
});

const initialValues: FormikValues = {
  title: "",
  description: "",
  image: "",
  categorie: [""],
};

const fields: Field[] = [
  {
    name: "title",
    label: "Title",
    required: true,
  },
  {
    name: "description",
    label: "Description",
    required: true,
  },
];

const MultipleFields: Field = {
  name: "categorie",
  label: "Categories",
  required: true,
};

const page = () => {
  const {
    setError,
    editing,
    adding,
    setAdding,
    setEditing,
    editingProduct,
    setEditingProduct,
    error
  } = useExhibitorPortalContext();
  console.log("editingProduct", editingProduct);

  const { file, setFile, upload } = useFileUploader();
  const [uploading, setUploading] = useState<boolean>(false);
  const dispatch = useDispatch()
  const { searchQuery } = useAppSelector((state) => state.userClients);


  const { currentUser } = useAppSelector(state => state.ExhibtorAuth)
  
  const filteredProducts = currentUser?.products
    ? currentUser?.products.filter((product) => {
        const searchLower = searchQuery.toLowerCase();    
        // const categoryLower = product.categorie.map((c) => c.toLowerCase());
        return (
          product.title.toLowerCase().includes(searchLower) ||
          product.description.toLowerCase().includes(searchLower)
          // ||
          // categoryLower.some((category) => category.includes(searchLower))
         
        );
      })
    : [];


  const handleSubmit = async (values: FormikValues) => {
    setUploading(true)
    const url = await upload()
    if (url) {
      const { data, status } = await axios.put("/api/exhibitor/add-product", {
        data: {
          categorie: values.categorie,
          description: values.description,
          image: url,
          title: values.title,
          imageFileName: file ? file[0].name : "",
          exhibitorId: currentUser?.id
        }
      })


      if (currentUser) {
        dispatch(updateExhibitorProfile({
          ...currentUser, products: [...currentUser.products, {
            categorie: values.categorie,
            description: values.description,
            image: url,
            title: values.title,
            id: data.data.id
          }]
        }))
        setUploading(false)
      }
    }

  };

  const handleDelete = async (product: Product | null) => {
    if (product) {
      setUploading(true)

      const newProducts = currentUser?.products.filter((item) => item.id !== product.id);
      if (currentUser && newProducts) {
        try {
          await axios.post("/api/exhibitor/add-product", {
            data: {
              id: product.id
            }
          })
          dispatch(updateExhibitorProfile({
            ...currentUser, products: newProducts
          }))
          setError("")

        } catch (error) {
          setError("Something went wrong")
        }

      }

      if (editing) {
        setEditing(false);
      }
      setUploading(false)

    }
  };

  const handleEdit = (product: Product | null) => {
    if (product) {
      const productWithCategoryNames = {
        ...product,
        categorie: Array.isArray(product.categorie) && typeof product.categorie[0] === 'string'
        ? product.categorie: product.categorie.map((category: any) => category.name)
      };
      setEditing(true);
      setAdding(false);
      setEditingProduct(productWithCategoryNames);
    }
  };

  // const Categories = ["Auto", "Construction", "Beauty", "Cash"];
  const [Categories, setCategories] = useState([]);
  useEffect(() => {
    const categoriesFunction = async () => {
      try {
        const response = await fetch("/api/exhibitor/get-categorie");
        if (!response.ok) {
          throw new Error("Failed to fetch categories");
        }
        const data = await response.json();
        const modifiedData = data.data.map((item :any) => item.name);
        setCategories(modifiedData);
      } catch (error) {
        console.error("Error fetching categories:", error);
      }
    };
    categoriesFunction();
  }, []);
    let categoryNamesEdit: string[] = Categories ?? []
  return (
    <div className="-mt-16 md:-mt-32">
      <p className=" text-center text-red-500">{error}</p>
      <div className=" md:px-2 mx-auto mr-5  ">
        <div className=" flex justify-between items-center mb-5 flex-row w-full gap-2 ">
          <Typography level="h2" className=" text-2xl  text-White">
            Products
          </Typography>
          <AppButton
            label="Add New Product"
            handleAction={() => {
              setEditing(false);
              setAdding(true);
            }}
            className=" bg-white  text-BlueLighter"

          />
        </div>
        <div className="h-[80vh] overflow-y-auto mx-auto">
          {
            uploading && <Box sx={{ width: '70%' }} className=" mb-5 mx-auto">
              <LinearProgress color="info" />
            </Box>
          }
          <AppForm
            initialValues={initialValues}
            onSubmit={handleSubmit}
            validationSchema={validationSchema}
          >
            <AppProductAdderMobile
              fields={fields}
              openModal={adding}
              multipleField={MultipleFields}
              options={Categories}
              setFile={setFile}

            />
            <div className=" flex flex-col md:flex-row justify-between gap-10 ">
              <div className=" flex flex-wrap">
                {
                  filteredProducts.length == 0 && <div className=" flex flex-col justify-center items-center w-[100%] mx-auto">
                    <div className=" w-full mx-auto  h-[50vh] p-10 flex flex-col items-center justify-center">
                      <h3 className=" text-base lg:text-2xl text-gray-400 font-bold">
                        {
                          searchQuery ? "No product found" : "You don't have a product yet"
                        }
                      </h3>

                    </div>
                  </div>
                }
                {filteredProducts.length > 0 && filteredProducts.map((product, index) => {
                  const categoryNames = product.categorie.map((category :any) => category.name);
                  return (
                    <AppProductLister
                      key={index}
                      product={product}
                      onEdit={handleEdit}
                      onDelete={handleDelete}
                      fields={fields}
                      multipleField={MultipleFields}
                      options={categoryNames}
                    />
                  )
                })}
              </div>
              {editing && (
                <AppProductEditorDesktop
                  setUploading={setUploading}
                  multipleField={MultipleFields}
                  // @ts-ignore
                  product={editingProduct}
                  setEditing={setEditing}
                  fields={fields}
                  options={categoryNamesEdit}
                  // @ts-ignore
                  initialValues={editingProduct}
                />
              )}
              {adding && (
                <div>
                  <AppProductAdderDesktop
                    fields={fields}
                    multipleField={MultipleFields}
                    options={Categories}
                    setFile={setFile}
                  />
                </div>
              )}
            </div>
          </AppForm>
        </div>
      </div>
    </div>
  );
};

export default page;

