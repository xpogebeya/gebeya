"use client";
import React, { useEffect, useState } from "react";
import { AppForm, AppFormField, AppSubmitButton, Pargraph, Progress } from "@/components";
import { FormikValues } from "formik";
import { Field } from "@/types/register";
import { AiOutlineLogin } from "react-icons/ai";
import { useAPI } from "@/hooks";
import * as Yup from "yup";
import { useRouter } from "next/navigation";
import {
  FormControl,
  FormControlLabel,
  FormGroup,
  Switch,
} from "@mui/material";

import axios from "axios";
import Cookies from "js-cookie";


const validationSchema = Yup.object().shape({
  password: Yup.string().required().min(8).label("Password"),
  email: Yup.string().required().email().label("Email"),
});

const initialValues: FormikValues = {
  email: "",
  password: "",
};
const FieldValue: Field[] = [
  {
    name: "email",
    label: "Email",
    required: true,
  },
  {
    name: "password",
    label: "Password",
    required: true,
    type: "password",
  },
];

const AppLoginForm = () => {
  const [fieldError, setFieldError] = useState<boolean>(false);
  const { message, progress, setSubmit, submit, error, status, setMessage } =
    useAPI();

  const router = useRouter();


  const handleSubmit = async (val: FormikValues) => {

    const { data } = await axios.post('/api/exhibitor/sign-in', {
      data: val
    })

    if (data.allow) {
      router.push(data.path)
      Cookies.set("exhibitor", data.user.id, { expires: 7 })
    } else {
      if (data.path) {
        router.push(`${data.path}?email=${val.email}`)
      }
      setSubmit(false)
      setMessage(data.message)
    }
  }

  const exhibitor = Cookies.get("exhibitor")
  useEffect(() => {
    if (exhibitor) {
      router.push("/exhibitor-portal/profile")
    }
  }, [exhibitor])

  return (
    <div className=" h-screen flex flex-col  gap-5 items-center justify-center max-w-[1200px] mx-auto">

      {submit ? (
        <Progress
          process={progress}
          message={message}
          error={error}
          status={status}
          URL={""}
        />
      ) : (
        <>
          <div className=" antialiased  w-[90%] xs:w-[80%]  sm:w-[75%] xss:w-[60%] lg:w-[55%] mx-auto border-dashed border-2 h-[60vh] mt-10 flex flex-col py-5">
            <div className=" xs:w-[85%] mx-auto flex flex-col ">
              <div className=" pb-0 mb-0 p-0 md:py-6 ">
                <h4 className="font-bold text-xl ">Sign In</h4>
                <p className="mb-0 opacity-60">
                  Enter your email and password to sign in
                </p>
              </div>

              <AppForm
                initialValues={initialValues}
                onSubmit={(val: FormikValues) => handleSubmit(val)}
                validationSchema={validationSchema}
              >
                {fieldError && (
                  <Pargraph
                    lable={"Please fill all required field"}
                    className=" text-red-600 text-center mt-5"
                  />
                )}
                {message && (
                  <Pargraph
                    lable={message}
                    className=" text-red-600 text-center mt-5"
                  />
                )}
                <div className=" flex flex-col mt-10 gap-6">
                  {FieldValue.map((item, index) => (
                    <AppFormField
                      key={item.name}
                      required={item.required}
                      name={item.name}
                      type={item.type}
                      label={item.label}
                      className=" min-w-[300px] xs:min-w-[400px] "
                    />
                  ))}


                  <AppSubmitButton
                    title={"Sign in"}
                    setProcess={setSubmit}
                    setError={setFieldError}
                    Icon={<AiOutlineLogin className=" text-White text-xl" />}
                    className="min-w-[300px] xs:min-w-[400px]  w-full bg-BlueLighter text-white"
                  />
                  <div className=" flex justify-between items-center">

                    <p
                      className=" cursor-pointer hover:underline  hover:text-BlueLighter hover:font-bold"
                      onClick={() => router.push("/exhibitor-register")}
                    >
                      Don't have an account?
                    </p>
                    <p
                      className=" cursor-pointer hover:underline  hover:text-BlueLighter hover:font-bold"
                      onClick={() => router.push("/exhibitors-forgot-password")}
                    >
                      Forgot password
                    </p>
                  </div>

                </div>
              </AppForm>
            </div>
            {/* <div className=" flex  flex-col items-center mt-5 gap-2">
              <p className=" text-gray-400 ">or</p>
              <Button
                startDecorator={<AiOutlineGoogle />}
                className=" bg-BlueLighter text-White hover:text-gray-300"
                onClick={() => signIn()}
              >Sign in with Google</Button>
            </div> */}
          </div>
          <p className=" text-center text-gray-500">
            Copyright © GebeyaExpo {new Date().getFullYear()}.
          </p>
        </>
      )}
    </div>
  );
};

export default AppLoginForm;
