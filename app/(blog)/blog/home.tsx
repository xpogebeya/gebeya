import Link from "next/link";
import Container from "@/components/container";
import PostList from "@/components/postlist";
import { AiFillStar } from "react-icons/ai";
import { useEffect, useState } from "react";
import AppBlogHero from "@/components/AppBlogHero";


export default function Post({ posts }: any) {

  const [currentIndex, setCurrentIndex] = useState(0);
  const postsPerPage = 1;

  const changePage = (index: number) => {
    if (currentIndex < 4) {

      setCurrentIndex(index);
    } else {
      setCurrentIndex(0);

    }
  };
  useEffect(() => {
    const interval = setInterval(() => {
      setCurrentIndex((prevIndex) => {
        const nextIndex = (prevIndex + 1) % Math.min(4, posts.length); // Ensure currentIndex doesn't exceed 4 elements
        return nextIndex;
      });
    }, 5000);

    return () => clearInterval(interval);
  }, [posts.length]);





  return (
    <>
      <Container>
        <div className=" py-5 mt-32">
          <div className="  overflow-y-clip">
            <div className="grid gap-10 lg:gap-10 ">
              {posts.slice(currentIndex, currentIndex + postsPerPage).map((post: any) => (
                <AppBlogHero
                  key={post._id}
                  post={post}
                  aspect="landscape"
                  preloadImage={true}
                />
              ))}
            </div>
            <div className="p-4 space-y-2 ">
              <div className="flex max-w-xs space-x-3 mx-auto items-center justify-center">
                {[...Array(Math.ceil(4))].map((_, index) => (
                  <span
                    key={index}
                    className={`w-12 h-2 rounded-sm cursor-pointer ${Math.floor(currentIndex / postsPerPage) === index ? ' bg-BlueLighter' : ' bg-BlueLighter/40'}`}
                    onClick={() => changePage(index * postsPerPage)}
                  ></span>
                ))}
              </div>
            </div>
          </div>

        </div>
        <div className=" mb-6 mx-auto w-full px-4 text-center lg:w-[85%] flex  items-center gap-5 mt-5 ">
          <div className=" h-[1px] w-1/2 bg-gray-400"></div>
          <div>
            <p className="  text-BlueLight text-2xl  flex flex-col  items-center justify-center">
              <AiFillStar />{" "}
            </p>

            <h2 className=" text-BlueDark antialiased tracking-normal text-xl  lg:text-4xl font-semibold leading-[1.3] text-blue-gray-900 mb-3">
              <span className=" text-BlueLighter">
                Trending Articles
              </span>
            </h2>
            <p className="block antialiased  text-base font-normal leading-relaxed text-gray-500">
              Check the latest news
              {" "}
            </p>
          </div>
          <div className=" h-[1px] w-1/2 bg-gray-400"></div>
        </div>
        <div className="grid gap-10 md:grid-cols-2 lg:gap-10 ">
          {posts.slice(4, 6).map((post: any) => (
            <PostList

              key={post._id}
              post={post}
              aspect="landscape"
              preloadImage={true}
            />
          ))}
        </div>
        <div className="mt-10 grid gap-10 md:grid-cols-2 lg:gap-10 xl:grid-cols-3 ">
          {posts.slice(6, 14).map((post: any) => (
            <PostList key={post._id} post={post} aspect="square" />
          ))}
        </div>
        <div className="mt-10 flex justify-center">
          <Link
            href="/blog/archive"
            className="relative inline-flex items-center gap-1 rounded-md border border-gray-300 bg-white px-3 py-2 pl-4 text-sm font-medium text-gray-500 hover:bg-gray-50 focus:z-20 disabled:pointer-events-none disabled:opacity-40  hover:bg-BlueLighter/10">
            <span>View all Posts</span>
          </Link>
        </div>
      </Container >
    </>
  );
}
