"use client"

import { Suspense } from "react";
import Container from "@/components/container";
import AppBlogCategory from "@/components/Category";
import Loading from "@/components/loading";

export const dynamic = "force-dynamic";
export const runtime = "edge";

export default async function CategoryPage({ params }: any) {
    return (
        <>
            <Container className="relative mt-20">
                <h1 className="text-center text-3xl font-semibold tracking-tight capitalize  text-BlueLighter/30 lg:text-4xl lg:leading-snug">
                    {params.category}
                </h1>
                <Suspense
                    key={params.category || "1"}
                    fallback={<Loading />}>
                    <AppBlogCategory searchParams={params} />
                </Suspense>
            </Container>
        </>
    );
}

// export const revalidate = 60;
