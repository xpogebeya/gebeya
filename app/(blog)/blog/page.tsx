'use client'

import { useEffect, useState } from "react";
import HomePage from "./home";
import { getAllPosts } from "@/sanity/client";
import { CircularProgress } from "@mui/material";

export default async function IndexPage() {
  const [posts, setPosts] = useState()
  const get = async () => {
    const posts = await getAllPosts();
    setPosts(posts)
  }
  useEffect(() => {
    get()
  }, [])
  return <div>
    {
      posts ?
        <HomePage posts={posts} /> :
        <div>
          <div className=" h-[100dvh] flex  flex-col items-center justify-center">
            <CircularProgress />
          </div>
        </div>
    }
  </div>;
}

// export const revalidate = 60;
