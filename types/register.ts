import { CountryType } from "@/constants/country";

export type Field = {
  name: string;
  label: string;
  required: boolean;
  type?: string;
  options?: string[] 
  api?:string
};
