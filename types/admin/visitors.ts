export type VisitorsType = {
  firstName: string;
  lastName: string;
  middleName?: string | undefined;
  email: string;
  phone: string;
  sex: string;
  companyName: string;
  jobTitle: string;
  telephone?: string | undefined;
};

export type MeetingType = {
  fullName: string;
  email: string;
  phone: string;
  message?: string;
  id: string;
};
