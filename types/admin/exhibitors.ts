export interface Customer {
  name: string;
  companyName: string;
  emailAddress: string;
  vatNumber: string;
}

export interface ApplicationInfo {
  name: string;
  amount: number;
}
