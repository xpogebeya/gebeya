type Permission = "Blogger" | "Ticketer" | "Admin";

interface ClientUsers {
  fullName: string;
  id?: string;
  email?: string;
  type: Permission[];
  assinedBy: string;
}

export default ClientUsers;
