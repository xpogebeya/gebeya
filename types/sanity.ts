export interface HeroType {
  _id: string;
  header: string;
  imageUrls: string[];
  startDate: string;
  subHeaderAbove: string;
  subHeaderBelow: string;
  backgroundImageUrl: string;
  videoId: string;
}

export interface SocialLinks {
  facebook?: string;
  instagram?: string;
  linkedin?: string;
  telegram?: string;
  youtube?: string;
}

export interface ContactInfo {
  phone: string[];
  location: string;
  _key: string;
}

export interface HomePageData {
  exhibitionName: string;
  description: string;
  whyExhibitImages: {
    title: string;
    exhibitImage: string;
    id: string;
  }[];
  facts: {
    title: string;
    number: number;
    shortDescription: string;
  }[];
  offerDescription: string;
  photoGalleryDescription: string;
  photoGalleryImages: string[];
  Partners: {
    partnerImage: string;
    partnerLevel: string;
  }[];
  socialLinks: SocialLinks[];
  ContactInfo: ContactInfo[];
  email: string;
}
export interface WhyExhibit {
  header: string;
  reasonToVist: {
    title: string;
    desc: string;
    imageUrl: string;
  }[];
  secondHeader: string;
  showCases: {
    title: string;
    imageUrl: string;
  }[];
  professionals: {
    image: string;
    lists: string[];
    title: string;
    desc: string;
  };
  buyers: {
    image: string;
    lists: string[];
    title: string;
    desc: string;
  };
}
interface packages {
  title: string;
  lists: {
    title: string;
    pack: string[];
  }[];
}

export type PlanProps = {
  plan: string;
  price: string;
  packages: packages[];
  features: string[];
};

export interface WhyVisit {
  header: string;
  reasonToVist: {
    title: string;
    desc: string;
    imageUrl: string;
  }[];
  secondHeader: string;
  showCases: {
    title: string;
    imageUrl: string;
  }[];
  piechart: {
    id: number;
    year: number;
    userGain: number;
    userLost: string;
  }[];
}

export interface Sponsors {
  header: string;
  subHeader: string;
  rightSideHeader: string;
  rightSideDescription: string;
  leftSideList: {
    title: string;
    description: string;
  }[];
  steps: {
    title: string;
    description: string;
  }[];
  faq: {
    title: string;
    description: string;
  }[];
  packages: PlanProps[];
}

export interface AboutUs {
  factAboutXpo: {
    title: string;
    description: string;
  }[];
  subHeader: string;
  backgroundOfCompany: string;
  leftSideList: {
    title: string;
    description: string;
  }[];
  teams: {
    name: string;
    position: string;
    image: string;
  }[];
}

export interface SponsorsType {
  id: string;
  fullName: string;
  email: string;
  message: string;
  plan: string;
  phoneNumber?: string;
}

export interface Inform {
  id: string;
  fullName: string;
  email: string;
  phone: string;
  telePhone?: string;
}

export interface Medias {
  id: string;
  firstName: string;
  lastName: string;
  middleName?: string | null;
  email: string;
  phone: string;
  sex: string;
  mediaName: string;
  logo?: string | null;
  telephone?: string | null;
  social: SocialLinks[];
}

export interface MediaCard {
  email: string;
  firstName: string;
  id: string;
  lastName: string;
  logo: string;
  mediaName: string;
  middleName: string;
  phone: string;
  sex: string;
  telephone: string;
  social: {
    faceBookLink: string;
    instagram: string;
    linkedinLink: string;
    telegramChannel: string;
    tikTokLink: string;
    tvChannelName: string;
    websiteLink: string;
    youTubeLink: string;
    id?: string;
  }[];
}
