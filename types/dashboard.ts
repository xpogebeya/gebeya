import { ReactNode } from "react";



export type User = {
  image: string;
};

export type MenuListType = {
  label: string;
  Icon: ReactNode;
  path: string;
};
