/**
 * @format
 * @type {import('tailwindcss').Config}
 */

module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./containers/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      width: {
        "screen-vw": "100vw",
        "screen-08-vw": "80vw",
      },
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
        "xpo-image": "url('/logo-08.png')",
      },
      colors: {
        BlueDark: "#0c415c",
        BlueLight: "#32b9ec",
        BlueLighter: "#0878ab",
        BluePurple: "#8d80d5",
        White: "#e6f1ff",
        GrayWHite: "#f2f2f2",
        textGray: "rgb(123, 128, 154",
      },
      screens: {
        _x: "360px",
        x: "500px",
        xs: "600px",
        xld: "765px",
        xss: "790px",
        xls: "1200px",
        xll: "1818px",
      },
      fontFamily: {
        Bentham: "Bentham",
        Roboto: "Roboto",
      },
    },
  },
  plugins: [],
  important: true,
};
