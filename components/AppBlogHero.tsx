import Image from "next/image";
import Link from "next/link";
import { cx } from "@/utils/all";
import { urlForImage } from "@/sanity/image";
import { parseISO, format } from "date-fns";
import { PhotoIcon } from "@heroicons/react/24/outline";
import CategoryLabel from "@/components/blog/category";
interface Category {
    slug: {
        current: string;
    };
    title: string;
    color: string;
}
export interface Post {
    mainImage?: {
        src: string;
        blurDataURL?: string;
        alt?: string;
    };
    author?: {
        image?: {
            src: string;
        };
        name?: string;
        slug?: {
            current: string;
        };
    };
    categories?: Category[];
    slug: {
        current: string;
    };
    title: string;
    excerpt?: string;
    publishedAt?: string;
    _createdAt?: string;
}

interface PostListProps {
    post: Post;
    aspect?: string;
    minimal?: boolean;
    pathPrefix?: string;
    preloadImage?: boolean;
    fontSize?: "large" | "small";
    fontWeight?: "normal" | "bold";
}

export default function PostList({
    post,
    aspect,
    minimal,
    pathPrefix,
    preloadImage,
    fontSize,
    fontWeight,
}: PostListProps) {


    const imageProps = post?.mainImage ? urlForImage(post.mainImage) : null;


    return (
        <>
            <div
                className={cx(
                    "group cursor-pointer",
                    minimal && "grid gap-10"
                )}>
                <div
                    className={cx(
                        " overflow-hidden rounded-md transition-all   dark:bg-gray-800"
                    )}>
                    <Link
                        className={cx(
                            "relative block",
                            aspect === "landscape"
                                ? "aspect-video"
                                : aspect === "custom"
                                    ? "aspect-[5/5]"
                                    : "aspect-square"
                        )}
                        href={`/blog/post/${pathPrefix ? `${pathPrefix}/` : ""}${post.slug.current
                            }`}>
                        {imageProps ? (
                            <Image
                                src={imageProps.src}
                                alt={post.mainImage?.alt || "Thumbnail"}
                                loading="eager"
                                fill
                                sizes="100vw"
                                className="object-cover"
                            />
                        ) : (
                            <span className="absolute left-1/2 top-1/2 h-16 w-16 -translate-x-1/2 -translate-y-1/2 ">
                                <PhotoIcon />
                            </span>
                        )}
                    </Link>
                </div>

                <div className={cx(minimal && "flex items-center text-center")}>
                    <div>
                        <CategoryLabel
                            categories={post.categories}
                            nomargin={minimal}
                        />
                        <h2
                            className={cx(
                                fontSize === "large"
                                    ? "text-2xl"
                                    : minimal
                                        ? "text-3xl"
                                        : "text-lg",
                                fontWeight === "normal"
                                    ? "line-clamp-2 font-medium  tracking-normal text-gray-500"
                                    : "font-semibold leading-snug tracking-tight",
                                "mt-2 "
                            )}>
                            <Link
                                href={`/blog/post/${pathPrefix ? `${pathPrefix}/` : ""}${post.slug.current
                                    }`}>
                                <span
                                    className=" text-4xl bg-gradient-to-r  from-BlueLighter/50  to-BlueLighter/20 bg-[length:0px_20px] bg-left-bottom bg-no-repeat transition-[background-size] duration-500 hover:bg-[length:100%_10px] group-hover:bg-[length:100%_20px]">
                                    {post.title}
                                </span>
                            </Link>
                        </h2>

                        <div className="hidden">
                            {post.excerpt && (
                                <p className="mt-2 line-clamp-3 text-sm text-gray-500 dark:text-gray-400">
                                    <Link
                                        href={`/blog/post/${pathPrefix ? `${pathPrefix}/` : ""
                                            }${post.slug.current}`}>
                                        {post.excerpt}
                                    </Link>
                                </p>
                            )}
                        </div>

                        <div className="mt-3 flex items-center space-x-3 text-gray-500 dark:text-gray-400">

                            <span className="text-xs text-gray-300 dark:text-gray-600">
                                &bull;
                            </span>
                            <time
                                className="truncate text-sm"
                                dateTime={post?.publishedAt || post._createdAt}>
                                {format(
                                    parseISO(post?.publishedAt || post._createdAt || ""),
                                    "MMMM dd, yyyy"
                                )}
                            </time>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
