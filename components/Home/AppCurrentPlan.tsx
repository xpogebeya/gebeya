import Button from '@mui/joy/Button'
import { useRouter } from 'next/navigation'
import React from 'react'


interface packages {
  title: string
  lists: {
    title: string
    pack: string[]
  }[]
}
interface Sponsorship {
  name: string
  price: string
  packages: packages[]
  handleSelect: (price: string, plan: string) => void
}
const AppCurrentPlan = ({ name, packages, price, handleSelect }: Sponsorship) => {

  return (
    <div className='justify-center items-center' id='detail'>
      <section className=" ">
        <div className="container max-w-5xl lg:px-4 md:pt-12 mx-auto">
          <div className="grid gap-4 mx-4 sm:grid-cols-12">
            <div className="col-span-12 sm:col-span-3">
              <div className="text-center sm:text-left lg:mb-14 mb-5 before:block before:w-24 before:h-3 before:mb-5 before:rounded-md before:mx-auto sm:before:mx-0 before:bg-BlueLighter">
                <h3 className=" text-xl lg:text-3xl font-semibold text-BlueDark">{name}{" "}Sponsorships</h3>
                <span className="text-sm font-bold tracki uppercase text-gray-600 ">{!Number.isNaN(parseInt(price)) && parseInt(price).toLocaleString()}</span>
                <br />
                <button
                  className="mt-3 w-full bg-BlueLighter bg-opacity-5 hover:bg-BlueLighter hover:bg-opacity-20 focus:outline-none transition duration-150 ease-in-out rounded text-BlueLighter px-8 text-base font-semibold py-3"
                  onClick={() =>
                    handleSelect(price, name)
                  }
                >
                  Choose
                </button>

              </div>
            </div>
            <div className="relative col-span-12 md:px-4 space-y-6 sm:col-span-9  overflow-y-auto pb-10 md:pl-3">
              <div className="col-span-12 space-y-12 relative px-4 sm:col-span-8 sm:space-y-8 sm:before:absolute sm:before:top-2 sm:before:bottom-0 sm:before:w-0.5 sm:before:-left-3 before:dark:bg-gray-700 ml-4">
                {
                  packages.map((pack) => (
                    <div
                      key={pack.title}
                      className="flex flex-col sm:relative sm:before:absolute sm:before:top-2 sm:before:w-4 sm:before:h-4 sm:before:rounded-full sm:before:left-[-35px] sm:before:z-[1] before:bg-BlueLighter">
                      <h3 className="text-xl font-semibold tracki">{pack.title} </h3>
                      {
                        pack.lists.map((list) => (
                          <div key={list.title}>
                            <p className="mt-3 text-xl ">{list.title}</p>
                            <ul className=' flex flex-col gap-2'>

                              {
                                list.pack.map((pack) => (
                                  <li className=' text-gray-600 pl-5' key={pack}>
                                    {pack}
                                  </li>
                                ))
                              }
                            </ul>
                          </div>
                        ))
                      }
                    </div>
                  ))
                }

              </div>
            </div>
          </div>
        </div>
      </section>

    </div>
  )
}

export default AppCurrentPlan