/** @format */

import React from "react";
import { useState } from "react";
import { PieChart } from "..";

type User_Data = {
  id: number;
  year: number;
  userGain: number;
  userLost: string;
};


const NetworkStatistics = ({UserData}:{UserData:User_Data[]}) => {

  const sum = UserData.reduce((total, item) => total + item.userGain, 0);
  const labels = UserData.map((data) => data.userLost);

  const [userData, setUserData] = useState({
    labels: labels,
    datasets: [
      {
        label: "percentile",
        data: UserData.map((data) => data.userGain),
        backgroundColor: [
          "rgba(75,192,192,1)",
          "#ecf0f1",
          "#50AF95",
          "#f3ba2f",
          "#2a71d0",
        ],
        borderColor: "black",
        borderWidth: 2,
      },
    ],
  });
  return (
    <div className=" md:ml-28 pt-20 md:pt-36 x:ml-12 lg:ml-20 xl:ml-44">
      <p className="text-2xl md:text-3xl lg:text-4xl px-8 md:px-0 text-BlueDark">
        Network with construction’s who’s who
      </p>
      <div className=" flex flex-col md:flex-row  lg:ml-10 xl:ml-28 pb-20 lg:py-20">
        <div className="ml-10 w-2/3 md:ml-0 lg:w-1/3 xl:w-1/4">
          <PieChart chartData={userData} />
        </div>

        <ul className=" pt-8 md:pt-20 list-disc ml-20  md:ml-4 lg:ml-20 xl:ml-96">
          {UserData.map(({ userLost, userGain }) => (
            <li className="pt-1 xl:pt-3 text-xl" key={userLost}>
              {`${userLost}   ${((userGain * 100) / sum).toFixed(2)}%`}
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default NetworkStatistics;
