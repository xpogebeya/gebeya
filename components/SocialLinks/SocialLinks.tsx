/** @format */

import React from "react";
import Link from "next/link";
import { BsFacebook, BsInstagram } from "react-icons/bs";
import {
  AiFillYoutube,
  AiOutlineTwitter,
  AiOutlineWhatsApp,
} from "react-icons/ai";
import { FaInstagramSquare, FaViber } from "react-icons/fa";
const SocialLinks = () => (
  <div className="">
    <p className="text-BlueDark mb-5">Get In Touch</p>
    <div className=" flex flex-wrap gap-5 x:w-3/4  ">
      <Link href="">
        <div className="flex flex-col w-20 gap-1 text-4xl rounded-lg bg-BlueLight bg-opacity-20 hover:bg-opacity-30  text-blue-600  px-5 pt-2 pb-0 cursor-pointer">
          <BsFacebook />
          <p className=" text-sm text-black font-bold">2000+</p>
        </div>
      </Link>
      <Link href="">
        <div className="flex flex-col gap-2  w-20 text-4xl rounded-lg bg-BlueLight bg-opacity-20 hover:bg-opacity-30 text-red-600 px-5 pt-2 pb-0 cursor-pointer">
          <AiFillYoutube />
          <p className=" text-sm text-black font-bold">5000+</p>
        </div>
      </Link>
      <Link href="">
        <div className="flex flex-col gap-2 w-20 text-4xl rounded-lg bg-BlueLight bg-opacity-20 hover:bg-opacity-30 text-orange-500 px-5 pt-2 pb-0 cursor-pointer">
          <BsInstagram />
          <p className=" text-sm text-black font-bold">1500+</p>
        </div>
      </Link>
      <Link href="">
        <div className="flex flex-col gap-2 w-20 text-4xl rounded-lg bg-BlueLight bg-opacity-20 hover:bg-opacity-30 text-blue-500 px-5 pt-2 pb-0 cursor-pointer">
          <AiOutlineTwitter />
          <p className=" text-sm text-black font-bold">7000+</p>
        </div>
      </Link>
      <Link href="">
        <div className="flex flex-col gap-2 w-20 text-4xl rounded-lg bg-BlueLight bg-opacity-20 hover:bg-opacity-30 text-green-500 px-5 pt-2 pb-0 cursor-pointer">
          <AiOutlineWhatsApp />
          <p className=" text-sm text-black font-bold">3500+</p>
        </div>
      </Link>
      <Link href="">
        <div className="flex flex-col gap-2 w-20 text-4xl rounded-lg bg-BlueLight bg-opacity-20 hover:bg-opacity-30 text-purple-600 px-5 pt-2 pb-0 cursor-pointer">
          <FaViber />
          <p className=" text-sm text-black font-bold">7500+</p>
        </div>
      </Link>
    </div>
  </div>
);

export default SocialLinks;
