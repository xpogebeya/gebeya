import React from "react";

type Props = { lable: string; className?: string };

export default function Header({ lable, className }: Props) {
  return (
    <h1
      className={`  text-3xl lg:text-5xl lg:leading-[3.5rem] max-w-[43rem] font-semibold capitalize  ${
        className ? className : "text-BlueDark"
      }`}
    >
      {lable}
    </h1>
  );
}
