import React from "react";
import { MediaCard } from "@/types";
import { AiFillYoutube, AiFillFacebook, AiFillInstagram } from "react-icons/ai";
import {
  BsTelegram,
  BsTiktok,
  BsTv,
  BsWebcam,
  BsLinkedin,
} from "react-icons/bs";
import Link from "next/link";

interface Props {
  media: MediaCard;
}

interface SocialIcons {
  [key: string]: {
    link: string;
    Icons: JSX.Element;
  };
}

const Socials: SocialIcons = {
  youTubeLink: {
    link: "",
    Icons: <AiFillYoutube />,
  },
  tikTokLink: {
    link: "",
    Icons: <BsTiktok />,
  },
  telegramChannel: {
    link: "",
    Icons: <BsTelegram />,
  },
  websiteLink: {
    link: "",
    Icons: <BsWebcam />,
  },
  linkedinLink: { link: "", Icons: <BsLinkedin /> },
  tvChannelName: { link: "", Icons: <BsTv /> },
  faceBookLink: { link: "", Icons: <AiFillFacebook /> },
  instagram: { link: "", Icons: <AiFillInstagram /> },
};

const mapSocialLinks = (mediaCard: MediaCard): SocialIcons => {
  const socialLinks: SocialIcons = {};

  mediaCard.social.forEach((socialItem) => {
    for (const key in Socials) {
      // @ts-ignore
      if (socialItem[key as keyof typeof Socials]) {
        socialLinks[key] = {
          // @ts-ignore
          link: socialItem[key as keyof typeof Socials],
          Icons: Socials[key].Icons,
        };
      }
    }
  });

  return socialLinks;
};
const Card = ({ media }: Props) => {
  const socialLinks: SocialIcons = mapSocialLinks(media);

  return (
    <div className=" shadow-sm sm:w-72  rounded-lg overflow-hidden ">
      <div className="flex items-center justify-between p-3 bg-BlueLighter/60 dark:text-gray-100 ">
        <div className="flex items-center space-x-2">
          <div className="">
            <h2 className="text-base font-semibold ">{media.mediaName}</h2>
            <span className="inline-block text-xs leadi dark:text-gray-100">{media.firstName}{" "}{media.middleName}{" "}{media.lastName}</span>
          </div>
        </div>
      </div>
      <img
        src={media.logo ? media.logo :"/logoMain.png"}
        alt={media.mediaName}
        className="object-cover object-center w-full h-52 "
      />
      <div className="p-3 border-t border-BlueLighter border-dashed">
        <div className="flex items-center justify-between">
          <div className="flex items-center space-x-3 py-1">
            <div className="flex justify-center items-center py-3">
              {Object.keys(socialLinks).map((key) =>
                socialLinks[key].link ? (
                  <Link
                    target="_blank"
                    rel="noopener noreferrer"
                    href={socialLinks[key].link}
                    key={key}
                    className=" text-BlueLighter hover:text-BlueLighter/80 text-lg cursor-pointer">
                    <div className="mx-2 cursor-pointer">{socialLinks[key].Icons}</div>
                  </Link>
                ) : null
              )}
            </div>
          </div>

        </div>
      </div>
    </div>
  );
};

export default Card;
