"use client";
import { Field } from "@/types/register";
import { FormikValues, useFormikContext } from "formik";
import React, { FC } from "react";
import {
    AppButton,
    AppFormField,
} from "..";
type Props = {
    steps: string[];
    FieldValue: Field[][];
};

const AppVisitorRegister: FC<Props> = ({ FieldValue, steps }) => {
    const {
        dirty,
        values,
        setValues,
        initialValues,
        isValid,
        setFieldTouched,
        submitForm
    } = useFormikContext<FormikValues>();

    const handleDiscard = () => {
        setValues(initialValues);
        FieldValue.forEach((field) => (
            field.forEach((item) => (
                setFieldTouched(item.name, false)
            ))
        ))

    };


    const handleEdit = () => {
        if (dirty && isValid) {
            submitForm().then(() => {
                handleDiscard()
            })
        }
    };

    return (
        <div className=" pb-20">
            {FieldValue.map((field, index) => (
                <div className=" mt-10" key={steps[index]}>
                    <div className=" flex justify-between 2xl:w-[80%] mb-5 align-middle items-center">
                        <h3 className=" text-xl text-BlueLight font-bold  uppercase ">
                            {steps[index]}
                        </h3>

                    </div>

                    <div className=" flex flex-wrap gap-5">
                        {field.map((item) => (
                            <AppFormField
                                key={item.name}
                                required={item.required}
                                name={item.name}
                                value={values[item.name]}
                                options={item.options}
                                label={item.label}
                                className=" min-w-[300px] xs:min-w-[260px]  lg:min-w-[400px] "
                            />
                        ))}
                    </div>
                </div>
            ))}
            <div className=" flex gap-5 mt-10">
                <AppButton
                    label={"Submit"}
                    handleAction={handleEdit}
                    disabled={!dirty}
                />
                <AppButton label={"Discard"} handleAction={handleDiscard} />
            </div>
        </div>
    );
};

export default AppVisitorRegister;
