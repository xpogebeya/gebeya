"use client";

import {
    AppEditor,
    AppForm,

} from "@/components";
import { useExhibitorPortalContext } from "@/context/ExhibitorPortalContext";
import { useFileUploader } from "@/hooks";
import { Address, SocialLinks } from "@/types/exhibitor";
import { Field } from "@/types/register";
import { updateExhibitorProfile, useAppSelector } from "@/utils";
import { CircularProgress } from "@mui/material";
import axios from "axios";
import { FormikValues } from "formik";
import React, { Dispatch, SetStateAction, useState } from "react";
import { useDispatch } from "react-redux";

type Props = {
    initialValues: FormikValues;
    validationSchema: any;
    fields: Field[][];
    steps: string[];
    setUploading: Dispatch<SetStateAction<boolean>>
};

const AppExhibitorProfileEditor: React.FC<Props> = ({
    fields,
    initialValues,
    validationSchema,
    setUploading,
    steps,
}) => {
    const [submit, setSubmit] = useState<boolean>(false)
    const { setError } = useExhibitorPortalContext();

    const { currentUser } = useAppSelector((state) => state.ExhibtorAuth)
    const dispatch = useDispatch()
    const { file, setFile, upload } = useFileUploader();
    const handleSubmit = async (values: FormikValues) => {
        setUploading(true)
        const url = await upload()
        const address: Address = {
            city: values.city,
            country: values.country,
            phone: values.phones,
            streetNumber: values.streetNumber,
            telephone: values.telephone,
            zipCode: values.zipCode,

        }
        const social: SocialLinks = {
            faceBookLink: values.faceBookLink,
            instagram: values.instagram,
            linkedinLink: values.linkedinLink,
            telegramChannel: values.telegramChannel,
            tikTokLink: values.tikTokLink,
            tvChannelName: values.tvChannelName,
            websiteLink: values.websiteLink,
            youTubeLink: values.youTubeLink,
            id: currentUser?.social.id,
            exhibitorId: currentUser?.id

        }
        const profile = {
            businessName: values.businessName,
            DescriptionOfBusiness: values.DescriptionOfBusiness,
            email: values.email,
            rentedBooth: values.rentedBooth,
            image: file ? url : currentUser?.image,
            bussinessType: values.bussinessType,
            imageFileName: file ? file[0].name : ""
        }
        try {
            await axios.post("/api/exhibitor/update-profile", {
                data: { ...currentUser, address, social, ...profile }
            })
            setError("")
        } catch (error) {
            setError("SOmething went wrong")
        }
        // @ts-ignore
        dispatch(updateExhibitorProfile({ ...currentUser, address, social, ...profile }))
        setUploading(false)

    }




    return (
        <div className="   flex flex-col ">
            <AppForm
                initialValues={initialValues}
                onSubmit={(values: FormikValues) => handleSubmit(values)}
                validationSchema={validationSchema}
            >
                {submit ? (
                    <div className=" h-screen flex-col flex justify-center items-center">
                        <CircularProgress />
                    </div>
                ) : (
                    <AppEditor
                        steps={steps}
                        FieldValue={fields}
                        setFile={setFile}
                    />
                )}
            </AppForm>
        </div>
    );
};
export default AppExhibitorProfileEditor;
