import * as React from "react";
import { Theme, useTheme } from "@mui/material/styles";
import FilledInput from "@mui/material/FilledInput";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import { FormikValues, useFormikContext } from "formik";

interface Props {
  label: string;
  field: string;
  options: string[] | undefined;
  value?: string[]
}
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 400,
    },
  },
};

function getStyles(name: string, personName: string[], theme: Theme) {
  return {
    fontWeight:
      personName.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
}

export default function AppMultipleSelect({ label, field, options, value }: Props) {
  const theme = useTheme();

  const { setFieldValue, values } = useFormikContext<FormikValues>();
  const handleChange = (event: SelectChangeEvent<typeof field>) => {
    const {
      target: { value },
    } = event;
    setFieldValue(field, typeof value === "string" ? value.split(",")[0] : value[0]);
  };

  return (
    <div>
      <FormControl variant="outlined" fullWidth className="  ">
        <InputLabel
          id="demo-multiple-name-label"
          className=" p-0  pt-1"
        >
          {label}
        </InputLabel>
        <Select
          labelId="demo-multiple-name-label"
          id="demo-multiple-name"
          multiple
          fullWidth
          value={value ? value : values[field]}
          onChange={handleChange}
          input={<FilledInput  />}
          MenuProps={MenuProps}
          variant="outlined"
          className="  bg-transparent min-w-[300px] p-0"
        >
          {options &&
            options.map((name) => (
              <MenuItem
                key={name}
                value={name}
                style={getStyles(name, values[field], theme)}
              >
                {name}
              </MenuItem>
            ))}
        </Select>
      </FormControl>
    </div>
  );
}
