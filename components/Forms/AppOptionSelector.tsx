import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
} from "@mui/material";
import { FormikValues, useFormikContext } from "formik";
import React, { FC } from "react";

type Props = {
  options: string[];
  label: string;
  handleChange: (event: SelectChangeEvent) => void;
  id: string;
};

const AppOptionSelector: FC<Props> = ({ options, label, handleChange, id }) => {
  const { values } = useFormikContext<FormikValues>();

  return (
    <div>
      <Box sx={{ minWidth: 120 }}>
        <FormControl fullWidth className=" p-0">
          <InputLabel id="demo-simple-select-label" className=" left-[-13px]">{label}</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={values[id]}
            onChange={handleChange}
            variant="standard"
          >
            {options.map((opt) => (
              <MenuItem key={opt} value={opt}>
                {opt}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Box>
    </div>
  );
};

export default AppOptionSelector;
