import React, { Dispatch, SetStateAction, useState } from "react";
import * as Yup from "yup";
import { FormikValues } from "formik";
import { useAPI } from "@/hooks";
import { AppCustomForm, AppForm } from "..";
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';
import { PlanSection } from "@/app/(home)/custome-plan/page";


type Props = {
  selectedValue?: string
  setSelectedPackaging: () => void
  selectedItem?: PlanSection[]
};
const validationSchema = Yup.object().shape({
  fullName: Yup.string().required().label("Full name"),
  email: Yup.string().required().email().label("Email"),
  message: Yup.string().label("Message"),
  plan: Yup.string().label("Plan"),
  phoneNumber: Yup.string().required().label("Phone"),
});

const initialValues: FormikValues = {
  fullName: "",
  email: "",
  message: "",
  plan: "",
  phoneNumber: ""
};

const Fields = [
  {
    fieldName: "plan",
    lable: "Selected Plan",
  },
  {
    fieldName: "fullName",
    lable: "Full Name",
  },
  {
    fieldName: "email",
    lable: " Email Address",
    type: "email",
  },
  {
    fieldName: "phoneNumber",
    lable: " Phone Number",
    type: "string",
  },
];

const TextArea = {
  fieldName: "message",
  lable: "Message",
};
const AppWorkWithUs = ({ selectedValue, setSelectedPackaging, selectedItem }: Props) => {
  const { submitForm } = useAPI();
  const [uploading, setUploading] = useState<boolean>(false);

  const URL: string = "/api/home/set-sponsor";
  const handleSubmit = async (values: FormikValues) => {
    setUploading(true)
    submitForm({ ...values, plan: selectedValue, selectedItem }, URL).then(() => {
      setUploading(false)
    })

    setSelectedPackaging()


  }
  return (
    <div id="workWithUS">
      <div className=" my-20">
        <div className="mx-auto w-full px-4 text-center lg:w-6/12">
          <h2 className=" text-BlueLighter antialiased tracking-normal  text-4xl font-semibold leading-[1.3] text-blue-gray-900 mb-3">
            Ready to Work with Us
          </h2>
          <p className="block antialiased  text-xl font-normal leading-relaxed text-gray-500">
            Fill Out the Form, and We'll Respond Within 24 Hours
          </p>
        </div>
        {
          uploading && <Box sx={{ width: '70%' }} className=" mb-5 mx-auto">
            <LinearProgress color="info" />
          </Box>
        }
        <AppForm
          initialValues={initialValues}
          onSubmit={(values: FormikValues) => handleSubmit(values)}
          validationSchema={validationSchema}
        >
          <AppCustomForm
            btnLable={"Send Message"}
            Fields={Fields}
            textArea={TextArea}
            selectedValue={selectedValue}
          />
        </AppForm>
      </div>
    </div>
  );
};

export default AppWorkWithUs;
