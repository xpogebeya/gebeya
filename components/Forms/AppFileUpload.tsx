import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useDropzone } from 'react-dropzone';
import { FormikValues, useFormikContext } from 'formik';
import { useFileUploader } from '@/hooks';

interface Props {
    fieldName: string,
    label: string
    api?: string
}

const AppFileUploader: React.FC<Props> = ({ fieldName, label, api }) => {
    const [uploadedImage, setUploadedImage] = useState<string>('');
    const [loading, setLoading] = useState<boolean>(false);
    const { setFieldValue, values } = useFormikContext<FormikValues>()
    const [userExist, setUserExis] = useState<boolean>(false)
    const { file, setFile, upload } = useFileUploader()

    const onDrop = async (acceptedFiles: FileList) => {
        if (acceptedFiles.length === 0) {
            return;
        }

        setLoading(true);

        const email = values['email']
        const { data } = await axios.get(`${api}?email=${email}`)
        setUploadedImage(acceptedFiles[0].name);
        setFile(acceptedFiles)

        if (data.user !== null) {
            setUserExis(true)
        }

    };

    const { getRootProps, getInputProps } = useDropzone({
        // @ts-ignore
        onDrop,
        // @ts-ignore
        accept: 'image/*',
    });


    const uploadFile = async () => {
        if (!userExist) {
            try {
                const url = await upload()
                if (url) {
                    setFieldValue(fieldName, url)
                    setFieldValue('imageFileName', uploadedImage)
                }

            } catch (error) {
                console.error('Error uploading image to Cloudinary:', error);
            }
        }
    }


    useEffect(() => {
        uploadFile()
    }, [uploadedImage])

    return (
        <div>
            <div {...getRootProps()} className="dropzone">
                <input {...getInputProps()} />
                {loading ? (
                    <p>{uploadedImage ? uploadedImage : "Loading"}</p>
                ) : (
                    <p className=' cursor-pointer p-2 border border-gray-300 rounded-lg text-gray-400 hover:text-black'>
                        Drag 'n' drop an image as {label} here, or click to select one.
                    </p>
                )}
            </div>
        </div>
    );
};

export default AppFileUploader;
