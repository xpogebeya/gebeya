import * as React from "react";
import Box from "@mui/material/Box";
import { countries } from "@/constants/country";
import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
} from "@mui/material";
import { FormikValues, useFormikContext } from "formik";

type Props = {
  handleChange: (event: SelectChangeEvent) => void;
  id: string;
  phone?: boolean;
};

export default function AppCountrySelect({ handleChange, id, phone }: Props) {
  const { values } = useFormikContext<FormikValues>();

  return (
    <Box sx={{ minWidth: 120 }}>
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label" className=" left-[-13px]">
          Country *
        </InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={values[id]}
          onChange={handleChange}
          variant="standard"
        >
          {countries.map((opt) => (
            <MenuItem key={opt.label} value={phone ? opt.phone : opt.label}>
              <Box
                className=" flex"
                sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
              >
                <img
                  loading="lazy"
                  width="20"
                  srcSet={`https://flagcdn.com/w40/${opt.code.toLowerCase()}.png 2x`}
                  src={`https://flagcdn.com/w20/${opt.code.toLowerCase()}.png`}
                  alt=""
                  className=" object-contain"
                />
                {phone ? opt.phone : opt.label}
              </Box>
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </Box>
  );
}
