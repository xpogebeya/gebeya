/** @format */

"use client";

import React, { Dispatch, FC, SetStateAction, useEffect } from "react";
import { Field } from "@/types/register";
import {
  AppButton,
  AppFormField,
  AppImageUploader,
  AppMultipleItemsForm,
} from "..";
import { useFileUploader, useProductAdder, useTeamAdder } from "@/hooks";

type Props = {
  fields: Field[];
  openModal: boolean;
  multipleField: Field;
  options: string[];
  setFile: Dispatch<SetStateAction<FileList | null>>

};

const AppProductAdderMobile = ({
  fields,
  openModal,
  multipleField,
  options,
  setFile
}: Props) => {
  const {
    handleDiscard,
    handleSave,
    dirty,
    isValid,
    handleAddMoreProducts,
    values,
    handleReset,
  } = useProductAdder();

  useEffect(() => {
    handleReset();
  }, []);


  return (
    openModal && (
      <div className=" w-[90%] mx-auto mt-2 flex flex-col  justify-center gap-5 md:hidden mb-10">
        <h3 className=" mt-6 text-base text-BlueLight font-bold  uppercase w-[90%] mx-auto ">
          Add Your products
        </h3>
        <AppImageUploader fieldName={"image"} setFile={setFile} />
        <div className=" flex flex-wrap gap-4 w-full  mx-auto items-center">
          {fields.map((item) => (
            <AppFormField
              key={item.name}
              required={item.required}
              name={item.name}
              value={values[item.name]}
              label={item.label}
              className=" min-w-[280px]  "
            />
          ))}
          <AppMultipleItemsForm
            fieldName={multipleField.name}
            label={multipleField.label}
            options={options}
          />
        </div>
        <div className=" flex gap-5">
          <AppButton
            label="Save"
            handleAction={handleSave}
            disabled={!isValid || !dirty}
          />
          <AppButton
            label="Add more"
            handleAction={handleAddMoreProducts}
            disabled={!isValid || !dirty}
          />
          <AppButton label="Discard" handleAction={handleDiscard} />
        </div>
      </div>
    )
  );
};

export default AppProductAdderMobile;
