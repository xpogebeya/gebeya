import { ZoomIn } from "@/utils/motion";
import { motion } from "framer-motion";
import React, { useEffect } from "react";
import { AiFillAlert } from "react-icons/ai";
import { AppButton, Pargraph } from "..";
import { BiCheck } from "react-icons/bi";
import { useRouter } from "next/navigation";

type Props = {
  error?: boolean;
  message: string;
  URL?: string
};

const AppMessage: React.FC<Props> = ({ error, message, URL }) => {
  const router = useRouter()
  useEffect(() => {
    if (!error && URL) {
      setTimeout(() => {
        router.push(URL)
      }, 200)
    }
  }, []);
  return (
    <motion.div
      variants={ZoomIn(0, 0.7)}
      initial="hidden"
      whileInView="show"
      className=" flex flex-col j justify-end items-center gap-2 h-[40vh]"
    >
      {error ? (
        <AiFillAlert className="  text-red-800 text-7xl transition-all duration-500 ease-in-out" />
      ) : (
        <BiCheck className=" text-BlueLight text-7xl transition-all duration-500 ease-in-out" />
      )}
      <Pargraph
        lable={message}
        className={error ? " text-red-600 " : " text-BlueLighter"}
      />
      {
        error && <AppButton label={"Return Home"} handleAction={() => router.push("/")} />
      }
    </motion.div>
  );
};

export default AppMessage;
