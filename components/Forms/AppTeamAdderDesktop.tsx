import React, { Dispatch, FC, SetStateAction } from "react";
import Box from "@mui/joy/Box";
import Typography from "@mui/joy/Typography";
import IconButton from "@mui/joy/IconButton";
import Divider from "@mui/joy/Divider";
import Sheet from "@mui/joy/Sheet";
import CloseIcon from "@mui/icons-material/Close";
import { Field } from "@/types/register";
import { FormikValues } from "formik";
import { AppButton, AppFormField, AppImageUploader } from "..";
import { useTeamAdder } from "@/hooks";

type Props = {
  setEditing: (editing: boolean) => void;
  fields: Field[];
  initialValues: FormikValues;
  setFile: Dispatch<SetStateAction<FileList | null>>

};

const AppTeamAdderDesktop: FC<Props> = ({ setEditing, fields, setFile }) => {
  const { handleDiscard, handleSave, dirty, isValid, handleAddMoreMembers, values } = useTeamAdder();

  return (
    <div className=" h-[50vh] mt-5">
      <Sheet
        sx={{
          display: { xs: "none", sm: "initial" },

        }}
      >
        <Box sx={{ pb: 2, display: "flex", alignItems: "center" }}>
          <Typography sx={{ flex: 1 }} className=" text-White">Add new member</Typography>
          <IconButton
            variant="outlined"
            color="neutral"
            size="sm"
            onClick={() => {
              setEditing(false)
              handleDiscard()
            }}
          >
            <CloseIcon className=" text-White  hover:text-BlueLighter" />
          </IconButton>
        </Box>
        <Divider />
        <AppImageUploader fieldName="image" setFile={setFile} />

        <Divider />
        <div className="flex flex-col gap-2 mt-5">
          {fields.map((item) => (
            <AppFormField
              key={item.name}
              required={item.required}
              name={item.name}
              label={item.label}
              value={values[item.name]}

              className="min-w-[300px] xs:min-w-[200px] lg:min-w-[300px]"
            />
          ))}
        </div>
        <div className="flex gap-5 mx-auto mt-5">
          <AppButton
            label="Save"
            handleAction={() => handleSave()}
            disabled={!isValid || !dirty}
          />
          <AppButton
            label="Add more"
            handleAction={() => handleAddMoreMembers()}
            disabled={!isValid || !dirty}
          />

          <AppButton label="Discard" handleAction={handleDiscard} />
        </div>
      </Sheet>
    </div>
  );
};

export default AppTeamAdderDesktop;
