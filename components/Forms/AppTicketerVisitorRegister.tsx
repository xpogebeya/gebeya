"use client";
import {
    AppEditor,
    AppForm,
    AppVisitorRegister,

} from "@/components";
import { useExhibitorPortalContext } from "@/context/ExhibitorPortalContext";
import { useAPI, useFileUploader } from "@/hooks";
import { Address, SocialLinks } from "@/types/exhibitor";
import { Field } from "@/types/register";
import { updateExhibitorProfile, useAppSelector } from "@/utils";
import { CircularProgress } from "@mui/material";
import axios from "axios";
import { FormikValues } from "formik";
import React, { Dispatch, SetStateAction, useState } from "react";
import { useDispatch } from "react-redux";

type Props = {
    initialValues: FormikValues;
    validationSchema: any;
    fields: Field[][];
    steps: string[];
    setUploading: Dispatch<SetStateAction<boolean>>
    URL: string
};

const AppTicketerVisitorRegister: React.FC<Props> = ({
    fields,
    initialValues,
    validationSchema,
    setUploading,
    steps,
    URL
}) => {
    const { submit,submitForm } = useAPI();


    const handleSubmit = async (values: FormikValues) => {
        setUploading(true)
        submitForm(values, URL).then(() => {
            setUploading(false)
        })
    }

    return (
        <div className="   flex flex-col ">
            <AppForm
                initialValues={initialValues}
                onSubmit={(values: FormikValues) => handleSubmit(values)}
                validationSchema={validationSchema}
            >
                {submit ? (
                    <div className=" h-screen flex-col flex justify-center items-center">
                        <CircularProgress />
                    </div>
                ) : (
                    <AppVisitorRegister
                        steps={steps}
                        FieldValue={fields}

                    />
                )}
            </AppForm>
        </div>
    );
};
export default AppTicketerVisitorRegister;
