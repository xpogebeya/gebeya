import React, { FC } from "react";
import { FormikValues, useFormikContext } from "formik";
import { Autocomplete, TextField, IconButton } from "@mui/material";
import { IoMdAddCircle, IoIosRemoveCircleOutline } from "react-icons/io";

interface Props {
  fieldName: string;
  label: string;
  options: string[];
}

const AppMultipleItemsForm: FC<Props> = ({ fieldName, label, options }) => {
  const { setFieldValue, values } = useFormikContext<FormikValues>();

  const handleAddItem = () => {
    const newItems = [...values[fieldName], ""];
    setFieldValue(fieldName, newItems);
  };

  const handleRemoveItem = (index: number) => {
    if (index >= 0 && index < values[fieldName].length) {
      const newItems = [...values[fieldName]];
      newItems.splice(index, 1);
      setFieldValue(fieldName, newItems);
    }
  };

  return (
    <div>
      {values[fieldName].map((item: any, index: number) => (
        <div key={index} className="flex gap-1 mb-2">
          <Autocomplete
            options={options}
            value={item}
            onChange={(event, newValue) => {
              const newItems = [...values[fieldName]];
              newItems[index] = newValue;
              setFieldValue(fieldName, newItems);
            }}
            renderInput={(params) => (
              <TextField {...params} label={`${label} ${index + 1}`} />
            )}
            className="min-w-[260px] xs:min-w-[200px] lg:min-w-[300px]"
          />
          <div className="flex flex-col gap-0">
            {index !== 0 && (
              <IconButton
                onClick={() => handleRemoveItem(index)}
                color="error"
                size="medium"
                className="py-0"
              >
                <IoIosRemoveCircleOutline />
              </IconButton>
            )}
            {index === values[fieldName].length - 1 && (
              <IconButton
                color="primary"
                onClick={handleAddItem}
                size="medium"
                className="px-0"
              >
                <IoMdAddCircle />
              </IconButton>
            )}
          </div>
        </div>
      ))}
    </div>
  );
};

export default AppMultipleItemsForm;
