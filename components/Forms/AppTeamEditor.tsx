import { Teams } from "@/types/exhibitor";
import React, { Dispatch, FC, SetStateAction } from "react";
import Box from "@mui/joy/Box";
import Typography from "@mui/joy/Typography";
import IconButton from "@mui/joy/IconButton";
import Divider from "@mui/joy/Divider";
import Sheet from "@mui/joy/Sheet";
import CloseIcon from "@mui/icons-material/Close";
import { Field } from "@/types/register";
import { FormikValues, useFormikContext } from "formik";
import { AppButton, AppFormField, AppImageUploader } from "..";
import { useFileUploader, useTeamEditor } from "@/hooks";

type Props = {
  team: Teams | null;
  setEditing: Dispatch<SetStateAction<boolean>>;
  fields: Field[];
  initialValues: FormikValues;
  setUploading: Dispatch<SetStateAction<boolean>>;
};

const AppTeamEditor: FC<Props> = ({
  team,
  setEditing,
  fields,
  initialValues,
  setUploading
}) => {
  const { handleDiscard, handleSave, dirty, isValid, values } = useTeamEditor({
    initialValues: initialValues,
  });
  const { handleReset } = useFormikContext<FormikValues>();
  const { file, setFile, upload } = useFileUploader();
  const handleUpdate = async (values: FormikValues) => {
    setUploading(true)
    if (file && file?.length > 0) {
      const image = await upload()
      if (image) {
        handleSave({ ...values, image, imageFileName: file[0].name })
      }
    } else {
      handleSave(values)
    }

    setUploading(false)

  }
  return (
    <div className=" h-[60vh] mt-5 ">
      {team && (
        <>
          <Sheet
            sx={{
              display: { xs: "none", sm: "initial" },

            }}

          >
            <Box sx={{ pb: 2, display: "flex", alignItems: "center" }}>
              <Typography sx={{ flex: 1 }} className=" text-White">Edit</Typography>
              <IconButton
                variant="outlined"
                color="neutral"
                size="sm"
                onClick={() => {
                  handleReset()
                  setEditing(false);
                }}
              >
                <CloseIcon className=" text-White  hover:text-BlueLighter" />
              </IconButton>
            </Box>
            <Divider />
            <AppImageUploader fieldName={"image"} imageUrl={team.image} setFile={setFile} />

            <Divider />
            <div className=" flex flex-col gap-2 mt-5">
              {fields.map((item) => (
                <AppFormField
                  key={item.name}
                  required={item.required}
                  name={item.name}
                  value={values[item.name]}
                  label={item.label}
                  className=" min-w-[300px] xs:min-w-[200px]  lg:min-w-[300px] "
                />
              ))}
            </div>

            <div className=" flex gap-5 mx-auto mt-5">
              <AppButton
                label="Update"
                handleAction={() => handleUpdate(values)}
                disabled={!isValid || !dirty}
              />

              <AppButton label="Discard" handleAction={handleDiscard} />
            </div>
          </Sheet>
        </>
      )}
    </div>
  );
};

export default AppTeamEditor;
