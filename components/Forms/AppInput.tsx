import React, { ChangeEvent } from "react";
import { TextField } from "@mui/material";
import { SelectChangeEvent } from "@mui/material/Select";
import { AppCountrySelect, AppFileUploader, AppdatePicker } from "..";
import AppOptionSelector from "./AppOptionSelector";
interface AppInputProps {
  setValue: (value: string) => void;
  id: string;
  options?: string[];
  label: string;
  type?: string;
  api?: string
}

export const AppInput: React.FC<AppInputProps> = ({
  setValue,
  id,
  options,
  api,
  label,
  type,
  ...others
}: AppInputProps) => {
  const handleChange = (event: SelectChangeEvent) => {
    setValue(event.target.value as string);
  };

  if (options && options.length > 0) {
    return (
      <AppOptionSelector
        options={options}
        label={label}
        handleChange={handleChange}
        id={id}
        {...others}
      />
    );
  }
  if (id == "birthDate") {
    return <AppdatePicker fieldName={id} label={label} />;
  }
  if (id == "country") {
    return <AppCountrySelect handleChange={handleChange} id={id} {...others} />;
  }
  if (id == 'logo' || id == 'image') {
    return <AppFileUploader fieldName={id} label={label} api={api} {...others} />
  }
  return (
    <TextField
      onChange={(event: ChangeEvent<HTMLInputElement>) => {
        setValue(event.target.value);
      }}
      id={id}
      label={label}
      type={type ? type : "text"}
      variant="standard"
      fullWidth
      {...others}
      size="medium"
    />
  );
};
