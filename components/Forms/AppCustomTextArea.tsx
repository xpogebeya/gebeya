import { FormikValues, useFormikContext } from "formik";
import React, { FC } from "react";

type Props = {
  fieldName: string;
  label: string;
};

const AppCustomTextArea: FC<Props> = ({ fieldName, label }) => {
  const { setFieldValue, values } = useFormikContext<FormikValues>();

  return (
    <div className="relative w-full min-w-[200px]">
      <textarea
        rows={8}
        className="w-full h-full min-h-[100px] bg-transparent text-blue-gray-700 font-sans font-normal outline outline-0 focus:outline-0 disabled:bg-blue-gray-50 disabled:border-0 disabled:resize-none transition-all border-b placeholder-shown:border-blue-gray-200 text-sm px-px pt-5 pb-2 border-blue-gray-200 focus:border-blue-500 !resize-none border-BlueLighter"
        onChange={(event: React.ChangeEvent<HTMLTextAreaElement>) => {
          setFieldValue(fieldName, event.target.value);
        }}
        value={values[fieldName]}
      ></textarea>
      <label className="flex w-full text-gray-400 pt-5 h-full select-none pointer-events-none absolute left-0 font-normal peer-placeholder-shown:text-blue-gray-500 leading-tight peer-focus:leading-tight peer-disabled:text-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500 transition-all -top-1.5 peer-placeholder-shown:text-sm text-[11px] peer-focus:text-[11px] after:content[' '] after:block after:w-full after:absolute after:-bottom-0 left-0 after:border-b-2 after:scale-x-0 peer-focus:after:scale-x-100 after:transition-transform after:duration-300 peer-placeholder-shown:leading-[4.875] text-blue-gray-500 peer-focus:text-blue-500 after:border-blue-500 peer-focus:after:border-blue-500">
        {label}
      </label>
    </div>
  );
};

export default AppCustomTextArea;
