/** @format */

"use client";

import { useFileUploader, useImageUploader } from "@/hooks";
import AspectRatio from "@mui/joy/AspectRatio";
import { FormikValues, useFormikContext } from "formik";
import React, { useState, useCallback, Dispatch, SetStateAction } from "react";
import { useDropzone } from "react-dropzone";

type FileUploaderProps = {
  imageUrl?: string;
  fieldName: string;
  setFile:Dispatch<SetStateAction<FileList | null>>
};

const AppImageUploader: React.FC<FileUploaderProps> = ({
  imageUrl,
  fieldName,
  setFile
}) => {

  const { handleFileUpload } = useImageUploader();
  const { values } = useFormikContext<FormikValues>();

  const onDrop = useCallback(
    (acceptedFiles: FileList) => {
      const file = acceptedFiles;
      setFile(file);
      handleFileUpload(file, fieldName);
    },
    [handleFileUpload, fieldName]
  );

  const { getRootProps, getInputProps } = useDropzone({
    // @ts-ignore
    onDrop,
    // @ts-ignore
    accept: "image/*",
    multiple: false,
  });

  return (
    <div className="cursor-pointer">
      <div {...getRootProps()} className="dropzone">
        <input {...getInputProps()} />
        {imageUrl && !values[fieldName] && (
          <AspectRatio ratio="16/9">
            <img
              src={imageUrl}
              alt="Uploaded"
              style={{ maxWidth: "100%", maxHeight: "300px" }}
              className="object-contain"
            />
          </AspectRatio>
        )}
        {imageUrl && values[fieldName] && (
          <div>
            <AspectRatio ratio="16/9">
              <img
                src={values[fieldName]}
                alt="Uploaded"
                style={{ maxWidth: "100%", maxHeight: "300px" }}
                className="object-contain"
              />
            </AspectRatio>
          </div>
        )}
        {!imageUrl && !values[fieldName] && (
          <AspectRatio ratio="21/9">
            <p className="text-center">
              Drag & drop an image file here, or click to select one
            </p>
          </AspectRatio>
        )}
        {!imageUrl && values[fieldName] && (
          <div>
            <AspectRatio ratio="16/9">
              <img
                src={values[fieldName]}
                alt="Uploaded"
                style={{ maxWidth: "100%", maxHeight: "300px" }}
                className="object-contain"
              />
            </AspectRatio>
          </div>
        )}
      </div>
    </div>
  );
};

export default AppImageUploader;
