import React, { FC } from "react";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { FormikValues, useFormikContext } from "formik";
import dayjs from "dayjs";

type Props = {
  fieldName: string;
  label: string;
  isString?: boolean
};
const AppdatePicker: FC<Props> = ({ fieldName, label, isString }) => {
  const { setFieldValue, values } = useFormikContext<FormikValues>();

  const handleChange = (newValue: string | number | Date | dayjs.Dayjs | null | undefined) => {
    if (isString) {
      const formattedDate = dayjs(newValue).format("YYYY-MM-DD");
      setFieldValue(fieldName, formattedDate)
    } else {
      setFieldValue(fieldName, newValue)
    }
  }
  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <DatePicker
        onChange={(newValue) => handleChange(newValue)}
        label={label}
        value={values[fieldName]}
        slotProps={{ textField: { variant: 'standard', fullWidth: true } }}
      />
    </LocalizationProvider>
  );
};

export default AppdatePicker;
