import React from 'react'
import { AppFormField } from '../AppFormField';
import { clientUsersFields } from '@/utils';
import { AppButton } from '@/components';
import { FormikValues, useFormikContext } from 'formik';



const AppAdminUserAdder = () => {
    const { submitForm, handleReset, values } = useFormikContext<FormikValues>()
    return (
        <div>
            <div className=" w-[90%] md:w-[60%] mx-auto mt-5">
                {clientUsersFields.map((item) => (
                    <AppFormField
                        key={item.label}
                        required={item.required}
                        name={item.name}
                        options={item.options}
                        label={item.label}
                        value={values[item.name]}
                    />
                ))}

                <div className=" mt-2 w-full">

                    <AppButton
                        className=" w-full my-5 bg-BlueLighter text-white"
                        label={"Save"} handleAction={() => {
                            submitForm().then(() => {
                                handleReset()
                            })

                        }}
                    />
                </div>
            </div></div>
    )
}

export default AppAdminUserAdder