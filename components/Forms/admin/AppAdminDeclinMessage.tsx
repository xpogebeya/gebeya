
import { AppCustomTextArea } from '@/components';
import Button from '@mui/joy/Button';
import { FormikValues, useFormikContext } from 'formik';
import React from 'react'

type Props = {
    
}

const AppAdminDeclinMessage = (props: Props) => {
    const { submitForm, resetForm } = useFormikContext<FormikValues>()
    return (
        <div>
            <AppCustomTextArea
                fieldName={"reasonForDecline"}
                label={"Reason For Decline"}
            />
            <Button
                variant="soft"
                color="primary"
                onClick={() => {
                    submitForm().then(() => {
                        resetForm()
                    })
                }}
            >
                Send Message
            </Button>
        </div>
    )
}

export default AppAdminDeclinMessage