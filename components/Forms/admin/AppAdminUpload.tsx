import React, { Dispatch, FC, SetStateAction, useCallback, useState } from 'react';
import { useDropzone } from 'react-dropzone';
import { Button, Box, CircularProgress } from '@mui/material';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import { AiOutlineDelete } from 'react-icons/ai';
import { useFileUploader } from '@/hooks';
import { AppButton, AppFormField } from '@/components';
import { Field } from '@/types/register';
import { FormikValues, useFormikContext } from 'formik';



type Props = {
  file: FileList | null,
  setFile: Dispatch<SetStateAction<FileList | null>>,
  sectionOptions?:string[]
  FieldValue:Field[]
}
const AppAdminUpload: FC<Props> = ({ file, setFile,FieldValue,sectionOptions }) => {

  const { submitForm, resetForm, values, setFieldValue } = useFormikContext<FormikValues>();
  const onDrop = useCallback((acceptedFiles: any) => {
    setFile(acceptedFiles);
  }, [setFile]);

  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });



  return (
    <>
      <div className="min-w-[280px] md:h-[250px] mb-10">
        <div {...getRootProps()} className="cursor-pointer">
          <input {...getInputProps()} />
          <Button
            variant="contained"
            component="span"
            startIcon={<CloudUploadIcon />}
            className={`w-full flex flex-col h-32 md:h-[250px] min-w-0 break-words border-0 border-transparent border-solid shadow-xl dark:bg-slate-850 dark:shadow-dark-xl rounded-2xl bg-clip-border -mt-32 bg-white text-BlueDark ${isDragActive ? 'border-dashed border-2 border-blue-500' : ''
              }`}
          >
            {isDragActive ? 'Drop the files here' : 'Upload File'}
          </Button>
        </div>
        <Box mt={2}>
          {file && (
            <div className="flex justify-between items-center">
              <p>Selected File: {file[0].name}</p>
              <AiOutlineDelete
                className="text-red-600 hover:scale-105 cursor-pointer text-xl"
                onClick={() => setFile(null)}
              />
            </div>
          )}
        </Box>
        {FieldValue.map((item, index) => (
          <AppFormField
            key={item.name}
            required={item.required}
            name={item.name}
            type={item.type}
            label={item.label}
            value={values[item.name]}
            options={item.options}

            className="min-w-[300px] xs:min-w-[400px]"
          />
        ))}
        <Box mt={2}>
          <AppButton
            label={'Upload'}
            handleAction={() => {
              submitForm().then(() => {
                setFile(null)
                resetForm()
              })
            }}
            disabled={!file}
            className="w-full"
          />
        </Box>
      </div>
    </>
  );
};

export default AppAdminUpload;
