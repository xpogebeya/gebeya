import React, { FC } from "react";
import { FormikValues, useFormikContext } from "formik";
import { AppButton, AppFormField, AppdatePicker } from "..";
import { Field } from "@/types/register";

type Props = {
  fields: Field[];
  buttonLable: string;
};

const AppFormWithDatePIcker: FC<Props> = ({ fields, buttonLable }) => {
  const { values, submitForm, setFieldValue, resetForm, isValid } =
    useFormikContext<FormikValues>();
  return (
    <div>
      <div className=" flex flex-col mt-2 gap-5 ">
        {fields.map((item) => (
          <AppFormField
            key={item.name}
            required={item.required}
            name={item.name}
            value={values[item.name]}
            label={item.label}
          />
        ))}
        {/* <AppdatePicker fieldName={"date"} label={"Meeting Day *"} /> */}
        <textarea
          rows={8}
          placeholder="Message (optional)"
          className="peer w-full h-full min-h-[100px] bg-transparent text-blue-gray-700 font-sans font-normal outline outline-0 focus:outline-0 disabled:bg-blue-gray-50 disabled:border-0 disabled:resize-none transition-all border-b placeholder-shown:border-blue-gray-200 text-sm px-px pt-5 pb-2 border-blue-gray-200 focus:border-blue-500 !resize-none"
          onChange={(event: React.ChangeEvent<HTMLTextAreaElement>) => {
            setFieldValue("message", event.target.value);
          }}
          value={values["message"]}
        ></textarea>
        <AppButton
          label={buttonLable}
          disabled={!isValid}
          handleAction={() => {
            submitForm().then(() => {
              resetForm();
            });
          }}
        />
      </div>
    </div>
  );
};

export default AppFormWithDatePIcker;
