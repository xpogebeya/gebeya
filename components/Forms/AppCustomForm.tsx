import { FormikValues, useFormikContext } from "formik";
import React, { FC } from "react";
import { AppCustomTextArea, AppCustomeInput } from "..";
interface PropsField {
  fieldName: string;
  lable: string;
  type?: string;
}

interface TextAreaField {
  fieldName: string;
  lable: string;
}

type Props = {
  btnLable: string;
  Fields: PropsField[];
  textArea?: TextAreaField;
  selectedValue?: string
};

const AppCustomForm: FC<Props> = ({ btnLable, Fields, textArea, selectedValue }) => {
  const { submitForm, handleReset, isValid, dirty } =
    useFormikContext<FormikValues>();

  return (
    <div className="mx-auto mt-12 max-w-3xl px-5 text-center">
      <div className="mb-8 grid grid-cols-1 gap-8">
        {Fields.map((field) => (
          <AppCustomeInput
            key={field.fieldName}
            fieldName={field.fieldName}
            label={field.lable}
            type={field.type}
            selectedValue={selectedValue}
          />
        ))}
      </div>
      {textArea && (
        <AppCustomTextArea
          fieldName={textArea.fieldName}
          label={textArea.lable}
        />
      )}

      <button
        className="middle  disabled:bg-gray-600 none font-sans font-bold center uppercase transition-all disabled:opacity-50 disabled:shadow-none disabled:pointer-events-none text-sm py-3.5 px-7 rounded-lg bg-gradient-to-tr  bg-BlueLighter text-white shadow-md shadow-blue-500/20 hover:shadow-lg hover:shadow-blue-500/40 active:opacity-[0.85] mt-8"
        type="button"
        onClick={() => {
          submitForm().then(() => {
            handleReset();
          });
        }}
        disabled={!isValid || !dirty}
      >
        {btnLable}
      </button>
    </div>
  );
};

export default AppCustomForm;
