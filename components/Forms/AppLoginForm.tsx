"use client";
import React, { useEffect, useState } from "react";
import { AppForm, AppFormField, AppSubmitButton, Pargraph, Progress } from "..";
import { FormikValues } from "formik";
import { Field } from "@/types/register";
import { AiOutlineLogin } from "react-icons/ai";
import { useAPI } from "@/hooks";
import * as Yup from "yup";
import { useRouter } from "next/navigation";
import {
  FormControl,
  FormControlLabel,
  FormGroup,
  Switch,
} from "@mui/material";

import axios from "axios";
import Cookies from "js-cookie";
type Props = {
  allowNewAccount?: boolean;
  handleNewAccount?: string;
  URL: string;
  successURL: string;
};

const validationSchema = Yup.object().shape({
  password: Yup.string().required().min(8).label("Password"),
  email: Yup.string().required().email().label("Email"),
});

const initialValues: FormikValues = {
  email: "",
  password: "",
};
const FieldValue: Field[] = [
  {
    name: "email",
    label: "Email",
    required: true,
  },
  {
    name: "password",
    label: "Password",
    required: true,
    type: "password",
  },
];

const AppLoginForm: React.FC<Props> = ({
  allowNewAccount,
  handleNewAccount,
  URL,
  successURL,
}) => {
  const [fieldError, setFieldError] = useState<boolean>(false);
  const { message, progress, setSubmit, submit, error, status, setMessage } =
    useAPI();
  const [checked, setChecked] = React.useState(false);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setChecked(event.target.checked);
  };

  const router = useRouter();
 



  const handleSubmit = async (val: FormikValues) => {

    const { data } = await axios.post('/api/admin/sign-in', {
      data: val
    })

    if (data.allow) {
      router.push(data.path)
      Cookies.set("user", data.user.id, { expires: 7 })
    } else {
      if (data.path) {
        router.push(`${data.path}?email=${val.email}`)
      }
      setSubmit(false)
      setMessage(data.message)
    }
  }

  const user = Cookies.get("user")
  useEffect(()=>{
    if(user){
      router.push("/admin/exhibitors")
    }
  },[user])

  return (
    <>
      {submit ? (
        <Progress
          process={progress}
          message={message}
          error={error}
          status={status}
          URL={successURL}
        />
      ) : (
        <>
          <div className=" antialiased  w-[90%] xs:w-[80%]  sm:w-[75%] xss:w-[60%] lg:w-[55%] mx-auto border-dashed border-2 h-[60vh] mt-10 flex flex-col py-5">
            <div className=" xs:w-[85%] mx-auto flex flex-col ">
              <div className=" pb-0 mb-0 p-0 md:py-6 ">
                <h4 className="font-bold text-xl ">Sign In</h4>
                <p className="mb-0 opacity-60">
                  Enter your email and password to sign in
                </p>
              </div>

              <AppForm
                initialValues={initialValues}
                onSubmit={(val: FormikValues) => handleSubmit(val)}
                validationSchema={validationSchema}
              >
                {fieldError && (
                  <Pargraph
                    lable={"Please fill all required field"}
                    className=" text-red-600 text-center mt-5"
                  />
                )}
                {message && (
                  <Pargraph
                    lable={message}
                    className=" text-red-600 text-center mt-5"
                  />
                )}
                <div className=" flex flex-col mt-10 gap-6">
                  {FieldValue.map((item, index) => (
                    <AppFormField
                      key={item.name}
                      required={item.required}
                      name={item.name}
                      type={item.type}
                      label={item.label}
                      className=" min-w-[300px] xs:min-w-[400px] "
                    />
                  ))}
                  <FormControl
                    component="fieldset"
                    variant="outlined"
                    className=" self-start "
                  >
                    <FormGroup>
                      <FormControlLabel
                        control={
                          <Switch
                            checked={checked}
                            onChange={handleChange}
                            color="info"
                            inputProps={{ "aria-label": "controlled" }}
                          />
                        }
                        label="Remember me"
                      />
                    </FormGroup>
                  </FormControl>

                  <AppSubmitButton
                    title={"Sign in"}
                    setProcess={setSubmit}
                    setError={setFieldError}
                    Icon={<AiOutlineLogin className=" text-White text-xl" />}
                    className="min-w-[300px] xs:min-w-[400px]  w-full"
                  />
                  {allowNewAccount && (
                    <p
                      className=" cursor-pointer hover:underline"
                      onClick={() => router.push(handleNewAccount ?? "/")}
                    >
                      Don't have an account?
                    </p>
                  )}
                </div>
              </AppForm>
            </div>
            {/* <div className=" flex  flex-col items-center mt-5 gap-2">
              <p className=" text-gray-400 ">or</p>
              <Button
                startDecorator={<AiOutlineGoogle />}
                className=" bg-BlueLighter text-White hover:text-gray-300"
                onClick={() => signIn()}
              >Sign in with Google</Button>
            </div> */}
          </div>
          <p className=" text-center text-gray-500">
            Copyright © GebeyaExpo {new Date().getFullYear()}.
          </p>
        </>
      )}
    </>
  );
};

export default AppLoginForm;
