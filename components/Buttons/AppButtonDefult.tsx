/** @format */

"use client";

import { ZoomIn } from "@/utils/motion";
import Button from "@mui/joy/Button";
import { motion } from "framer-motion";
import React from "react";
type Props = {
  label: string;
  handleAction: () => void;
  conditionalClass?: string;
};

export default function AppButtonDefult({
  handleAction,
  label,
  conditionalClass,
}: Props) {
  return (
    <Button
      size="lg"
      className={` bg-BlueLighter hover:bg-BlueLighter/90 rounded-lg text-White  px-2 xl:px-7 py-3 text-xs ${conditionalClass}`}
      onClick={handleAction}
    >
      {label}
    </Button>
  );
}
