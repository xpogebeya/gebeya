
import Button from "@mui/joy/Button";
import React, { ReactNode } from "react";
type Props = {
  label: string;
  handleAction: () => void;
  Icon?: ReactNode;
  disabled?: boolean;
  className?: string
};

export default function AppButton({
  handleAction,
  label,
  Icon,
  disabled,
  className
}: Props) {
  return (
    <Button
      variant="soft"
      size="lg"
      onClick={handleAction}
      disabled={disabled}
      className={` disabled:bg-BlueLighter/70 disabled:cursor-not-allowed rounded-lg   px-2 xl:px-7 py-1 text-xs flex gap-2 ${className} ${!className && "text-White bg-BlueLighter hover:bg-BlueLighter/90"}`}
    >
      {label}
      {Icon}
    </Button>
  );
}
