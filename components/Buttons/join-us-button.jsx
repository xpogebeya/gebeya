/** @format */

import React from "react";

import styles from "./join-us-buttun.module.css";

const JoinUs = () => {
  return <button className={styles.button}> Join us</button>;
};

export default JoinUs;
