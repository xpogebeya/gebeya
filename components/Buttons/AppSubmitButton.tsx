import { useFormikContext } from "formik";
import AppButton from "./AppButton";
import { Dispatch, ReactNode, SetStateAction } from "react";

interface App {
  title: string;
  iconOnly?: boolean;
  setProcess: (val: boolean) => void;
  setError?: Dispatch<SetStateAction<boolean>>;
  Icon?: ReactNode;
  disabled?: boolean;
  className?:string
}
export const AppSubmitButton = ({
  title,
  iconOnly,
  setProcess,
  setError,
  Icon,
  disabled,
  className
}: App) => {
  const { handleSubmit, isValid, dirty } = useFormikContext();
  const handleClick = () => {
    if (dirty && isValid) {
      setProcess(true);
      setError && setError(false);
      handleSubmit();
    } else {
      setError && setError(true);
    }
  };

  return (
    <div  className=" w-full">
      {iconOnly ? (
        <div
          className="text-BlueLighter text-4xl cursor-pointer"
          onClick={handleClick}
        >
          {Icon}
        </div>
      ) : (
        <AppButton
          label={title}
          handleAction={handleClick}
          Icon={Icon}
          disabled={disabled}
          className={className}
        />
      )}
    </div >
  );
};
