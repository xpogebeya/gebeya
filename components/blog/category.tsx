import Link from "next/link";
import Label from "@/components/ui/label";

interface Category {
  slug: {
    current: string;
  };
  title: string;
  color: string;
}

interface CategoryLabelProps {
  categories?: Category[];
  nomargin?: boolean;
}

export default function CategoryLabel({
  categories,
  nomargin = false,
}: CategoryLabelProps) {
  return (
    <div className="flex gap-3">
      {categories?.length &&
        categories.slice(0).map((category, index) => (
          <Link
            href={`/blog/category/${category.slug.current}`}
            key={index}
          >
            <Label nomargin={nomargin} color={category.color}>
              {category.title}
            </Label>
          </Link>
        ))}
    </div>
  );
}
