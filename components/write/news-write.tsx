/** @format */
"use client";
import React, { useState } from "react";
interface CloudResult {
  public_id: string;
}
const NewsWrite = () => {
  const [publicId, setPublicId] = useState("");
  const [subtitle, setSubtitle] = useState(false);
  const subtitleHandler = () => (
    <>
      <div className="flex items-center ml-8">
        <input
          className=" p-5 focus:outline-none text-xl w-[90%]"
          placeholder="sub-title"
          type="text"
          autoFocus={true}
        />
      </div>
      <div className="ml-8">
        <textarea
          className="border-0 focus:outline-none w-[90%] h-max"
          placeholder="Description"
        />
      </div>
    </>
  );

  return (
    <div className="">
      <div className="flex">
        <img
          className="w-[70vw] h-[250px] rounded-xl object-cover pt-2"
          src="https://images.pexels.com/photos/6685428/pexels-photo-6685428.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
          alt=""
        />
        {/* <CldUploadWidget
          options={{
            sources: ["local"],
            multiple: false,
            maxFiles: 5,
            styles: {},
          }}
          onUpload={(result, widget) => {
            if (!result.event) return;
            const info = result.info as CloudResult;
            setPublicId(info.public_id);
          }}
          uploadPreset="qbkglyhr"
        >
          {({ open }) => (
            <button onClick={() => open()} className="btn btn-primary">
              upload image
            </button>
          )}
        </CldUploadWidget> */}
      </div>
      <form className="relative">
        <div className="flex items-center ml-8">
          <input
            className=" p-5 focus:outline-none text-2xl w-[90%]"
            placeholder="Title"
            type="text"
            autoFocus={true}
          />
        </div>
        <div className="ml-8">
          <textarea
            className="border-0 focus:outline-none w-[90%] h-max"
            placeholder="Description"
          />
        </div>
        {subtitle ? subtitleHandler() : null}
        <button className="absolute top-4 right-8 px-4 py-2 text-white bg-BlueDark rounded-md text-sm flex items-center">
          Publish
        </button>
      </form>
      <div className=" absolute mt-40 right-12">
        <button
          className="bg-BlueDark text-white px-4 py-2 rounded-lg "
          onClick={() => setSubtitle(!subtitle)}
        >
          subtitles
        </button>
      </div>
    </div>
  );
};

export default NewsWrite;
