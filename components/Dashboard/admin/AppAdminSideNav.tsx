'use client'
import { MenuListType } from "@/types/dashboard";
import { Avatar, Divider } from "@mui/material";
import Cookies from "js-cookie";
import Link from "next/link";
import { redirect, usePathname, useRouter } from "next/navigation";
import React, { ReactNode } from "react";
import {
  AiOutlineAppstoreAdd,
  AiOutlineCloudUpload,
  AiOutlineUserAdd,
} from "react-icons/ai";
import { BiSolidUserCircle } from "react-icons/bi";
import { BsPersonBadge } from "react-icons/bs";
import { FaFileArchive, FaPersonBooth } from "react-icons/fa";
import { GoSignOut } from "react-icons/go";
import { TfiLayoutMediaCenterAlt } from "react-icons/tfi";



type Props = {
  MenuList: MenuListType[]
  signout: () => void
};


const AppAdminSideNav = ({ MenuList, signout }: Props) => {



  const BelowMenu: MenuListType[] = [
    // {
    //   label: "Profile",
    //   Icon: <BiSolidUserCircle />,
    //   path: "/admin/profile",
    // },
    {
      label: "Sign out",
      Icon: <GoSignOut />,
      path: "/",
    },
  ];

  const router = useRouter()
  const checkActive = (path: string): boolean => {
    return path === usePathname();
  };


  return (
    <aside className=" z-50 inset-y-5  xls:block hidden fixed left-12 top-8 bg-white border shadow-sm rounded-xl h-[85vh] w-[300px]">
      <div>
        <Link href={"/"}>
          <img
            src="/logoMain.png"
            className=" h-24 w-full object-contain cursor-pointer"
          />
        </Link>
      </div>
      <Divider />
      <ul className=" flex flex-col gap-3 mt-5">
        {MenuList.map((menu) => (
          <li
            className="mt-0.5 w-full font-bold cursor-pointer"
            key={menu.label}
          >
            <Link
              className={`py-2.7 text-base hover:border-b-2 hover:border-b-BlueLight hover:text-BlueLighter  hover:bg-BlueLighter hover:bg-opacity-20  rounded-sm text-gray-400 my-0 mx-2 flex items-center whitespace-nowrap px-4 transition-colors ${checkActive(menu.path) &&
                "text-BlueLighter bg-opacity-20 bg-BlueLighter border-b-BlueLight  border-b-2 "
                }`}
              href={menu.path}
            >
              <div className="mr-2 flex items-center justify-center rounded-lg bg-center stroke-0 text-center p-2.5 text-xl">
                {menu.Icon}
              </div>
              <span className="ml-1 duration-300 opacity-100 pointer-events-none ease">
                {menu.label}
              </span>
            </Link>
          </li>
        ))}
      </ul>
      <Divider className=" mt-10" />
      <ul className=" flex flex-col gap-3 mt-5 ">
        {BelowMenu.map((menu) => (
          <li
            className="mt-0.5 w-full font-bold cursor-pointer hover:border-b-2 hover:border-b-BlueLight   hover:bg-BlueLighter hover:bg-opacity-20"
            key={menu.label}
          >
            <button
              className="py-2.7 text-base hover:text-BlueLighter   rounded-sm text-gray-400 my-0 mx-2 flex items-center whitespace-nowrap px-4 transition-colors "
              onClick={() => signout()}
            >
              <div className="mr-2 flex items-center justify-center rounded-lg bg-center stroke-0 text-center xl:p-2.5 text-xl">
                {menu.Icon}
              </div>
              <span className="ml-1 duration-300 opacity-100 pointer-events-none ease">
                {menu.label}
              </span>
            </button>
          </li>
        ))}
        {/* <div className=" mx-4 ml-8 px-4 flex gap-2 items-center w-full">

          <Avatar src={session?.user?.image ?? ""} />
          <p className=" text-base text-gray-400 antialiased">{session?.user?.name}</p>
        </div> */}
      </ul>
    </aside>
  );
};

export default AppAdminSideNav;
