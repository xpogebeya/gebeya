'use client'

import IconButton from "@mui/joy/IconButton";
import { Divider } from "@mui/material";
import Cookies from "js-cookie";
import Link from "next/link";
import { usePathname, useRouter } from "next/navigation";
import React, { Dispatch, ReactNode, SetStateAction } from "react";
import { GoSignOut } from "react-icons/go";
import { IoCloseOutline } from "react-icons/io5";

type Props = {
  show: boolean;
  setShow: Dispatch<SetStateAction<boolean>>;
  MenuList: MenuListType[]
  signout: () => void
};
type MenuListType = {
  label: string;
  Icon: ReactNode;
  path: string;
};
const AppAdminSideNavMobile = ({ show, setShow, MenuList, signout }: Props) => {

  const BelowMenu: MenuListType[] = [
    // {
    //   label: "Profile",
    //   Icon: <BiSolidUserCircle />,
    //   path: "/admin/profile",
    // },
    {
      label: "Sign out",
      Icon: <GoSignOut />,
      path: "/admin/profile",
    },
  ];
  const checkActive = (path: string): boolean => {
    return path === usePathname();
  };

  return (
    <div>
      {show && (
        <aside className=" z-50 fixed left-6 sm:left-12 top-5 bg-white rounded-xl h-[85vh] w-[300px]">
          <IconButton
            className=" absolute right-3 top-2 text-2xl  text-BlueDark"
            onClick={() => setShow(false)}
          >
            <IoCloseOutline />
          </IconButton>
          <div>
            <Link href={"/"}>
              <img
                src="/logoMain.png"
                className=" h-24 w-full object-contain cursor-pointer"
              />
            </Link>
          </div>
          <Divider />
          <ul className=" flex flex-col gap-3 mt-5">
            {MenuList.map((menu) => (
              <li
                className="mt-0.5 w-full font-bold cursor-pointer"
                key={menu.label}
              >
                <Link
                  className={`py-2.7 text-base hover:border-b-2 hover:border-b-BlueLight hover:text-BlueLighter  hover:bg-BlueLighter hover:bg-opacity-20  rounded-sm text-gray-400 my-0 mx-2 flex items-center whitespace-nowrap px-4 transition-colors ${checkActive(menu.path) &&
                    "text-BlueLighter bg-opacity-20 bg-BlueLighter border-b-BlueLight  border-b-2 "
                    }`}
                  href={menu.path}
                >
                  <div className="mr-2 flex items-center justify-center rounded-lg bg-center stroke-0 text-center p-2.5 text-xl">
                    {menu.Icon}
                  </div>
                  <span className="ml-1 duration-300 opacity-100 pointer-events-none ease">
                    {menu.label}
                  </span>
                </Link>
              </li>
            ))}
          </ul>
          <Divider className=" mt-10" />
          <ul className=" flex flex-col gap-3 mt-5">
            {BelowMenu.map((menu) => (
              <li
                className="mt-0.5 w-full font-bold cursor-pointer"
                key={menu.label}
              >
                <button
                  className="py-2.7 text-base hover:border-b-2 hover:border-b-BlueLight hover:text-BlueLighter  hover:bg-BlueLighter hover:bg-opacity-20  rounded-sm text-gray-400 my-0 mx-2 flex items-center whitespace-nowrap px-4 transition-colors "
                  onClick={signout}
                >
                  <div className="mr-2 flex items-center justify-center rounded-lg bg-center stroke-0 text-center xl:p-2.5 text-xl">
                    {menu.Icon}
                  </div>
                  <span className="ml-1 duration-300 opacity-100 pointer-events-none ease">
                    {menu.label}
                  </span>
                </button>
              </li>
            ))}
          </ul>
        </aside>
      )}
    </div>
  );
};

export default AppAdminSideNavMobile;
