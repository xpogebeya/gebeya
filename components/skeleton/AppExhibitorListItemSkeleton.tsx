import React from "react";
import { ListItem, Typography, Button, Skeleton } from "@mui/material";

const AppExhibitorListItemSkeleton = () => {
  return (
    <ListItem className="bg-gray-100" >
      <div className="flex flex-col">
        <Typography variant="subtitle1" className="text-sm leading-normal">
          <Skeleton animation="wave" width={200} />
        </Typography>
        <Typography variant="body2" className=" text-xs leading-tight /80">
          <span className="font-semibold text-slate-700 sm:ml-2">
            <Skeleton animation="wave" width={100} />
          </span>
        </Typography>
        <Typography variant="body2" className=" text-xs leading-tight /80">
          <span className="font-semibold text-slate-700 sm:ml-2">
            <Skeleton animation="wave" width={150} />
          </span>
        </Typography>
        <Typography variant="body2" className="text-xs leading-tight /80">
          <span className="font-semibold text-slate-700 sm:ml-2">
            <Skeleton animation="wave" width={120} />
          </span>
        </Typography>
      </div>
      <div className="ml-auto text-right">
        <Button variant="contained" color="primary" disabled>
          <Skeleton animation="wave" width={80} height={10} />
        </Button>
      </div>
    </ListItem>
  );
};

export default AppExhibitorListItemSkeleton;
