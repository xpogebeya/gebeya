/** @format */

import React from "react";
import { FieldValues, UseFormRegister } from "react-hook-form";

import ImageUploader from "../uploader/ImageUploader";

const Subtitles = ({
  index,
  register,
}: {
  index: number;
  register: UseFormRegister<FieldValues>;
}) => {
  return (
    <div className="ml-5">
      <div className="flex items-center ml-8">
        <input
          className=" p-5 pl-0 focus:outline-none text-xl w-[90%]"
          placeholder={`Sub-Title-${index}(optional)`}
          {...register(`subtitles_${index}`)}
          id={`title-${index}`}
          type="text"
          autoFocus={true}
        />
        <ImageUploader />
      </div>
      <div className="ml-8">
        <textarea
          id={`desc-${index}`}
          {...register(`subdescription_${index}`)}
          className="border-0 focus:outline-none w-[90%] h-max"
          placeholder={`Sub-Description-${index}(optional)`}
        />
      </div>
    </div>
  );
};

export default Subtitles;
