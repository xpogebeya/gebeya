/** @format */

// /** @format */

// "use client";
// import { useCallback, useState } from "react";
// import { useDropzone } from "react-dropzone";
// import { FaCloudArrowUp } from "react-icons/fa6";
// import { RiDeleteBin5Line } from "react-icons/ri";

// const ImageUploader: React.FC = () => {
//   const [selectedImages, setSelectedImages] = useState<File[]>([]);

//   const onDrop = useCallback((acceptedFiles: File[]) => {
//     setSelectedImages((prevSelectedImages) => [
//       ...prevSelectedImages,
//       ...acceptedFiles,
//     ]);
//   }, []);

//   const removeImage = (
//     index: number,
//     event: React.MouseEvent<HTMLButtonElement>
//   ) => {
//     event.stopPropagation();
//     setSelectedImages((prevSelectedImages) => {
//       const updatedImages = [...prevSelectedImages];
//       updatedImages.splice(index, 1);
//       return updatedImages;
//     });
//   };
//   const { getRootProps, getInputProps, isDragActive } = useDropzone({
//     onDrop,
//     multiple: true,
//   });

//   return (
//     <div className="flex flex-col items-center justify-center">
//       <div
//         {...getRootProps()}
//         className={`p-4 m-4 border-4 border-dashed rounded-lg ${
//           isDragActive ? "border-blue-600" : "border-gray-400"
//         }`}
//       >
//         <input {...getInputProps()} accept="image/*" />

//         {selectedImages.length > 0 ? (
//           <div
//             className="max-h-40 overflow-y-auto flex flex-col w-40 gap-4"
//             style={{ width: "500px" }}
//           >
//             {selectedImages.map((image, index) => (
//               <div key={index} className="relative ">
//                 <img
//                   src={URL.createObjectURL(image)}
//                   alt={`Selected ${index + 1}`}
//                   className="h-32 w-full object-cover"
//                   style={{ flex: "0 0 calc(33.33% - 8px)" }}
//                 />

//                 <button
//                   className="absolute top-2 right-2 bg-white rounded-full p-1 text-red-600 text-xl"
//                   onClick={(event) => removeImage(index, event)}
//                 >
//                   <RiDeleteBin5Line />
//                 </button>
//               </div>
//             ))}
//           </div>
//         ) : (
//           <div className="flex flex-col cursor-pointer items-center justify-center">
//             <div className="text-white bg-BlueDark rounded-3xl text-5xl">
//               <FaCloudArrowUp />
//             </div>

//             <span className="text-gray-500">Images</span>
//           </div>
//         )}
//       </div>
//     </div>
//   );
// };

// export default ImageUploader;
import { useCallback, useState } from "react";
import { useDropzone } from "react-dropzone";
import { FaCloudArrowUp } from "react-icons/fa6";
import { RiDeleteBin5Line } from "react-icons/ri";

const ImageUploader: React.FC = () => {
  const [selectedImages, setSelectedImages] = useState<File[]>([]);

  const onDrop = useCallback((acceptedFiles: File[]) => {
    setSelectedImages((prevSelectedImages) => [
      ...prevSelectedImages,
      ...acceptedFiles,
    ]);
  }, []);

  const removeImage = (
    index: number,
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    event.stopPropagation();
    setSelectedImages((prevSelectedImages) => {
      const updatedImages = [...prevSelectedImages];
      updatedImages.splice(index, 1);
      return updatedImages;
    });
  };

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
    multiple: true,
  });

  return (
    <div className="flex flex-col items-center justify-center">
      <div
        {...getRootProps()}
        className={`p-4 m-4 border-4 border-dashed rounded-lg ${
          isDragActive ? "border-blue-600" : "border-gray-400"
        }`}
      >
        <input {...getInputProps()} accept="image/*" />

        {selectedImages.length > 0 ? (
          <div
            className="max-h-40 overflow-y-auto flex flex-col w-40 gap-4"
            style={{ width: "500px" }}
          >
            {selectedImages.map((image, index) => (
              <div key={index} className="relative">
                <img
                  src={URL.createObjectURL(image)}
                  alt={`Selected ${index + 1}`}
                  className="h-32 w-full object-cover"
                  style={{ flex: "0 0 calc(33.33% - 8px)" }}
                />

                <button
                  className="absolute top-2 right-2 bg-white rounded-full p-1 text-red-600 text-xl"
                  onClick={(event) => removeImage(index, event)}
                >
                  <RiDeleteBin5Line />
                </button>
              </div>
            ))}
          </div>
        ) : (
          <div className="flex flex-col cursor-pointer items-center justify-center">
            <div className="text-white bg-blue-600 rounded-3xl text-5xl">
              <FaCloudArrowUp />
            </div>

            <span className="text-gray-500">Images</span>
          </div>
        )}
      </div>
    </div>
  );
};

export default ImageUploader;
