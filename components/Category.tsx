import PostList from "@/components/postlist";
import Pagination from "@/components/blog/pagination";

import { getPostsByCategory } from "@/sanity/client";


interface Props {
    searchParams: {
        category: string
    }
    related?: boolean
}
export default async function AppBlogCategory({ searchParams, related }: Props) {
    const page = searchParams.category;
    const posts = await getPostsByCategory(page);
    return (
        <>
            {posts && posts?.length === 0 && (
                <div className="flex h-40 items-center justify-center">
                    <span className="text-lg text-gray-500">
                        End of the result!
                    </span>
                </div>
            )}
            {
                related ?
                    <div className="mt-10 grid gap-10 md:grid-cols-2 lg:gap-10 xl:grid-cols-3">
                        {posts.slice(0, 3).map((post: any) => (
                            <PostList key={post._id} post={post} aspect="square" />
                        ))}
                    </div>
                    :

                    <div className="mt-10 grid gap-10 md:grid-cols-2 lg:gap-10 xl:grid-cols-3">
                        {posts.map((post: any) => (
                            <PostList key={post._id} post={post} aspect="square" />
                        ))}
                    </div>
            }

        </>
    );
}
