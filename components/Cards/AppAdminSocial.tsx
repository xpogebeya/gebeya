import { SocialLinks } from '@/constants/homepage'
import { useAppSelector } from '@/utils'
import React from 'react'

type Props = {}

const AppAdminSocial = (props: Props) => {
    const { socialLinks } = useAppSelector(state => state.homePage)

    return (
        <div> <div className=" flex gap-5 mt-2 text-xl  text-BlueLighter/80 hover:text-BlueLighter">
            {socialLinks[0] && SocialLinks.map((socila, index) => (
                // @ts-ignore
                <a href={socialLinks[0][socila.path] || "/"} key={index} target='_blank' className=" cursor-pointer hover:text-BlueLight">
                    {socila.Icon}
                </a>
            ))}
        </div></div>
    )
}

export default AppAdminSocial