/** @format */

import React, { FC } from "react";
import Image from "next/image";

interface props {
  title: string;
  imageUrl: string;
}

const ShowCaseCards: FC<props> = ({ title, imageUrl }) => {
  return (
    <div className="group relative rounded-lg cursor-pointer items-center justify-center overflow-hidden transition-shadow hover:shadow-xl hover:shadow-BlueDark/30">
      <div className="h-96 w-72">
        <img
          className="h-full w-full object-cover rounded-lg transition-transform duration-500 group-hover:scale-125"
          src={imageUrl}
          width="200"
          height="400"
          alt=""
        />
      </div>
      <div className="absolute inset-0 bg-gradient-to-b from-transparent via-transparent to-BlueDark group-hover:from-BlueDark/70 group-hover:via-BlueDark/60 group-hover:to-BlueDark/70"></div>
      <div className="absolute inset-0 flex translate-y-[68%] flex-col items-center justify-center px-9 text-center transition-all duration-500 group-hover:translate-y-0">
        <h1 className=" text-3xl font-bold text-white">{title}</h1>
      </div>
    </div>
  );
};

export default ShowCaseCards;
