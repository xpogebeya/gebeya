import React from "react";
import { GoogleMap, LoadScript, Marker } from "@react-google-maps/api";

const AppMap = () => {
  const containerStyle = {
    width: "100%",
    height: "80vh",
    margin: "auto",
    borderRadius: "15px",
  };

  const center = {
    lat: 9.00567,
    lng: 38.78905,
  };

  return (
    <div className=" w-[95%] lg:w-[85%] mx-auto">
      <LoadScript
        googleMapsApiKey={process.env.NEXT_PUBLIC_GOOGLE_API ?? ""}
        libraries={["places"]}
      >
        <div className="-mt-20 rounded-lg  overflow-hidden">
          <GoogleMap
            mapContainerStyle={containerStyle}
            center={center}
            zoom={12}
          >
            <Marker position={center} />
          </GoogleMap>
        </div>
      </LoadScript>
    </div>
  );
};

export default AppMap;
