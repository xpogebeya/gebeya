import * as React from "react";
import Typography from "@mui/material/Typography";
import Breadcrumbs from "@mui/material/Breadcrumbs";

import HomeIcon from "@mui/icons-material/Home";
import WhatshotIcon from "@mui/icons-material/Whatshot";
import GrainIcon from "@mui/icons-material/Grain";
import { usePathname, useRouter } from "next/navigation";
import { LinkItem } from "@/types/exhibitor";
import Link from "next/link";

function handleClick(event: React.MouseEvent<HTMLDivElement, MouseEvent>) {
  event.preventDefault();
  console.info("You clicked a breadcrumb.");
}

type Props = {
  mainPath: LinkItem[];
  className?: string;
};

export default function AppBreadcrumbs({ mainPath, className }: Props) {
  const router = useRouter();
  return (
    <div role="presentation" onClick={handleClick} className=" mb-3">
      <Breadcrumbs aria-label="breadcrumb">
        <Link
          color="inherit"
          href="/"
          className={` flex items-center hover:underline ${
            className ? className : " hover:text-BlueLighter text-gray-400"
          }`}
        >
          <HomeIcon sx={{ mr: 0.5 }} fontSize="inherit" />
          Home
        </Link>
        {mainPath.map((item) => (
          <Link
            color="inherit"
            href={item.path}
            className={` flex items-center hover:underline ${
              className ? className : "hover:text-BlueLighter text-gray-400"
            }`}
            key={item.path}
          >
            {item.label}
          </Link>
        ))}
      </Breadcrumbs>
    </div>
  );
}
