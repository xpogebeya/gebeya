/** @format */

import React from "react";
type Props = {
  title: string;
  desc: string;
  list: string[];
};
type List = {
  item: string;
};
const ExhibitListCard = ({ title, desc, list }: Props) => {
  return (
    <div className={" w-full xl:w-1/2 text-BlueDark lg:pt-10 xl:p-10 "}>
      <p
        className={"font-bold text-3xl md:text-4xl p-4 pl-0 pb-3 text-BlueDark"}
      >
        {title}
      </p>
      <p className={" text-xl text-BlueDark"}>{desc}</p>
      <ul className="list-disc">
        <div className="grid pl-16 md:grid-cols-2">
          {list.map((item) => (
            <li className=" pt-2 text-lg  md:text-xl " key={item}>
              {item}
            </li>
          ))}
        </div>
      </ul>
    </div>
  );
};

export default ExhibitListCard;
