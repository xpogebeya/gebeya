/** @format */

import React from "react";

interface props {
  title: string;
  desc: string;
}
const WhyCard: React.FC<props> = ({ title, desc }) => {
  return (
    <div
      className={
        " w-screen-08-vw md:w-2/3 pl-2 x:pl-1 md:pl-4 md:p-4 h-88   border-solid"
      }
    >
      <p className={"text-xl md:text-3xl  md:pb-3 text-BlueDark"}>{title}</p>
      <p className={" text-sm md:text-xl text-BlueDark"}>{desc}</p>
    </div>
  );
};

export default WhyCard;
