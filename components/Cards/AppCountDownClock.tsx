/** @format */

"use client";
import { Skeleton } from "@mui/material";
import React, { useEffect, useState } from "react";

interface CountdownProps {
  targetDate: string;
}

const AppCountdownClock: React.FC<CountdownProps> = ({ targetDate }) => {
  const [timeLeft, setTimeLeft] = useState({
    days: 0,
    hours: 0,
    minutes: 0,
    seconds: 0,
  });

  useEffect(() => {
    const storedTargetDate = localStorage.getItem("targetDate") || targetDate;
    const targetTime = new Date(storedTargetDate).getTime();

    const intervalId = setInterval(() => {
      const now = new Date().getTime();
      const timeDifference = targetTime - now;

      if (timeDifference <= 0) {
        clearInterval(intervalId);
        setTimeLeft({
          days: 0,
          hours: 0,
          minutes: 0,
          seconds: 0,
        });
      } else {
        const days = Math.floor(timeDifference / (1000 * 60 * 60 * 24));
        const hours = Math.floor(
          (timeDifference % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
        );
        const minutes = Math.floor(
          (timeDifference % (1000 * 60 * 60)) / (1000 * 60)
        );
        const seconds = Math.floor((timeDifference % (1000 * 60)) / 1000);

        setTimeLeft({
          days,
          hours,
          minutes,
          seconds,
        });
      }
    }, 1000);

    return () => clearInterval(intervalId);
  }, [targetDate]);

  return (
    <>
      <h1 className=" font-bold hidden md:block text-2xl text-BlueDark subpixel-antialiased">
        Expo will start in..
      </h1>
      <h1 className="md:hidden block font-bold text-lg text-BlueDark subpixel-antialiased">
        Expo will start in..
      </h1>
      {timeLeft.days > 0 ? (
        <>
          <div className="grid grid-cols-2 sm:grid-cols-4 gap-5 w-[100%] mt-5  ">
            <div className="flex flex-col gap-1 bg-BlueLighter bg-opacity-30 p-5 rounded-md shadow-sm items-center min-w-[140px]">
              <span className="font-bold text-3xl antialiased text-BlueLight">
                {timeLeft.days}
              </span>
              <span className="antialiased font-semibold">Days</span>
            </div>
            <div className="flex flex-col gap-1 bg-BlueLighter bg-opacity-30 p-5 rounded-md shadow-sm items-center min-w-[140px]">
              <span className="font-bold text-3xl antialiased text-BlueDark">
                {timeLeft.hours}
              </span>
              <span className="antialiased font-semibold">Hours</span>
            </div>
            <div className="flex flex-col gap-1 bg-BlueLighter bg-opacity-30 p-5 rounded-md shadow-sm items-center min-w-[140px]">
              <span className="font-bold text-3xl antialiased text-BlueLighter">
                {timeLeft.minutes}
              </span>
              <span className="antialiased font-semibold">Minutes</span>
            </div>
            <div className="flex flex-col gap-1 bg-BlueLighter bg-opacity-30 p-5 rounded-md shadow-sm items-center min-w-[140px]">
              <span className="font-bold text-3xl antialiased text-BluePurple">
                {timeLeft.seconds}
              </span>
              <span className="antialiased font-semibold">Seconds</span>
            </div>
          </div>
        </>
      ) : (
        <div className="grid grid-cols-2 sm:grid-cols-4 gap-5 w-[100%]  mt-5 ">
          <div className="flex flex-col gap-1 bg-BlueLighter bg-opacity-30 p-5 rounded-md shadow-sm items-center min-w-[140px]">
            <Skeleton variant="text" width={80} height={40} animation="wave" />
            <Skeleton variant="text" width={60} height={20} animation="wave" />
          </div>
          <div className="flex flex-col gap-1 bg-BlueLighter bg-opacity-30 p-5 rounded-md shadow-sm items-center min-w-[140px]">
            <Skeleton variant="text" width={80} height={40} animation="wave" />
            <Skeleton variant="text" width={60} height={20} animation="wave" />
          </div>
          <div className="flex flex-col gap-1 bg-BlueLighter bg-opacity-30 p-5 rounded-md shadow-sm items-center min-w-[140px]">
            <Skeleton variant="text" width={80} height={40} animation="wave" />
            <Skeleton variant="text" width={60} height={20} animation="wave" />
          </div>
          <div className="flex flex-col gap-1 bg-BlueLighter bg-opacity-30 p-5 rounded-md shadow-sm items-center min-w-[140px]">
            <Skeleton variant="text" width={80} height={40} animation="wave" />
            <Skeleton variant="text" width={60} height={20} animation="wave" />
          </div>
        </div>
      )}
    </>
  );
};

export default AppCountdownClock;
