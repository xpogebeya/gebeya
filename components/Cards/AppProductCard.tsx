import { Product } from "@/types/exhibitor";
import { textCliper } from "@/utils";
import React, { FC } from "react";
import { AppButton } from "..";
import { AiOutlineArrowRight } from "react-icons/ai";
import { useRouter } from "next/navigation";

type Props = {
  product: Product;
  id: number | string;
};

const AppProductCard: FC<Props> = ({ product, id }) => {
  const router = useRouter();
  return (
    <div className=" bg-white border border-gray-200 rounded-lg shadow  ">
      <img
        className="rounded-t-lg h-[250px] w-full object-cover  object-top"
        src={product.image}
        alt=""
      />

      <div className="p-5">
        <a href="#">
          <h5 className="mb-2 text-2xl font-bold tracking-tight  text-BlueDark">
            {product.title}
          </h5>
        </a>
        <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
          {textCliper(product.description, 100)}
        </p>
        <AppButton
          label={"Read more"}
          Icon={<AiOutlineArrowRight className=" text-base" />}
          handleAction={() =>
            router.push(
              `/exhibitor-public/${id}/exhibitor-product/${product.id}`
            )
          }
        />
      </div>
    </div>
  );
};

export default AppProductCard;
