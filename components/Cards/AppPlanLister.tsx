'use client'

import React from 'react'
import { AppPlan } from '@/components';
import { Sponsors } from '@/types';
import { TbPackages } from "react-icons/tb";

const AppPlanLister = ({ sponsors, setSelectedPackaging }: { sponsors: Sponsors, setSelectedPackaging: (word: string) => void }) => {
    return (
        <section className="px-6 xl:px-0">
            <div className=" mx-auto container">
                <div className="flex flex-col lg:items-center justify-center w-full">
                    <div className=" mb-10  mx-auto  w-full px-4 text-center lg:w-[85%] flex  items-center gap-5 mt-16">
                        <div className=" h-[1px] w-1/2 bg-gray-400"></div>
                        <div>
                            <p className="   text-BlueLight text-2xl flex flex-col items-center justify-center">
                                <TbPackages />
                            </p>

                            <h2 className=" text-BlueDark antialiased tracking-normal text-xl  lg:text-4xl font-semibold leading-[1.3] text-blue-gray-900 mb-3">
                                <span className=" text-BlueLighter">
                                    The Right Plan for your business
                                </span>
                            </h2>
                            <p className="block antialiased  text-[12px]  md:text-base font-normal leading-relaxed text-gray-500">
                                We have several plans to showcase your Business. Get everything you need{" "}
                            </p>
                        </div>
                        <div className=" h-[1px] w-1/2 bg-gray-400"></div>
                    </div>

                </div>
                <div className="flex items-center justify-center w-full">
                    <div className="pt-14">
                        <div className="container mx-auto">
                            <div className=" grid xs:grid-cols-2 lg:grid-cols-3 mb-12 justify-between sm:justify-center -mx-6">
                                {sponsors?.packages && sponsors?.packages.map((plan, index) => (
                                    <AppPlan key={index} plan={plan.plan} features={plan.features} price={plan.price} setSelectedPackaging={setSelectedPackaging} />
                                ))}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default AppPlanLister