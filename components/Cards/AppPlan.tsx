'use client'
import { usePathname } from 'next/navigation';
import React, { useState } from 'react';
import { IoCheckmarkDone } from 'react-icons/io5';

type PlanProps = {
    plan: string;
    features: string[];
    price: string;
    setSelectedPackaging: (word: string) => void
};

const AppPlan = ({ plan, features, price, setSelectedPackaging }: PlanProps) => {
    const [displayedFeatures] = useState<string[]>(features.slice(0, 5));
    const curretnPath = usePathname()
    return (
        <div className="mb-4 px-6 h-full flex flex-col justify-between">
            <div className="py-5 px-4 bg-white border border-gray-200 shadow rounded-lg text-left flex-1">
                <h4 className="text-2xl text-BlueLighter font-semibold pb-8">
                    {plan}{' '}
                    <span className="text-[11px] text-gray-400">Sponsorships</span>
                </h4>
                <ul className="flex flex-col mb-6">
                    {displayedFeatures.map((feature) => (
                        <li className="flex gap-2 items-center mb-2.5" key={feature}>
                            <IoCheckmarkDone className="text-BlueLighter text-xl" />
                            <div className="text-gray-800 text-base font-normal">{feature}</div>
                        </li>
                    ))}
                </ul>
            </div>
            <div className="py-3">
                <p className="text-base text-BlueLighter relative pl-3">
                    <span className="text-2xl font-semibold">{!Number.isNaN(parseInt(price)) && parseInt(price).toLocaleString()}</span>
                    <span className="text-gray-600 font-light text-lg"> {!Number.isNaN(parseInt(price)) ? "Birr" : "check below"} </span>
                </p>
                <button
                    className="mt-3 w-full bg-BlueLighter bg-opacity-5 hover:bg-BlueLighter hover:bg-opacity-20 focus:outline-none transition duration-150 ease-in-out rounded text-BlueLighter px-8 text-base font-semibold py-3"
                    onClick={() => {
                        setSelectedPackaging(plan)
                    }}
                >
                    {curretnPath == "/custome-plan" ? "See Detail" : "Select"}
                </button>
            </div>
        </div>
    );
};

export default AppPlan;
