"use client";
import React from "react";
import { Pargraph } from "..";
import { useRouter } from "next/navigation";

type Props = {
  partner: {
    partnerImage: string;
    partnerLevel: string;
  };
};

export default function AppSponsorCard({
  partner: { partnerImage, partnerLevel },
}: Props) {
  const router = useRouter();
  return (
    <div
      className={` opacity-60 hover:opacity-100 text-center w-[125px] md:w-[250px] h-[180px]  px-5 py-10 rounded-[15px] cursor-pointer group relative hover:scale-105 transition-transform duration-200 ease-in-out `}
      onClick={() => router.push("/sponsers")}
    >
      <div className="absolute inset-0 bg-BlueLighter opacity-0 group-hover:opacity-20 transition-opacity rounded-[15px]"></div>
      <img
        src={partnerImage}
        alt="partner partnerImage"
        className="w-full h-full object-contain"
      />
      <Pargraph lable={`${partnerLevel} Partner`} className="" />
    </div>
  );
}
