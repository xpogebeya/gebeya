/** @format */

"use client";

import React, { useState } from "react";
import CountUp from "react-countup"
import VisibilitySensor from "react-visibility-sensor";
type Props = {
  Icon: React.ReactNode;
  Item: {
    number: number;
    title: string;
    shortDescription: string;
  };
};

export default function MoreCardCircle({
  Item: { shortDescription, title, number },
  Icon,
}: Props) {
  const [isVisible, setIsVisible] = useState(false);

  const onVisibilityChange = (isVisible: boolean) => {
    if (isVisible) {
      setIsVisible(true);
    }
  };

  return (
    <div className="group mx-auto bg-white hover:cursor-pointer shadow-xl rounded-lg p-5">
      <div className="text-BlueLight group-hover:border-BlueLight group-hover:border-2 text-3xl h-16 w-16 border border-BlueDark border-opacity-20 flex flex-col justify-center items-center rounded-full">
        {Icon}
      </div>

      <h4 className="mt-[16px] mb-[12px] font-[700] text-[1.25rem] text-gray-600">
        <VisibilitySensor
          onChange={onVisibilityChange}
          partialVisibility={true}
          delayedCall={true}
        >
          {({ isVisible }: { isVisible: boolean }) => (
            <span className="text-BlueLighter text-3xl">
              {isVisible ? <CountUp end={number} duration={2} /> : number}+
            </span>
          )}
        </VisibilitySensor>{" "}
        {title}
      </h4>
      <p className="antialiased text-[1rem] font-[300] leading-[1.6] pr-[48px] text-gray-400 max-w-xs">
        {shortDescription}
      </p>
    </div>
  );
}
