/** @format */

import React from "react";
import Link from "next/link";
import { SlCalender } from "react-icons/sl";

type props = {
  category?: string;
  latest?: boolean;
  heading: string;
  desc: string;
  date?: string;
  period?: string;
  large_title?: boolean;
  description?: boolean;
  topic?: boolean;
  id?: string;
  nolink?: boolean;
};
const BlogCard = ({
  heading,
  desc,
  date,
  period,
  large_title,
  description,
  category,
  latest,
  topic,
  id,
  nolink,
}: props) => {
  return (
    <div
      className={
        large_title
          ? "flex flex-col p-10 pt-0 xl:px-0 xl:pt-0 xl:pr-20 xl:pb-0  xl:mt-32 w-full xl:w-[90%]"
          : topic
          ? "flex flex-col md:pr-10 lg:pr-0  pt-0 xl:px-0 xl:pt-0 2xl:pr-20 xl:pb-0 2xl:mt-5 w-full xl:w-[70%]"
          : description
          ? "flex flex-col md:pr-10 lg:pr-0  pt-0 xl:px-0 xl:pt-0 2xl:pr-20 xl:pb-0 2xl:mt-5 w-full xl:w-[80%]"
          : latest
          ? "flex flex-col ml-5 w-full xl:w-[75%]"
          : "flex flex-col ml-5 w-full my-5 xl:pr-5 2xl:pr-0 xl:w-[50%]"
      }
    >
      {nolink ? (
        <h3
          className={
            large_title
              ? " text-sm _x:text-xl mt x:text-2xl md:text-3xl lg:text-4xl 2xl:text-5xl 2xl:leading-[4rem] pb-4 text-BlueDark hover:text-BlueLighter font-bold"
              : description
              ? " text-lg md:text-xl lg:text-lg xl:text-xl  2xl:text-2xl pb-2 leading-7 text-BlueDark   hover:text-BlueLighter xl:mt-2 font-bold"
              : latest
              ? " text-sm  pb-2 text-BlueDark   hover:text-BlueLighter font-bold"
              : " text-sm md:text-lg lg:text-[1rem] lg:leading-6 2xl:text-lg pb-2 text-BlueDark   hover:text-BlueLighter font-bold"
          }
        >
          {heading}
        </h3>
      ) : (
        <Link href={`/media-and-press/${category}/${id}`}>
          <h3
            className={
              large_title
                ? " text-sm _x:text-xl mt x:text-2xl md:text-3xl lg:text-4xl 2xl:text-5xl 2xl:leading-[4rem] pb-4 text-BlueDark hover:text-BlueLighter font-bold"
                : description
                ? " text-lg md:text-xl lg:text-lg xl:text-xl  2xl:text-2xl pb-2 leading-7 text-BlueDark   hover:text-BlueLighter xl:mt-2 font-bold"
                : latest
                ? " text-sm  pb-2 text-BlueDark   hover:text-BlueLighter font-bold"
                : " text-sm md:text-lg lg:text-[1rem] lg:leading-6 2xl:text-lg pb-2 text-BlueDark   hover:text-BlueLighter font-bold"
            }
          >
            {heading}
          </h3>
        </Link>
      )}

      {description ? (
        <p
          className={
            large_title
              ? "  leading-normal text-gray-500 text-xs x:text-sm md:text-lg line-clamp-3 "
              : topic
              ? "leading-normal text-gray-500 text-sm md:text-lg lg:text-sm line-clamp-2 "
              : " leading-normal text-gray-500 text-sm md:text-lg lg:text-sm line-clamp-4 "
          }
        >
          {desc}
        </p>
      ) : null}

      <div className={description ? "flex pt-5" : "flex pt-1"}>
        <div className="flex ">
          {date ? <SlCalender className="pt-2 md:pt-1 pb-0" /> : null}
          {/* <Link href={`/media-and-press/:details=${date}`}> */}
          <span className=" text-xs md:text-sm lg:text-xs ml-[1px] md:ml-1 hover:text-BlueLighter ">
            {date}
          </span>
          {/* </Link> */}
          <span className=" pt-1 pl-10  text-xs text-gray-500">{period}</span>
        </div>
      </div>
    </div>
  );
};

export default BlogCard;
