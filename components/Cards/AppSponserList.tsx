/** @format */

"use client";
import { useEffect, useState } from "react";
import { AppSponsorCard } from "..";
import { PartnersData } from "@/constants/homepage";
import { SiCoop } from "react-icons/si";
import { HomePageData } from "@/types";


export function AppSponsorCardList({ data }: { data: HomePageData }) {
  const [lastIndex, setLastIndex] = useState<number>(10);
  const [firstIndex, setFirstIndex] = useState<number>(0);
  const [increasing, setIncreasing] = useState<boolean>(true);

  useEffect(() => {
    if (data.Partners.length >= 12) {

      const timer = setTimeout(() => {
        if (increasing) {
          if (lastIndex < PartnersData.length) {
            setFirstIndex((prev) => prev + 5);
            setLastIndex((prev) => prev + 5);
          } else {
            setIncreasing(false);
          }
        } else {
          if (lastIndex > 10) {
            setFirstIndex((prev) => prev - 5);
            setLastIndex((prev) => prev - 5);
          } else {
            setIncreasing(true);
          }
        }
      }, 2000);

      return () => clearTimeout(timer);
    }
  }, [lastIndex, firstIndex, increasing]);

  return (
    <>
      <div className="mx-auto  w-full px-4 text-center lg:w-[85%] flex  items-center gap-5  ">
        <div className=" h-[1px] w-1/2 bg-gray-400"></div>
        <div>
          <p className="   text-BlueLight text-2xl flex flex-col items-center justify-center">
            <SiCoop />
          </p>

          <h2 className=" text-BlueDark antialiased tracking-normal text-xl  lg:text-4xl font-semibold leading-[1.3] text-blue-gray-900 mb-3">
            <span className=" text-BlueLighter">Meet Our Partners</span>
          </h2>
          <p className="block antialiased text-[12px]  md:text-base font-normal leading-relaxed text-gray-500">
            Our partners span the globe and represent a diverse range of
            organizations and businesses. We work together to build thriving
            communities.{" "}
          </p>
        </div>
        <div className=" h-[1px] w-1/2 bg-gray-400"></div>
      </div>
      <div
        className=" w-[85%] mx-auto grid grid-cols-2 xs:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 gap-5 lg:pt-10"
        id="partners"
      >

        {data.Partners.slice(firstIndex, lastIndex).map((partner, index) => (
          <AppSponsorCard key={index} partner={partner} />
        ))}
      </div>
    </>
  );
}
