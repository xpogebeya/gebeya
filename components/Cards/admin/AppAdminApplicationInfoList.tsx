import { ApplicationInfo } from "@/types";
import { useAppSelector } from "@/utils";
import React, { FC, useEffect } from "react";

interface ApplicationInfoListProps {
  applicationData: ApplicationInfo[];
}

const AppAdminApplicationInfoList: FC<ApplicationInfoListProps> = ({
  applicationData,
}) => {
  return (
    <ul className="flex flex-col pl-0 mb-0 rounded-lg">
      {applicationData.map((app, index) => (
        <li
          key={app.name}
          className={`relative flex justify-between px-4 py-2 pl-0 mb-2 border-0 ${
            index === 0 ? "rounded-t-inherit" : ""
          } ${
            index === applicationData.length - 1 ? "rounded-b-inherit" : ""
          } text-inherit rounded-xl`}
        >
          <div className="flex items-center">
            <div className="flex flex-col">
              <h6 className="mb-1 text-sm leading-normal  text-slate-700">
                {app.name}
              </h6>
            </div>
          </div>
          <div className="">
            <p className={` text-gray-600 text-base`}>{app.amount}</p>
          </div>
        </li>
      ))}
    </ul>
  );
};

export default AppAdminApplicationInfoList;
