/** @format */

"use client";
import { ExhibitorsDataType, InitialExibitors } from "@/types/exhibitor";
import Button from "@mui/joy/Button";
import React, { Dispatch, FC, SetStateAction, useState } from "react";
import { useDispatch } from "react-redux";
import { AppDispatch, approve, decline } from "@/utils";
import { AppForm } from "@/components";
import { FormikValues } from "formik";
import * as Yup from "yup";
import axios from "axios";
import AppAdminDeclinMessage from "@/components/Forms/admin/AppAdminDeclinMessage";

type Props = {
  currentluUpdatind: number | string | null;
  setCurrentluUpdatind: Dispatch<SetStateAction<number | string | null>>;
  data: ExhibitorsDataType;
  setUploading: Dispatch<SetStateAction<boolean>>;
  setError: Dispatch<SetStateAction<string>>;
};

const validationSchema = Yup.object().shape({
  reasonForDecline: Yup.string().required().label("Message"),
});

const initialValues: FormikValues = {
  reasonForDecline: "",
};

const AppAdminExhibitorCard: FC<Props> = ({
  data: {
    businessName,
    email,
    bussinessType,
    id,
    DescriptionOfBusiness,
    address,
    status,
  },
  setCurrentluUpdatind,
  currentluUpdatind,
  setError,
  setUploading
}) => {
  const [update, setUpdate] = useState<boolean>(false);
  const [declineMessage, setDeclineMessage] = useState<boolean>(false);

  const dispatch = useDispatch<AppDispatch>();
  const handleApproval = async (id: string) => {
    setUploading(true)
    try {
      await axios.put("/api/admin/get-exhibitors", {
        type: "approve",
        id,
        email
      })
      dispatch(approve(id));

    } catch (error) {
      setError("Something went wrong")
    }
    setUploading(false)

  };
  const handleDecline = () => {
    setDeclineMessage(true);
  };

  const submitDeclin = async (value: FormikValues) => {
    setUploading(true)
    try {
      const { status } = await axios.put("/api/admin/get-exhibitors", {
        type: "decline",
        id,
        email,
        messageForDeclien: value.reasonForDecline
      })

      if (status == 200) {
        setDeclineMessage(false);
        dispatch(decline(id));
      }

    } catch (error) {
      setError("Something went wrong")
    }
    setUploading(false)

  }
  return (
    <div className=" p-6 mb-2 border-0 rounded-lg bg-gray-100 dark:bg-slate-850">
      <li className="relative flex ">
        <div className="flex flex-col">
          <h6 className="mb-4 text-sm leading-normal">{businessName}</h6>
          <span className="mb-2 text-xs leading-tight /80">
            Company Name:{" "}
            <span className="font-semibold text-slate-700 sm:ml-2">
              {businessName}
            </span>
          </span>
          <span className="mb-2 text-xs leading-tight /80">
            Email address:{" "}
            <span className="font-semibold text-slate-700 sm:ml-2">
              {email}
            </span>
          </span>
          <span className="text-xs leading-tight /80">
            Bussiness Type:{" "}
            <span className="font-semibold text-slate-700 sm:ml-2">
              {bussinessType}
            </span>
          </span>
          {update && currentluUpdatind == id && (
            <div className="  flex flex-col gap-1">
              <span className="text-xs leading-tight /80">
                Bussiness Description:{" "}
                <span className="font-semibold text-slate-700 sm:ml-2 line-clamp-5">
                  {DescriptionOfBusiness}
                </span>
              </span>
              <div key={address.phone} className=" flex flex-col">
                <span className="text-xs leading-tight /80">
                  Phone:{" "}
                  <span className="font-semibold text-slate-700 sm:ml-2">
                    {address.phone}
                  </span>
                </span>
                <span className="text-xs leading-tight /80">
                  Country:{" "}
                  <span className="font-semibold text-slate-700 sm:ml-2">
                    {address.country}
                  </span>
                </span>
                <span className="text-xs leading-tight /80">
                  City:{" "}
                  <span className="font-semibold text-slate-700 sm:ml-2">
                    {address.city}
                  </span>
                </span>
                {address.telephone && (
                  <span className="text-xs leading-tight /80">
                    Telephone:{" "}
                    <span className="font-semibold text-slate-700 sm:ml-2">
                      {address.telephone}
                    </span>
                  </span>
                )}
                {address.streetNumber && (
                  <span className="text-xs leading-tight /80">
                    address:{" "}
                    <span className="font-semibold text-slate-700 sm:ml-2">
                      {address.streetNumber},{address.zipCode}
                    </span>
                  </span>
                )}
              </div>
              <div className=" mt-2">
                {/* <AppSocial socila={social} /> */}
              </div>
              <div className=" mt-2 flex gap-1">
                {status !== "accepted" && (
                  <Button
                    variant="soft"
                    color="success"
                    onClick={() => handleApproval(id)}
                  >
                    Approve
                  </Button>
                )}
                {!declineMessage && status !== "rejected" && (
                  <Button
                    variant="soft"
                    color="danger"
                    onClick={() => handleDecline()}
                  >
                    Decline
                  </Button>
                )}
              </div>
              {declineMessage && id == currentluUpdatind && (
                <>
                  <AppForm
                    initialValues={initialValues}
                    onSubmit={(values) => submitDeclin(values)}
                    validationSchema={validationSchema}
                    children={
                      <AppAdminDeclinMessage />
                    }
                  />
                </>
              )}
            </div>
          )}
        </div>
        <div className="ml-auto text-right">
          <Button
            variant="soft"
            color={update && id == currentluUpdatind ? "danger" : "neutral"}
            onClick={() => {
              if (update && id == currentluUpdatind) {
                setUpdate(false);
                setCurrentluUpdatind(null);
                setDeclineMessage(false);
              } else {
                setCurrentluUpdatind(id);
                setUpdate(true);
              }
            }}
          >
            {update && id == currentluUpdatind ? "Close" : "Update"}
          </Button>
        </div>
      </li>
    </div>
  );
};

export default AppAdminExhibitorCard;
