/** @format */

"use client";
import { InitialExibitors } from "@/types/exhibitor";
import Button from "@mui/joy/Button";
import React, { Dispatch, FC, SetStateAction, useState } from "react";
import AppSocial from "../AppSocial";
import { useDispatch } from "react-redux";
import {
  AppDispatch,
  approveMedia,
  declineMedia,
} from "@/utils";
import { AppForm } from "@/components";
import { FormikValues } from "formik";
import * as Yup from "yup";
import { Media } from "@/types";
import axios from "axios";
import AppAdminDeclinMessage from "@/components/Forms/admin/AppAdminDeclinMessage";

type Props = {
  currentluUpdatind: string | null;
  setCurrentluUpdatind: Dispatch<SetStateAction<string | null>>;
  data: Media;
  setUploading: Dispatch<SetStateAction<boolean>>;
  setError: Dispatch<SetStateAction<string>>;
};

const validationSchema = Yup.object().shape({
  reasonForDecline: Yup.string().required().label("Message"),
});

const initialValues: FormikValues = {
  reasonForDecline: "",
};

const AppAdminVistorsCard: FC<Props> = ({
  data: {
    email,
    status,
    firstName,
    lastName,
    mediaName,
    phone,
    sex,
    faceBookLink,
    instagram,
    linkedinLink,
    middleName,
    logo,
    id,
    tikTokLink,
    tvChannelName,
    websiteLink,
    telegramChannel,
    youTubeLink,
  },
  setCurrentluUpdatind,
  currentluUpdatind,
  setUploading,
  setError
}) => {
  const [update, setUpdate] = useState<boolean>(false);
  const [declineMessage, setDeclineMessage] = useState<boolean>(false);

  const dispatch = useDispatch<AppDispatch>();
  const handleApproval = async (email: string) => {
    setUploading(true)
    try {
      await axios.put("/api/admin/get-medias", {
        type: "approve",
        id,
        email
      })
      dispatch(approveMedia(email));
      setError('')
    } catch (error) {
      setError('Something went wrong')
    }
    setUploading(false)

  };
  const handleDecline = () => {
    setDeclineMessage(true);
  };

  const social = {
    faceBookLink,
    instagram,
    linkedinLink,
    telegramChannel,
    websiteLink,
    tvChannelName,
    youTubeLink,
    tikTokLink,
  };

  const submitDeclin = async (value: FormikValues) => {

    try {
      const { status } = await axios.put("/api/admin/get-medias", {
        type: "decline",
        id,
        email,
        messageForDeclien: value.reasonForDecline
      })

      if (status == 200) {
        setDeclineMessage(false);
        dispatch(declineMedia(email));
      }
    } catch (error) {
      setError("Something went wrong")
    }

  }
  return (
    <div className=" p-6 mb-2 border-0 rounded-lg bg-gray-100 dark:bg-slate-850">
      <li className="relative flex ">
        <div className="flex flex-col">
          <h6 className="mb-4 text-sm leading-normal">
            {firstName} {middleName} {lastName}
          </h6>
          <span className="mb-2 text-xs leading-tight /80">
            Media Name:{" "}
            <span className="font-semibold text-slate-700 sm:ml-2">
              {mediaName}
            </span>
          </span>
          <span className="mb-2 text-xs leading-tight /80">
            Email Address:{" "}
            <span className="font-semibold text-slate-700 sm:ml-2">
              {email}
            </span>
          </span>
          <span className="text-xs leading-tight /80">
            Phone:{" "}
            <span className="font-semibold text-slate-700 sm:ml-2">
              {phone}
            </span>
          </span>
          {update && currentluUpdatind == email && (
            <div className="  flex flex-col gap-1">
              <div className=" mt-2">
                {/* <AppSocial socila={social} /> */}
              </div>
              <div className=" mt-2 flex gap-1">
                {status !== "accepted" && (
                  <Button
                    variant="soft"
                    color="success"
                    onClick={() => handleApproval(email)}
                  >
                    Approve
                  </Button>
                )}
                {!declineMessage && status !== "rejected" && (
                  <Button
                    variant="soft"
                    color="danger"
                    onClick={handleDecline}
                  >
                    Decline
                  </Button>
                )}
              </div>
              {declineMessage && email == currentluUpdatind && (
                <>
                  <AppForm
                    initialValues={initialValues}
                    onSubmit={(values) => submitDeclin(values)}
                    validationSchema={validationSchema}
                    children={
                      <AppAdminDeclinMessage />
                    }
                  />

                </>
              )}
            </div>
          )}
        </div>
        <div className="ml-auto text-right">
          <Button
            variant="soft"
            color={update && email == currentluUpdatind ? "danger" : "neutral"}
            onClick={() => {
              if (update && email == currentluUpdatind) {
                setUpdate(false);
                setCurrentluUpdatind(null);
                setDeclineMessage(false);
              } else {
                setCurrentluUpdatind(email);
                setUpdate(true);
              }
            }}
          >
            {update && email == currentluUpdatind ? "Close" : "Update"}
          </Button>
        </div>
      </li>
    </div>
  );
};

export default AppAdminVistorsCard;
