import React, { FC, ReactNode } from "react";

export type Card = {
  title: string;
  newAmount: string;
  newStat: string;
  time: string;
  Icon: ReactNode;
  inc: boolean;
};

type Props = {
  currentValue: Card;
};

const AppAdminStaticCard: FC<Props> = ({
  currentValue: { Icon, newAmount, newStat, time, title, inc },
}) => {
  return (
    <div className="w-full max-w-full px-3 mb-6 sm:w-1/2 sm:flex-none xl:mb-0 xl:w-1/4">
      <div className="relative flex flex-col min-w-0 break-words bg-white shadow-xl  rounded-2xl bg-clip-border">
        <div className="flex-auto p-4">
          <div className="flex flex-row -mx-3">
            <div className="flex-none w-2/3 max-w-full px-3">
              <div>
                <p className="mb-0 opacity-60 font-sans text-sm font-semibold leading-normal uppercase ">
                  {title}
                </p>
                <h5 className="mb-2 font-bold ">{newAmount}</h5>
                <p className="mb-0  opacity-60">
                  <span
                    className={`text-sm font-bold leading-normal ${
                      inc ? "text-emerald-500" : "text-red-600"
                    }`}
                  >
                    {newStat}%{" "}
                  </span>
                  since {time}
                </p>
              </div>
            </div>
            <div className="px-3 text-right basis-1/3">
              <div className="inline-block w-12 h-12 text-center rounded-full bg-gradient-to-tl from-orange-500 to-yellow-500">
                <i className="ni leading-none ni-money-coins text-lg relative top-3.5 text-white left-3.5">
                  {Icon}
                </i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AppAdminStaticCard;
