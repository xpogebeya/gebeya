/** @format */

"use client";
import Button from "@mui/joy/Button";
import React, { Dispatch, FC, SetStateAction } from "react";
import { useDispatch } from "react-redux";
import { AppDispatch, removeClientUser } from "@/utils";

import { ClientUsers } from "@/types";
import { AiFillDelete } from "react-icons/ai";
import axios from "axios";

type Props = {
  data: ClientUsers;
  setUploading: Dispatch<SetStateAction<boolean>>
  setErrMsg:Dispatch<SetStateAction<string>>
};

const AppAdminUserPermissionCard: FC<Props> = ({
  data,
  setUploading,
  setErrMsg
}) => {
  const dispatch = useDispatch<AppDispatch>();
  const handleDelete = async () => {
    setUploading(true)
    try {
      await axios.delete(`/api/admin/get-admins?id=${data.id}`)
      dispatch(removeClientUser(data.email));
    } catch (error) {
      setErrMsg("something went wrong")
    }
    setUploading(false)

  }

  return (
    <div className=" p-6 mb-2 border-0 rounded-lg bg-gray-100 dark:bg-slate-850">
      <li className="relative flex ">
        <div className="flex flex-col gap-1">
          <h6 className="mb-4 text-sm leading-normal">{data.fullName}</h6>
          <span className="text-xs leading-tight /80">
            Full Name:{" "}
            <span className="font-semibold text-slate-700 sm:ml-2">
              {data.fullName}
            </span>
          </span>
          <span className=" text-xs leading-tight /80">
            Email Address:{" "}
            <span className="font-semibold text-slate-700 sm:ml-2">
              {data.email}
            </span>
          </span>
          <span className="text-xs leading-tight /80">
            Permission to:{" "}
            <span
              className="font-semibold text-slate-700 sm:ml-2  capitalize"

            >
              {data.type}
            </span>
          </span>

        </div>
        <div className="ml-auto text-right flex items-center self-start">
          <Button
            variant="soft"
            color={"danger"}
            onClick={handleDelete}
          >
            <AiFillDelete className=" text-red-500 text-xl" />
          </Button>
        </div>
      </li>
    </div>
  );
};

export default AppAdminUserPermissionCard;
