import {
  Menu,
  MenuHandler,
  MenuList,
  MenuItem,
  Avatar,
  Typography,
} from "@material-tailwind/react";
import IconButton from "@mui/joy/IconButton";
import { useRouter } from "next/navigation";
import React from "react";
import { LuSettings2 } from "react-icons/lu";

type Props = {
  menuList: string[];
  currentStatus: string;
  link: string;
};

export function AppAdminSettingsIcon({ menuList, currentStatus, link }: Props) {
  const [isMenuOpen, setIsMenuOpen] = React.useState(false);
  const router = useRouter();

  return (
    <Menu allowHover open={isMenuOpen} handler={setIsMenuOpen}>
      <MenuHandler>
        <IconButton className=" text-xl  text-white hover:bg-BlueLighter hover:scale-105">
          <LuSettings2 />
        </IconButton>
      </MenuHandler>
      <MenuList className="flex flex-col gap-2 mr-8">
        {menuList.map((menu) => (
          <MenuItem
            className={`flex items-center gap-2 pl-2 hover:bg-BlueLighter hover:bg-opacity-20 cursor-pointer pb-0 ${
              currentStatus == menu ? "bg-BlueLighter bg-opacity-20" : ""
            }`}
            key={menu}
            onClick={() => router.push(`${link}${menu}`)}
          >
            <Typography
              variant="small"
              color="gray"
              className="font-normal"
              key={menu}
            >
              {menu}
            </Typography>
          </MenuItem>
        ))}
      </MenuList>
    </Menu>
  );
}
