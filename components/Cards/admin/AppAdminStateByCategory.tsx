import Button from "@mui/joy/Button";
import { useRouter } from "next/navigation";
import React from "react";

interface CountryData {
  countryName: string;
  flagImageSrc: string;
  value: number;
}

interface MediasByCountryProps {
  title: string;
  countryData: CountryData[];
  url: string;
}

const AppAdminStateByCategory: React.FC<MediasByCountryProps> = ({
  title,
  countryData,
  url,
}) => {
  const router = useRouter();
  return (
    <div className="w-full max-w-full px-3 mt-0 mb-6 lg:mb-0 lg:w-2/5 lg:flex-none">
      <div className="relative flex flex-col min-w-0 break-words bg-white border-0 border-solid shadow-xl shadow-dark-xl border-black-125 rounded-2xl bg-clip-border">
        <div className="p-4 pb-0 mb-0 rounded-t-4">
          <div className="flex justify-between">
            <h6 className="mb-2">{title}</h6>
            <Button
              variant="soft"
              size="sm"
              className=" text-gray-400 hover:text-BlueLighter"
              onClick={() => router.push(url)}
            >
              See all
            </Button>
          </div>
        </div>
        <div className="overflow-x-auto">
          <table className="items-center w-full mb-4 align-top border-collapse border-gray-200 dark:border-white/40">
            <tbody>
              {countryData.map((data, index) => (
                <tr key={index}>
                  <td className="p-2 align-middle bg-transparent border-b w-3/10 whitespace-nowrap dark:border-white/40">
                    <div className="flex items-center px-2 py-1">
                      <div>
                        <img
                          src={data.flagImageSrc}
                          alt={`${data.countryName} flag `}
                          className=" h-5 w-5 object-contain"
                        />
                      </div>
                      <div className="ml-6">
                        <p className="mb-0 text-xs font-semibold leading-tight dark:opacity-60">
                          Country:
                        </p>
                        <h6 className="mb-0 text-sm leading-normal">
                          {data.countryName}
                        </h6>
                      </div>
                    </div>
                  </td>
                  <td className="p-2 align-middle bg-transparent border-b whitespace-nowrap dark:border-white/40">
                    <div className="text-center">
                      <p className="mb-0 text-xs font-semibold leading-tight dark:opacity-60">
                        Amount:
                      </p>
                      <h6 className="mb-0 text-sm leading-normal">
                        {data.value}
                      </h6>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default AppAdminStateByCategory;
