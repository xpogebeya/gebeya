/** @format */

"use client";
import { useImageUploader } from "@/hooks";
import AspectRatio from "@mui/joy/AspectRatio";
import { FormikValues, useFormikContext } from "formik";
import React, { useState, useCallback, Dispatch, SetStateAction } from "react";
import { useDropzone } from "react-dropzone";

type FileUploaderProps = {
  imageUrl?: string;
  fieldName: string;
  setFile: Dispatch<SetStateAction<FileList | null>>
};

const AppExhibitoImageUploader: React.FC<FileUploaderProps> = ({
  imageUrl,
  fieldName,
  setFile
}) => {
  const { handleFileUpload } = useImageUploader();
  const { values } = useFormikContext<FormikValues>();

  const onDrop = useCallback(
    (acceptedFiles: FileList) => {
      const file = acceptedFiles;
      setFile(file);
      handleFileUpload(file, fieldName);
    },
    [handleFileUpload, fieldName]
  );

  const { getRootProps, getInputProps } = useDropzone({
    // @ts-ignore
    onDrop,
    // @ts-ignore
    accept: "image/*",
    multiple: false,
  });

  return (
    <div className="cursor-pointer">
      <div {...getRootProps()} className="dropzone">
        <input {...getInputProps()} />
        {imageUrl && !values[fieldName] && (
          <div className="h-32 w-32  md:h-48 md:w-48">
            <img
              className=" h-32  md:h-48 w-full rounded-full object-contain   border border-BlueLighter "
              src={imageUrl}
              alt=""
            />
          </div>
        )}
        {imageUrl && values[fieldName] && (
          <div className="h-32 w-32  md:h-48 md:w-48">
            <img
              className=" h-32  md:h-48 w-full rounded-full object-contain   border border-BlueLighter "
              src={values[fieldName]}
              alt=""
            />
          </div>
        )}
        {!imageUrl && !values[fieldName] && (
          <div className="flex items-center justify-center h-48  md:h-48 w-48 rounded-full border border-BlueLighter">
            <p className="text-center">
              Drag & drop an image file here, or click to select one
            </p>
          </div>
        )}
        {!imageUrl && values[fieldName] && (
          <div className="h-32 w-32  md:h-48 md:w-48">
            <img
              className=" h-32  md:h-48 w-full rounded-full object-contain   border border-BlueLighter "
              src={values[fieldName]}
              alt=""
            />
          </div>
        )}
      </div>
    </div>
  );
};

export default AppExhibitoImageUploader;
