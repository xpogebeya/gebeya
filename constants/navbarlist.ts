/** @format */

import { NavList as NavListType } from "@/types/navbarlist";

export const NavList: NavListType[] = [
  {
    lable: "Home",
    path: "/",
  },
  {
    lable: "Exhibit",
    options: [
      {
        lable: "Exibitor request",
        path: "/exhibitor-register",
      },
      {
        lable: "Exhibitors list",
        path: "/exhibitor-public",
      },
      {
        lable: "Exhibitors Portal",
        path: "/exhibitor-login",
      },
      {
        lable: "Why Exhibit ",
        path: "/why-exibit",
      },
    ],
  },
  {
    lable: "Media",
    options: [
      {
        lable: "Media badge request",
        path: "/media-badge-request",
      },
      {
        lable: "Media list",
        path: "/media-list",
      },
      {
        lable: "Gallery",
        path: "/media-gallery",
      },
      {
        lable: "News and Articles",
        path: "/blog",
      },
    ],
  },
  {
    lable: "Visit",
    options: [
      {
        lable: "Visitor badge request",
        path: "/visitor-register",
      },
      {
        lable: "Maps and Directions",
        path: "/Maps-and-Directions",
      },
      {
        lable: "Don't let me miss out!",
        path: "/inform-me-when-registration-is-live",
      },
      {
        lable: "Why Visit",
        path: "/why-visit",
      },
    ],
  },
  {
    lable: "About",
    options: [
      {
        lable: "Partners and Sponsorship",
        path: "/sponsers",
      },
      {
        lable: "Contact us",
        path: "/contactus",
      },
      {
        lable: "About Us",
        path: "/aboutus",
      },
    ],
  },
];
