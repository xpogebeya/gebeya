interface FAQ {
    title: string;
    description: string;
  }

export const faqs: FAQ[] = [
    {
      title: "Is this where I can include some frequently asked questions?",
      description:
        "Yes, exactly. Here you can provide immediate answers to a few common and pressing questions This will not only reduce your support tickets, but it will also reassure users – and make them more likely to click your CTA.",
    },
    {
      title: "  This is a frequently asked question. Ipsum dolor sit amet?",
      description:
        "Yes, exactly. Here you can provide immediate answers to a few common and pressing questions This will not only reduce your support tickets, but it will also reassure users – and make them more likely to click your CTA.",
    },
    {
      title: "  This is a frequently asked question. Ipsum dolor si?",
      description:
        "Yes, exactly. Here you can provide immediate answers to a few common and pressing questions This will not only reduce your support tickets, but it will also reassure users – and make them more likely to click your CTA.",
    },
  ];