/** @format */

import { Hero as HeroType } from "@/types/homePage";
import {
  BsCameraVideoFill,
  BsFacebook,
  BsFillFlagFill,
  BsInstagram,
  BsLinkedin,
  BsTelegram,
  BsYoutube
} from "react-icons/bs";
import { FiUsers } from "react-icons/fi";
import { MdPrecisionManufacturing } from "react-icons/md";

export const PartnersData = [
  {
    logo: "/partner1.webp",
    partnerLevel: "Golden",
  },
  {
    logo: "/partner2.webp",
    partnerLevel: "Golden",
  },
  {
    logo: "/partner3.png",
    partnerLevel: "Golden",
  },
  {
    logo: "/partner4.png",
    partnerLevel: "Golden",
  },
  {
    logo: "/partner5.jpg",
    partnerLevel: "Golden",
  },
  {
    logo: "/partner6.png",
    partnerLevel: "Golden",
  },
  {
    logo: "https://imgs.search.brave.com/1EV59ybDCPJeEQ0CY4WfG1Fq0-2YLYmKBuES8-1Xoh0/rs:fit:500:0:0/g:ce/aHR0cHM6Ly9pbWFn/ZXMtcGxhdGZvcm0u/OTlzdGF0aWMuY29t/Ly95UTNxWC02UUlw/ZTl5Zk5JUUpXN1JC/d0Noc1k9LzB4NTk6/ODgzeDk0Mi9maXQt/aW4vNTAweDUwMC85/OWRlc2lnbnMtY29u/dGVzdHMtYXR0YWNo/bWVudHMvNjAvNjA1/OTYvYXR0YWNobWVu/dF82MDU5NjA3Mg.jpeg",
    partnerLevel: "Golden",
  },
  {
    logo: "https://1000logos.net/wp-content/uploads/2017/06/samsung-tumb.jpg",
    partnerLevel: "Golden",
  },
  {
    logo: "https://1000logos.net/wp-content/uploads/2018/02/KFC-Logo.jpg",
    partnerLevel: "Golden",
  },
  {
    logo: "https://1000logos.net/wp-content/uploads/2016/12/Starbucks-logo-tumb.jpg",
    partnerLevel: "Golden",
  },
  {
    logo: "https://1000logos.net/wp-content/uploads/2018/07/Los-Angeles-Rams-logo-tumb.jpg",
    partnerLevel: "Golden",
  },
  {
    logo: "https://1000logos.net/wp-content/uploads/2017/10/Louis-Vuitton-Logo.jpg",
    partnerLevel: "Golden",
  },
  {
    logo: "https://1000logos.net/wp-content/uploads/2021/11/Nike-Logo-tumb.jpg",
    partnerLevel: "Golden",
  },
  {
    logo: "https://1000logos.net/wp-content/uploads/2018/09/Honda-logo-500x281.jpg",
    partnerLevel: "Golden",
  },
  {
    logo: "https://1000logos.net/wp-content/uploads/2017/10/Louis-Vuitton-Logo.jpg",
    partnerLevel: "Golden",
  },
  {
    logo: "https://imgs.search.brave.com/yjRDgsI2mbUlqxSZAkAut51bvVdQseY7lGH3FUNmifI/rs:fit:500:0:0/g:ce/aHR0cHM6Ly9keW5h/bWljLmJyYW5kY3Jv/d2QuY29tL2Fzc2V0/L2xvZ28vOTFmYjg2/ODQtYTlmMS00ZjM1/LTllZGMtYjljNzE2/ZTI0MjhhL2xvZ28t/c2VhcmNoLWdyaWQt/MXg_bG9nb1RlbXBs/YXRlVmVyc2lvbj0y/JnY9NjM4MzI1OTA4/NjM3MTcwMDAw.jpeg",
    partnerLevel: "Golden",
  },
  {
    logo: "https://imgs.search.brave.com/yjRDgsI2mbUlqxSZAkAut51bvVdQseY7lGH3FUNmifI/rs:fit:500:0:0/g:ce/aHR0cHM6Ly9keW5h/bWljLmJyYW5kY3Jv/d2QuY29tL2Fzc2V0/L2xvZ28vOTFmYjg2/ODQtYTlmMS00ZjM1/LTllZGMtYjljNzE2/ZTI0MjhhL2xvZ28t/c2VhcmNoLWdyaWQt/MXg_bG9nb1RlbXBs/YXRlVmVyc2lvbj0y/JnY9NjM4MzI1OTA4/NjM3MTcwMDAw.jpeg",
    partnerLevel: "Golden",
  },
  {
    logo: "https://imgs.search.brave.com/HY6_3wfAE0NYxVP92nQafXU6XL0rGx5zsM9Gvsc4T3g/rs:fit:500:0:0/g:ce/aHR0cHM6Ly9keW5h/bWljLmJyYW5kY3Jv/d2QuY29tL2Fzc2V0/L2xvZ28vMjRiZGVm/ZGQtNDNkYy00Mjcz/LTkxYTUtNGM2ZjMz/NmZkOTZiL2xvZ28t/c2VhcmNoLWdyaWQt/MXg_bG9nb1RlbXBs/YXRlVmVyc2lvbj0y/JnY9NjM4Mjg0NjYy/NDE4MjMwMDAw.jpeg",
    partnerLevel: "Golden",
  },
  {
    logo: "https://imgs.search.brave.com/WW2fU11STpg1q2icXgC8jTMn0eKnqfK89fOs25ZcnLA/rs:fit:500:0:0/g:ce/aHR0cHM6Ly9pbWFn/ZXMtcGxhdGZvcm0u/OTlzdGF0aWMuY29t/Ly8wbXM3QWFCa0hO/R2ZZZW9wUGphSUk0/VDgwTFU9LzB4MDox/NTM2eDE1MzYvZml0/LWluLzUwMHg1MDAv/OTlkZXNpZ25zLWNv/bnRlc3RzLWF0dGFj/aG1lbnRzLzcyLzcy/Nzc3L2F0dGFjaG1l/bnRfNzI3Nzc3NDQ.jpeg",
    partnerLevel: "Golden",
  },
  {
    logo: "https://imgs.search.brave.com/4Kvdm-iJSGT3qVB4zcdUcBHLHu1VHmUWCfSlXWSnYpk/rs:fit:500:0:0/g:ce/aHR0cHM6Ly9keW5h/bWljLmJyYW5kY3Jv/d2QuY29tL2Fzc2V0/L2xvZ28vZmUzYzEy/NDQtOWVlZC00YTM1/LTgzZGMtODlkZmQ5/NWUyNTJlL2xvZ28t/c2VhcmNoLWdyaWQt/MXg_bG9nb1RlbXBs/YXRlVmVyc2lvbj0y/JnY9NjM4MzA4NjA0/MDg1NTAwMDAw.jpeg",
    partnerLevel: "Golden",
  },
  {
    logo: "/partner6.png",
    partnerLevel: "Golden",
  },
  {
    logo: "/partner1.webp",
    partnerLevel: "Golden",
  },
  {
    logo: "/partner2.webp",
    partnerLevel: "Golden",
  },
  {
    logo: "/partner3.png",
    partnerLevel: "Golden",
  },
  {
    logo: "/partner4.png",
    partnerLevel: "Golden",
  },
];

export const MoreFactData = {
  backGroundImage: "/logo-08.png",
  Data: [
    {
      Icon: <MdPrecisionManufacturing />,
      number: 120,
      lable: "Exhibitors",
      description: "Various sectors industry leaders.",
    },
    {
      Icon: <BsFillFlagFill />,
      number: 8,
      lable: "Panel discussions",
      description:
        "Discover forward-thinking dialogues at our curated convenings",
    },
    {
      Icon: <FiUsers />,
      number: 54000,
      lable: "Visitors",
      description: "Actors, stakeholders and enthusiasts",
    },
    {
      Icon: <BsCameraVideoFill />,
      number: 18,
      lable: " Experimental categories",
      description: "Demonstrate your construction technologies in action",
    },
  ],
};

export const SocialLinks = [
  {
    Icon: <BsFacebook />,
    path: "facebook",
  },
  {
    Icon: <BsInstagram />,
    path: "instagram",
  },
  {
    Icon: <BsLinkedin />,
    path: "linkedin",
  },
  {
    Icon: <BsTelegram />,
    path: "telegram",
  },
  {
    Icon: <BsYoutube />,
    path: "youtube",
  },
];

export const FooterLinks = {
  ExpoName: "GebeyaXpo",
  year: new Date().getFullYear(),
  Data: [
    {
      title: "Useful Links",
      links: [
        {
          name: "Visit",
          link: "/Maps-and-Directions",
        },
        {
          name: "Exibit",
          link: "/why-exibit",
        },

        {
          name: "Media & Press",
          link: "/media-and-press",
        },
        {
          name: "Our Partners",
          link: "/sponsers",
        },
        {
          name: "Become a Partner",
          link: "/sponsers#workWithUS/",
        },
      ],
    },
    {
      title: "Community",
      links: [
        {
          name: "Blog",
          link: "/media-and-press",
        },
        {
          name: "Newsletters",
          link: "/media-and-press",
        },
        {
          name: "Terms and services",
          link: "/terms-and-services",
        },
      ],
    },
  ],
};
