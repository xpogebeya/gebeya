import { storage } from "./firebase";
import {
  clientUsers,
  clientUsersInitialValues,
  clientUsersFields,
  clientUsersMultipleFields,
  validPermissions,
} from "./schema/admin";
import { AppDispatch } from "./redux/store";
import textCliper from "./textCliper";
import AppReduxProvider from "./redux/ReduxProvider";
import { useAppSelector } from "./redux/store";
import {
  approve,
  decline,
  setExhibitors,
  updateLoading,
  updateStatus,
} from "./redux/features/adminExhibitorSlice";

import {
  approveMedia,
  declineMedia,
  setMedia,
  updateMediaLoading,
  updateMediaStatus,
} from "./redux/features/adminMediaSlice";

import {
  removeClientUser,
  addClientUser,
  setClientUsers,
  setLoading,
  setSearchQuery,
} from "./redux/features/userClients";

import { prisma } from "./prisma";
import { NextAuthOption } from "./nextAuthOptions";
import { setAdminUser } from "./redux/features/adminAuthSlice";
import {
  setExhibitorUser,
  removeExhibitorUser,
  setExhibitorLoading,
  updateExhibitorProfile,
} from "./redux/features/AppExhibitorSlice";

import {
  deleteFile,
  setFiles,
  updateIsLoading,
  updateFiles,
} from "./redux/features/adminFileSlice";

import { deleteFirebaseFile } from "./firebaseFileMange";
import {
  setSocialLinks,
  setExpoName,
  setOfficeAddress,
  setHomePageData,
  setHeroData,
  setWhyVisitData,
  setWhyExhibitData,
  setAboutUsData,
  setSponsorsData,
  updateHomeLoading,
} from "./redux/features/homePageSlice";
export {
  textCliper,
  AppReduxProvider,
  useAppSelector,
  approve,
  decline,
  declineMedia,
  approveMedia,
  clientUsers as clientUsersSchema,
  clientUsersInitialValues,
  clientUsersFields,
  clientUsersMultipleFields,
  validPermissions as clinetUsersOptions,
  removeClientUser,
  addClientUser,
  prisma,
  NextAuthOption,
  setAdminUser,
  setClientUsers,
  setLoading,
  setExhibitors,
  updateLoading,
  updateStatus,
  setMedia,
  updateMediaLoading,
  updateMediaStatus,
  setSearchQuery,
  setExhibitorUser,
  removeExhibitorUser,
  storage,
  deleteFile,
  setFiles,
  updateIsLoading,
  updateFiles,
  setExhibitorLoading,
  updateExhibitorProfile,
  deleteFirebaseFile,
  setSocialLinks,
  setExpoName,
  setOfficeAddress,
  setHomePageData,
  setHeroData,
  setWhyVisitData,
  setWhyExhibitData,
  setAboutUsData,
  setSponsorsData,
  updateHomeLoading,
};
export type { AppDispatch };
