"use client";

import React, { PropsWithChildren } from "react";
import { Provider } from "react-redux";
import { store } from "./store";
type Props = {};

const AppReduxProvider = ({ children }: PropsWithChildren) => {
  return <Provider store={store}>
    {children}

  </Provider>;
};

export default AppReduxProvider;
