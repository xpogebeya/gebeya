"use client"
import { configureStore } from "@reduxjs/toolkit";
import { TypedUseSelectorHook, useSelector } from "react-redux";
import exhibtorReducers from "./features/adminExhibitorSlice";
import mediasReducer from "./features/adminMediaSlice";
import userClients from "./features/userClients";
import adminAuth from "./features/adminAuthSlice";
import ExhibtorAuth from "./features/AppExhibitorSlice";
import fileSlice from "./features/adminFileSlice";
import homePage from "./features/homePageSlice";
export const store = configureStore({
  reducer: {
    exhibtorReducers,
    mediasReducer,
    userClients,
    adminAuth,
    ExhibtorAuth,
    fileSlice,
    homePage,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
