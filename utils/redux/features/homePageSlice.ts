import {
  AboutUs,
  ContactInfo,
  HeroType,
  HomePageData,
  SocialLinks,
  Sponsors,
  WhyExhibit,
  WhyVisit,
} from "@/types";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface InitialState {
  socialLinks: SocialLinks[];
  expoName: string;
  officeAddress: ContactInfo[];
  homePageData: HomePageData | null;
  heroData: HeroType | null;
  whyVisit: WhyVisit | null;
  whyExhibit: WhyExhibit | null;
  aboutus: AboutUs | null;
  sponsors: Sponsors | null;
  isMediaLoading: boolean;
  
}

const initialState: InitialState = {
  socialLinks: [],
  expoName: "",
  officeAddress: [],
  homePageData: null,
  heroData: null,
  whyVisit: null,
  whyExhibit: null,
  aboutus: null,
  sponsors: null,
  isMediaLoading: true,

};

const homePage = createSlice({
  name: "home page",
  initialState,
  reducers: {
    setSocialLinks: (state, action: PayloadAction<SocialLinks[]>) => {
      state.socialLinks = action.payload;
    },
    setExpoName: (state, action: PayloadAction<string>) => {
      state.expoName = action.payload;
    },
    setOfficeAddress: (state, action: PayloadAction<ContactInfo[]>) => {
      state.officeAddress = action.payload;
    },
    setHomePageData: (state, action: PayloadAction<HomePageData>) => {
      state.homePageData = action.payload;
    },
    setHeroData: (state, action: PayloadAction<HeroType>) => {
      state.heroData = action.payload;
    },
    setWhyVisitData: (state, action: PayloadAction<WhyVisit>) => {
      state.whyVisit = action.payload;
    },
    setWhyExhibitData: (state, action: PayloadAction<WhyExhibit>) => {
      state.whyExhibit = action.payload;
    },
    setAboutUsData: (state, action: PayloadAction<AboutUs>) => {
      state.aboutus = action.payload;
    },
    setSponsorsData: (state, action: PayloadAction<Sponsors>) => {
      state.sponsors = action.payload;
    },
    updateHomeLoading: (state, action: PayloadAction<boolean>) => {
      state.isMediaLoading = action.payload;
    },
  },
});

export default homePage.reducer;

export const {
  setSocialLinks,
  setExpoName,
  setOfficeAddress,
  setHomePageData,
  setHeroData,
  setWhyVisitData,
  setWhyExhibitData,
  setAboutUsData,
  setSponsorsData,
  updateHomeLoading
} = homePage.actions;
