import { ExhibitorsDataType, InitialExibitors } from "@/types/exhibitor";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";

type Status = {
  total: number;
  accepted: number;
  rejected: number;
  pending: number;
};

type InitialState = {
  exhibitors: ExhibitorsDataType[];
  status: Status;
  isLoading: boolean;
};

const initialState: InitialState = {
  exhibitors: [],
  status: {
    total: 0,
    accepted: 0,
    rejected: 0,
    pending: 0,
  },
  isLoading: true,
};

export const exhibtors = createSlice({
  name: "exhibitors",
  initialState,
  reducers: {
    approve: (state, action: PayloadAction<string>) => {
      const id = action.payload;
      const updatedExhibitors = state.exhibitors.map((exhibit) =>
        exhibit.id === id ? { ...exhibit, status: "accepted" } : exhibit
      );

      const exhibitorToApprove = state.exhibitors.find((a) => a.id === id);
      if (exhibitorToApprove) {
        switch (exhibitorToApprove.status) {
          case "pending":
            state.status.pending -= 1;
            break;
          case "rejected":
            state.status.rejected -= 1;
            break;
        }

        state.status.accepted += 1;
      }
      state.exhibitors = updatedExhibitors;
    },
    decline: (state, action: PayloadAction<string>) => {
      const id = action.payload;
      const updatedExhibitors = state.exhibitors.map((exhibit) =>
        exhibit.id === id ? { ...exhibit, status: "rejected" } : exhibit
      );

      const exhibitorToApprove = state.exhibitors.find((a) => a.id === id);
      if (exhibitorToApprove) {
        switch (exhibitorToApprove.status) {
          case "pending":
            state.status.pending -= 1;
            break;
          case "accepted":
            state.status.accepted -= 1;
            break;
        }

        state.status.rejected += 1;
      }
      state.exhibitors = updatedExhibitors;
    },
    setExhibitors: (state, action: PayloadAction<ExhibitorsDataType[]>) => {
      state.exhibitors = action.payload;
    },
    updateLoading: (state, action: PayloadAction<boolean>) => {
      state.isLoading = action.payload;
    },
    updateStatus: (state, action: PayloadAction<Status>) => {
      state.status = action.payload;
    },
  },
});

export default exhibtors.reducer;
export const { approve, decline, setExhibitors, updateLoading, updateStatus } =
  exhibtors.actions;
