import { createSlice, PayloadAction } from "@reduxjs/toolkit";
const randomFloat = Math.random();
interface Files {
  title: string;
  type: string;
  url: string;
  uploadBy: string;
  id?: number;
  section:string
}

interface InitialState {
  files: Files[] | null;
  isLoading: boolean;
}

const initialState: InitialState = {
  files: null,
  isLoading: true,
};

const adminFileSlice = createSlice({
  name: "admin file",
  initialState,
  reducers: {
    updateIsLoading: (state, action: PayloadAction<boolean>) => {
      state.isLoading = action.payload;
    },
    setFiles: (state, action: PayloadAction<Files[]>) => {
      state.files = action.payload;
    },
    deleteFile: (state, action: PayloadAction<number>) => {
      if (action.payload && state.files) {
        state.files = state.files.filter((file) => file.id !== action.payload);
      }
    },
    updateFiles: (state, action: PayloadAction<Files>) => {
      if (state.files) {
        state.files = [{ id: randomFloat, ...action.payload }, ...state.files];
      }
    },
  },
});

export default adminFileSlice.reducer;
export const { deleteFile, setFiles, updateIsLoading, updateFiles } =
  adminFileSlice.actions;
