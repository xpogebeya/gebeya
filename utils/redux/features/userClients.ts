import { ClientUsers } from "@/types";
import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { v4 as uuidv4 } from "uuid";

const getId = (): string => {
  const uniqueId = uuidv4();
  return uniqueId;
};

type InitialState = {
  users: ClientUsers[];
  isLoading: boolean;
  searchQuery: string;
};

const initialState: InitialState = {
  users: [],
  isLoading: true,
  searchQuery: "",
};
const userClients = createSlice({
  name: "user clients",
  initialState,
  reducers: {
    removeClientUser: (state, action: PayloadAction<string | undefined>) => {
      if (action.payload) {
        state.users = state.users.filter(
          (user) => user.email !== action.payload
        );
      }
    },
    addClientUser: (state, action: PayloadAction<ClientUsers>) => {
      state.users = [{ id: getId(), ...action.payload }, ...state.users];
    },
    setClientUsers: (state, action: PayloadAction<ClientUsers[]>) => {
      state.users = action.payload;
    },
    setLoading: (state, action: PayloadAction<boolean>) => {
      state.isLoading = action.payload;
    },
    setSearchQuery: (state, action: PayloadAction<string>) => {
      state.searchQuery = action.payload;
    },
  },
});

export default userClients.reducer;
export const {
  removeClientUser,
  addClientUser,
  setClientUsers,
  setLoading,
  setSearchQuery,
} = userClients.actions;
