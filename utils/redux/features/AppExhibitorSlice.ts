import { ExhibitorsDataType, InitialExibitors } from "@/types/exhibitor";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface InitialState {
  currentUser: ExhibitorsDataType | null;
  exhibitorLoading: boolean;
}

const initialState: InitialState = {
  currentUser: null,
  exhibitorLoading: true,
};

const ExhibtorAuth = createSlice({
  name: "Exhibitor Auth",
  initialState,
  reducers: {
    setExhibitorUser: (state, action: PayloadAction<ExhibitorsDataType>) => {
      state.currentUser = action.payload;
    },
    removeExhibitorUser: (state, action: PayloadAction<null>) => {
      state.currentUser = null;
    },
    setExhibitorLoading: (state, action: PayloadAction<boolean>) => {
      state.exhibitorLoading = action.payload;
    },
    updateExhibitorProfile: (
      state,
      action: PayloadAction<ExhibitorsDataType>
    ) => {
      state.currentUser = action.payload;
    },
  },
});

export default ExhibtorAuth.reducer;

export const {
  setExhibitorUser,
  removeExhibitorUser,
  setExhibitorLoading,
  updateExhibitorProfile,
} = ExhibtorAuth.actions;
