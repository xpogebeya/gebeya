import { Media } from "@/types";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";

type Status = {
  total: number;
  accepted: number;
  rejected: number;
  pending: number;
};

type InitialState = {
  media: Media[];
  status: Status;
  isMediaLoading: boolean;
};

const initialState: InitialState = {
  media: [],
  status: {
    total: 0,
    accepted: 0,
    rejected: 0,
    pending: 0,
  },
  isMediaLoading: true,
};

export const medias = createSlice({
  name: "media",
  initialState,
  reducers: {
    approveMedia: (state, action: PayloadAction<string>) => {
      const email = action.payload;
      const updatedMedias = state.media.map((media) =>
        media.email === email ? { ...media, status: "accepted" } : media
      );

      const mediaToApprove = state.media.find((a) => a.email === email);
      if (mediaToApprove) {
        switch (mediaToApprove.status) {
          case "pending":
            state.status.pending -= 1;
            break;
          case "rejected":
            state.status.rejected -= 1;
            break;
        }

        state.status.accepted += 1;
      }
      state.media = updatedMedias;
    },
    declineMedia: (state, action: PayloadAction<string>) => {
      const email = action.payload;
      const updatedMedias = state.media.map((media) =>
        media.email === email ? { ...media, status: "rejected" } : media
      );

      const mediaToApprove = state.media.find((a) => a.email === email);
      if (mediaToApprove) {
        switch (mediaToApprove.status) {
          case "pending":
            state.status.pending -= 1;
            break;
          case "accepted":
            state.status.accepted -= 1;
            break;
        }

        state.status.rejected += 1;
      }
      state.media = updatedMedias;
    },
    setMedia: (state, action: PayloadAction<Media[]>) => {
      state.media = action.payload;
    },
    updateMediaLoading: (state, action: PayloadAction<boolean>) => {
      state.isMediaLoading = action.payload;
    },
    updateMediaStatus: (state, action: PayloadAction<Status>) => {
      state.status = action.payload;
    },
  },
});

export default medias.reducer;
export const {
  approveMedia,
  declineMedia,
  setMedia,
  updateMediaLoading,
  updateMediaStatus,
} = medias.actions;
