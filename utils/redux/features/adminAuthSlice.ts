import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface User {
  id: number;
  email: string;
  type: string[];
}

interface InitialState {
  user: User | null;
}

const initialState: InitialState = {
  user: null,
};

const AdminAuth = createSlice({
  name: "Admin Auth",
  initialState,
  reducers: {
    setAdminUser: (state, action: PayloadAction<User>) => {
      state.user = action.payload;
    },
  },
});

export default AdminAuth.reducer;

export const { setAdminUser } = AdminAuth.actions;
