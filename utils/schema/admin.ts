import { Field } from "@/types/register";
import { FormikValues } from "formik";
import * as yup from "yup";

export const validPermissions = ["Blogger", "Ticketer", "Admin"];

export const clientUsers = yup.object().shape({
  fullName: yup.string().required("Full name is required"),
  email: yup.string().email("Invalid email format").required(),
  permissions: yup.string().required("Permissions are required"),
});

export const clientUsersInitialValues: FormikValues = {
  fullName: "",
  email: "",
  permissions: "",
};

export const clientUsersFields: Field[] = [
  {
    label: "Full name",
    name: "fullName",
    required: true,
  },
  {
    label: "Email",
    name: "email",
    required: true,
  },
  {
    label: "Permissions",
    name: "permissions",
    required: true,
    options: validPermissions,
  },
];

export const clientUsersMultipleFields: Field = {
  name: "permissions",
  label: "Permissions",
  required: true,
  options: validPermissions,
};
