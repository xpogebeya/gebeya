// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getStorage } from "firebase/storage";
const firebaseConfig = {
  apiKey: process.env.NEXT_PUBLIC_IREBASE_APIkEY,
  authDomain: process.env.NEXT_PUBLIC_IREBASE_AUTHDOMAIN,
  projectId: process.env.NEXT_PUBLIC_IREBASE_PROJECTID,
  storageBucket: process.env.NEXT_PUBLIC_FIREBASE_STORAGEBUCKET,
  messagingSenderId: process.env.NEXT_PUBLIC_IREBASE_MESSAGINGSENDERID,
  appId: process.env.NEXT_PUBLIC_IREBASE_APPID,
  measurementId: process.env.NEXT_PUBLIC_IREBASE_MEASUREMENTID,
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const storage = getStorage(app);
