// Import Firebase Storage
import { ref, deleteObject } from "firebase/storage";
import { storage } from "@/utils";
export const deleteFirebaseFile = async (fileName: string): Promise<boolean> => {
  try {
    const fileRef = ref(storage, `files/${fileName}`);
    await deleteObject(fileRef);

    return true;
  } catch (error) {
    return false;
  }
};
