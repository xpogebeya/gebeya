import { motion } from "framer-motion";
import React from "react";

import { SocialLinks } from "@/constants/homepage";
import { NavVariants } from "@/utils/motion";
import { useAppSelector } from "@/utils";

type Props = {
  isOpen: boolean;
};

export default function Sponsor({ isOpen }: Props) {
  const { socialLinks, officeAddress } = useAppSelector(state => state.homePage)


  return (
    <div>
      {isOpen && (
        <motion.div
          variants={NavVariants}
          initial="hidden"
          whileInView="show"
          className=" flex flex-col md:flex-row justify-between mt-48 md:mt-20 px-5 text-White"
        >
          <div className=" md:w-[50%] leading-6">
            <h3 className=" text-2xl font-bold">Our Officess</h3>
            {
              officeAddress &&
              <div className=" flex justify-between mt-5">
                {officeAddress.map((office, index) => (
                  <div key={index}>
                    <h4 className=" text-base font-semibold ">
                      Office {index + 1}
                    </h4>
                    <p className=" text-sm font-thin">{office.location}</p>
                    {office.phone.map((num, i) => (
                      <p className=" text-sm font-thin" key={i}>
                        {num}
                      </p>
                    ))}
                  </div>
                ))}
              </div>

            }
          </div>
          <div className=" mt-5">
            <h3 className=" text-xl">Follow us</h3>
            <div className=" flex gap-5 mt-10 text-xl text-White">
              {socialLinks[0] && SocialLinks.map((socila, index) => (
                // @ts-ignore
                <a href={socialLinks[0][socila.path] || "/"} key={index} className=" cursor-pointer hover:text-BlueLight">
                  {socila.Icon}
                </a>
              ))}
            </div>
          </div>
        </motion.div>
      )}
    </div>
  );
}
