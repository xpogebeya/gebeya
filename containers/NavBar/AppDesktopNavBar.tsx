import React from "react";
import {
  Navbar,
  Typography,
  Menu,
  MenuHandler,
  MenuList,
  MenuItem,
} from "@material-tailwind/react";
import { BiChevronDown } from "react-icons/bi";
import { NavList as navList } from "@/constants/navbarlist";
import { NavList as NavListType } from "@/types/navbarlist";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { ThemeProvider } from "@material-tailwind/react";
function NavListMenu({ lable, options, path }: NavListType) {
  const [isMenuOpen, setIsMenuOpen] = React.useState(false);
  const currentPath = usePathname();

  function containsLabel(searchString: string): boolean {
    if (options) {
      return options.some((option) => searchString.startsWith(option.path));
    } else if (path === searchString) {
      return true;
    }
    return false;
  }

  const containsString = containsLabel(currentPath);

  const renderItems =
    options &&
    options.map((opt) => (
      <Link href={opt.path} key={opt.lable} className="   ">
        <MenuItem className=" hover:border-none border-none hover:bg-BlueLighter hover:bg-opacity-20 py-1 ">
          <Typography variant="h6" className=" font-light text-left  px-4">
            {opt.lable}
          </Typography>
        </MenuItem>
      </Link>
    ));

  return (
    <React.Fragment>
      <Menu
        allowHover
        open={isMenuOpen}
        handler={setIsMenuOpen}
        animate={{
          mount: { y: 0 },
          unmount: { y: 25 },
        }}
      >
        <MenuHandler>
          <Typography variant="small">
            <MenuItem
              className={`items-center gap-2  flex text-sm xl:text-base hover:text-BlueLighter hover:font-bold pb-0 ${
                containsString ? " text-BlueLighter font-bold " : ""
              }`}
            >
              {lable}{" "}
              {options && (
                <BiChevronDown
                  strokeWidth={2}
                  className={`h-3 w-3 transition-transform ${
                    isMenuOpen ? "rotate-180" : ""
                  }`}
                />
              )}
            </MenuItem>
          </Typography>
        </MenuHandler>
        {options && (
          <MenuList className=" overflow-hidden mt-2  hover:border-none    ">
            {renderItems}
          </MenuList>
        )}
      </Menu>
    </React.Fragment>
  );
}

function NavList() {
  const currentPath = usePathname();

  return (
    <ul className="flex justify-between flex-row items-center text-sm xl:text-base ">
      {navList.map(
        (nav) =>
          !nav.options && (
            <Link
              key={nav.lable}
              href={nav.path as string}
              className="font-normal"
            >
              <MenuItem
                className={`flex items-center gap-2 hover:text-BlueLighter hover:font-bold pb-0 ${
                  nav.path === currentPath ? "text-BlueLighter font-bold" : ""
                }`}
              >
                {nav.lable}
              </MenuItem>
            </Link>
          )
      )}
      {navList.map(
        (nav) =>
          nav.options && (
            <NavListMenu
              key={nav.lable}
              lable={nav.lable}
              options={nav.options}
              path={nav.path}
            />
          )
      )}
    </ul>
  );
}

export function AppDesktopNavBarXl() {
  return (
    <Navbar className=" text-BlueDark mx-auto p-2 lg:pl-6 px-2  xl:px-10 hidden lg:block w-[48%] shadow-sm  ">
      <NavList />
    </Navbar>
  );
}
