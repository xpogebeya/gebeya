import Image from "next/image";
import Link from "next/link";
import React from "react";
import { AiOutlineMenu, AiOutlineClose } from "react-icons/ai";
import { AppDesktopNavBarXl } from "./AppDesktopNavBar";

type Props = {
  handleMenuClick: () => void;
  logoChange: boolean;
};

const AppDesktopNavBar: React.FC<Props> = ({ handleMenuClick, logoChange }) => {
  const commonImageProps = {
    alt: "Main logo",
    width: 200,
    height: 200,
  };

  return (
    <div className="md:flex justify-between align-middle items-center hidden gap-5">
      <Link href="/">
        <Image
          src={logoChange ? "/logoBlack.png" : "/logoMain.png"}
          {...commonImageProps}
        />
      </Link>
      <AppDesktopNavBarXl />
      <div className="align-middle items-center flex gap-5">
        <Image
          src="/partner.png"
          alt="Main logo"
          width={420}
          height={200}
          className=" object-contain"
        />
        {logoChange ? (
          <AiOutlineClose
            className="text-3xl text-White cursor-pointer block lg:hidden"
            onClick={handleMenuClick}
          />
        ) : (
          <AiOutlineMenu
            className="text-3xl text-BlueLighter cursor-pointer block lg:hidden"
            onClick={handleMenuClick}
          />
        )}
      </div>
    </div>
  );
};

export default AppDesktopNavBar;
