/** @format */

"use client";

import React from "react";
import { motion } from "framer-motion";
import {
  AppButtonDefult,
  Header,
  Pargraph,
  PhotoGalleryCard,
} from "@/components";
import { useRouter } from "next/navigation";
import { HomePageData } from "@/types";

type Props = {
  data:HomePageData
};

export default function PhotoGallery({data}: Props) {
  const router = useRouter();
  return (
    <section id="photGallery">
      <motion.div
        initial="hidden"
        whileInView="show"
        viewport={{ once: false, amount: 0.25 }}
        className=" mb-20 lg:mb-0 lg:h-[80vh] w-[85%] mx-auto lg:items-center flex flex-col lg:flex-row gap-10 lg:justify-between overflow-x-scroll"
      >
        <div className=" flex flex-col gap-4  lg:gap-5">
          <Header lable="Photo Gallery" />
          <Pargraph 
          lable={data.photoGalleryDescription}
          />
          <AppButtonDefult
            label="Show More"
            handleAction={() => router.push("/media-gallery")}
            conditionalClass=" w-1/2"
          />
        </div>
        <motion.div
          initial="hidden"
          whileInView="show"
          viewport={{ once: true, amount: 0.25 }}
          className=" flex  gap-5 overflow-x-scroll w-[90%]"
        >
          {data.photoGalleryImages.map((photo, index) => (
            <PhotoGalleryCard index={index} photo={photo} key={index} />
          ))}
        </motion.div>
      </motion.div>
    </section>
  );
}
