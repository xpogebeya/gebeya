"use client";

import { AppMoreCard, WhyExibitCard } from "@/components";
import styles from "@/styles";
import { HomePageData } from "@/types";
import { motion } from "framer-motion";
import { useRouter } from "next/navigation";
import React, { useState } from "react";


type Props = {
  data: HomePageData
};

export default function WhyExibiti({ data }: Props) {
  const [active, setActive] = useState<string>("res-2");
  const { description, exhibitionName, whyExhibitImages } = data;
  const router = useRouter();

  return (
    <div className=" h-screen w-[85%] mx-auto relative">
      <div className="mx-auto w-full px-4 text-center lg:w-[85%] flex  items-center gap-5">
        <div className=" h-[1px] w-1/2 bg-gray-400"></div>
        <div>
          <p className="  text-BlueDark">Why exhibit at</p>
          <h2 className=" text-BlueDark antialiased tracking-normal text-xl  lg:text-6xl font-semibold leading-[1.3] text-blue-gray-900 mb-3">
            <span className=" text-BlueLight">{exhibitionName}</span>
          </h2>
          <p className="block antialiased  text-base font-normal leading-relaxed text-gray-500">
            {description}
          </p>
        </div>
        <div className=" h-[1px] w-1/2 bg-gray-400"></div>
      </div>
      <motion.div
        initial="hidden"
        whileInView="show"
        viewport={{ once: true, amount: 0.25 }}
        className={`${styles.innerWidth} mx-auto flex flex-col`}
      >
        <div className="mt-[50px] flex lg:flex-row flex-col min-h-[70vh] gap-5">
          {whyExhibitImages.map((item, index) => (
            <WhyExibitCard
              key={index}
              index={index}
              item={item}
              active={active}
              handleClick={setActive}
            />
          ))}
          <AppMoreCard index={4} handleClick={() => router.push("/aboutus")} />
        </div>
      </motion.div>
    </div>
  );
}
