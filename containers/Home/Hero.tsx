/** @format */

"use client";

import React, { useEffect, useState } from "react";

import { AppButtonDefult, AppCountdownClock } from "@/components";
import { LogoContainer } from "..";
import { useRouter } from "next/navigation";
import { HeroType } from "@/types";
type Props = {
  data: HeroType
};


export default function Hero({ data }: Props) {
  const router = useRouter();

  return (
    <LogoContainer
      backGroundImage={data?.backgroundImageUrl || ""}
      className=" min-h-screen relative px-[5%] md:px-0 lg:px-0 w-[95%] xld:w-[85%] mx-auto flex md:flex-row flex-col gap-10 justify-center  md:justify-between  lg:items-center pb-8 pt-3"
    >
      <div className=" px-[20px] mt-20 xld:mt-32 text-left  ">
        <h4 className=" text-sm md:text-xl text-BlueDark subpixel-antialiased">
          {data?.subHeaderAbove}
        </h4>
        <h1 className=" subpixel-antialiased text-[4rem] md:text-[6rem] lg:text-[7rem] xl:text-[8rem] 2xl:text-[10rem] font-bold text-BlueDark  leading-[4.25rem] md:leading-[6rem] md:py-5 xl:py-14 2xl:leading-[9rem] xll:py-0 xll:leading-normal">
          {data?.header.slice(0, -3)}
          <br className="  xll:hidden" />
          <span className=" text-BlueLight">{data?.header.slice(-3)}</span>
        </h1>
        <p className=" text-sm max-w-sm text-BlueDark mb-5 subpixel-antialiased">
          {data?.subHeaderBelow}
        </p>
        <AppButtonDefult
          label={"Let's talk"}
          handleAction={() => router.push("contactus")}
          conditionalClass=" md:block hidden"
        />
        <div className=" mt-5 hidden md:block lg:pt-20">
          {
            data?.startDate &&
            <AppCountdownClock targetDate={data?.startDate} />
          }
        </div>
      </div>
      <div className="">
        <div className=" py-20 mt-2 md:hidden block m mx-auto w-[85%] mb-10 ">
          <div className=" h-36 w-36 sm:h-40 sm:w-40 rounded-full border border-gray-300 absolute top-[21rem] x:top-[20rem] left-16 x:left-[8rem] sm:left-[8rem]">
            <img
              src={data?.imageUrls[0]}
              className=" object-cover w-full h-full rounded-full"
            />
          </div>
          <div className=" h-36 w-36 sm:h-40 sm:w-40 rounded-full border border-gray-300 absolute top-[30rem] left-[12rem] x:left-[15rem] ">
            <img
              src={data?.imageUrls[1]}

              className=" object-cover w-full h-full rounded-full"
            />
          </div>

          <div className=" h-24 w-24 sm:h-32 sm:w-32 rounded-full border border-gray-300 absolute top-[20rem] left-[15rem] x:left-[19rem] sm:left-[20rem]">
            <img
              src={data?.imageUrls[2]}

              className=" w-full rounded-full h-full object-cover"
            />
          </div>
          <div className="h-28 w-28 rounded-full border border-gray-300 absolute top-[31rem] left-12 x:left-[6rem]">
            <img
              src={data?.imageUrls[3]}

              className=" w-full h-full rounded-full object-cover"
            />
          </div>
        </div>

        <div className=" hidden md:block">
          <div className=" h-[10rem] w-[10rem] lg:h-[12rem] lg:w-[12rem]  xl:h-48 xl:w-48 rounded-full border border-gray-300 absolute top-[11rem] lg:top-[10rem] xl:top-[8rem] right-[6.50rem] lg:right-[12.75rem] xl:right-0 xll:right-24">
            <img
              src={data?.imageUrls[0]}

              className=" w-full h-full rounded-full object-cover"
            />
          </div>
          <div className=" w-36 h-36 xl:h-48 xl:w-48 rounded-full border border-gray-300 absolute top-[23rem] lg:top-[26rem] lg:right-[16rem] xl:right-[28rem] right-[10rem] xll:right-[35rem]">
            <img
              src={data?.imageUrls[1]}

              className=" w-full h-full object-cover rounded-full"
            />
          </div>

          <div className=" h-36 w-36 lg:h-36 lg:w-36 xl:h-64 xl:w-64 rounded-full border border-gray-300 absolute top-32 right-[-2.5rem] lg:right-[0.5rem] xl:right-64 xll:right-96">
            <img
              src={data?.imageUrls[2]}

              className=" w-full rounded-full h-full object-cover"
            />
          </div>
          <div className=" h-[10rem] w-[10rem] lg:h-[12rem] lg:w-[12rem] xl:h-96 xl:w-96 rounded-full border border-gray-300 absolute top-[21rem] right-[-2rem] lg:right-[2rem] xl:right-[1rem] xl:top-[24rem] xll:right-24">
            <img
              src={data?.imageUrls[3]}

              className=" w-full h-full rounded-full object-cover"
            />
          </div>
        </div>
      </div>

      <div className=" mt-5 md:hidden block">
        {
          data?.startDate &&
          <AppCountdownClock targetDate={data.startDate} />
        }
      </div>
      <AppButtonDefult
        label={"Let's talk"}
        handleAction={() => router.push("contactus")}
        conditionalClass=" block md:hidden w-full"
      />
    </LogoContainer>
  );
}
