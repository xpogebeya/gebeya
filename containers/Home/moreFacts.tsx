/** @format */

"use client";
import React from "react";

import { MoreCardCircle } from "@/components";
import { MoreFactData } from "@/constants/homepage";
import { AiFillStar } from "react-icons/ai";
import { Typography } from "@mui/material";
import { HomePageData } from "@/types";

type Props = {
  facts:HomePageData
};

export default function MoreFacts({facts}: Props) {
  const { Data } = MoreFactData;

  return (
    <div className=" mb-20 lg:mb-0 lg:h-[100vh] xll:h-[80vh] bg-BlueLighter w-[85%] mx-auto  py-5 rounded-md ">
      <div className="mx-auto w-full px-4 text-center lg:w-[85%] flex  items-center gap-5">
        <div className=" h-[1px] w-1/2 bg-gray-100"></div>
        <div>
          <p className="   text-gray-200">More Facts About</p>

          <h2 className=" text-BlueDark antialiased tracking-normal text-xl xl:text-6xl font-semibold leading-[1.3] text-blue-gray-900 mb-3">
            <span className=" text-White">{facts.exhibitionName}</span>
          </h2>
        </div>
        <div className=" h-[1px] w-1/2 bg-gray-100"></div>
      </div>
      <div className=" mt-[3rem] w-[85%] justify-between mx-auto flex lg:flex-row gap-10 flex-col-reverse  lg:items-center mb-10">
        <div className="  grid grid-cols-1 gap-5 md:grid-cols-2 mx-auto 2xl:pr-32">
          {facts.facts.map((item, index) => (
            <MoreCardCircle Item={item} Icon={Data[index].Icon} key={index} />
          ))}
        </div>

        <div className=" flex flex-col gap-5 text-left">
          <div className=" text-2xl text-BlueLight h-16 w-16 rounded-full shadow-sm shadow-BlueLighter items-center flex flex-col  justify-center bg-white">
            <AiFillStar />
          </div>
          <Typography className="  text-White leading-[1.375] font-[700] mb-[0.75rem] text-[1.875rem]">
            We have a lot to offer!
          </Typography>
          <p className=" max-w-md block antialiased  text-base leading-relaxed mb-8 font-normal text-gray-100">
            "{facts.offerDescription}"
          </p>
        </div>
      </div>
    </div>
  );
}
