"use client";

import React, { useState } from "react";

type Props = {};

import toast, { Toast } from "react-hot-toast";
const AppContactUs = (props: Props) => {
  const [email, setEmail] = useState<string>();
  return (
    <div className="flex lg:justify-end mt-10">
      <div className="max-w-sm">
        <h2 className="font-display text-xl font-semibold tracking-wider  text-White">
          Sign up for our newsletter
        </h2>
        <p className="mt-4 text-sm  text-gray-100">
          Subscribe to get the latest news, articles, resources and inspiration.
        </p>
        <div className="relative mt-6">
          <input
            placeholder="Email address"
            aria-label="Email address"
            className="block w-full rounded-2xl border border-neutral-300 bg-transparent py-4 pl-6 pr-20 text-base/6  text-White ring-4 ring-transparent transition  placeholder:text-White focus:border-BlueLighter focus:outline-none focus:ring-neutral-950/5"
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <div className="absolute inset-y-1 right-1 flex justify-end">
            <button
              onClick={() => {
                setEmail("");
                toast.success("Thank you for signing up");
              }}
              aria-label="Submit"
              className="flex aspect-square h-full items-center justify-center rounded-xl  bg-BlueLighter text-white transition  hover:bg-opacity-80"
            >
              <svg viewBox="0 0 16 6" aria-hidden="true" className="w-4">
                <path
                  fill="currentColor"
                  d="M16 3 10 .5v2H0v1h10v2L16 3Z"
                ></path>
              </svg>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AppContactUs;
