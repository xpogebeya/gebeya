
"use client";


import { LinkItem } from "@/types/exhibitor";
import React, { FC, ReactNode } from "react";
import { usePathname, useRouter } from "next/navigation";
import { AiOutlineCloudUpload, AiOutlineSearch, AiOutlineUserAdd } from "react-icons/ai";
import { MenuListType } from "@/types/dashboard";
import { FaPersonBooth } from "react-icons/fa";
import { BsPersonBadge } from "react-icons/bs";
import { TfiLayoutMediaCenterAlt } from "react-icons/tfi";
import { AppDashboardLayout } from "..";
import { SiCoop } from "react-icons/si";
import { TfiAnnouncement } from "react-icons/tfi";
import Cookies from "js-cookie";
type Props = {
  children: ReactNode;
};

const MenuList: MenuListType[] = [

  {
    label: "Exhibitors",
    Icon: <FaPersonBooth />,
    path: "/admin/exhibitors",
  },
  {
    label: "Visitors",
    Icon: <BsPersonBadge />,
    path: "/admin/visitors",
  },
  {
    label: "Media",
    Icon: <TfiLayoutMediaCenterAlt />,
    path: "/admin/media",
  },
  {
    label: "Upload",
    Icon: <AiOutlineCloudUpload />,
    path: "/admin/upload",
  },
  {
    label: "User Access",
    Icon: <AiOutlineUserAdd />,
    path: "/admin/user-access",
  },
  {
    label: "Sponsor & Public",
    Icon: <SiCoop />,
    path: "/admin/sponsor",
  },

  {
    label: "Waiting for announcement",
    Icon: <TfiAnnouncement />,
    path: "/admin/announcement",
  },
];


const AppAdminLayout: FC<Props> = ({ children }) => {
  const currentPath = usePathname()
  const router = useRouter()


  function extractStringAfterLastSlash(): string {
    const parts = currentPath.split("/");

    if (parts.length >= 2) {
      const lastPart = parts.pop()!;
      lastPart[0].toLocaleUpperCase();
      return lastPart;
    }
    return "";
  }

  const linkItem: LinkItem[] = [
    {
      label: "Admin",
      path: "#",
    },
    {
      label: extractStringAfterLastSlash(),
      path: currentPath,
    },
  ];

  const signout = () => {
    Cookies.remove('user')
    router.push("/admin-auth")
  }
  return (
    <div>
      <AppDashboardLayout
        showAvator={false}
        linkItems={linkItem}
        MenuList={MenuList}
        signout={signout}
        showSearch={true}
        extractStringAfterLastSlash={extractStringAfterLastSlash}
      >{children}</AppDashboardLayout>
    </div>
  );
};

export default AppAdminLayout;

