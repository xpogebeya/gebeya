"use client";

import React, { FC, ReactNode } from "react";
import { MenuListType } from "@/types/dashboard";
import { CircularProgress } from "@mui/material";
import { removeExhibitorUser, useAppSelector } from "@/utils";
import { usePathname, useRouter } from "next/navigation";
import { LinkItem } from "@/types/exhibitor";
import Cookies from "js-cookie";
import { AppDashboardLayout } from "..";
import { useDispatch } from "react-redux";

type Props = {
  children: ReactNode;
  menuItems: MenuListType[];
  notifications?: boolean;
};

const AppAuthWrapper: FC<Props> = ({ children, menuItems, notifications }) => {

  const { exhibitorLoading, currentUser } = useAppSelector((state) => state.ExhibtorAuth)
  const currentPath = usePathname()
  const router = useRouter()
  const dispatch = useDispatch()

  function extractStringAfterLastSlash(): string {
    const parts = currentPath.split("/");

    if (parts.length >= 2) {
      const lastPart = parts.pop()!;
      lastPart[0].toLocaleUpperCase();
      return lastPart;
    }
    return "";
  }

  const linkItem: LinkItem[] = [
    {
      label: "Exhibitor",
      path: "#",
    },
    {
      label: extractStringAfterLastSlash(),
      path: currentPath,
    },
  ];

  const signout = () => {
    if (currentUser) {
      router.push("/exhibitor-login")
      dispatch(removeExhibitorUser(null))
      Cookies.remove("exhibitor")
    }
  }
  return (
    <AppDashboardLayout
      showAvator={true}
      linkItems={linkItem}
      MenuList={menuItems}
      signout={signout}
      extractStringAfterLastSlash={extractStringAfterLastSlash}
      avator={currentUser?.image}
      avatorLink='/exhibitor-portal/profile'
      showSearch={true}
    >
      <>
        {
          exhibitorLoading ? <div className=" h-[50vh] flex flex-col items-center justify-center">
            <CircularProgress />
          </div>
            :
            children
        }
      </>
    </AppDashboardLayout>
  );
};

export default AppAuthWrapper;

