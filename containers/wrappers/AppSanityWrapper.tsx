'use client'

import { getAboutData, getHeroData, getHomePageData, getSponsorData, getWhyExhibitData, getWhyVisitData } from '@/sanity'
import { setAboutUsData, setExpoName, setHeroData, setHomePageData, setOfficeAddress, setSocialLinks, setSponsorsData, setWhyExhibitData, setWhyVisitData } from '@/utils'
import React, { PropsWithChildren, useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'



const AppSanityWrapper = ({ children }: PropsWithChildren) => {
  const dispatch = useDispatch()

  const getData = async () => {
    const heroData = await getHeroData()
    const homePage = await getHomePageData()
    const whyVisit = await getWhyVisitData()
    const whyExhibit = await getWhyExhibitData()
    const aboutUs = await getAboutData()
    const sponsors = await getSponsorData()

    if (homePage[0].socialLinks) {
      dispatch(setSocialLinks(homePage[0]?.socialLinks))
    }
    if (homePage[0]?.exhibitionName) {
      dispatch(setExpoName(homePage[0]?.exhibitionName))
    }
    if (homePage[0]?.ContactInfo) {
      dispatch(setOfficeAddress(homePage[0]?.ContactInfo))
    }
    if (homePage[0]) {
      dispatch(setHomePageData(homePage[0]))
    }
    if (heroData) {
      dispatch(setHeroData(heroData[0]))
    }
    if (whyExhibit) {
      dispatch(setWhyExhibitData(whyExhibit[0]))
    }
    if (whyVisit) {
      dispatch(setWhyVisitData(whyVisit[0]))
    }
    if (aboutUs) {
      dispatch(setAboutUsData(aboutUs[0]))
    }
    if (sponsors) {
      dispatch(setSponsorsData(sponsors[0]))
    }

  }
  useEffect(() => {
    getData()
  }, [])
  return (
    <div>
      {children}
    </div>
  )
}

export default AppSanityWrapper