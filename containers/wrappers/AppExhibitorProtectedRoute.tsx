'use client'
import Cookies from 'js-cookie'
import { useRouter } from "next/navigation";
import { PropsWithChildren, useEffect } from "react";
import { AppDispatch, setExhibitorLoading, setExhibitorUser } from "@/utils";
import { useDispatch } from "react-redux";
import axios from "axios";



const AppExhibitorProtectedRoute = ({ children }: PropsWithChildren) => {
    const exhibitor = Cookies.get("exhibitor")
    const router = useRouter()
    const dispatch = useDispatch<AppDispatch>();

    const getExhibitor = async () => {
        const { data, status } = await axios.get(`/api/exhibitor/get-exhibitor?id=${exhibitor}`)
        if (status == 200 && data.exhibitor) {
            dispatch(setExhibitorUser(data.exhibitor.data))
            dispatch(setExhibitorLoading(false))
        }
    }


    useEffect(() => {

        if (!exhibitor) {
            router.push("/exhibitor-login")
        } else {
            getExhibitor()
        }
    }, [exhibitor])

    return <>{children}</>
}

export default AppExhibitorProtectedRoute