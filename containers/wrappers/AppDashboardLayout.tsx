/** @format */

"use client";

import {
  AppAdminSideNav,
  AppAdminSideNavMobile,
  AppBreadcrumbs,
} from "@/components";
import { LinkItem } from "@/types/exhibitor";
import React, { ChangeEvent, FC, ReactNode, useState } from "react";
import { Avatar } from "@mui/material";
import { MdMenu, MdMenuOpen } from "react-icons/md";
import IconButton from "@mui/joy/IconButton";
import Input from "@mui/joy/Input";
import { AiOutlineSearch } from "react-icons/ai";
import { useDispatch } from "react-redux";
import { AppDispatch, setSearchQuery } from "@/utils";
import { MenuListType } from "@/types/dashboard";
import { usePathname, useRouter } from "next/navigation";

type Props = {
  children: ReactNode;
  showAvator: boolean;
  linkItems: LinkItem[];
  MenuList: MenuListType[];
  signout: () => void;
  avator?: string;
  extractStringAfterLastSlash: () => string;
  avatorLink?: string;
  showSearch?: boolean;
};

const AppDashboardLayout: FC<Props> = ({
  children,
  linkItems,
  showAvator,
  MenuList,
  signout,
  avator,
  extractStringAfterLastSlash,
  avatorLink,
  showSearch,
}) => {
  const dispatch = useDispatch<AppDispatch>();
  const router = useRouter();
  const currentPath = usePathname();
  const pathsToAvoid = [
    "/exhibitor-portal/profile",
    "/ticketer/visitors-register",
  ];
  const pathAvoided = pathsToAvoid.includes(currentPath);

  const [show, setShow] = useState<boolean>(false);
  const handleSearch = (query: ChangeEvent<HTMLInputElement>) => {
    dispatch(setSearchQuery(query.target.value));
  };
  return (
    <div className=" relative antialiased h-screen overflow-y-auto bg-white rounded-t-[3rem] w-full">
      <>
        <AppAdminSideNav MenuList={MenuList} signout={signout} />
        <AppAdminSideNavMobile
          show={show}
          setShow={setShow}
          MenuList={MenuList}
          signout={signout}
        />
        <div className=" h-[35vh] w-full bg-BlueLighter ">
          <div className="px-6 xls:ml-[380px] md:px-4  ">
            <div
              className={`pr-8 pt-8 text-White flex md:flex-row ${
                !pathAvoided && "flex-col "
              } gap-y-3 justify-between md:items-center`}
            >
              <div>
                <AppBreadcrumbs
                  mainPath={linkItems}
                  className="  text-White  hover:text-gray-200"
                />
                <h6 className="mb-0 font-bold text-white capitalize">
                  {extractStringAfterLastSlash()}
                </h6>
              </div>
              <div className=" flex gap-3 items-center float-right">
                {showSearch && !pathAvoided && (
                  <Input
                    variant="soft"
                    placeholder="Type here..."
                    startDecorator={<AiOutlineSearch />}
                    className=" shadow-sm"
                    onChange={(e) => handleSearch(e)}
                  />
                )}
                <IconButton
                  className=" xls:hidden block hover:bg-BluePurple text-white text-2xl hover:scale-105"
                  onClick={() => setShow(!show)}
                >
                  {show ? <MdMenuOpen /> : <MdMenu />}
                </IconButton>
                {showAvator && avatorLink && (
                  <div
                    className=" cursor-pointer"
                    onClick={() => router.push(avatorLink)}
                  >
                    <Avatar src={avator} />
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </>
      <div className="px-4 xls:ml-[380px]">{children}</div>
    </div>
  );
};

export default AppDashboardLayout;
