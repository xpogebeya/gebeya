
"use client";


import { LinkItem } from "@/types/exhibitor";
import React, { FC, ReactNode } from "react";
import { usePathname, useRouter } from "next/navigation";
import { MenuListType } from "@/types/dashboard";
import { BsPersonBadge } from "react-icons/bs";
import { AppDashboardLayout } from "..";
import { SiCoop } from "react-icons/si";
import Cookies from "js-cookie";
type Props = {
    children: ReactNode;
};

const MenuList: MenuListType[] = [
    {
        label: "Visitors Register",
        Icon: <BsPersonBadge />,
        path: "/ticketer/visitors-register",
    },
    {
        label: "Visitors List",
        Icon: <SiCoop />,
        path: "/ticketer/visitors-list",
    },
];


const AppTicketerLayout: FC<Props> = ({ children }) => {
    const currentPath = usePathname()
    const router = useRouter()

    function extractStringAfterLastSlash(): string {
        const parts = currentPath.split("/");

        if (parts.length >= 2) {
            const lastPart = parts.pop()!;
            lastPart[0].toLocaleUpperCase();
            return lastPart;
        }
        return "";
    }

    const linkItem: LinkItem[] = [
        {
            label: "Ticketer",
            path: "#",
        },
        {
            label: extractStringAfterLastSlash(),
            path: currentPath,
        },
    ];

    const signout = () => {
        Cookies.remove('user')
        router.push("/admin-auth")
    }
    return (
        <div>
            <AppDashboardLayout
                showAvator={false}
                linkItems={linkItem}
                MenuList={MenuList}
                signout={signout}
                showSearch={true}
                extractStringAfterLastSlash={extractStringAfterLastSlash}
            >{children}</AppDashboardLayout>
        </div>
    );
};

export default AppTicketerLayout;

