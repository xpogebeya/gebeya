"use client";
import React, { useState } from "react";
import { AppNavBar, Footer } from "..";
import { Toaster } from "react-hot-toast";
type Props = {
  children: React.ReactNode;
};

export default function AppSliderWrapper({ children }: Props) {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [logoChange, setLogoChange] = useState<boolean>(false);

  const handleMenuClick = () => {
    setIsOpen(!isOpen);

    if (logoChange) {
      setTimeout(() => {
        setLogoChange(!logoChange);
      }, 700);
    } else {
      setTimeout(() => {
        setLogoChange(!logoChange);
      }, 150);
    }
  };
  const pathSelected = () => {
    setIsOpen(false);

    if (logoChange) {
      setTimeout(() => {
        setLogoChange(!logoChange);
      }, 350);
    } else {
      setTimeout(() => {
        setLogoChange(!logoChange);
      }, 100);
    }
  };

  return (
    <div className=" relative">
      <AppNavBar
        isOpen={isOpen}
        handleMenuClick={handleMenuClick}
        logoChange={logoChange}
        setIsOpen={pathSelected}
      />
      <div>
        <Toaster />
      </div>

      <div
        className={` absolute top-2 left-0 right-0 bg-white  rounded-t-[3rem]  transition-transform transform ease-in-out duration-1000 scroll-smooth ${
          isOpen ? " translate-y-[120vh] md:translate-y-[85vh] " : "top-2 "
        }`}
      >
        {children}
        <Footer />
      </div>
    </div>
  );
}
