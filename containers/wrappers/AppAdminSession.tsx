'use client'
import Cookies from "js-cookie";
import { useRouter } from "next/navigation";
import { PropsWithChildren, useEffect } from "react";
import { AppDispatch, setAdminUser, setClientUsers, setLoading } from "@/utils";
import { useDispatch } from "react-redux";
import axios from "axios";

export default function ProtectedRoute({ children }: PropsWithChildren) {

    const user = Cookies.get("user")
    const router = useRouter()
    const dispatch = useDispatch<AppDispatch>();

    const getAdmin = async () => {
        const { data, status } = await axios.get(`/api/admin/get-admin?id=${user}`)
        if (status == 200) {
            dispatch(setAdminUser(data.admin))
        }

    }

    const setAdmins = async () => {

        const { data, status } = await axios.get('/api/admin/get-admins')
        if (status == 200) {
            dispatch(setClientUsers(data.admins))
            dispatch(setLoading(false))
        }
    }
    
    useEffect(() => {

        if (!user) {
            router.push("/admin-auth")

        } else {
            getAdmin()
            setAdmins()
        }
    }, [user])

    return <>{children}</>

}