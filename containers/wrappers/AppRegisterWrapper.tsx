"use client";

import {
  AppEditor,
  AppExhibitoImageUploader,
  AppForm,
  Progress,
} from "@/components";
import AppSteppper from "@/components/Forms/AppSteppper";
import { useAPI, useFileUploader } from "@/hooks";
import { Field } from "@/types/register";
import { FormikValues } from "formik";
import React from "react";

type Props = {
  initialValues: FormikValues;
  validationSchema: any;
  fields: Field[][];
  URL: string;
  steps: string[];
  withEditor?: boolean;
  type?: string;
  successUrl?: string
};

const RegisterWrapper: React.FC<Props> = ({
  fields,
  initialValues,
  validationSchema,
  URL,
  steps,
  withEditor,
  type,
  successUrl
}) => {
  const { message, progress, setSubmit, submit, status, error, submitForm } = useAPI();
  const { file, setFile, upload } = useFileUploader();


  return (
    <div className="   flex flex-col ">
      <AppForm
        initialValues={initialValues}
        onSubmit={(values: FormikValues) => submitForm(values, URL)}
        validationSchema={validationSchema}
      >
        {submit ? (
          <Progress process={progress} error={error} message={message} URL={successUrl} status={status} />
        ) : withEditor ? (
          <>
            <AppEditor
              steps={steps}

              FieldValue={fields}
              setFile={setFile} />
          </>
        ) : (
          <>
            <AppSteppper
              steps={steps}
              setSubmit={setSubmit}
              FieldValue={fields}
              message={message} /></>
        )}
      </AppForm>
    </div>
  );
};
export default RegisterWrapper;
