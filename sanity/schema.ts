/** @format */

import { type SchemaTypeDefinition } from "sanity";

import home from "./schemas/home";
import heroSection from "./schemas/hero";
import sponsors from "./schemas/sponsors";
import aboutus from "./schemas/about";
import whyVisit from "./schemas/whyVisit";
import whyExibit from "./schemas/why-exibit";
import blog from "./schemas/blog";
import blogCategory from "./schemas/blogCategory";
import blockContent from "./schemas/blockContent";
import category from "./schemas/category";
import post from "./schemas/post";
import settings from "./schemas/settings";
export const schema: { types: SchemaTypeDefinition[] } = {
  types: [
    heroSection,
    home,
    sponsors,
    aboutus,
    whyVisit,
    whyExibit,
    blog,
    blogCategory,
    blockContent,
    post,
    category,
    settings,
  ],
};
