import { getHomePageData } from "./utils/get-home-page";
import { getHeroData } from "./utils/get-hero-data";
import { getAboutData } from "./utils/get-about";
import { getSponsorData } from "./utils/get-sponsors";
import { getWhyExhibitData } from "./utils/get-why-exhibit";
import { getWhyVisitData } from "./utils/get-why-visit";

export {
  getHeroData,
  getHomePageData,
  getAboutData,
  getSponsorData,
  getWhyExhibitData,
  getWhyVisitData,
};
