import { defineField, defineType } from "sanity";

export default defineType({
  name: "whyExhibit",
  title: "Why Exhibit",
  type: "document",
  fields: [
    defineField({
      name: "header",
      title: "Header",
      type: "string",
    }),
    defineField({
      name: "reasonToVist",
      title: "4 reason to Exhibit",
      type: "array",
      of: [
        {
          type: "object",
          fields: [
            {
              name: "title",
              type: "string",
              title: "Title",
            },
            {
              name: "desc",
              type: "string",
              title: "Short description",
            },
            {
              name: "imageUrl",
              title: "Image",
              type: "image",
              options: {
                hotspot: true,
              },
            },
          ],
        },
      ],
      
    }),
    defineField({
      name: "secondHeader",
      title: "Second Header",
      type: "string",
    }),
    defineField({
      name: "showCases",
      title: "10 Lists",
      type: "array",
      of: [
        {
          type: "object",
          fields: [
            {
              name: "title",
              type: "string",
              title: "Title",
            },
            {
              name: "imageUrl",
              title: "Image",
              type: "image",
              options: {
                hotspot: true,
              },
            },
          ],
        },
      ],
      validation: (Rule) => Rule.max(10),
    }),
    defineField({
      name: "professionals",
      title: "Professionals to be there",
      type: "object",
      fields: [
        {
          name: "image",
          title: "Image",
          type: "image",
          options: {
            hotspot: true,
          },
        },
        {
          name: "title",
          title: "Header Below Image",
          type: "string",
        },
        {
          name: "desc",
          title: "Description Below Image",
          type: "string",
        },
        {
          name: "lists",
          title: "Lists",
          type: "array",
          of: [
            {
              type: "string",
            },
          ],
        },
      ],
    }),
    defineField({
      name: "buyers",
      title: "Buyers to be there",
      type: "object",
      fields: [
        {
          name: "image",
          title: "Image",
          type: "image",
          options: {
            hotspot: true,
          },
        },
        {
          name: "title",
          title: "Header Below Image",
          type: "string",
        },
        {
          name: "desc",
          title: "Description Below Image",
          type: "string",
        },
        {
          name: "lists",
          title: "Lists",
          type: "array",
          of: [
            {
              type: "string",
            },
          ],
        },
      ],
    }),
  ],

  preview: {
    select: {
      title: "name",
      media: "image",
    },
  },
});
