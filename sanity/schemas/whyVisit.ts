import { defineField, defineType } from "sanity";

export default defineType({
  name: "whyVisit",
  title: "Why Visit",
  type: "document",
  fields: [
    defineField({
      name: "header",
      title: "Header",
      type: "string",
    }),
    defineField({
      name: "reasonToVist",
      title: "4 reason to visit",
      type: "array",
      of: [
        {
          type: "object",
          fields: [
            {
              name: "title",
              type: "string",
              title: "Title",
            },
            {
              name: "desc",
              type: "string",
              title: "Short description",
            },
            {
              name: "imageUrl",
              title: "Image",
              type: "image",
              options: {
                hotspot: true,
              },
            },
          ],
        },
      ],
      validation: (Rule) => Rule.max(4),
    }),
    defineField({
      name: "secondHeader",
      title: "Second Header",
      type: "string",
    }),
    defineField({
      name: "showCases",
      title: "Show Cases",
      type: "array",
      of: [
        {
          type: "object",
          fields: [
            {
              name: "title",
              type: "string",
              title: "Title",
            },
            {
              name: "imageUrl",
              title: "Image",
              type: "image",
              options: {
                hotspot: true,
              },
            },
          ],
        },
      ],
      validation: (Rule) => Rule.max(10),
    }),
    defineField({
      name: "piechart",
      title: "Pie chart Data",
      type: "array",
      of: [
        {
          type: "object",
          fields: [
            {
              name: "id",
              type: "number",
              title: "ID",
            },
            {
              name: "year",
              type: "number",
              title: "Year",
            },
            {
              name: "userGain",
              type: "number",
              title: "User gain/amount",
            },
            {
              name: "userLost",
              type: "string",
              title: "Sector/Type",
            },
          ],
        },
      ],
      
    }),
  ],

  preview: {
    select: {
      title: "name",
      media: "image",
    },
  },
});
