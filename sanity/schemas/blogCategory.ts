/** @format */

import { defineField, defineType } from "sanity";
export default defineType({
  name: "category",
  title: "BlogCategory",
  type: "document",
  fields: [
    defineField({
      name: "centralImage",
      title: "Central Image",
      type: "image",
      options: {
        hotspot: true,
      },
    }),
    defineField({
      name: "technology",
      title: "Technology",
      type: "image",
      options: {
        hotspot: true,
      },
    }),
    defineField({
      name: "construction",
      title: "Construction",
      type: "image",
      options: {
        hotspot: true,
      },
    }),
    defineField({
      name: "policy",
      title: "Policy",
      type: "image",
      options: {
        hotspot: true,
      },
    }),
    defineField({
      name: "finance",
      title: "Finance",
      type: "image",
      options: {
        hotspot: true,
      },
    }),
    defineField({
      name: "machinery",
      title: "Machinery and spare parts",
      type: "image",
      options: {
        hotspot: true,
      },
    }),
  ],
});
