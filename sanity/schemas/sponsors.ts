import { defineField, defineType } from "sanity";

export default defineType({
  name: "sponsors",
  title: "Sponsors",
  type: "document",
  fields: [
    defineField({
      name: "header",
      title: "Header",
      type: "string",
    }),
    defineField({
      name: "subHeader",
      title: "Sub header",
      type: "string",
    }),
    defineField({
      name: "rightSideHeader",
      title: "Right side sub header",
      type: "string",
    }),
    defineField({
      name: "rightSideDescription",
      title: "Right side Description",
      type: "string",
    }),
    defineField({
      name: "leftSideList",
      title: "Left side list",
      type: "array",
      of: [
        {
          type: "object",
          fields: [
            {
              name: "title",
              type: "string",
              title: "Title",
            },
            {
              name: "description",
              type: "string",
              title: "Short description",
            },
          ],
        },
      ],
      validation: (Rule) => Rule.max(4),
    }),
    defineField({
      name: "steps",
      title: "Step to become a partners",
      type: "array",
      of: [
        {
          type: "object",
          fields: [
            {
              name: "title",
              type: "string",
              title: "Title",
            },
            {
              name: "description",
              type: "string",
              title: "Short description",
            },
          ],
        },
      ],
      validation: (Rule) => Rule.max(4),
    }),
    defineField({
      name: "faq",
      title: "FAQ",
      type: "array",
      of: [
        {
          type: "object",
          fields: [
            {
              name: "title",
              type: "string",
              title: "Title",
            },
            {
              name: "description",
              type: "string",
              title: "Short description",
            },
          ],
        },
      ],
    }),

    defineField({
      name: "packages",
      title: "Packages",
      type: "array",
      of: [
        {
          type: "object",
          fields: [
            {
              name: "plan",
              type: "string",
              title: "Plan",
            },
            {
              name: "price",
              type: "string",
              title: "Price",
            },
            {
              name: "features",
              title: "Features",
              type: "array",
              of: [
                {
                  type: "string",
                },
              ],
            },
            {
              name: "packages",
              title: "Full Packages",
              type: "array",
              of: [
                {
                  type: "object",
                  fields: [
                    {
                      name: "title",
                      type: "string",
                      title: "Title",
                    },
                    {
                      name: "lists",
                      title: "Lists",
                      type: "array",
                      of: [
                        {
                          type: "object",
                          fields: [
                            {
                              name: "title",
                              type: "string",
                              title: "Title",
                            },
                            {
                              name: "pack",
                              title: "Packages",
                              type: "array",
                              of: [
                                {
                                  type: "string",
                                },
                              ],
                            },
                          ],
                        },
                      ],
                    },
                  ],
                },
              ],
            },
          ],
        },
      ],
    }),
  ],

  preview: {
    select: {
      title: "name",
      media: "image",
    },
  },
});
