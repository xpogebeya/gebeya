import { defineField, defineType } from "sanity";

export default defineType({
  name: "hero",
  title: "Hero",
  type: "document",
  fields: [
    defineField({
      name: "header",
      title: "Header",
      type: "string",
    }),
    defineField({
      name: "videoId",
      title: "Video ID",
      type: "string",
    }),
    defineField({
      name: "subHeaderAbove",
      title: "Sub Header Above",
      type: "string",
    }),
    defineField({
      name: "subHeaderBelow",
      title: "Sub Header Below",
      type: "string",
    }),
    defineField({
      name: "startDate",
      title: "Date of Start",
      type: "date",
    }),
    defineField({
      name: "images",
      title: "Images",
      type: "array",
      of: [{ type: "image" }],
      options: {
        layout: "grid",
      },
      validation: (Rule) => Rule.max(4),
    }),
    defineField({
      name: "backgroundImage",
      title: "Background image",
      type: "image",
      options: {
        hotspot: true,
      },
    }),
  ],

  preview: {
    select: {
      title: "name",
      media: "image",
    },
  },
});
