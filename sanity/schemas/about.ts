/** @format */

import { defineField, defineType } from "sanity";

export default defineType({
  name: "aboutus",
  title: "About us",
  type: "document",
  fields: [
    defineField({
      name: "factAboutXpo",
      title: "3 Facts about the exhibition",
      type: "array",
      of: [
        {
          type: "object",
          fields: [
            {
              name: "title",
              type: "string",
              title: "Title",
            },
            {
              name: "description",
              type: "string",
              title: "Short description",
            },
          ],
        },
      ],
      validation: (Rule) => Rule.max(3),
    }),
    defineField({
      name: "subHeader",
      title: "Sub header",
      type: "string",
    }),
    defineField({
      name: "backgroundOfCompany",
      title: "Background of the company",
      type: "string",
    }),
    defineField({
      name: "leftSideList",
      title: "Left side list",
      type: "array",
      of: [
        {
          type: "object",
          fields: [
            {
              name: "title",
              type: "string",
              title: "Title",
            },
            {
              name: "description",
              type: "string",
              title: "Short description",
            },
          ],
        },
      ],
      validation: (Rule) => Rule.max(4),
    }),
    defineField({
      name: "teams",
      title: "Teams",
      type: "array",
      of: [
        {
          type: "object",
          fields: [
            {
              name: "name",
              title: "Name",
              type: "string",
            },
            {
              name: "position",
              title: "Postion",
              type: "string",
            },
            {
              name: "image",
              title: "Image",
              type: "image",
              options: {
                hotspot: true,
              },
            },
          ],
        },
      ],
    }),
  ],

  preview: {
    select: {
      title: "name",
      media: "image",
    },
  },
});
