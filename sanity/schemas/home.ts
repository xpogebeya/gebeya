import { defineField, defineType } from "sanity";

export default defineType({
  name: "home",
  title: "Home page",
  type: "document",
  fields: [
    defineField({
      name: "exhibitionName",
      title: "Exhibition Name",
      type: "string",
    }),
    defineField({
      name: "description",
      title: "Description",
      type: "string",
    }),
    defineField({
      name: "whyExhibitImages",
      title: "Why Exhibit Images",
      type: "array",
      of: [
        {
          type: "object",
          fields: [
            {
              name: "title",
              type: "string",
              title: "Title",
            },
            {
              name: "image",
              type: "image",
              title: "Image",
              options: {
                hotspot: true,
              },
            },
            {
              name: "id",
              title: "ID",
              type: "slug",
              options: {
                source: "title",
                maxLength: 96,
              },
            },
          ],
        },
      ],
    }),
    defineField({
      name: "facts",
      title: "Facts",
      type: "array",
      of: [
        {
          type: "object",
          fields: [
            {
              name: "title",
              type: "string",
              title: "Title",
            },
            {
              name: "number",
              type: "string",
              title: "Number",
            },
            {
              name: "shortDescription",
              type: "string",
              title: "Short Description",
            },
          ],
        },
      ],
    }),
    defineField({
      name: "offerDescription",
      title: "Offer Description",
      type: "string",
    }),
    defineField({
      name: "photoGalleryDescription",
      title: "Photo Gallery Description",
      type: "string",
    }),
    defineField({
      name: "photoGalleryImages",
      title: "Photo Gallery Images",
      type: "array",
      of: [{ type: "image" }],
      options: {
        layout: "grid",
      },
      validation: (Rule) => Rule.max(3),
    }),
    defineField({
      name: "Partners",
      title: "Partners",
      type: "array",
      of: [
        {
          type: "object",
          fields: [
            {
              name: "logo",
              type: "image",
              title: "Logo",
              options: {
                hotspot: true,
              },
            },
            {
              name: "partnerLevel",
              type: "string",
              title: "Partner Level",
            },
          ],
        },
      ],
    }),
    defineField({
      name: "socialLinks",
      title: "Social LInks",
      type: "array",
      of: [
        {
          type: "object",
          fields: [
            {
              name: "facebook",
              type: "string",
              title: "Facebook",
            },
            {
              name: "instagram",
              type: "string",
              title: "Instagram",
            },
            {
              name: "linkedin",
              title: "Linkedin",
              type: "string",
            },

            {
              name: "telegram",
              type: "string",
              title: "Telegram",
            },
            {
              name: "youtube",
              title: "Youtube",
              type: "string",
            },
          ],
        },
      ],
      validation: (Rule) => Rule.max(1),
    }),
    defineField({
      name: "ContactInfo",
      title: "Contact Information",
      type: "array",
      of: [
        {
          type: "object",
          fields: [
            {
              name: "location",
              title: "Location",
              type: "string",
            },
            {
              name: "phone",
              title: "Phone Numbers",
              type: "array",
              of: [
                {
                  type: "string",
                },
              ],
            },
          ],
        },
      ],
    }),
    defineField({
      name: "email",
      title: "Email",
      type: "string",
    }),
  ],

  preview: {
    select: {
      title: "name",
      media: "image",
    },
  },
});
