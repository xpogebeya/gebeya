import { groq } from "next-sanity";
import { client } from "./client";

export const getHeroData = async () => {
  return client.fetch(groq`*[_type=='hero']{
        _id,
      header,
        subHeaderAbove,
        "backgroundImageUrl": backgroundImage.asset->url,
        videoId,
        subHeaderBelow,
        startDate,
        "imageUrls": images[].asset->url
      }
    `);
};
