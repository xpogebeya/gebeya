import { groq } from "next-sanity";
import { client } from "./client";

export const getAboutData = async () => {
  return client.fetch(groq`*[_type == 'aboutus'] {
    factAboutXpo[]{
      title,
      description
    },
    subHeader,
    backgroundOfCompany,
    leftSideList[]{
      title,
      description
    },
    teams[]{
      name,
      position,
      "image": image.asset->url
    }
  }  
  `);
};
