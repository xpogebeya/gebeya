import { groq } from "next-sanity";
import { client } from "./client";

export const getWhyExhibitData = async () => {
  return client.fetch(groq`*[_type == 'whyExhibit'] {
    header,
    reasonToVist[]{
      title,
      desc,
      "imageUrl": imageUrl.asset->url
    },
    secondHeader,
    showCases[]{
      title,
      "imageUrl": imageUrl.asset->url
    },
    professionals{
      "image": image.asset->url,
      title,
      desc,
      lists
    },
    buyers{
      "image": image.asset->url,
      title,
      desc,
      lists
    }
  }
  `);
};
