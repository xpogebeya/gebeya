import { apiVersion, dataset, projectId } from "../env";
import { createClient, groq } from "next-sanity";

export const client = createClient({
  projectId,
  dataset,
  apiVersion,
});