import { groq } from "next-sanity";
import { client } from "./client";

export const getSponsorData = async () => {
  return client.fetch(groq`*[_type == 'sponsors'] {
    header,
    subHeader,
    rightSideHeader,
    rightSideDescription,
    leftSideList[]{
      title,
      description
    },
    steps[]{
      title,
      description
    },
    faq[]{
      title,
      description
    },
    packages[]{
      price,
      plan,
      features,
      packages[]{
        title,
        lists[]{
          title,
          pack[]
        }
      }
    }
  }
  
  
  `);
};
