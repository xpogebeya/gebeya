import { groq } from "next-sanity";
import { client } from "./client";

export const getHomePageData = async () => {
  return client.fetch(groq`*[_type=='home']{
    exhibitionName,
    description,
    facts[] {
      title,
      number,
      shortDescription
    },
    offerDescription,
    photoGalleryDescription,
    "photoGalleryImages": photoGalleryImages[].asset->url,
     Partners[] {
      "partnerImage": logo.asset->url,
      partnerLevel
    },
    whyExhibitImages[] {
      title,
      "exhibitImage": image.asset->url,
      "id":id.current
    },
    socialLinks,
    ContactInfo,
    email
  }
    `);
};
