import { groq } from "next-sanity";
import { client } from "./client";

export const getWhyVisitData = async () => {
  return client.fetch(groq`*[_type == 'whyVisit'] {
    header,
    reasonToVist[]{
      title,
      desc,
      "imageUrl": imageUrl.asset->url
    },
    secondHeader,
    showCases[]{
      title,
      "imageUrl": imageUrl.asset->url
    },
    piechart[]{
      id,
      year,
      userGain,
      userLost
    }
  }
  
  `);
};
