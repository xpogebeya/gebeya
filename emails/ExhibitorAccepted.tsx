import React, { FC } from 'react'
import { Html, Body, Container, Text, Link, Preview, Tailwind, Img } from '@react-email/components'
import { resend } from '.'

const ExhibitorAccepted = ({ passCode }: { passCode?: string }) => {

    return (
        <Html>
            <Preview >Welcome aboard</Preview>
            <Tailwind>
                <Body className=' bg-white Text-4 rounded-lg shadow-lg' >
                    <Container>
                        <Text className="text-3xl font-bold mb-4">Welcome to GebeyaXpo</Text>
                        <Text className="text-lg text-gray-700 mb-4">
                            I hope this email finds you well. We are delighted to inform you that your application to exhibit at our upcoming event has been successfully accepted. Welcome to our exhibition, and we are thrilled to have you on board as our partner!

                            As we gear up for this exciting event, we have set up a dedicated exhibitor dashboard that will allow you to manage your team and update your exhibitor profile. This dashboard is designed to make your experience with us as seamless as possible. To get started, please follow these simple steps:
                        </Text>

                        <Container>
                            <Text className="text-lg text-gray-700 mb-4">

                                Access Your Dashboard:
                                To log in to your exhibitor dashboard, please visit
                                <Link href="https://www.gebeyaxpo.et/exhibitor-login" className="text-blue-600 hover:underline mr-4">{" "}Here</Link>
                                .
                                You can use your registered email address as your username.

                                Temporary Password:
                                We have generated a temporary password for your initial login. Your temporary password is: {passCode}. Please make a note of it.

                                Change Your Password:
                                Upon your first login, you will be prompted to change your password for added security. We recommend selecting a strong and unique password to protect your account.
                            </Text>
                        </Container>

                        <Container>
                            <Text className="text-lg text-gray-700 mb-4">

                                Once you have successfully logged in, you will have access to a range of features and tools, including the ability to manage your team, update your exhibitor profile, and access important event-related information.

                                Should you encounter any issues or have questions about using the exhibitor dashboard, please do not hesitate to reach out to our dedicated support team at [Support Email/Phone Number]. We are here to assist you every step of the way.

                                Thank you for choosing to partner with us for this event. We are excited about the opportunity to work together and look forward to a successful exhibition. If there are any specific requirements or information you need as you prepare for the event, please let us know, and we will be happy to assist you.
                            </Text>
                        </Container>
                        <Text className="text-lg text-gray-700 mb-4">
                            Don't miss out on the latest updates and news. Subscribe to our newsletter for exclusive information and updates.
                        </Text>
                        <Container>
                            <Link href="https://www.gebeyaxpo.et/" className="text-blue-600 hover:underline mr-4">Go to Home Page</Link>
                            <Link href="https://www.gebeyaxpo.et/media-and-press" className="text-blue-600 hover:underline">Subscribe to Newsletter</Link>
                        </Container>
                        <Container>
                            <Text>
                                Once again, welcome to our exhibition, and we can't wait to see the impact of your presence at the event.

                                Best regards,
                            </Text>
                        </Container>
                    </Container>
                </Body>
            </Tailwind>
        </Html>
    )
}


export default ExhibitorAccepted




export const SendAcceptedEmail = (email: string, passCode?: string) => {
    try {
        resend.emails.send({
            from: process.env.NEXT_PUBLIC_SUPPORT_EMAIL as string,
            to: email,
            subject: 'Welcome to GebeyaXpo: Your Application Has Been Accepted!',
            react: <ExhibitorAccepted passCode={passCode} />,
        });

    } catch (error) {
        return {
            status: 500,
            message: 'Email sending failed',
        };
    }
}