import React, { FC } from 'react'
import { Html, Body, Container, Text, Link, Preview, Tailwind, Img } from '@react-email/components'
import { resend } from '.'

const MediaAccepted = () => {

    return (
        <Html>
            <Preview >Welcome aboard</Preview>
            <Tailwind>
                <Body className=' bg-white Text-4 rounded-lg shadow-lg' >
                    <Container>
                        <Text className="text-3xl font-bold mb-4">Welcome to GebeyaXpo</Text>
                        <Text className="text-lg text-gray-700 mb-4">
                            We are thrilled to inform you that your application to become a partner media for our upcoming exhibition has been accepted. Congratulations! We are excited to have you on board and look forward to working closely with you to make this event a resounding success.

                            Your commitment to providing quality coverage and support aligns perfectly with our goals for this exhibition. We believe your involvement will significantly enhance the experience for both participants and attendees, and we can't wait to see the valuable contribution you will bring. </Text>

                        <Container>
                            <Text className="text-lg text-gray-700 mb-4">

                                As an esteemed partner media, you will have exclusive access to our event, including behind-the-scenes content, interviews with key speakers, and the opportunity to connect with our exhibitors and participants. Your presence and coverage will play a crucial role in capturing the essence of our event and sharing it with a wider audience.

                                We are confident that this partnership will be mutually beneficial, and we are eager to explore various ways to maximize the impact of your media coverage. Our team is at your disposal to provide any information, support, or resources you may require as we prepare for the exhibition.  </Text>
                        </Container>

                        <Container>
                            <Text className="text-lg text-gray-700 mb-4">

                                Please let us know if there are any specific details or arrangements you would like to discuss in advance. We are committed to ensuring that your experience with us is as seamless and productive as possible.  </Text>
                        </Container>
                        <Text className="text-lg text-gray-700 mb-4">
                            Don't miss out on the latest updates and news. Subscribe to our newsletter for exclusive information and updates.
                        </Text>
                        <Container>
                            <Link href="https://www.gebeyaxpo.et/" className="text-blue-600 hover:underline mr-4">Go to Home Page</Link>
                            <Link href="https://www.gebeyaxpo.et/media-and-press" className="text-blue-600 hover:underline">Subscribe to Newsletter</Link>
                        </Container>
                        <Container>
                            <Text>
                                Once again, welcome to our exhibition, and we can't wait to see the impact of your presence at the event.

                                Best regards,
                            </Text>
                        </Container>
                    </Container>
                </Body>
            </Tailwind>
        </Html>
    )
}


export default MediaAccepted




export const SendMediaAcceptedEmail = (email: string, passCode?: string) => {
    try {
        resend.emails.send({
            from: process.env.NEXT_PUBLIC_SUPPORT_EMAIL as string,
            to: email,
            subject: 'Congratulations! Your Application as a Partner Media Has Been Accepted',
            react: <MediaAccepted />,
        });

    } catch (error) {
        return {
            status: 500,
            message: 'Email sending failed',
        };
    }
}