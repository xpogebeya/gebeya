import React from 'react'
import { Html, Body, Container, Text, Link, Preview, Tailwind } from '@react-email/components'
import { resend } from '.'

type Props = {
    passcode: string,

}

const ForgotPassword = ({ passcode }: Props) => {
    return (
        <Html>
            <Preview >Verify your email address</Preview>
            <Tailwind>
                <Body className=' bg-white Text-4 rounded-lg shadow-lg' >
                    <Container>
                        <Text className="text-3xl font-bold mb-4">Dear User</Text>
                        <Text className="text-lg text-gray-700 mb-4">
                            Thank you for requesting a verification code to reset your account password. Your security is important to us.
                        </Text>
                        <Text className="text-lg text-gray-700 mb-4">
                            Please find your verification code below:
                        </Text>
                        <Container>
                            <Text className="text-lg text-gray-700 mb-4">
                                Verification Code: {passcode}
                            </Text>  </Container>

                        <Container>
                            <Text>Please use this code to verify your account and proceed with the password reset process. If you did not request this verification code, please ignore this email.

                            </Text>

                            <Text>If you have any questions or need assistance, please feel free to contact our support team at
                                support@gebeyaxpo.et.</Text>

                            <Text>Thank you for being a part of our community.</Text>

                            <Text>Sincerely,</Text>
                            <Text>Admin</Text>
                        </Container>
                    </Container>
                </Body>
            </Tailwind>
        </Html>
    )
}

export default ForgotPassword



export const SendForgotPasswordEmail = (email: string, passcode: string) => {
    try {
        resend.emails.send({
            from: process.env.NEXT_PUBLIC_SUPPORT_EMAIL as string,
            to: email,
            subject: 'Password Reset Verification Code!',
            react: <ForgotPassword passcode={passcode} />,
        });

    } catch (error) {
        return {
            status: 500,
            message: 'Email sending failed',
        };
    }
}