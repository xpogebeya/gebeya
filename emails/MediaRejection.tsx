import React, { FC } from 'react'
import { Html, Body, Container, Text, Link, Preview, Tailwind, Img } from '@react-email/components'
import { resend } from '.'

const MediaRejection = ({ reasonForDecline }: { reasonForDecline?: string }) => {

    return (
        <Html>
            <Preview >Welcome aboard</Preview>
            <Tailwind>
                <Body className=' bg-white Text-4 rounded-lg shadow-lg' >
                    <Container>
                        <Text className="text-3xl font-bold mb-4">Update on Your Application to Be a Partner Media</Text>
                        <Text className="text-lg text-gray-700 mb-4">
                            I hope this message finds you well. We want to express our appreciation for your interest in becoming a partner media for our upcoming exhibition. Your application was carefully reviewed, and we would like to take a moment to provide you with an update.                            </Text>

                        <Container>
                            <Text className="text-lg text-gray-700 mb-4">
                                After a thorough evaluation of all the applications we received, we regret to inform you that we are unable to accept your application for this particular event. We understand that this news may be disappointing, and we genuinely appreciate your interest and the effort you put into your proposal.
                                {reasonForDecline}

                            </Text>
                        </Container>
                        <Container>
                            <Text className="text-lg text-gray-700 mb-4">
                                The decision to decline an application is never an easy one. Our selection process is highly competitive, and we had to make choices to ensure a balanced representation of our exhibitors and partners, considering the specific focus and goals of this exhibition.

                                We hope that you do not view this decision as a reflection of the quality of your media outlet or your potential to work with us in the future. We truly admire the work you do, and we recognize the value you bring to the industry.

                                While your application for this event has not been accepted, we would like to emphasize that we regularly host a variety of events and initiatives where your expertise and offerings may align more closely with our objectives. We encourage you to stay connected with us and consider future opportunities for collaboration.

                            </Text>
                        </Container>



                        <Text className="text-lg text-gray-700 mb-4">
                            Don't miss out on the latest updates and news. Subscribe to our newsletter for exclusive information and updates.
                        </Text>
                        <Container>
                            <Link href="https://www.gebeyaxpo.et/" className="text-blue-600 hover:underline mr-4">Go to Home Page</Link>
                            <Link href="https://www.gebeyaxpo.et/media-and-press" className="text-blue-600 hover:underline">Subscribe to Newsletter</Link>
                        </Container>
                        <Container>
                            <Text>

                                If you have any questions or would like to receive feedback on your application, please feel free to reach out to us. We are more than willing to provide insights that may be helpful for future endeavors.

                                We genuinely appreciate your interest in partnering with us, and we look forward to the possibility of working together in the future. Your enthusiasm and commitment to your work are highly regarded.

                                Thank you once again for your application and understanding.

                                Best regards,
                            </Text>
                        </Container>
                    </Container>
                </Body>
            </Tailwind>
        </Html>
    )
}


export default MediaRejection




export const SendMediaRejectionEmail = (email: string, reasonForDecline?: string) => {
    try {
        resend.emails.send({
            from: process.env.NEXT_PUBLIC_SUPPORT_EMAIL as string,
            to: email,
            subject: 'Update on Your Application to Be a Partner Media',
            react: <MediaRejection reasonForDecline={reasonForDecline} />,
        });

    } catch (error) {
        return {
            status: 500,
            message: 'Email sending failed',
        };
    }
}