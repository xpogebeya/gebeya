import React, { FC } from 'react'
import { Html, Body, Container, Text, Link, Preview, Tailwind, Img } from '@react-email/components'
import { resend } from '.'

const WelcomeTemplate = ({ imgUrl, passCode, optionalMessage }: { imgUrl?: string, passCode?: string, optionalMessage?: string }) => {

    return (
        <Html>
            <Preview >Welcome aboard</Preview>
            <Tailwind>
                <Body className=' bg-white Text-4 rounded-lg shadow-lg' >
                    <Container>
                        <Text className="text-3xl font-bold mb-4">Welcome to GebeyaXpo</Text>
                        <Text className="text-lg text-gray-700 mb-4">
                            Thank you for joining us at GebeyaXpo! We're excited to have you here and explore the amazing exhibitions.
                        </Text>
                        {
                            optionalMessage && <Text className="text-lg text-gray-700 mb-4">
                                {optionalMessage}
                            </Text>
                        }
                        <Text className="text-lg text-gray-700 mb-4">
                            Don't miss out on the latest updates and news. Subscribe to our newsletter for exclusive information and updates.
                        </Text>
                        <Container>
                            <Link href="https://www.gebeyaxpo.et/" className="text-blue-600 hover:underline mr-4">Go to Home Page</Link>
                            <Link href="https://www.gebeyaxpo.et/media-and-press" className="text-blue-600 hover:underline">Subscribe to Newsletter</Link>
                        </Container>
                        {
                            imgUrl && <Container>

                                <Text className="text-lg text-gray-700 mb-4">
                                    Here is your ticket
                                </Text>
                                <Img
                                    src={imgUrl}
                                    width="300"
                                    height="300"
                                    alt="Qr code"
                                />
                                <Text className="text-lg text-gray-700 mb-4">
                                    Or you can use this pass code at the gate
                                </Text>
                                <Text className="text-3xl text-gray-700 mb-4">
                                    {passCode}
                                </Text>
                            </Container>
                        }
                    </Container>
                </Body>
            </Tailwind>
        </Html>
    )
}


export default WelcomeTemplate




export const SendWelcomeEmail = (email: string, imgUrl?: string, passCode?: string, optionalMessage?: string) => {
    try {
        resend.emails.send({
            from: process.env.NEXT_PUBLIC_SUPPORT_EMAIL as string,
            to: email,
            subject: 'Welcome to GebeyaXpo: Your Journey Begins Here!',
            react: <WelcomeTemplate imgUrl={imgUrl} passCode={passCode} optionalMessage={optionalMessage} />,
        });

    } catch (error) {
        return {
            status: 500,
            message: 'Email sending failed',
        };
    }
}