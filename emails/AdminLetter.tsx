import React from 'react'
import { Html, Body, Container, Text, Link, Preview, Tailwind } from '@react-email/components'
import { resend } from '.'

type Props = {
    password: string,
    email: string
}

const AdminLetter = ({ password }: Props) => {
    return (
        <Html>
            <Preview >Welcome aboard</Preview>
            <Tailwind>
                <Body className=' bg-white Text-4 rounded-lg shadow-lg' >
                    <Container>
                        <Text className="text-3xl font-bold mb-4">Welcome to GebeyaXpo</Text>
                        <Text className="text-lg text-gray-700 mb-4">
                            You have been designated as an admin on our platform. To get started, you'll need to set a new password for your account. Please follow the instructions below:
                        </Text>
                        <Text className="text-lg text-gray-700 mb-4">
                            Click on the link below to change your password:
                        </Text>
                        <Container>
                            <Link href="https://www.gebeyaxpo.et/admin-auth" className=" text-BlueLighter hover:underline mr-4">Change Password</Link>
                        </Container>
                        <Container>
                            <Text>Use the following temporary password for signing in:</Text>
                            <Text className=' text-xl font-bold'>{password}</Text>
                        </Container>
                        <Container>
                            <Text>For security reasons, we recommend that you change your password immediately.</Text>

                            <Text>If you have any questions or need assistance, please feel free to contact our support team at
                                support@gebeyaxpo.et.</Text>

                            <Text>Thank you for being a part of our community.</Text>

                            <Text>Sincerely,</Text>
                            <Text>Admin</Text>
                        </Container>
                    </Container>
                </Body>
            </Tailwind>
        </Html>
    )
}

export default AdminLetter



export const SendAdminEmail = (email: string, password: string) => {
    try {
        resend.emails.send({
            from: process.env.NEXT_PUBLIC_SUPPORT_EMAIL as string,
            to: email,
            subject: 'Welcome to GebeyaXpo: Your Journey Begins Here!',
            react: <AdminLetter password={password} email={email} />,
        });
    } catch (error) {
        return {
            status: 500,
            message: 'Email sending failed',
        };
    }
}