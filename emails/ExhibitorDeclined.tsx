import React, { FC } from 'react'
import { Html, Body, Container, Text, Link, Preview, Tailwind, Img } from '@react-email/components'
import { resend } from '.'

const ExhibitorRejected = ({ reasonForDecline }: { reasonForDecline?: string }) => {

    return (
        <Html>
            <Preview >Welcome aboard</Preview>
            <Tailwind>
                <Body className=' bg-white Text-4 rounded-lg shadow-lg' >
                    <Container>
                        <Text className="text-3xl font-bold mb-4">Welcome to GebeyaXpo</Text>
                        <Text className="text-lg text-gray-700 mb-4">
                            I hope this email finds you well. I want to extend our sincere gratitude for your interest in partnering with us as an exhibitor for our upcoming event. We truly appreciate your application and the effort you put into your proposal.

                            After careful consideration and review of all the applications we received, we regret to inform you that we are unable to accept your application for this particular event. This decision was not made lightly, and we understand the disappointment it may bring. Please allow me to explain the reasons behind our decision.      </Text>

                        <Container>
                            <Text className="text-lg text-gray-700 mb-4">

                                {reasonForDecline}

                                We want to emphasize that our decision should not be perceived as a reflection of your company's capabilities or your future potential to work with us. We genuinely admire your work and believe that there may be opportunities for collaboration in the future. We value the relationships we build with our partners, and we would be thrilled to explore possibilities for future projects or events where your expertise and offerings align more closely with our objectives.

                                We appreciate your understanding and encourage you to stay connected with us. We regularly host events and initiatives that may better suit your company's profile, and we would be eager to have you on board in the future.

                                Thank you again for considering a partnership with us. We genuinely hope that we can explore opportunities to collaborate in the coming months. If you have any questions or would like further feedback on your application, please do not hesitate to reach out to us.

                                Wishing you continued success in all your endeavors, and we look forward to the possibility of working together in the future.
                            </Text>
                        </Container>


                        <Text className="text-lg text-gray-700 mb-4">
                            Don't miss out on the latest updates and news. Subscribe to our newsletter for exclusive information and updates.
                        </Text>
                        <Container>
                            <Link href="https://www.gebeyaxpo.et/" className="text-blue-600 hover:underline mr-4">Go to Home Page</Link>
                            <Link href="https://www.gebeyaxpo.et/media-and-press" className="text-blue-600 hover:underline">Subscribe to Newsletter</Link>
                        </Container>
                        <Container>
                            <Text>

                                Best regards,
                            </Text>
                        </Container>
                    </Container>
                </Body>
            </Tailwind>
        </Html>
    )
}


export default ExhibitorRejected




export const SendRejectionEmail = (email: string, reasonForDecline?: string) => {
    try {
        resend.emails.send({
            from: process.env.NEXT_PUBLIC_SUPPORT_EMAIL as string,
            to: email,
            subject: 'GebeyaXpo: Your Application Has Been Rejected!',
            react: <ExhibitorRejected reasonForDecline={reasonForDecline} />,
        });

    } catch (error) {
        return {
            status: 500,
            message: 'Email sending failed',
        };
    }
}