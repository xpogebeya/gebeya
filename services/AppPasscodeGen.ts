import { customAlphabet } from "nanoid";
const alphabet =
  "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
const passcodeLength = 8;

const AppPasscodeGen = (): string => {
  const generatePasscode = customAlphabet(alphabet, passcodeLength);
  return generatePasscode();
};

export default AppPasscodeGen;
