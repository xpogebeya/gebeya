import * as nodemailer from "nodemailer";

export function sendEmail(receiverEmail: string, message: string): void {
  // Create a Nodemailer transporter with your email service provider's details.
  const transporter = nodemailer.createTransport({
    host: "mail.gebeyaxpo.et", // SMTP server (Outgoing Server)
    port: 465, // SMTP port (usually 465 for SSL/TLS)
    secure: true, // Use SSL/TLS
    auth: {
      user: "support@gebeyaxpo.et", // SMTP username
      pass: "gebeyaxpo.et@support", // SMTP password
    },
  });

  // Email data
  const mailOptions: nodemailer.SendMailOptions = {
    from: "support@gebeyaxpo.et",
    to: receiverEmail, // The recipient's email address
    subject: "Test",
    text: message, // The email message
  };

  // Send the email
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.error("Error sending email: " + error);
    } else {
      console.log("Email sent: " + info.response);
    }
  });
}
