// import { prisma } from "@/utils";
// import { sub } from "date-fns";

// interface ReturnType {
//   status: number;
//   data?: number;
//   error?: any;
// }

// const oneWeekAgo = sub(new Date(), { weeks: 1 });

// export const countVisitorsSinceLastWeek = async (): Promise<ReturnType> => {
//   try {
//     const visitorCount = await prisma.visitor.count({
//       where: {
//         createdAt: {
//           gte: oneWeekAgo,
//         },
//       },
//     });
//     return {
//       status: 200,
//       data: visitorCount,
//     };
//   } catch (error) {
//     return {
//       status: 404,
//       error,
//     };
//   } finally {
//     await prisma.$disconnect();
//   }
// };

// export const countMediaSinceLastWeek = async (): Promise<ReturnType> => {
//   try {
//     const mediaCount = await prisma.media.count({
//       where: {
//         createdAt: {
//           gte: oneWeekAgo,
//         },
//       },
//     });
//     return {
//       status: 200,
//       data: mediaCount,
//     };
//   } catch (error) {
//     return {
//       status: 404,
//       error,
//     };
//   } finally {
//     await prisma.$disconnect();
//   }
// };

// export const countExhibitorsSinceLastWeek = async (): Promise<ReturnType> => {
//   try {
//     const exhibitorsCount = await prisma.exhibitors.count({
//       where: {
//         createdAt: {
//           gte: oneWeekAgo,
//         },
//       },
//     });
//     return {
//       status: 200,
//       data: exhibitorsCount,
//     };
//   } catch (error) {
//     return {
//       status: 404,
//       error,
//     };
//   } finally {
//     await prisma.$disconnect();
//   }
// };
