import { ref, getDownloadURL, uploadString } from "firebase/storage";
import { storage } from "@/utils"; // Assuming `storage` is correctly initialized

export async function AppUploadQRCodeToCloudinary(
  qrCodeBuffer: Buffer
): Promise<string> {
  const base64Image = qrCodeBuffer.toString("base64");
  const imageString = `data:image/png;base64,${base64Image}`;
  const qrCodeFileName = `qrcodes/${Date.now()}_qrcode.png`; // Adjust the folder and file name if needed
  const fileRef = ref(storage, qrCodeFileName); // Update the reference to target the correct path
  try {
    await uploadString(fileRef, imageString, "data_url");
    const url = await getDownloadURL(fileRef);
    return url;
  } catch (error) {
    console.error("Error uploading QR code to Firebase:", error);
    throw error;
  }
}
