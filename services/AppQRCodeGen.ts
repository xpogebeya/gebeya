import qrcode from "qrcode";

export type QRCodeData = {
  data: string;
  size?: number;
};

export async function AppGenerateQRCode(data: QRCodeData): Promise<Buffer> {
  const { data: text, size = 256 } = data;
  return await qrcode.toBuffer(text, { width: size });
}
